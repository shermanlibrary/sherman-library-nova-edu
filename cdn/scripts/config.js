/*------------------------------------*\
  #Config
\*------------------------------------*/
requirejs.config({

  /**
   * Define paths to common scripts.
   */
   paths : {

    accordion         : '//sherman.library.nova.edu/cdn/scripts/min/o--accordion-1.0.min',
    bootstrap : '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min',
    angular           : '//ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min',
    kids              : '//public.library.nova.edu/wp-content/themes/asl-wp-theme-child-public/library/scripts/app',
    jquery            : '//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min',
    matchHeight : 'https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.0/jquery.matchHeight-min',
    masonry           : '//cdnjs.cloudflare.com/ajax/libs/masonry/3.3.2/masonry.pkgd.min',

    // React and dependencies
    react             : 'https://fb.me/react-0.14.7.min',
    reactDOM          : 'https://fb.me/react-dom-0.14.7.min',
    react__ftf        : '//systemsdev.library.nova.edu/cdn/scripts/c--ftf', // A full-text finder form.

    searchWidget      : '//sherman.library.nova.edu/cdn/scripts/min/o--search-widget-1.0.min',
    tableOfContents   : '//sherman.library.nova.edu/cdn/scripts/min/o--toc-1.0.min',
    tabs              : '//sherman.library.nova.edu/cdn/scripts/min/o--tabs-1.0.min'
  },

  /**
   * If any of the paths above have depencies, e.g., jQuery,
   * let's create a shim.
   */
  shim : {

      'bootstrap' : {
        deps: [ 'jquery' ]
      },

    'kids' : {
      deps: [ 'angular' ]
    },

    'tabs' : {
      deps: [ 'jquery' ]
    }

  }

});

/*------------------------------------*\
  #Conditions
\*------------------------------------*/
if ( document.querySelector( '.accordion' ) ) { require( ['accordion'] ); }
if ( document.querySelector( '.tabs') ) { require( [ 'tabs' ] ); }
if ( document.querySelector( '[ng-app="kidsLander"]' ) ) { require( [ 'kids' ] ); }
if ( document.querySelector( '.js-masonry' ) ) { require( ['masonry'] ); }
if ( document.querySelector( 'article h2[id], article h3[id], article h4[id]' ) ) { require( [ 'tableOfContents' ] ); }
if ( document.querySelector( '#search-widget' ) ) { require( [ 'searchWidget' ] ); }
if ( document.querySelector( '.js-full-text-finder' ) ) { require( ['ftf' ] ); }
if ( document.querySelector( '.jsx-ftf' ) ) { require( ['react__ftf'] ); }

/*------------------------------------*\
  #Utilities
\*------------------------------------*/
/**
 * Lazy Images
 */
[].forEach.call(document.querySelectorAll('img[data-src]'), function(img) {
  img.setAttribute('src', img.getAttribute('data-src'));
  img.onload = function() {
    img.removeAttribute('data-src');
  };
});
