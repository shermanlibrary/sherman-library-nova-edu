// IE8 ployfill for GetComputed Style (for Responsive Script below)
if (!window.getComputedStyle) {
    window.getComputedStyle = function(el, pseudo) {
        this.el = el;
        this.getPropertyValue = function(prop) {
            var re = /(\-([a-z]){1})/g;
            if (prop == 'float') prop = 'styleFloat';
            if (re.test(prop)) {
                prop = prop.replace(re, function () {
                    return arguments[2].toUpperCase();
                });
            }
            return el.currentStyle[prop] ? el.currentStyle[prop] : null;
        }
        return this;
    }
}

/* ==================
 * Upfront Variables
 * ================== */
var responsive_viewport = $(window).width();

(function(){
/* ==================
 * Menu */
tinsleyfied_menu = function() {
	var menu 					= $('nav.menu li'),
		menuItem				= $('nav.menu li > a.link'),
		menuLink				= $('nav.menu a.menu-link'),
		subMenuParent			= $('li.has-subnav a'),
		subMenu 				= $('ul.children'),
		menuUL					= $('nav.menu > ul');


	/* ==================
	 * Toggle the Mobile Menu */
	menuLink.on('click', function( e ) { menuUL.slideToggle(); e.preventDefault(); })

	if ( responsive_viewport > 1024 ) { menuLink.css('display','none'); }

	/* ==================
	 * Multi-Toggle Dropdown */
	menuItem.on('click', function( e ) {
		
		var activeMenu 	= $(this).parent('li');
		
		collapse_menu = function() {
			activeMenu.removeClass('active');
			subMenu.removeClass('active');
		}

		open_menu = function( e ) {
			subMenu.removeClass('active');
			menu.removeClass('active');
			activeMenu.addClass('active');

			$('html').on('click', function( e ) {

				if ($(e.target).parents().index($('nav.menu')) == -1 ) {
					if ( $('nav.menu').is(':visible') ) {
						collapse_menu();
					}
				}
			});
		}

		if ( activeMenu.hasClass('active') ) { collapse_menu(); } else { open_menu(); }

	});

    /* ==================
     * Portlet Sub Menu
     * ================== */
    $('.vertical-menu a[href=#parent]').on('click', function() {
        
        if ( $(this).hasClass('active') ) {
            $(this).removeClass('active');
            $(this).next('.sub-menu').slideUp();
        } else {
            $(this).addClass('active');
            $(this).next('.sub-menu').css('position','relative').slideDown();
        }

    });
	/* ==================
	 * Convenient Links
	 * ================== */	
	$('.utility-menu > li').on('click', function(){
        $(this).toggleClass('active');
    });

    // To-Do: .drop-down to quick-links class
    
	/* ==================
	 * Sub-Menu Toggle */
	subMenuParent.on('click', function() {

		var activeSubMenu = $(this).parent('li.has-subnav').children('ul.children');		

		if ( activeSubMenu.hasClass('active') ) {
			
			activeSubMenu.removeClass('active');
		
		} else {

			activeSubMenu.addClass('active');

		}

	});	


	/* ==================
	 * Portlet Sub Menu
	 * ================== */
	$('.widget_nav_menu a[href=#parent]').on('click', function() {
		
		if ( $(this).hasClass('active') ) {
			$(this).removeClass('active');
			$(this).next('.sub-menu').slideUp();
		} else {
			$(this).addClass('active');
			$(this).next('.sub-menu').css('position','relative').slideDown();
		}

	});

	 $('.utilities > .lib-button-small > a.has-subnav').on('click', function( e ) {

        $(this).parent().toggleClass('active');
        e.preventDefault();
    });

} // tinsleyfied_menu()

/* ==================
 * Ask a Librarian */
tinsleyfied_ask = function() {
	    
    var ask_badge = '<header class="gradient--rtl"><span class="icon-bubbles" aria-hidden="true"></span> <span class="h3">Ask a Librarian</span></header><div class="wrap"><div class="ask-badge"><div class="twocol first"><span title="Visit the reference desk" class="icon-user active" aria-hidden="true" data-description="Speak with a librarian in person at the second-floor <b>Reference Desk</b>. <br><table style=width: 75%; font-size: .85em; margin-top: 1em;><tr><td><b>Mon - Fri:</b></td><td>9a.m. - 9p.m.</td></tr><tr><td><b>Saturday:</b></td><td>9a.m. - 8p.m.</td></tr><tr><td><b>Sunday:</b></td><td>11a.m. - 9p.m.</td></tr></table>"></span></div><div class="twocol"><span title="Call the reference desk" class="icon-phone" aria-hidden="true" data-description="<span style=font-size:.85em;><b>Local</b>: <a href=tel:9542624613>(954) 262 - 4613</a><br><b>Toll Free (USA)</b>: <a href=tel:18005416682>1 (800) 541 - 6682 x. 24613</a><br><b>Toll Free (Canada, Panama, Caribbean)</b>: <a href=tel:18005546682>1 (800) 554 - 6682 x. 24613</a></span>"></span></div><!-- Email a Question======================--><div class="twocol"><span title="Email a research question" class="icon-mail" aria-hidden="true" data-description="<b>Email</b> a brief question and receive an answer typically within one day. <a href=http://nova.edu/library/main/ask.html#by-email title=Write an Email>Click here</a>."></span></div><div class="twocol"><span title="Chat with a librarian" class="icon-comments" aria-hidden="true" data-description="<strong>Chat</strong> is presently unavailable"></span></div><div class="twocol"><span title="Send us a text message" class="icon-keyboard" data-description="<b>Text-a-Librarian</b> to <a href=tel:9543723505>(954) 372 - 3505</a>. <br>Write <b>NSU</b> at the beginning of your text and then ask a brief question. Your carrier\'s normal texting charges and limits apply."></span></div><div class="twocol last"><span title="Schedule an appointment" class="icon-users" aria-hidden="true" data-description="<b>Make an appointment</b> to meet one-on-one with a librarian for an instructional session. <a href=http://www.nova.edu/library/main/ask.html#by-appointment title=Appointment Form>Start here</a>."></span></div></div><p class="ask-message">Speak with a librarian in person at the second-floor <b>Reference Desk</b>. </p><span style="display: none;" id="libchat_350f6f82c9ebecede6fa0c8773102b53"></span></div>';
	    
	    $('section#ask-a-librarian')
	    	.append( ask_badge )
	    	.addClass( 'shadow' ); 

	    var ask_icons       = $('.ask-badge span'),
	        ask_message     = $('.ask-message'),
	        ask_chat 		= $( '.icon-comments' );

	    ask_icons.on('click', function( e ) {

	        var description = $(this).data('description');

	        ask_icons.removeClass('active');
	        $(this).addClass('active');
	        ask_message.html(description);

	        if ( ask_chat.hasClass( 'active' ) ) {
	        	libchat.show();
	        } else {
	        	libchat.hide();
	        }

	        e.preventDefault();

	    }); 

	    var libchat = $( '[id^=libchat]' );

		if ( libchat ) {
		  	
		  	$.when(
		      $.getScript( '//v2.libanswers.com/load_chat.php?hash=350f6f82c9ebecede6fa0c8773102b53')
		    	
		    ).done(function(){

		    	$( window ).load( function() { 
			      		        
			        // This is a little inelegant but
			        // I am just having some fun.
			        var libchat__button = $( 'button[class^=libchat]' ),
			        	libchat__status = ( libchat__button.hasClass( '[class$=offline]' ) ? true : false );

			       	if ( !libchat__status ) {		       	
			       		
			       		ask_icons.removeClass( 'active' );
			       		ask_chat.addClass( 'active' );
			       		ask_message.html( '' );
			       		libchat__button.html( 'Need help? <strong><u>Chat with an NSU Librarian</strong></u> right now!')
			       		libchat.show();

			       	} else {
			       		libchat__button.html( 'We aren\'t on <strong>chat</strong> yet, but <u>submit your question</u> and we will respond as fast as we can.');
			       	}

			       	ask_chat.data('description', '' );

			        libchat__button
			          .attr( 'style', 'text-align: left; background-color: transparent; box-shadow: none; color: white !important; margin: 0 0 1em !important; padding: 0 !important; border: none; font-family: "Calibri","Myriad Pro",Myriad,Tahoma,"DroidSansRegular",Geneva,"Helvetica Neue",Helvetica,Arial,sans-serif;' );

	          	});

		    });
		 
		}
    
}

/* ==================
 * Init!
 * ================== */
tinsleyfied_menu();
tinsleyfied_ask();

/* if is above or equal to 768px */
if (responsive_viewport >= 768) {

	if ( $('body').hasClass('home') ) {
		$.getScript( '//sherman.library.nova.edu/cdn/scripts/functions/old-search-widget.js' );
		$.getScript( '//sherman.library.nova.edu/cdn/scripts/functions/sherman-carousel.js' );	

	} 

}

})();