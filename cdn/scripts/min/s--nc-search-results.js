var novacatResults = (function() {

  var inputs = {}

  function search() {
    document.querySelector( 'form[name="searchtool"]' ).submit();
  }

  function hideUnavailable() {
    document.querySelector( 'input[ name="availlim"] ').checked = true;
    search();
  }

  return {
    hideUnavailable : hideUnavailable
  }

})();

( function () {



  function init() {

    createUtilityMenu();

  }

  /*------------------------------------*\
    #VARIABLES
  \*------------------------------------*/
  // jQuery Required

  var searchtool                            = ( document.querySelector( 'form[name="searchtool"]' ) ? document.querySelector( 'form[name="searchtool"]' ) : document.querySelector( '.browseSearchtool > form' ) ),
      searchtool_message                    = ( document.querySelector( '.browseSearchtoolMessage' ) ? document.querySelector( '.browseSearchtoolMessage' ) : document.querySelector( '.bibSearchtoolMessage' )),
      searchtool_results                    = $( '.browseSearchtoolMessage > i, .bibSearchtoolMessage > i'),
      searchtool_searchScope                = $( 'select[id="searchscope"]'),
      searchtool_searchType                 = $( 'select[id="searchtype"]'),
      button_modifySearch                   = ( document.querySelector( '.navigationRow > form > a' ) ? $( '.navigationRow > form > a' ).attr( 'href' ) : $( '.record__actions a[href*=NOSRCH]').attr( 'href' ) );

/*------------------------------------*\
  #Search Utility Menu
\*------------------------------------*/
function createUtilityMenu() {
  if ( document.querySelector( 'tr.browseHeader > td.browseHeaderData' ) ) {
  var utilityMenu = document.querySelector( 'tr.browseHeader > td.browseHeaderData' );
  var utilityMenu__bookmarkOptions = document.querySelector( 'form[id="export_form"]' );

  /**
   * Create elements.
   */
  var utilityMenuSort = document.createElement( 'select' );
  var utilityMenuSelect = document.createElement( 'div' );
  var utilityMenuLabel = document.createElement( 'label' );
  var utilityMenuCount = document.createElement( 'span' );
  var utilityMenu__print = document.createElement( 'button' );
  //var utilityMenu__details = document.createElement( 'button' );
  var utilityMenu__actions = document.createElement( 'div' );


  /**
   * Fetch elements that have already been generated, but that
   * we'll move to the utility menu.
   */
  var bookmarks = document.querySelector( '.navigationRow a[href*=export]');

  if ( searchtool_message ) {
    var selectedSortNode = searchtool_message.querySelector( 'strong' );
    var selectedSort = document.createElement( 'option' );
  }

  // Choosing a <select> option will follow that options "value".
  utilityMenuSort.setAttribute( 'onChange', 'window.location.href = this.value' );
  utilityMenuSort.setAttribute( 'id', 'sortOptions' );

  if ( selectedSortNode ) {
    utilityMenuSelect.appendChild( utilityMenuSort );
    $( utilityMenuSelect ).addClass( 'form__select form__select--transparent' );
    utilityMenuLabel.setAttribute('for','sortOptions');
    utilityMenuLabel.textContent = "Sort by ";
  }

  /**
   * Create a reference to the generated sorted-by text,
   * then: remove this node, and add its text to
   * utilityMenuSort.
   */

  if ( searchtool_message ) {

    selectedSort.setAttribute( 'selected', 'true' );

    if ( selectedSortNode ) {
      selectedSort.textContent = selectedSortNode.textContent;
      selectedSortNode.parentNode.removeChild( selectedSortNode );
  }

    utilityMenuSort.appendChild( selectedSort );

    /**
     * NovaCat sort options are links, so create their references
     * then for each create <option>, remove the original node,
     * and append these options to utilityMenuSort.
     */
    $$( 'a', searchtool_message ).forEach( function( sortOption ) {

      var href = sortOption.href,
          text  = sortOption.textContent,
          option = document.createElement( 'option' );
      option.value = href;
      option.textContent = text;
      sortOption.parentNode.removeChild( sortOption );
      utilityMenuSort.appendChild( option );

    });
  } else {

    var sortDropDown = document.querySelector( 'select#sortdropdown' );

    if ( sortDropDown ) {

      sortDropDown.parentNode.classList.add( 'hide-accessible' );
      sortDropDownClone = sortDropDown.cloneNode( true );
      sortDropDownClone.value = sortDropDown.value;

      utilityMenuLabel.setAttribute('for','sortdropdown');
      utilityMenuLabel.textContent = "Sort by ";
      $( utilityMenuSelect ).addClass('form__select form__select--transparent' );
      utilityMenuSelect.appendChild( sortDropDownClone );

      sortDropDownClone.setAttribute( 'onChange', 'systemSort( this.value )' );

      systemSort = function( systemSortOption ) {
        var systemSortButton = document.querySelector( '#sortbutton' );
        sortDropDown.value = systemSortOption;
        systemSortButton.click();
      }


    }
  }

  /**
   * Capture the generated item count, then
   * remove all text but that count, and wrap it with a span.
   */
  // Given a string KEYWORD (1 - 150), remove everything outside parantheses.
  if ( utilityMenu.textContent ) {
    if ( utilityMenu.textContent.match(/ *\d[^)]*\)*/g, "") ){
      utilityMenuCount.textContent = utilityMenu.textContent.match(/ *\d[^)]*\)*/g, "")[0].replace(')','');
    }
    utilityMenu.textContent = '';
    utilityMenuCount.classList.add( 'item-count' );
  }

  /**
   * Lists / Bookmarks
   */
   if ( bookmarks ) {
     //var clearBookmarks = document.querySelector( '.navigationRow a[href*=clear_saves]');
     //bookmarks.textContent = "Bookmarks";
     //utilityMenu.appendChild( bookmarks);
     //utilityMenu.appendChild( clearBookmarks);
     //utilityMenu.appendChild( utilityMenu__bookmarkOptions );
   }

  /**
   * Let's move everything into place.
   */
  utilityMenu.appendChild( utilityMenuLabel );
  utilityMenu.appendChild( utilityMenuSelect );
  utilityMenu.appendChild( utilityMenuCount );


  /**
   * Buttons
   */
   utilityMenu__print.setAttribute( 'onClick', 'window.print();' );
   $( utilityMenu__print ).addClass( 'button button--small button--actions button--flat no-margin' );
   utilityMenu__print.innerHTML = '<svg class="svg" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M19 8H5c-1.66 0-3 1.34-3 3v6h4v4h12v-4h4v-6c0-1.66-1.34-3-3-3zm-3 11H8v-5h8v5zm3-7c-.55 0-1-.45-1-1s.45-1 1-1 1 .45 1 1-.45 1-1 1zm-1-9H6v4h12V3z"/><path d="M0 0h24v24H0z" fill="none"/></svg> <span>Print</span>';

  //utilityMenu__details.setAttribute( 'onClick', 'toggleDetails();' );
  //utilityMenu__details.classList.add ('button', 'button--small', 'button--actions', 'button--flat', 'no-margin' );
  //utilityMenu__details.innerHTML = '<svg class="svg" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z"/><path d="M0 0h24v24H0z" fill="none"/></svg><span>Expand</span>';

   //utilityMenu__actions.appendChild( utilityMenu__details );
   utilityMenu__actions.appendChild( utilityMenu__print );
   utilityMenu__actions.classList.add( 'menu--utility__actions' );
   utilityMenu.appendChild( utilityMenu__actions );
   if ( document.querySelector( 'label[for="hide-unavailable"]') ) {
     utilityMenu__actions.appendChild( document.querySelector( 'label[for="hide-unavailable"]') );
     document.querySelector( 'input[name="availlim"]' ).setAttribute( 'id', 'hide-unavailable');
     document.querySelector( 'label[for="hide-unavailable"]')
      .setAttribute( 'class', 'button button--small button--actions button--flat no-margin');
      document.querySelector( 'label[for="hide-unavailable"]')
       .setAttribute( 'style', 'top: 0;');
    document.querySelector( 'label[for="hide-unavailable"]')
      .setAttribute( 'onclick', 'novacatResults.hideUnavailable()' );
      document.querySelector( 'label[for="hide-unavailable"]')
      .innerHTML = '<svg class="svg" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M0 0h24v24H0zm0 0h24v24H0zm0 0h24v24H0zm0 0h24v24H0z" fill="none"/><path d="M12 7c2.76 0 5 2.24 5 5 0 .65-.13 1.26-.36 1.83l2.92 2.92c1.51-1.26 2.7-2.89 3.43-4.75-1.73-4.39-6-7.5-11-7.5-1.4 0-2.74.25-3.98.7l2.16 2.16C10.74 7.13 11.35 7 12 7zM2 4.27l2.28 2.28.46.46C3.08 8.3 1.78 10.02 1 12c1.73 4.39 6 7.5 11 7.5 1.55 0 3.03-.3 4.38-.84l.42.42L19.73 22 21 20.73 3.27 3 2 4.27zM7.53 9.8l1.55 1.55c-.05.21-.08.43-.08.65 0 1.66 1.34 3 3 3 .22 0 .44-.03.65-.08l1.55 1.55c-.67.33-1.41.53-2.2.53-2.76 0-5-2.24-5-5 0-.79.2-1.53.53-2.2zm4.31-.78l3.15 3.15.02-.16c0-1.66-1.34-3-3-3l-.17.01z"/></svg><span>Hide Unavailable</span>';
    }
  // Adding this class signals that the utility menu is heavily
  // reliant on JS. The CSS also uses this class to
  // display the utility menu after it has rendered.
  $( utilityMenu ).addClass( 'js-keywords-bar menu--utility clearfix' );
}
}

init();

/*------------------------------------*\
  #Searchtool
\*------------------------------------*/
/**
 * Appends "### Results found" to the search box.
 */
searchtool_searchType.wrapAll('<span id="search-options"><div class="form__select"></div></span>');
searchtool_searchScope.appendTo( $( '#search-options' ) );
searchtool_searchScope.wrapAll( '<div class="form__select form__select--scope form__select--transparent"></div>');
$( searchtool_message ).children( 'button, a').appendTo( $('#search-options' ) );
$( searchtool_message ).addClass('browseSearchtool__results small-text').appendTo( $( searchtool ) );
if ( searchtool_message ) {
  searchtool_message.textContent = searchtool_message.textContent.replace('Sorted by','');
  searchtool_message.textContent = searchtool_message.textContent.replace('|   |  .','');
}
/**
 * Adds "Advanced Search" link underneath the search box.
 */
if ( button_modifySearch && button_modifySearch.indexOf( 'NOSRCH' ) > -1  ) {
  $( searchtool ).append( '<a class="link link--undecorated browseSearchtool__option-link browseSearchtool__option-link--adv small-text" href="' + button_modifySearch + '">Advanced Search</a>');
  $( '.form__select--scope').addClass( 'js-pull-left' );
}


$( '#search-options' )
.show();
/*------------------------------------*\
  #Save Search
\*------------------------------------*/
if ( document.querySelector( 'input[name="SUBPREF"]' ) ) {

  var el = document.querySelector( 'input[name="SUBPREF"]' );

  //el.outerHTML = '<a class="browseSearchtool__option-link browseSearchtool__option-link--save link link--undecorated small-text" href="//systemsdev.library.nova.edu/auth/index.php?saved=' + location.search + '">Save Search</a>';

  } else {

var el      = document.createElement( 'a' ),
    // searchtype=, searcharg=, SUBPREF=t
    // search/X/?=
    query   = location.pathname + location.search;

//searchtool.appendChild( el );
//el.outerHTML = '<a class="browseSearchtool__option-link browseSearchtool__option-link--save link link--undecorated small-text" href="//systemsdev.library.nova.edu/auth/index.php?saved=' + query + '">Save Search</a>';

}

$( searchtool ).addClass( 'col-sm--tencol col-lg--eightcol col--centered form' );
$( '.browseList' ).addClass( 'col-md--tencol col-lg--eightcol col--centered' );
$( '.browseScreen' ).addClass( 'js-visible' );

/*------------------------------------*\
Search Results Page
\*------------------------------------*/

/**
 * Turn spell suggestions into an alert
 */
if ( document.querySelector( '#spellcheck' ) ) {

  var el = document.querySelector( '#spellcheck' );

  // Use jQuery for IE9 Support
  $( el ).addClass( 'alert alert--warning no-margin').insertAfter( '.browseSearchtool' );

}

if ( document.querySelector( 'tr.browseSubEntry' ) ) {

  $$( 'tr.browseSubEntry > td' ).forEach( function( browseSubEntry ) {
      browseSubEntry.innerHTML = browseSubEntry.innerHTML.replace(/&nbsp;/g,'');
  });

}

if ( document.querySelector( '.catalog__result_number') ) {

  $( '.catalog__result_number' ).each( function() {

    var el = $(this),
        checkbox = el.prev( 'input[type=checkbox'),
        value = checkbox.val();

    checkbox.attr( 'id', 'bookmark-' + value );
    el.attr( 'for', 'bookmark-' + value );

  });

}

})();
