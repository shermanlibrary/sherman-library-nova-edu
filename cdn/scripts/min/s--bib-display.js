var $clean__standard_number = $( '#raw__standard_number .bibInfoEntry tr:first-child .bibInfoData' )
                                .text()
                                .trim()
                                .match(/^[^\s]+/),

    $clean__author = $( '#raw__author .bibInfoEntry tr:first-child .bibInfoData' ).text().trim(),
    $clean__title = $( '#raw__title .bibInfoEntry tr:first-child .bibInfoData' ).text().trim(),
    $clean__description = $( '#raw__description .bibInfoEntry tr:first-child .bibInfoData' ).text().trim(),
    $clean__oclc  = $( '#raw__oclc .bibInfoEntry tr:first-child .bibInfoData' ).text().trim(),
    $clean__publisher = $( '#raw__publisher .bibInfoEntry tr:first-child .bibInfoData' ).text().trim(),
    $clean__upc, 
    $clean__summary = $( '#raw__notes .bibInfoLabel:contains("Summary")')
            .next( '.bibInfoData' ).text().trim(),
    $clean__holds = document.getElementById( "holds" ).innerHTML.match( /\d+\shold[s\s]/ );

var strippedInfo = document.getElementById( 'illiadinfo' ).innerHTML.replace(/(<([^>]+)>)/ig,"");
var openURL = strippedInfo.replace("ISBN","&isbn=").replace("Author", " &aulast=").replace("Title", "&title=").replace("Publisher", "&date=").replace(/=\s/,"=")

// ISBN / UPC Slug
var $syndetics_cover_slug = ( $clean__standard_number[0].length === 12 ||  $clean__standard_number[0].length === 14 ? 'upc' : 'isbn' );

// Item cover
var $syndetics_cover = '//www.syndetics.com/index.php?' + $syndetics_cover_slug + '=' + $clean__standard_number + '/lc.gif&client=novaseu';


var recordinfo = {
  author: $clean__author,
  cover: ( $clean__standard_number || $clean__oclc || $clean__upc ? $syndetics_cover : null ),
  description: $clean__description,
  holds: ( $clean__holds ? $clean__holds : null ),
  openURL: openURL,
  oclc: ( $clean__oclc ? $clean__oclc : null ),  
  publisher: $clean__publisher,
  summary: ( $clean__summary ? $clean__summary : null ),
  title: $clean__title
}

function showRecord() {
  var template = $( '#template--bibDisplay' ).html();
  Mustache.parse( template );

  var rendered = Mustache.render( template, recordinfo );
  $( 'article' ).append( rendered );
}
  
showRecord();

// New holdbutton
var holdButton = document.querySelector( 'a[href*="/request"]' );

if(holdButton) {
  
  var holdButtonPath = holdButton.getAttribute('href'),
      resultsButton = document.querySelector('a[href*=browse\\#anchor]'),   
      auth = "https://systemsdev.library.nova.edu/auth/index.php?hold=",
      resultsButtonPath;


      if ( resultsButton ) {
        
        resultsButtonPath = resultsButton.getAttribute('href').replace('#','key');
        holdButton
          .setAttribute( 'href', auth + holdButtonPath + 'searchResults='+ resultsButtonPath );

      }      

      holdButton.innerHTML = "Hold for Pick-Up";
      holdButton.className = "button button--default button--flat button--small small-text record__hold";


  }

var actionsModifySearch = document.querySelector( '.record__actions a[href*=NOSRCH]' );
if ( actionsModifySearch ) {
  actionsModifySearch.parentNode.removeChild( actionsModifySearch );
}


var actionsReturn = document.querySelector( '.record__actions a[href*=browse\\#anchor]' );
if ( actionsReturn ) {
  actionsReturn.parentNode.removeChild( actionsReturn );
}

var actionsMarc = document.querySelector( '.record__actions a[href*=marc]' );
if ( actionsMarc ) {
  actionsMarc.className = "button button--default button--small button--flat small-text";
}

var actionsList = document.querySelector( '.record__actions a[href*=save]' );
if ( actionsList ) {
  actionsList.className = "button button--default button--small button--flat small-text";
  actionsList.textContent = 'Add to My List';
}