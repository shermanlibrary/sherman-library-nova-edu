var UserMgt_CreateForm;
var UserMgt_CreateWindow;

var UserMgt_ContactedField;
var UserMgt_SpecifyField;
var UserMgt_HearField;
var UserMgt_EmailField;
var UserMgt_GenderField;
var UserMgt_BirthdateField;
var UserMgt_PhoneField;
var UserMgt_ZIPField;
var UserMgt_StateField;
var UserMgt_CityField;
var UserMgt_AptField;
var UserMgt_StreetAddressField;
var UserMgt_LastNameField;
var UserMgt_MIField;
var UserMgt_FirstNameField;

var UserMgt_Text;

var UserMgt_gender_store;
var UserMgt_hear_store;
var UserMgt_contact_store;


var online19;
var online20;
var online21;
var online22;

var UserMgt_combo;


function stripAddr() {
	var doc = document.selfRegForm;
	var getCity = UserMgt_CityField.getValue();
	var getState = UserMgt_StateField.getValue();
	var getZIP = UserMgt_ZIPField.getValue();
	var addrString = getCity + ", " + getState + " " + getZIP;

	return addrString;
}

function reason() {
	var	getOther = UserMgt_HearField.getValue();
	var	getHow = UserMgt_SpecifyField.getValue();
	var reason = '';
	switch(getOther){
		case 'Word of mouth':
			reason = '1 - Friend';
			break;		
		case 'Advertisement':
			reason = '2 - Advertisement';
			break;		
		case 'Outreach community event':
			reason = '3 - Outreach/community event';
			break;
		case 'Other':
			reason = '4 - Other: ' + getHow;
			break;
	}		

	return reason;
}

function validate() {
	
	//var option = Ext.get('formNo').dom.value;
	var currentTime = new Date();
	var cmonth = currentTime.getMonth() + 1;
	var cdate = currentTime.getDate();
	var cyear = currentTime.getFullYear();
	
	//var parseddate = UserMgt_BirthdateField.getRawValue();
	
	var parseddate5 = UserMgt_BirthdateField.getValue();
	
	var parseddate6 = parseddate5.split("-");
	
	var month = parseddate6[0];
	var date = parseddate6[1];
	var year = parseddate6[2];
	
	//var month =  parseddate.substring(0,2);
	//var date = parseddate.substring(2,4);
	//var year = parseddate.substring(4);
	
	var theYear = cyear - year;
	var theMonth = cmonth - month;
	var theDate = cdate - date;

	var days = "";
	if (cmonth == 0 || cmonth == 2 || cmonth == 4 || cmonth == 6 || cmonth == 7 || cmonth == 9 || cmonth == 11) days = 31;
	if (cmonth == 3 || cmonth == 5 || cmonth == 8 || cmonth == 10) days = 30;
	if (cmonth == 1) days = 28;

	if (month < cmonth && date > cdate) {
	theYear = theYear + 1;
	}
	else if (month > cmonth && date <= cdate) {
	theYear = theYear - 1;
	theMonth = ((12 - -(theMonth)) + 1);
	}
	else if (month > cmonth && date > cdate) {
	theMonth = ((12 - -(theMonth)));
	}
	if (date < cdate) {
		theDate = theDate;
	}
	else if (date == cdate) {
		theDate = 0;
	}
	else {
		theYear = theYear - 1;
	} 

	var option = "-1";
	//Ext.MessageBox.alert('test', theYear);
	if((theYear >= 0) && (theYear <= 12))
	{
		//22
		option = "22"; 
	}
	else if((theYear >= 13) && (theYear <= 17))
	{
		//21
		option = "21";
	}
	else if((theYear >= 18) && (theYear <= 54))
	{
		//20
		option = "20";
	}
	else if(theYear >= 55)
	{
		//19
		option = "19";
	}
	
	var params = [];
	var params1 = [];
	
	params["nfirst"] = UserMgt_FirstNameField.getValue();
	params["nmiddle"] = UserMgt_MIField.getValue();
	params["nlast"] = UserMgt_LastNameField.getValue();
	params["full_aaddress"] = UserMgt_StreetAddressField.getValue();
	//params["full_aaddress"] = UserMgt_AptField.getValue();
	params1.push(UserMgt_AptField.getValue(), stripAddr());
	params["city"] = UserMgt_CityField.getValue();
	params["zip"] = UserMgt_ZIPField.getValue();
	//params["full_aaddress"] = stripAddr();
	//params["full_aaddress"] = stripAddr();
	params["tphone1"] = UserMgt_PhoneField.getValue();
	
	var dob = UserMgt_BirthdateField.getValue();
	
	var mdys = dob.split("-");
	
	params["ddMonth"] = mdys[0];
	params["ddDay"] = mdys[1];
	params["ddYear"] = mdys[2];
	
	params["F051birthdate"] = dob;
	params["F044pcode1"] = UserMgt_GenderField.getValue();
	params["zemailaddr"] = UserMgt_EmailField.getValue();
	params["how"] = reason();
	params["ddepartment"] = reason();
	params["pphone2"] = UserMgt_ContactedField.getValue();
	//params["submit"] = "Submit";
	

	
	url = "";
	switch(option)
	{
		case "19":
			url = "https://novacat.nova.edu/selfreg/Online19";
			post_to_url(url, params, params1);
			break;
		case "20":
			url = "https://novacat.nova.edu/selfreg/Online20";
			post_to_url(url, params, params1);
			break;
		case "21":
			url = "https://novacat.nova.edu/selfreg/Online21";
			post_to_url(url, params, params1);
			break;
		case "22":
			url = "https://novacat.nova.edu/selfreg/Online22";
			post_to_url(url, params, params1);
			break;		
		case "-1":
			Ext.MessageBox.alert("Error", "Your birthday is not valid.");
			break;
	}
}
		
function post_to_url(path, params, params1, method) {
	method = method || "post"; // Set method to post by default, if not specified. 

	// The rest of this code assumes you are not using a library. 
	// It can be made less wordy if you use one. 
	var form = document.createElement("form");
	form.setAttribute("name", "selfRegForm");
	form.setAttribute("id", "selfRegForm");
	form.setAttribute("method", method);
	form.setAttribute("action", path);
	
	//form.setAttribute("target", "MyIFrame");

	for (var key in params) {
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", key);
		hiddenField.setAttribute("value", params[key]);

		form.appendChild(hiddenField);
	}
	
	for(var i=0; i<params1.length; i++) {
		var value = params1[i];
		
		var hiddenField = document.createElement("input");
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", "full_aaddress");
		if(params1[i] == '')
			hiddenField.setAttribute("value", ' ');
		else
			hiddenField.setAttribute("value", params1[i]);

		form.appendChild(hiddenField);
	}
	
	document.body.appendChild(form);    // Not entirely sure if this is necessary
	form.submit();

}

//https://novacat.nova.edu/selfreg/Online20

		

Ext.onReady(function(){
	Ext.QuickTips.init();
	var test = Ext.QuickTips.getQuickTip();
	//Ext.QuickTips.init();
	Ext.apply(test, {
		dismissDelay: 10000
	});
	
	/*
	var store = new Ext.data.JsonStore({
        root: 'topics',
        totalProperty: 'totalCount',
        idProperty: 'threadid',
        remoteSort: true,

        fields: [
            'title', 
			'forumtitle', 
			'forumid', 
			'author',
            {name: 'replycount', type: 'int'},
            {name: 'lastpost', mapping: 'lastpost', type: 'date', dateFormat: 'timestamp'},
            'lastposter', 
			'excerpt'
        ],
		autoLoad: true,
        // load using script tags for cross domain, if the data in on the same domain as
        // this page, an HttpProxy would be better
        proxy: new Ext.data.ScriptTagProxy({
            //url: 'http://extjs.com/forum/topics-browse-remote.php'
			url: 'http://libsystems.library.nova.edu/js/getTime.php'
        })
    });
    store.setDefaultSort('lastpost', 'desc');
	*/
	/*
	var store = new Ext.data.Store({
        proxy: new Ext.data.ScriptTagProxy({
            url: 'http://libsystems.library.nova.edu/js/getTime.php'
        }),
		reader: new Ext.data.JsonReader({
            root: 'results',
            totalProperty: 'total'
        }, [
            {name: 'ServerTime', mapping: 'ServerTime', type: 'date', dateFormat: 'm-d-Y'}
        ])
    });
	*/
 /*
 var store11 = new Ext.data.JsonStore({
        proxy: new Ext.data.ScriptTagProxy({
            url: 'http://libsystems.library.nova.edu/js/getTime.php',
			method: 'POST'
        }),
		baseParams:{task: "LISTING"},
		reader: new Ext.data.JsonReader({   
			root: 'results',
			totalProperty: 'total',
			id: 'ServerTime'
		},[ 
			{name: 'ServerTime', mapping: 'ServerTime', type: 'string'}
		])
    });
*/
	/*
 Ext.Ajax.request({   
			waitMsg: 'Please wait...',
			scriptTag:true,
			url: "http://libsystems.library.nova.edu/js/getTime.php",
			method: 'POST',
			params : { action : 'getDate' },
			success: function(result, request){
				Ext.MessageBox.alert('Success', 'Data return from the server: '+ result.responseText); 
			},
			failure: function ( result, request) { 
				Ext.MessageBox.alert('Failed', result.statusText); 
			} 
		});
*/
	
	
	(function() {
		//Store the original blur event
		var originalBlur = Ext.form.TextField.prototype.onBlur;

		//Override TextField
		Ext.override(Ext.form.TextField, {
			upper: false,
			onBlur: function() {
				//If the upper property is true
				if (this.upper) {
					//Change the text of the TextField to uppercase
					this.setValue(this.getValue().toUpperCase());
				}

				//call default TextField blur
				originalBlur.apply(this, arguments);
			}
		});
	})();

				
		function UserMgt_resetForm(){
			UserMgt_FirstNameField.reset();
			UserMgt_MIField.reset();
			UserMgt_LastNameField.reset();
			UserMgt_StreetAddressField.reset();
			UserMgt_AptField.reset();
			UserMgt_CityField.reset();
			UserMgt_StateField.reset();
			UserMgt_ZIPField.reset();
			UserMgt_PhoneField.reset();
			UserMgt_BirthdateField.reset();
			UserMgt_GenderField.reset();
			UserMgt_EmailField.reset();
			UserMgt_HearField.reset();
			UserMgt_SpecifyField.reset();
			UserMgt_ContactedField.reset();
		}

		// check if the form is valid
		function UserMgt_isFormValid(){
			return (
				UserMgt_FirstNameField.isValid() &&
				//UserMgt_MIField.isValid() &&
				UserMgt_LastNameField.isValid() &&
				UserMgt_StreetAddressField.isValid() &&
				UserMgt_AptField.isValid() &&
				UserMgt_CityField.isValid() &&
				UserMgt_StateField.isValid() &&
				UserMgt_ZIPField.isValid() &&
				UserMgt_PhoneField.isValid() &&
				UserMgt_BirthdateField.isValid() &&
				UserMgt_GenderField.isValid() &&
				UserMgt_EmailField.isValid() &&
				UserMgt_HearField.isValid() &&
				//UserMgt_SpecifyField.isValid() &&
				UserMgt_ContactedField.isValid()
			);
		}
		

		// display or bring front the form
		function UserMgt_displayFormWindow(){
			if(!UserMgt_CreateWindow.isVisible()){
				UserMgt_resetForm();
				UserMgt_CreateWindow.show();
			} 
			else {
				//CreateWindow.toFront();
					UserMgt_CreateWindow.hide();
					UserMgt_CreateWindow.show();
			}
		}
		
		UserMgt_gender_store = new Ext.data.ArrayStore({
				fields: ['id', 'field'],
				data : [['f','F'],['m','M']]
		});

		UserMgt_hear_store = new Ext.data.ArrayStore({
				fields: ['id', 'field'],
				data : [
					['1 - Friend','<b>Word of mouth</b><br>Lo o&iacute; nombrar'],
					['2 - Advertisement','<b>Advertisement</b><br>Propaganda'],
					['3 - Outreach/community event','<b>Outreach community event</b><br>Evento comunitario '],
					['4 - Other:','<b>Other</b><br>Otro']
				]
		});
			
		UserMgt_contact_store = new Ext.data.ArrayStore({
				fields: ['id', 'field'],
				data : [['y','Yes / Si'],['n','No']]
		});
	//////////////////////////////////////////////////////
	//////////////related to register form////////////////
	//////////////////////////////////////////////////////
		UserMgt_FirstNameField = new Ext.form.TextField({
			id: 'UserMgt_FirstNameField',
			fieldLabel: '<b>First Name</b><br> Nombre',
			minLength: 2,
			maxLength: 50,
			allowBlank: false,
			msgTarget:'side',
			upper: true,
			//validationEvent:false,
			validateOnBlur : true,
			anchor : '93%'
		});
		
		UserMgt_MIField = new Ext.form.TextField({
			id: 'UserMgt_MI',
			fieldLabel: '<b>M.I.</b><br> Inicial',
			msgTarget:'side',
			//validationEvent:false,
			validateOnBlur : true,
			upper: true,
			anchor : '93%'
		});

		UserMgt_LastNameField = new Ext.form.TextField({
			id: 'UserMgt_LastName',
			fieldLabel: '<b>Last Name</b><br>Apellido',
			minLength: 2,
			maxLength: 50,
			allowBlank: false,
			msgTarget:'side',
			//validationEvent:false,
			validateOnBlur : true,
			upper: true,
			anchor : '93%'
		});
		
		UserMgt_StreetAddressField = new Ext.form.TextField({
			id: 'UserMgt_StreetAddressField',
			fieldLabel: '<b>Street Address</b><br> Direcci&oacute;n',
			//minLength: 3,
			//maxLength: 100,
			allowBlank: false,
			msgTarget:'side',
			upper: true,
			anchor : '93%',
			validateOnBlur : true,
			validator: function(v) {
				if(v.length < 5)
				{
					return 'Enter valid street address.';
				}
				else
				{
					return true;
				}
			}
		});
		
		UserMgt_AptField = new Ext.form.TextField({
			id: 'UserMgt_AptField',
			fieldLabel: '<b>Apt.</b><br>Apto.',
			msgTarget:'side',
			upper: true,
			anchor : '93%'
		});
		
		UserMgt_CityField = new Ext.form.TextField({
			id: 'UserMgt_CityField',
			fieldLabel: '<b>City</b><br>Ciudad',
			//minLength: 3,
			//maxLength: 15,
			allowBlank: false,
			msgTarget:'side',
			//validationEvent:false,
			upper: true,
			anchor : '93%',
			validateOnBlur : true,
			validator: function(v) {
				if(v.length < 4)
				{
					return 'Enter valid city.';
				}
				else
				{
					return true;
				}
			}
			
		});
		
		UserMgt_StateField = new Ext.form.TextField({
			id: 'UserMgt_StateField',
			fieldLabel: '<b>State</b><br>Estado',
			minLength: 2,
			maxLength: 15,
			allowBlank: false,
			msgTarget:'side',
			//validationEvent:false,
			//validateOnBlur : true,
			upper: true,
			anchor : '93%',
			value: 'FL',
			disabled: true
		});
		UserMgt_ZIPField = new Ext.form.TextField({
			id: 'UserMgt_ZIPField',
			fieldLabel: '<b>ZIP</b><br>C&oacute;digo postal',
			minLength: 5,
			maxLength: 5,
			allowBlank: false,
			msgTarget:'side',
			maskRe: /[0-9]/,
			//validationEvent:false,
			validateOnBlur : true,
			upper: true,
			anchor : '93%',
			validator: function(value) {
				var subSection = value.substring(2,0);
				var answer = false;

				if((subSection != '33' )||(value.length != 5))
					return  'Enter Broward, Miami-Dade, or Palm Beach county zip code (5 digits only).';
			
			}
		});	
		
		UserMgt_PhoneField = new Ext.form.TextField({
			id: 'UserMgt_PhoneField',
			fieldLabel: '<b>Phone number (xxx-xxx-xxxx)</b><br>Tel&eacute;fono',
			minLength: 3,
			maxLength: 12,
			allowBlank: false,
			maskRe: /[0-9\-]/,
			upper: true,
			msgTarget:'side',
			//validationEvent:false,
			validateOnBlur : true,
			anchor : '93%',
			validator: function(v) {
				if(v.length == 0)
				{
					return 'This field is required.';
				}
				else
				{
					return /[0-9]{3}\-[0-9]{3}\-[0-9]{4}/.test(v)? true : 'Entered phone number must be of the form xxx-xxx-xxxx, where x represent digits 0-9.';
				}
			}
		});	
		
		
		UserMgt_EmailField = new Ext.form.TextField({
			id: 'UserMgt_EmailField',
			fieldLabel: '<b>Email address</b><br>Correo electr&oacute;nico',
			vtype: 'email',
			//minLength: 3,
			maxLength: 50,
			allowBlank: false,
			msgTarget:'side',
			//validationEvent:false,
			anchor : '93%',
			validateOnBlur : true
		});	

	
		/*
		UserMgt_BirthdateField = new Ext.form.DateField({
			id: 'UserMgt_BirthdateField',
			fieldLabel: '<b>Birthdate</b><br>Fecha de Nacimiento',
			format : 'mdY',
			allowBlank: false,
			msgTarget:'side',
			//validationEvent:false,
			validateOnBlur : true,
			showToday: false,
			maskRe: /[0-9\-]/,
			anchor:'93%'
		});
		*/
		
		UserMgt_BirthdateField = new Ext.form.TextField({
			id: 'UserMgt_BirthdateField',
			fieldLabel: '<b>Birthdate (mm-dd-yyyy)</b><br>Fecha de Nacimiento',
			//format : 'm-d-Y',
			allowBlank: false,
			maxLength: 10,
			msgTarget:'side',
			//validationEvent:false,
			validateOnBlur : true,
			showToday: false,
			maskRe: /[0-9\-]/,
			anchor:'93%',
			validator: function(v) {
				var msg = "";
				if(v.length == 0)
				{
					msg = 'This field is required.';
				}
				else
				{
					if(/[0-9]{2}\-[0-9]{2}\-[0-9]{4}$/.test(v))
					{
						var mdys = v.split("-");
						var monVal = -1;
						var dayVal = -1;
						switch(mdys[0])
						{
							case '01':
								monVal = 1;
								break;
							case '02':
								monVal = 2;
								break;
							case '03':
								monVal = 3;
								break;
							case '04':
								monVal = 4;
								break;
							case '05':
								monVal = 5;
								break;
							case '06':
								monVal = 6;
								break;
							case '07':
								monVal = 7;
								break;
							case '08':
								monVal = 8;
								break;
							case '09':
								monVal = 9;
								break;
							case '10':
								monVal = 10;
								break;
							case '11':
								monVal = 11;
								break;
							case '12':
								monVal = 12;
								break;
						}
						
						switch(mdys[1])
						{
							case '01':
								dayVal = 1;
								break;
							case '02':
								dayVal = 2;
								break;
							case '03':
								dayVal = 3;
								break;
							case '04':
								dayVal = 4;
								break;
							case '05':
								dayVal = 5;
								break;
							case '06':
								dayVal = 6;
								break;
							case '07':
								dayVal = 7;
								break;
							case '08':
								dayVal = 8;
								break;
							case '09':
								dayVal = 9;
								break;
							case '10':
								dayVal = 10;
								break;
							case '11':
								dayVal = 11;
								break;
							case '12':
								dayVal = 12;
								break;
							case '13':
								dayVal = 13;
								break;
							case '14':
								dayVal = 14;
								break;
							case '15':
								dayVal = 15;
								break;
							case '16':
								dayVal = 16;
								break;
							case '17':
								dayVal = 17;
								break;
							case '18':
								dayVal = 18;
								break;
							case '19':
								dayVal = 19;
								break;
							case '20':
								dayVal = 20;
								break;
							case '21':
								dayVal = 21;
								break;
							case '22':
								dayVal = 22;
								break;
							case '23':
								dayVal = 23;
								break;
							case '24':
								dayVal = 24;
								break;
							case '25':
								dayVal = 25;
								break;
							case '26':
								dayVal = 26;
								break;
							case '27':
								dayVal = 27;
								break;
							case '28':
								dayVal = 28;
								break;
							case '29':
								dayVal = 29;
								break;
							case '30':
								dayVal = 30;
								break;
							case '31':
								dayVal = 31;
								break;
						}
						
						if((monVal < 1) || (monVal > 12)){msg = 'Enter valid month (mm).';}
						else if((dayVal < 1) || (dayVal > 31)){msg = 'Enter valid day (dd).';}
						if((parseInt(mdys[2]) < 1900) || (parseInt(mdys[2]) > 2011)){ msg = 'Enter valid year(yyyy)';}
						else{
							//Get Server Time
							var currentTime = new Date();
							
							var cmonth = currentTime.getMonth() + 1;
							var cdate = currentTime.getDate();
							var cyear = currentTime.getFullYear();
							
							var parseddate5 = v;
							
							var parseddate6 = parseddate5.split("-");
							
							var month = parseddate6[0];
							var date = parseddate6[1];
							var year = parseddate6[2];

							var theYear = cyear - year;
							var theMonth = cmonth - month;
							var theDate = cdate - date;

							var days = "";
							if (cmonth == 0 || cmonth == 2 || cmonth == 4 || cmonth == 6 || cmonth == 7 || cmonth == 9 || cmonth == 11) days = 31;
							if (cmonth == 3 || cmonth == 5 || cmonth == 8 || cmonth == 10) days = 30;
							if (cmonth == 1) days = 28;

							if (month < cmonth && date > cdate) {
								theYear = theYear + 1;
							}
							else if (month > cmonth && date <= cdate) {
								theYear = theYear - 1;
								theMonth = ((12 - -(theMonth)) + 1);
							}
							else if (month > cmonth && date > cdate) {
								theMonth = ((12 - -(theMonth)));
							}
							if (date < cdate) {
								theDate = theDate;
							}
							else if (date == cdate) {
								theDate = 0;
							}
							else {
								theYear = theYear - 1;
							}
							
							option = -1;
							if((theYear >= 0) && (theYear <= 12))
							{
								//22
								option = "22"; 
							}
							else if((theYear >= 13) && (theYear <= 17))
							{
								//21
								option = "21";
							}
							else if((theYear >= 18) && (theYear <= 54))
							{
								//20
								option = "20";
							}
							else if(theYear >= 55)
							{
								//19
								option = "19";
							}
	
							switch(option)
							{
								case "19":
								case "20":
								case "21":
								case "22":
									break;		
								case -1:
									 msg = "Your birth date is not valid.";
									break;
							}
							
						}
					}
					else
					{
						msg = 'Entered date of birth must be of the form mm-dd-yyyy.';
					}	
				}
				
				if(msg != "")
				{
					return msg;
				}
				else
				{
					return true;
				}
			}
		});
		
		UserMgt_GenderField = new Ext.form.ComboBox({
			id: 'UserMgt_GenderField',
			fieldLabel: '<b>Gender</b><br>G&eacute;nero',
			store: 			UserMgt_gender_store,
			displayField:	'field',
			valueField : 	'id',
			typeAhead: 		true,
			editable: 		false,
			allowBlank: 	false,
			validateOnBlur : true,
			mode: 			'local',
			triggerAction: 	'all',
			emptyText:		'Select one...',
			selectOnFocus:	true,
			//validationEvent:false,
			grow: true,
			anchor:'93%'
		});	
		
		
		UserMgt_HearField = new Ext.form.ComboBox({
			id: 'UserMgt_HearField',
			fieldLabel: '<b>How did you hear about us?</b><br>&iquest;C&oacute;mo supo acerca de nosotros?',
			store: 			UserMgt_hear_store,
			displayField:	'field',
			valueField : 	'id',
			editable: 		false,
			typeAhead: 		true,
			mode: 			'local',
			triggerAction: 	'all',
			emptyText:		'Select one...',
			selectOnFocus:	true,
			//validationEvent:false,
			validateOnBlur : true,
			allowBlank: 	false,
			grow: true,
			anchor:'93%',		
			listeners:{
				select: function(combo, record, index) {
					//Ext.MessageBox.alert('alert',combo.getValue());
					switch(combo.getValue()){
						case '1 - Friend':
							Ext.getCmp('UserMgt_HearField').setValue('Word of mouth');
							Ext.getCmp('UserMgt_SpecifyField').setVisible(false);
							break;
						case '2 - Advertisement':
							Ext.getCmp('UserMgt_HearField').setValue('Advertisement');
							Ext.getCmp('UserMgt_SpecifyField').setVisible(false);
							break;
						case '3 - Outreach/community event':
							Ext.getCmp('UserMgt_HearField').setValue('Outreach community event');
							Ext.getCmp('UserMgt_SpecifyField').setVisible(false);
							break;
						case '4 - Other:':
							Ext.getCmp('UserMgt_HearField').setValue('Other');
							Ext.getCmp('UserMgt_SpecifyField').setVisible(true);
							break;
					}
					//Ext.MessageBox.alert('alert',combo.getValue());
				}
			}
		});	
		
		UserMgt_SpecifyField = new Ext.form.TextField({
			id: 'UserMgt_SpecifyField',
			fieldLabel: '<b>If "Other", please specify</b><br>Si escogi&oacute; "Otro", por favor explique',
			msgTarget:'side',
			validationEvent:false,
			hidden:true,
			upper: true,
			anchor : '93%'
		});	
		
		
		UserMgt_ContactedField = new Ext.form.ComboBox({
			id: 'UserMgt_ContactedField',
			fieldLabel: '<b>Would you like to be contacted about library news?</b><br>&iquest;Desea recibir noticias de la biblioteca?',
			store: 			UserMgt_contact_store,
			displayField:	'field',
			valueField : 	'id',
			typeAhead: 		true,
			editable: 		false,
			mode: 			'local',
			triggerAction: 	'all',
			emptyText:		'Select one...',
			validateOnBlur : true,
			allowBlank: 	false,
			selectOnFocus:	true,
			//validationEvent:false,
			grow: true,
			anchor:'93%' 
		});	

	//////////////////////////////////////////////////////
	//////////////////////////////////////////////////////
	//////////////////////////////////////////////////////
		online19 = '<span class="pageMainAreaSubHeader"><img src="http://novacat.nova.edu:2082/screens/patlogin_icon.gif" align="left" alt="" />Sherman Library Card Application (Broward Adults 55+)<br /><em>Solicitud de Tarjeta de la Biblioteca Alvin Sherman (Adultos de Broward mayores de 55 a&ntilde;os)</em></span><br /><p>If you live, work, or attend school in Broward County, you are eligible to apply for an Alvin Sherman Library card.  Proof of Broward County affiliation is required, and types may vary by patron. (<a href="selfregcardid.html" onClick="wincardid(); return false;"><b>See list...</b></a>)<br />The Law and Health Professions Division libraries allow browsing and photocopying materials, but do not check-out to the public.</p><p><em>Si usted vive, trabaja, o estudia en el condado de Broward, puede solicitar una tarjeta de la biblioteca Alvin Sherman. Constancia de su afiliaci&oacute;n con Broward es indispensable, y puede variar en naturaleza seg&uacute;n el caso del solicitante. (<a href="selfregcardid.html#s" onClick="wincardids(); return false;"><b>Ver lista...</b></a>)<br />Las bibliotecas de Derecho y Profesiones M&eacute;dicas solamente permiten examinar y fotocopiar materiales, mas no hacen pr&eacute;stamos al p&uacute;blico.</em><br /><br />';
		/*
		online20 ='<span class="pageMainAreaSubHeader"><img src="http://novacat.nova.edu:2082/screens/patlogin_icon.gif" align="left" alt="" />Sherman Library Card Application (Broward Adults 18-54)<br /><em>Solicitud de Tarjeta de la Biblioteca Alvin Sherman (Adultos de Broward 18-54 a&ntilde;os)</em></span><br /><p>If you live, work, or attend school in Broward County, you are eligible to apply for an Alvin Sherman Library card.  Proof of Broward County affiliation is required, and types may vary by patron. (<a href="selfregcardid.html" onClick="wincardid(); return false;"><b>See list...</b></a>)<br />The Law and Health Professions Division libraries allow browsing and photocopying materials, but do not check-out to the public.</p><p><em>Si usted vive, trabaja, o estudia en el condado de Broward, puede solicitar una tarjeta de la biblioteca Alvin Sherman. Constancia de su afiliaci&oacute;n con Broward es indispensable, y puede variar en naturaleza seg&uacute;n el caso del solicitante. (<a href="selfregcardid.html#s" onClick="wincardids(); return false;"><b>Ver lista...</b></a>)<br />Las bibliotecas de Derecho y Profesiones M&eacute;dicas solamente permiten examinar y fotocopiar materiales, mas no hacen pr&eacute;stamos al p&uacute;blico.</em><br /><br />';
		*/
		online20 ='<span class="pageMainAreaSubHeader"><img src="http://novacat.nova.edu:2082/screens/patlogin_icon.gif" align="left" alt="" />Sherman Library Card Application <br /><em>Solicitud de Tarjeta de la Biblioteca Alvin Sherman</em></span><br /><p>If you live, work, or attend school in Broward County, you are eligible to apply for an Alvin Sherman Library card.  Proof of Broward County affiliation is required, and types may vary by patron. (<a href="selfregcardid.html" onClick="wincardid(); return false;"><b>See list...</b></a>)<br />The Law and Health Professions Division libraries allow browsing and photocopying materials, but do not check-out to the public.</p><p><em>Si usted vive, trabaja, o estudia en el condado de Broward, puede solicitar una tarjeta de la biblioteca Alvin Sherman. Constancia de su afiliaci&oacute;n con Broward es indispensable, y puede variar en naturaleza seg&uacute;n el caso del solicitante. (<a href="selfregcardid.html#s" onClick="wincardids(); return false;"><b>Ver lista...</b></a>)<br />Las bibliotecas de Derecho y Profesiones M&eacute;dicas solamente permiten examinar y fotocopiar materiales, mas no hacen pr&eacute;stamos al p&uacute;blico.</em><br /><br />';
		
		online21 = '<span class="pageMainAreaSubHeader"><img src="http://novacat.nova.edu:2082/screens/patlogin_icon.gif" align="left" alt="" />Sherman Library Card Application (Broward Young Adults 13-17)<br /><em>Solicitud de Tarjeta de la Biblioteca Alvin Sherman (J&oacute;venes de Broward 13-17 a&ntilde;os)</em></span><br /><p>If you live, work, or attend school in Broward County, you are eligible to apply for an Alvin Sherman Library card.  Proof of Broward County affiliation is required, and types may vary by patron. (<a href="selfregcardid.html" onClick="wincardid(); return false;"><b>See list...</b></a>)<br />The Law and Health Professions Division libraries allow browsing and photocopying materials, but do not check-out to the public.</p><p><em>Si usted vive, trabaja, o estudia en el condado de Broward, puede solicitar una tarjeta de la biblioteca Alvin Sherman. Constancia de su afiliaci&oacute;n con Broward es indispensable, y puede variar en naturaleza seg&uacute;n el caso del solicitante. (<a href="selfregcardid.html#s" onClick="wincardids(); return false;"><b>Ver lista...</b></a>)<br />Las bibliotecas de Derecho y Profesiones M&eacute;dicas solamente permiten examinar y fotocopiar materiales, mas no hacen pr&eacute;stamos al p&uacute;blico.</em><br /><br />';
		
		online22 = '<span class="pageMainAreaSubHeader"><img src="http://novacat.nova.edu:2082/screens/patlogin_icon.gif" align="left" alt="" />Sherman Library Card Application (Broward Children 0-12)<br /><em>Solicitud de Tarjeta de la Biblioteca Alvin Sherman (Ni&ntilde;os de Broward 0-12 a&ntilde;os)</em></span><br /><p>If you live, work, or attend school in Broward County, you are eligible to apply for an Alvin Sherman Library card.  Proof of Broward County affiliation is required, and types may vary by patron. (<a href="selfregcardid.html" onClick="wincardid(); return false;"><b>See list...</b></a>)<br />The Law and Health Professions Division libraries allow browsing and photocopying materials, but do not check-out to the public.</p><p><em>Si usted vive, trabaja, o estudia en el condado de Broward, puede solicitar una tarjeta de la biblioteca Alvin Sherman. Constancia de su afiliaci&oacute;n con Broward es indispensable, y puede variar en naturaleza seg&uacute;n el caso del solicitante. (<a href="selfregcardid.html#s" onClick="wincardids(); return false;"><b>Ver lista...</b></a>)<br />Las bibliotecas de Derecho y Profesiones M&eacute;dicas solamente permiten examinar y fotocopiar materiales, mas no hacen pr&eacute;stamos al p&uacute;blico.</em><br /><br />';
		
		UserMgt_Text = new Ext.Panel({
			id: 'UserMgt_Text',
			border: false,
			html:''
		});

		
		//alert(screen.width);
	if(screen.width < 1000)
	{
		UserMgt_CreateForm = new Ext.FormPanel({
			labelAlign: 'top',
			autoScroll:true,
			bodyStyle:'padding:5px',
			frame: true,
			items: [
					UserMgt_Text,
					UserMgt_FirstNameField,
					UserMgt_MIField,
					UserMgt_LastNameField,
					UserMgt_StreetAddressField,
					UserMgt_AptField,
					UserMgt_CityField,
					UserMgt_StateField,
					UserMgt_ZIPField,
					UserMgt_PhoneField,
					UserMgt_BirthdateField,
					UserMgt_GenderField,
					UserMgt_EmailField,
					UserMgt_HearField,
					UserMgt_SpecifyField,
					UserMgt_ContactedField
			]
		});
		
		// This was added in tutorial 5
		UserMgt_CreateWindow= new Ext.Window({
			id: 'UserMgt_CreateWindow',
			//bodyStyle: 'padding:15px;background:transparent',
			title: 'Sherman Library Card Application.',
			closable:false,
			autoScroll:true,
			width: 500,
			height: 355,
			modal:true,
			//plain:true,
			//layout: 'fit',
			items: UserMgt_CreateForm,
			monitorValid:true,
			buttons: [{
				text: 'Submit',
				formBind:true,
				handler: function(){
					if(UserMgt_isFormValid()){
						validate();
					}
				}
			},{
				text: 'Cancel',
				handler: function(){
					UserMgt_CreateWindow.hide();
					UserMgt_resetForm();
					UserMgt_CreateWindow.getEl().scrollTo('top',0);
					UserMgt_CreateWindow.getEl().scrollTo('left',0);
					UserMgt_resetForm();
					Ext.getCmp('UserMgt_SpecifyField').setVisible(false);
				}
			}]
		});
	}
	else
	{
		UserMgt_CreateForm = new Ext.FormPanel({
			//labelAlign: 'top',
			autoScroll:true,
			bodyStyle:'padding:5px',
			//title: 'Sherman Library Card Application',
			//renderTo: Ext.get('kevinhere'),
			monitorValid:true,
			//layoutConfig: {columns: 2},
			frame: true,
			//width: 610,
			items: [{
				//layout:'column',
				border:false,
				items:[
						UserMgt_Text,
						{
							layout:'column',
							border:false,
							items: [{
								labelWidth :150,
								columnWidth:.4,
								layout: 'form',
								items: [
									UserMgt_FirstNameField,
									UserMgt_MIField,
									UserMgt_LastNameField,
									UserMgt_StreetAddressField,
									UserMgt_AptField,
									UserMgt_CityField,
									UserMgt_StateField,
									UserMgt_ZIPField
								]
							},{
									labelWidth :200,
									columnWidth:.6,
									layout: 'form',
									items: [
										UserMgt_PhoneField,
										UserMgt_BirthdateField,
										UserMgt_GenderField,
										UserMgt_EmailField,
										UserMgt_HearField,
										UserMgt_SpecifyField,
										UserMgt_ContactedField
								  ]
							}]
						}
					]
			}],
			buttons: [{
				text: 'Submit',
				formBind:true,
				handler: function(){
					if(UserMgt_isFormValid()){
						validate();
					}
				}
			},{
				text: 'Cancel',
				handler: function(){
					//getTime();
					UserMgt_CreateWindow.hide();
					UserMgt_resetForm();
					UserMgt_CreateWindow.getEl().dom.scrollTop;
					UserMgt_CreateWindow.getEl().dom.scrollLeft;
					UserMgt_resetForm();
					Ext.getCmp('UserMgt_SpecifyField').setVisible(false);

					//var tt = store.getCount()
					//Ext.MessageBox.alert('Message', tt); 
	
				}
			}]
		});
		
		// This was added in tutorial 5
		UserMgt_CreateWindow= new Ext.Window({
			id: 'UserMgt_CreateWindow',
			//bodyStyle: 'padding:15px;background:transparent',
			title: 'Sherman Library Card Application.',
			closable:false,
			autoScroll:true,
			width: 850,
			height: 560,
			modal:true,
			//plain:true,
			layout: 'fit',
			items: UserMgt_CreateForm
		});
	}
	
	//Ext.getCmp('UserMgt_ZIPField').setValue('33');
	UserMgt_CreateWindow.show();
	UserMgt_CreateWindow.hide();
	//store.load({params:{start:0, limit:25}});
	//store.load();
});


/*

var viewportwidth;
var viewportheight;

 // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight

 if (typeof window.innerWidth != 'undefined')
 {
      viewportwidth = window.innerWidth,
      viewportheight = window.innerHeight
 }

// IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)

 else if (typeof document.documentElement != 'undefined'
     && typeof document.documentElement.clientWidth !=
     'undefined' && document.documentElement.clientWidth != 0)
 {
       viewportwidth = document.documentElement.clientWidth,
       viewportheight = document.documentElement.clientHeight
 }

 // older versions of IE

 else
 {
       viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
       viewportheight = document.getElementsByTagName('body')[0].clientHeight
 }






function get_cookie(Name) {
var search = Name + "="
var returnvalue = "";
if (document.cookie.length > 0) {
offset = document.cookie.indexOf(search)
// if cookie exists
if (offset != -1) {
offset += search.length
// set index of beginning of value
end = document.cookie.indexOf(";", offset);
// set index of end of cookie value
if (end == -1) end = document.cookie.length;
returnvalue=unescape(document.cookie.substring(offset, end))
}
}
return returnvalue;
}



document.cookie="viewportwidth="+viewportwidth;


function skipMobileSite() {
  // Skip mobile site with a cookie setting, no expiration date
  document.cookie = 'occSkipMobile=true; path=/';
}

function allowMobileSite() {
  // Just delete cookie by setting it to past date
  document.cookie = 'occSkipMobile=true; expires=Fri, 3 Aug 2001 20:47:11 UTC; path=/';
}
*/