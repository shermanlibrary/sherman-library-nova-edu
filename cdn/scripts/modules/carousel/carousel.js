var carousel = ( function () {

  var el, container, next, pos = 0, prev;

  function toggleClasses() {
    el.classList.add( 'carousel__container--js' );
    if (el.scrollWidth > el.clientWidth ) {
      el.classList.add( 'carousel__container--is-overflown');
    }

 }

  function addEventListeners() {
    next.addEventListener( 'click', scroll, false );
    prev.addEventListener( 'click', scroll, false );
  }

  function init( element ) {

    el = element.querySelector( '.carousel__container' );
    next = element.querySelector( '.carousel__button--next' );
    prev = element.querySelector( '.carousel__button--prev' );

    toggleClasses();
    addEventListeners();
  }

  function isOverflown() {
    if ( el.clientWidth < el.scrollWidth ) {
      el.classList.add( 'carousel__container--is-overflown' );
    }
  }

  function scroll() {
    var difference = el.scrollWidth - el.clientWidth;
    var perTick = difference / 400 * 10;

    if ( this.classList.contains( 'carousel__button--next' ) ) {
      pos += el.clientWidth;
    } else {
      pos -= el.clientWidth;
    }

    require( ['jquery'], function() {
        jQuery( el ).animate({ scrollLeft : pos });
    });

  }

  return {
    init : init
  }

})();

console.log( 'here' );
carousel.init( document.querySelector( '.carousel' ) );
