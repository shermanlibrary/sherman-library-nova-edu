(function(){
    'use strict';
    var app = angular.module('hpdApp', []);
})();

//Data Service
(function() {
    'use strict';

    var dataService = function($http, $q) {
        this.$http = $http;
        this.$q = $q;
    };

    dataService.$inject = ['$http', '$q'];

    dataService.prototype.getTitles  = function( query ) {
        var _this = this;
        return _this.$http.get('http://sherman.library.nova.edu/ftf/endpoint.php?query='+query )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    angular.module('hpdApp')
        .service('dataService', dataService);

}());

//FTF Controller
(function(){
    'use strict';
    var ftfController = function(  $timeout, dataService ){
        var vm = this;
        vm.selected = false;
        vm.filterTitle = 'startsWith';	
		vm.metadata = null;

        var getTitles = function (){        
            if(vm.model !== undefined) {			
                vm.model = vm.model.replace(/\s\s+/g, ' ');
                vm.model = vm.model.replace('.', ' ');
            }
   

            dataService.getTitles(vm.model).then(function(data){
                vm.items = data;
            });
        };

        var startsWith = function (actual, expected) {
            var lowerStr = (actual + "").toLowerCase();
            return lowerStr.indexOf(expected.toLowerCase()) === 0;
        };

        var setCurrent = function(index){			
            vm.model = index.title;
            vm.selected = true;
            vm.metadata = index;         
        };


		
        vm.getTitles    = getTitles;
        vm.startsWith   = startsWith;
        vm.setCurrent   = setCurrent;

    };

    ftfController .$inject = ['$timeout', 'dataService' ];
    angular.module('hpdApp')
        .controller('ftfController', ftfController);
})();
