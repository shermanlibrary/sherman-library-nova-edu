/*------------------------------------*\
  Public Library Services
  - The publicServices module used
  - on public.library.nova.edu
\*------------------------------------*/
var app = angular.module('publicServices', [ 'ngCookies', 'ngSanitize' ]);

//Data Service
(function() {
    'use strict';

  var dataService = function($http, $q) {
      this.$http = $http;
      this.$q = $q;
  };

  dataService.$inject = ['$http', '$q'];

  dataService.prototype.getApplication  = function( pid ) {
      var _this = this;
      return _this.$http.get('//sherman.library.nova.edu/api/novapoint.php?job=novaCatApplication&pid='+pid)
          .then(function(response){
              if (typeof response.data === 'object') {
                  return response.data;
              } else {
                  return response.data;
              }
          }, function(response) {

              return _this.$q.reject(response.data);
          })
  };

  dataService.prototype.getEvents  = function( audience ) {
      var _this = this;
      var audience = typeof audience !== 'undefined' ? audience : 'all';

      return _this.$http.jsonp('//sherman.library.nova.edu/sites/spotlight/wp-json/events/v2/upcoming/?audience=' + audience + '&_jsonp=JSON_CALLBACK' )
        .then( function (response) {
          if (typeof response.data === 'object') {
              return response.data;
          } else {
              return response.data;
          }
        }, function(response) {
            return _this.$q.reject(response.data);
        })
  };

  dataService.prototype.getList  = function( slug ) {
      var _this = this;

      return _this.$http.get('//sherman.library.nova.edu/sites/spotlight/list/' + slug + '/?json=true' )
          .then(function(response){
              if (typeof response.data === 'object') {
                  return response.data;
              } else {
                  return response.data;
              }

          }, function(response) {
              return _this.$q.reject(response.data);
          });
  };

  dataService.prototype.getNewestList  = function( slug ) {
      var _this = this;

      return _this.$http.jsonp('//sherman.library.nova.edu/sites/spotlight/wp-json/lists/v2/list/?_jsonp=JSON_CALLBACK' )
          .then(function(response){
              if (typeof response.data === 'object') {
                  return response.data;
              } else {
                  return response.data;
              }

          }, function(response) {
              return _this.$q.reject(response.data);
          });
  };

  dataService.prototype.getFeatures  = function( audience ) {
      var _this = this;
      var audience = typeof audience !== 'undefined' ? audience : 'all';

      return _this.$http.jsonp('//sherman.library.nova.edu/sites/wp-json/features/v2/ads/?audience=' + audience + '&_jsonp=JSON_CALLBACK' )
          .then(function(response){
              if (typeof response.data === 'object') {
                  return response.data;
              } else {
                  return response.data;
              }

          }, function(response) {
              return _this.$q.reject(response.data);
          })
  };

  angular.module('publicServices')
      .service('dataService', dataService);

}());

(function(){

    'use strict';

    var ApplicationController = function( $rootScope, $window, $cookies, dataService ){

        var vm = this;
        vm.hours = '';

        if ($cookies.get( 'pid' )){
      		vm.pid = $cookies.get( 'pid' );
      	} else {
      		vm.pid			= 'public';
      	}

        vm.col = 'n';
        vm.libraries = [
            { code: 'main', label: 'Alvin Sherman Library'},
            { code: 'ocean', label: 'Oceanographic Center Library'}
        ];

        function getApplication(){
            dataService.getApplication( vm.pid ).then(function(data){
                if(data) {
                    vm.status = data.response.AppStatus;

                    if(vm.status !== 'public'){
                        vm.info             = data.response.info;
                        vm.email_update     = data.response.info.email;
                        vm.activity         = data.response.activity;
                        vm.col              = vm.info.col;
                        vm.selectedLibrary =  vm.info.home_lib;
                    }
                }
            });
        }
        getApplication();

        var userMenu = (function() {

          $rootScope.userMenuToggle = false;

          function toggle() {

            $rootScope.userMenuToggle = !$rootScope.userMenuToggle;
          }

          return {
            toggle: toggle
          };

        })();

        var librariesMenu = (function() {

          $rootScope.librariesMenuToggle = false;

          function toggle() {

            $rootScope.librariesMenuToggle = !$rootScope.librariesMenuToggle;
          }

          return {
            toggle: toggle
          };

        })();
        $rootScope.librariesMenu = librariesMenu;
        $rootScope.userMenu = userMenu;

    };

    ApplicationController.$inject = [ '$rootScope', '$window', '$cookies', 'dataService' ];

    angular.module('publicServices')
        .controller('ApplicationController', ApplicationController);

})();


(function(){

    'use strict';

    var AdController = function( $attrs, dataService ) {

      var vm    = this;
      vm.audience = $attrs.audience;

      /**
       * Uses dataService.prototype.getFeatures - be sure to remove if this controller is removed too
       */
      function  getFeatures( audience ){

          dataService.getFeatures( audience ).then(function(data){
              if( data ) {
                vm.ads = data;
              }
          });
      }

      getFeatures( vm.audience );

    }

    AdController.$inject = [ '$attrs', 'dataService'];
    angular.module('publicServices').controller('AdController', AdController);

})();

(function(){

    'use strict';

    var EventController = function( $attrs, dataService ) {

      var vm = this;
          vm.audience = $attrs.audience;
          vm.events = '';

      /**
       * Uses dataService.prototype.getFeatures - be sure to remove if this controller is removed too
       */
      function  getEvents( audience ){
        dataService.getEvents( audience ).then(function(data){
          if ( data ) {
            vm.events = data;
          }
        });
      }

      getEvents( vm.audience );

    }

    EventController.$inject = [ '$attrs', 'dataService' ];
    angular.module('publicServices').controller('EventController', EventController);

})();

(function(){

    'use strict';

    var ListController = function( $attrs, dataService ) {

      var vm        = this;
          vm.slug   = ( $attrs.slug  ? $attrs.slug : '' );

          function  getList( slug ){

              dataService.getList( slug ).then(function(data){

                  if( data ) {
                    vm.list = data;
                  }

              }).then(function() {
                initializeCarousel();
              });

          }

          function  getNewestList(){

              dataService.getNewestList().then(function(data){
                  if( data ) {
                    vm.lists = data;
                  }
              });
          }

      if ( vm.slug !== '' ) {
        function loadCarousel() {
          var script = document.createElement("script");
          script.type = "text/javascript";
          script.id = 'carousel-js'
          script.src = "//sherman.library.nova.edu/cdn/scripts/modules/carousel/carousel.js";
          document.querySelector("body").appendChild(script);
        }

        function initializeCarousel() {
          if ( !document.querySelector( 'script#carousel-js' ) ) {
            loadCarousel();
          }
        }

        getList( vm.slug );
      }

      else {
        getNewestList();
      }

    }

    ListController.$inject = [ '$attrs', 'dataService' ];
    angular.module('publicServices').controller('ListController', ListController);

})();

(function() {

  angular.module( 'publicServices' ).filter( 'trusted', ['$sce', function( $sce ) {
    return function( url ) {
      return $sce.trustAsResourceUrl( url );
    }
  }]);

})();
