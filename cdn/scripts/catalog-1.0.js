/*------------------------------------*\
 #Config
 \*------------------------------------*/
requirejs.config({

    /**
     * Define paths to common scripts.
     */
    paths : {
        accordion         : '//sherman.library.nova.edu/cdn/scripts/min/o--accordion-1.0.min',
        advancedSearch    : '//sherman.library.nova.edu/cdn/scripts/min/o--advanced-search-2.0.min',
        jquery            : '//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min',
        loadCSS           : '//sherman.library.nova.edu/cdn/scripts/min/loadcss.min',
        searchResults     : '//systemsdev.library.nova.edu/cdn/scripts/min/s--nc-search-results',
        tabs              : '//sherman.library.nova.edu/cdn/scripts/min/o--tabs-1.0.min',
        waypoints         : 'https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.0/noframework.waypoints.min'
    },

    /**
     * If any of the paths above have depencies, e.g., jQuery,
     * let's create a shim.
     */
    shim : {

        'advancedSearch' : {
            deps: [ 'jquery' ]
        },

        'searchResults' : {
            deps: [ 'jquery' ]
        },

        'tabs' : {
            deps: [ 'jquery' ]
        }

    }

});

/*------------------------------------*\
 #Conditions
 \*------------------------------------*/
var parameters      = location.search,
    pathname        = location.pathname;

if ( document.querySelector( '#advanced-search' ) ) { require( ['advancedSearch' ] ); }
if (
    document.querySelector( 'form[name="searchtool"]' ) ||
    document.querySelector( '.browseSearchtool > form' ) ||
    pathname.indexOf( '/ftlist' ) > -1 ) { require( ['searchResults'] ); }
if ( document.querySelector( '.tabs') ) { require( [ 'tabs' ] ); }

/**
 * For lists of saved records and related screens,
 * require ['searchResults'] -- because NovaCat returns a
 * an index list -- and require ['loadCSS'] to load a specific
 * stylesheet.
 */
if ( document.querySelector( 'form[action*="++export"]' ) ) {
    require( ['searchResults', 'loadCSS'],
        function() {
            loadCSS( '//sherman.library.nova.edu/cdn/styles/css/public-global/s--catalog-list.css');
        });
}


/*------------------------------------*\
 #Classes
 \*------------------------------------*/

var browseList      = ( document.querySelector( 'table.browseList' ) ? document.querySelector( 'table.browseList' ) : false ),
    message         = ( document.querySelector( 'tr.msg' ) ? document.querySelector( 'tr.msg' ) : false );

if ( browseList ) {
    if (
        parameters.indexOf( 'sortdropdown=-' ) >-1 ||
        !parameters.indexOf( 'sortdropdown' ) > -1 ||
        parameters.indexOf( '/fB' ) >-1 ||
        parameters.indexOf( 'fJ+' ) >-1 ||
        parameters.indexOf( '?/t' ) >-1 ||
        parameters.indexOf( '?/c' ) >-1 ||
        parameters.indexOf( '?/a' ) >-1 ||
        parameters.indexOf( '?/fV' ) >-1 ) {
        browseList.classList.add( 'is-index' );
    }

    if ( message && message.textContent.indexOf( 'No matches found' ) > -1 ) {
        document.querySelector( 'table.browseScreen' ).classList.add( 'js-no-matches' );
    }

    if ( parameters.indexOf( '/++export' ) >-1 ) {
        browseList.classList.add( 'is-list' );
    }

} else if ( parameters.indexOf( '/marc' ) >-1 ) {
    document.querySelector( 'div[align="left"]').classList.add( 'col-md--eightcol', 'col--centered');
    document.querySelector( 'table').classList.add( 'col-md--eightcol', 'col--centered');
}

/*------------------------------------*\
 #Accessibility
 \*------------------------------------*/


/*------------------------------------*\
 #Utilities
 \*------------------------------------*/
function $$(selector, context) {
    context = context || document;
    var elements = context.querySelectorAll(selector);
    return Array.prototype.slice.call(elements);
}

function open_new_window( new_URL ) {
    var w = (window.open(new_URL, 'patwin', 'width=550,height=550,status=yes,scrollbars=yes,resizable'));
    w.focus();
    return false;
}

/*------------------------------------*\
 Header
 \*------------------------------------*/

/**
 * Update the title of the page to repreresent wayfinding.
 */
var pageTitle = document.querySelector( '.pill-menu__title' );

if ( location.pathname.indexOf( 'search' ) !== -1 ) {

    var pageSubtitle = document.createTextNode( '— Search Results' );

    pageTitle.appendChild( pageSubtitle );

}

if ( location.pathname.indexOf( 'patroninfo' ) !== -1 ) {

    var pageSubtitle = document.createTextNode( '— My Library Account' );

    pageTitle.appendChild( pageSubtitle );

}

/*------------------------------------*\
 Feedback Bar
 \*------------------------------------*/
var reporter = document.querySelector( 'form[name="reporter"]' );
var footerReporter = document.querySelector( 'form#footerReporter' );
var reporter__url = reporter.querySelector( 'input[id="reported_url"]');
var reporter__url1 = footerReporter.querySelector( 'input[id="reported_url1"]');
var reporter__bib = reporter.querySelector( 'input[id="reported_biburl"]' );
var reporter__bib1 = footerReporter.querySelector( 'input[id="reported_biburl1"]' );
var record__permanentLink = document.querySelector( '#content a.link[href*="/record"]' );

reporter__url.setAttribute( 'value', location.href );
reporter__url1.setAttribute( 'value', location.href );

if ( record__permanentLink ) {
    reporter__bib.setAttribute( 'value', record__permanentLink.href );
    reporter__bib1.setAttribute( 'value', record__permanentLink.href );
}
