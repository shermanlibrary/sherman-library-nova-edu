/* ==================
 * Get Spotlight
 * ================== */
get_spotlight = function( $size ) {

    $('[data-spotlight]').each(function() {

        var el                  =   $(this),
            audience            =   el.data( 'audience' ),
            markup              =   '',
            number              =   el.data( 'count' ),
            postNo              =   el.data('post'),
            type                =   el.data('spotlight'),
            template            =   el.data( 'template' ),

            // placeholders
            api, baseAPI, category;

        baseAPI = '//sherman.library.nova.edu/sites/spotlight/api/taxonomy/get_taxonomy_posts/?taxonomy=library-audience&callback=?';

        // Who is the audience?
        baseAPI = baseAPI + '&slug=' + ( audience ? audience : 'public' ) + ( number ? '&count=' + number : '' );

        if ( type == 'database' ) {

            api = baseAPI + '&post_type=spotlight_databases';

        } 

        else if ( type == 'event' || type == 'feature' || type == 'bricks' ) {
            api = baseAPI + '&post_type=spotlight_events';
        }

        else if ( type == 'librarylearn' ) {
            api = baseAPI + '&post_type=academy_video';
      	}

        else if ( type == 'list' || type == 'picks' ) {
            api = baseAPI + '&post_type=list';
        }

        $.getJSON( api )

            .success( function( response ) {

                var count = 0;

                $.each( response.posts, function( i, post ) {

                    if ( type == 'bricks') {

                        var end = ( post.custom_fields['event_end'] ) ? post.custom_fields['event_end'][0] : post.custom_fields['event_start'][0];
                            end = end.replace(/(\d{4})(\d{2})(\d{2})/, '$1-$2-$3'),
                            end = Date.parse( end ),
                            now = new Date();

                        if ( post.custom_fields['is_feature'][0] == 'no' && post.custom_fields['date_text'] && end > now && post.thumbnail_images ) {

                            var $size = ( responsive_viewport >= 720 ) ? ( (responsive_viewport >= 1024) ? ( responsive_viewport > 1140 ? 'full' : 'media-large' ) : 'media-medium' ) : 'media-small';
                            var excerpt = post.excerpt.replace(/(<([^>]+)>)/ig,""),
                                excerpt = excerpt.substring( 0, 275 ),
                                date    = post.custom_fields['date_text'],
                                poster  = post.thumbnail_images[ $size ]['url'],
                                color   = post.custom_fields['sp_card_background_color'][0],
                                link    = ( !post.custom_fields['overlay_link'][0] ) ? post.url : post.custom_fields['overlay_link'];         

                            count++;    

                            var brick   = ( count == 1 ? 'brick--ninecol' : 'brick' );

                            markup +=
                            '<div class="' + brick + '">' +
                                '<a href="' + link + '">' +
                                '<article class="brick__post clearfix has-background-image" style="background-image: url(' + poster + ');">' +
                                    '<header class="brick__header"' + ( color ? 'style="background-color:' + color + ';"' : '' ) + '>' +
                                        '<h3 class="brick__title" rel="' + ( post.taxonomy_series[0] ? post.taxonomy_series[0]['title'] : post.taxonomy_event_type[0]['title'] ) + '">' + ( post.custom_fields['overlay_title'][0] ? post.custom-fields['overlay_title'][0] : post.title ) + '</h3>' +
                                        '<p>' + date + '</p>' +
                                    '</header>' +
                                '</article>' + 
                                '</a>' +
                            '</div>';

                        }

                    }

                    if ( type == 'feature' ) {

                        count++;
                        var random = Math.round(Math.random() * ( count - 1 ) + 1);

                        if ( count == random ) {

                            var end = ( post.custom_fields['event_end'] ) ? post.custom_fields['event_end'][0] : post.custom_fields['event_start'][0];
                                end = end.replace(/(\d{4})(\d{2})(\d{2})/, '$1-$2-$3'),
                                end = Date.parse( end ),
                                now = new Date();

                            var $size = ( responsive_viewport >= 720 ) ? ( (responsive_viewport >= 1024) ? ( responsive_viewport > 1140 ? 'full' : 'media-large' ) : 'media-medium' ) : 'media-small';                                

                           if ( post.custom_fields['is_feature'][0] == 'yes' && post.custom_fields['date_text'] && end > now ) {

                                var excerpt 	= post.excerpt.replace(/(<([^>]+)>)/ig,""),
                                    excerpt 	= excerpt.substring( 0, 275 ),
                                    eventType 	= post.taxonomy_event_type[0],
                                    date    	= post.custom_fields['date_text'],
                                    link    	= ( !post.custom_fields['overlay_link'][0] ) ? post.url : post.custom_fields['overlay_link'],
                                    button  	= ( !post.custom_fields['overlay_button_text'][0] ) ? 'Read More' : post.custom_fields['overlay_button_text'][0],
                                    backgroundColor = ( !post.custom_fields['sp_card_background_color'][0] ) ? 'base' : post.custom_fields['sp_card_background_color'][0],
                                    buttonColor = ( !post.custom_fields['sp_card_button_color'][0] ) ? 'green' : post.custom_fields['sp_card_button_color'][0];                                    

                                markup = 
                                    '<section class="background-' + backgroundColor + ' has-background has-background-image hero hero-event hero-event_large clearfix" style="background-image: url(' + post.thumbnail_images[ $size ]['url'] + ');">' +

                                        '<article class="card clearfix" itemscope itemtype="http://schema.org/Event">' +
                                            
                                            '<div class="wrap clearfix">' +

                                            '<div class="align-center media thumbnail">' +
                                                '<img src=' + post.thumbnail_images[ 'media-medium' ]['url'] + '>' +  
                                            '</div>' +

                                           	'<div class="information">' +

                                                '<header>' +
                                                    '<a href="' + post.url + '" itemprop="url">' +
                                                        '<h3 class="title" itemprop="name">' + post.custom_fields['overlay_title'][0] + '</h3>' +
                                                    '</a>' +
                                                '</header>' +

                                                '<div class="has-excerpt">' +
                                                    '<p class="epsilon excerpt" itemprop="description">' + excerpt + ' | <a href="' + link + '">' + button + '</a>' + '</p>' +
                                                '</div>' +

                                            '</div>' +

                                 			
                                            '</div>' +

                                        '</article>' +

                                    '</section>';
                
                            }

                        } // if count is random
                    }

                    else if ( type == 'event' ) {

                        var end = ( post.custom_fields['event_end'] ) ? post.custom_fields['event_end'][0] : post.custom_fields['event_start'][0];
                            end = end.replace(/(\d{4})(\d{2})(\d{2})/, '$1-$2-$3'),
                            end = Date.parse( end ),
                            now = new Date();

                       if ( post.custom_fields['is_feature'][0] == 'no' && post.custom_fields['date_text'] && end > now ) {

                            var excerpt = post.excerpt.replace(/(<([^>]+)>)/ig,""),
                                excerpt = excerpt.substring( 0, 275 ),
                                date    = post.custom_fields['date_text'];  

                            count++;                                  

                            markup = 
                            '<section class="assorted-features background-base has-background hero clearfix">' + 
                                '<div class="feature-event wrap clearfix">' +
                                    
                                    '<article class="card" itemscope itemtype="http://schema.org/Event">' +                           
                                    
                                    '<div class="fourcol first">' +
                                        '<a href="' + post.url + '"><img src="' + post.thumbnail_images['media-small']['url'] + '" alt="' + post.title_plain + '"></a>' +                                        

                                    '</div>' +

                                    '<div class="eightcol last">' +

                                        '<header>' +
                                            '<a href="' + post.url + '" itemprop="url">' +
                                                '<h3 class="emphasis no-margin" itemprop="name">' + post.custom_fields['overlay_title'][0] + '</h3>' +
                                            '</a>' +
                                            '<time class="delta">' +
                                                '<span itemprop="startDate">' +
                                                    date + 
                                                '</span>' +
                                            '</time>' +
                                        '</header>' +

                                       '<p class="epsilon excerpt">' + excerpt + '</p>' +
                                    '</div>' +

                                    '</article>' +
                                '</div>' + 
                            '</section>';
            
                        }

                    }

                    else if ( type == 'librarylearn' ) {

                       if ( template != 'feature' ) { 

                           if ( post.custom_fields['is_feature'][0] == 'no' ) {

                                var excerpt = post.excerpt.replace(/(<([^>]+)>)/ig,""),
                                    excerpt = excerpt.substring( 0, 275 );  

                                count++;                                  

                                markup = 
                                '<section class="background-base has-background hero clearfix">' + 
                                        
                                    '<article class="card wrap" role="article">' +                           
                                    
                                    '<div class="twocol first media">' +
                                        '<a class="thumbnail" href="' + post.url + '"><img src="' + post.thumbnail_images['media-small']['url'] + '" alt="' + post.title_plain + '"></a>' +                                        
                                    '</div>' +

                                    '<div class="tencol last">' +
                                        '<header>' +
                                            '<a href="' + post.url + '" itemprop="url">' +
                                                '<h3 class="no-margin" itemprop="name">' + ( post.custom_fields['overlay_title'][0] ? post.custom_fields['overlay_title'][0] : post.title ) + '</h3>' +
                                            '</a>' +
                                        '</header>' +
                                        '<section class="post-content clearfix">' +
                                       		'<p class="epsilon excerpt">' + excerpt + '</p>' +
                                       	'</section>' +
                                    '</div>' +

                                    '</article>' +

                                '</section>';
                
                            }

                        }

                        else {

                           if ( post.custom_fields['is_feature'][0] == 'yes' ) {

                                var excerpt = post.excerpt.replace(/(<([^>]+)>)/ig,""),
                                    excerpt = excerpt.substring( 0, 275 );  

                                count++;                                  

                                markup = 
                                '<section class="feature">' +

                                    '<div class="feature-event video" style="background-image: url(' + post.thumbnail_images['media-medium']['url'] + ');">';

                                    if ( responsive_viewport >= 1024 ) {

                                    markup +=
                                        '<video controls poster="' + post.thumbnail_images['media-large']['url'] + '" height="100%" width="100%">' +
                                            '<source type=\'video/webm; codecs="vp8, vorbis"\' src="//nova.edu/library/video/' +  post.custom_fields['academy_video_file'][0] + '.webm" >' +
                                            '<source type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\' src="//nova.edu/library/video/' +  post.custom_fields['academy_video_file'][0] + '.mp4" >' +
                                        '</video>';
                                    }

                                    markup+=
                                        '<article class="card" itemscope itemtype="http://schema.org/Event">' +   
                                            '<header><a href="' + post.url + '">' +
                                                '<h3 class="beta title no-margin" itemprop="name">' + ( post.custom_fields['overlay_title'][0] ? post.custom_fields['overlay_title'][0] : post.title ) + '</h3>' +
                                            '</a></header>' +

                                            '<div class="has-excerpt">' +
                                                '<p class="epsilon excerpt">' + excerpt + '</p>' +
                                            '</div>' +
                                        
                                            '<a class="button coral" href="' + post.url + '">Watch</a>' +

                                        '</article>' +
                                    '</div>' + 
                                '</section>';
                
                            }

                        }

                    }

                    else if ( type == 'database' ) {

                        count++;
                        var random = Math.round(Math.random() * ( count - 1 ) + 1);

                        if ( template == 'feature' && count == random ) {

                            var excerpt = post.excerpt,
                                excerpt = excerpt.replace(/(<([^>]+)>)/ig,"");

                            markup =
                            '<section class="hero has-background background-base clearfix">' +
                                '<div class="wrap">' +
                                    '<div class="fourcol first media">' +
                                        '<img class="shadow" src="' + post.thumbnail_images['media-medium']['url'] + '">' +
                                    '</div>' +

                                    '<div class="eightcol last">' +
                                        '<header>' +                                            
                                            '<h3 class="title">' + post.title + '</h3>' +
                                        '</header>' +
                                        '<p>' + excerpt + '</p>' +
                                    '</div>' +
                                '</div>' +
                            '</section>';
                        }

                    }

                    else if ( type == 'list' ) {

                        count++;
                        var random = Math.round(Math.random() * ( count - 1 ) + 1);

                        if ( count == random ) {

                            markup = 
                            '<article class="hero wrap clearfix" role="article">' +

                                '<div class="tencol last">' +
                                    '<span class="epsilon">We put together a list ...</span>' +
                                    '<h3 class="delta">' +
                                        '<a href="' + post.url + '">' + post.title + '</a>' +
                                    '</h3>' +
                                    '<p class="no-margin">' +
                                        post.content +
                                    '</p>' +                                 

                                '</div>' +

                               '<div class="twocol first media">' + 
                                   '<a class="record-link thumbnail" href="//novacat.nova.edu/record=' + post.custom_fields['list_feature_brecord'][0] + '">' +
                                        '<span id="book-hook" class="gbs-thumbnail-large shadow" title="ISBN:' + post.custom_fields['list_feature_isbn'][0] + '"></span>' + 
                                    '</a>' + 
                                '</div>' + 
                            '</article>';

                        } // if count == random

                        $.when(

                            $.getScript( '//novacat.nova.edu/screens/majax.js' ),
                            $.getScript( '//sherman.library.nova.edu/cdn/scripts/libs/min/google-books-classes.min.js')

                        ).done();
                    } // if type == list

                    else if ( type == 'picks' ) {

                        markup += 
                            '<li class="brick__book">' + 
                                '<a class="brick__book__cover" href="//novacat.nova.edu/record=' + post.custom_fields['list_feature_brecord'][0] + '">' +
                                    '<span id="book-hook" class="gbs-thumbnail-large shadow" title="ISBN:' + post.custom_fields['list_feature_isbn'][0] + '"></span>' + 
                                '</a>' +
                            '</li>';

                        $.when( // Check if these exist first

                            $.getScript( '//sherman.library.nova.edu/cdn/scripts/libs/min/google-books-classes.min.js')

                        ).done();
                    } // if type == picks

                });

                el.replaceWith( markup );

            });
    });
}
