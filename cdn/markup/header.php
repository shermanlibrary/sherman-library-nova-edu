<!-- Header
======================
-->	<header class="header tinsley-gradient" role="banner">
		<div id="inner-header" class="wrap clearfix">
			
			<a class="nsu hide-text" href="http://www.nova.edu" title="Nova Southeastern University">Nova Southeastern University</a>
			<a href="http://www.nova.edu/library/main" title="Alvin Sherman Library, Research, and Information Technology Center" class="hide-text">
				<h1 id="logo">
					Alvin Sherman Library, Research and Information Technology Center
				</h1>
			</a>

			<div class="utilities">
				<form action="http://sharksearch.nova.edu/search" class="search-container" id="aslritc-search" method="get">

					<input id="searchbox" name="q" type="text" placeholder="Search the Library's Website" x-webkit-speech speech/>
					<input type="hidden" name="client" value="aslritc" />
					<input type="hidden" name="proxystylesheet" value="main" />
					<input type="hidden" name="output" value="xml_no_dtd" />
					<input type="hidden" name="access" value="p" />
					<input type="hidden" name="ie" value="UTF-8" />
					<input type="hidden" name="oe" value="UTF-8" />
					<input type="hidden" name="site" value="aslritc" id="sopswap" />

					<a class="gradient--vertical button" onclick="document.getElementById('aslritc-search').submit();" type="submit">
						<span class="icon-search" aria-hidden="true"></span>
					</a>
				</form>

				<div class="lib-button-small gradient--vertical"> 
					<a class="has-subnav icon-link" href="http://novacat.nova.edu/patroninfo" title="My Library Account"></a>
					<ul class="sub-menu">
						<li><span class="h3">Quick Links</span></li>
						<li class="icon-link">
							<a class="link" href="http://sharklink.nova.edu/" target="new">Blackboard</a>
						</li>
						<li class="icon-link">
							<a class="link" href="http://www.nova.edu/emergency/" target="new">Emergency Alerts</a>
						</li>
						<li class="icon-link">
							<a class="link" href="http://sharklink.nova.edu/" target="new">SharkLink</a>
						</li>
						<li class="icon-link">
							<a class="link" href="http://webstar.nova.edu/" target="new">WebSTAR</a>
						</li>
					</ul>
				</div> 

				<div class="lib-button-small gradient--orange" title="Contact Us">
					<a class="has-subnav icon-bubbles" href="http://www.nova.edu/library/main/feedback.html" title="Give in Touch">
						Contact Us
					</a>					
					<ul class="sub-menu">

					<!-- Feedback Form
					======================
					-->	<li>
							<form action="http://systems.library.nova.edu/form/embed.php?#main_body" id="form_49" method="post">
								<ol>
									
								<!-- Comment
								======================
								-->	<li>										
										<textarea id="element_3" name="element_3" placeholder="It would be great if ..." required></textarea>
									</li>

								<!-- Information
								======================
								-->	<li>
										<label for="element_1">Your Name</label>
										<input type="text" id="element_1"name="element_1" placeholder="Jane Doe" required>
									</li>

								<!-- Email
								======================
								-->	<li>
										<label for="element_2">Email</label>
										<input type="email" id="element_2" name="element_2" required>
									</li>
									
									<li>
										<input type="hidden" name="form_id" value="49">
										<input type="hidden" name="submit" value="1">
										<input id="saveForm" name="submit" onclick="" type="submit">
									</li>
								</ol>
							</form>
						</li>

					<!-- Call
					======================
					-->	<li><span class="h3 icon-mobile"> Phone Numbers</span>
							<ul>								
								<li class="icon-library">
									<b>General Info:</b> <a href="tel:19542624600" title="Call for general information">(954) 262-4613</a>
								</li>
								
								<li class="icon-help">
									<b>Reference:</b> <a href="tel:19542624613" title="Call the reference desk.">(954) 262-4613</a>
								</li>

								<li class="icon-book">
									<b>Public Desk:</b> <a href="tel:19542625477" title="Call the public desk.">(954) 262-5477</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
				
			</div><!--/.utilities-->
		</div><!--/#inner-header-->
	</header>	