<!-- Navigation
======================
--> <nav class="menu gradient--rtl" role="navigation">
		
		<a class="menu-link icon-grid-view" href="#">Menu</a>
		<ul>
			<li class="icon-home"><a href="http://nova.edu/library/main">Home</a></li>

			<li class="icon-user">
				<a class="link">Patron Portals</a>
				<ul class="sub-menu">
					<li> 
						<p style="color: #444; padding: .5em; line-height: 1; margin: 0 auto;">
							We're making things you care about easier to find. 
						</p>
					</li>
					<li><a class="link" href="http://nova.edu/library/main/local-students.html">Local Students</a></li>
					<li><a class="link" href="http://nova.edu/library/main/distance-students.html">Distance Students</a></li>
					<li><a class="link" href="http://nova.edu/library/main/faculty-and-staff.html">Faculty and Staff</a></li>
					<li><a class="link" href="http://nova.edu/library/main/alumni.html">Alumni</a></li>
				
					<li><a class="link" href="http://nova.edu/library/main/public.html">Broward County Patrons</a></li>

				</ul>
			</li>

			<li class="icon-rocket">
				<a class="link">Electronic Resources</a>
				<ul class="sub-menu">
					<li class="has-subnav"> 
						<a href="#parent" rel="nofollow">Databases</a> 
						<ul class="children">
							<a href="http://sherman.library.nova.edu/e-library/index.php" class="link" title="E-Library Resources by Subject">Databases by Subject</a>
							<a href="http://sherman.library.nova.edu/e-library/index.php?action=all&col=n" class="link" title="A Complete Alphabetical List of Databases"> Complete A-Z List </a>
						</ul>
					</li>
					<li> <a href="http://atoz.ebsco.com/Search.asp?id=nseu" class="link">Journal Finder</a> </li>
					<li> <a href="//novacat.nova.edu/screens/opacmenu.html" class="link">Library Catalog ("NovaCat")</a> </li>
					<li> <a href="http://www.nova.edu/library/main/other-catalogs.html" class="link">Other Library Catalogs</a> </li>
					<li> <a href="//nova.campusguides.com/quickereference" class="link">Quick e-Reference</a> </li>
					<li> <a href="//sherman.library.nova.edu/doi" class="link hint--info hint--right" data-hint="Digital Object Identifiers are used to uniquely cite electronic documents">DOI Tools</a> </li>
					<li> <a href="//illiad.library.nova.edu" class="link">Document Delivery / ILLiad</a> </li>
				</ul>
			</li>

			<li class="icon-wifi">
				<a class="link">Services</a>
				<ul class="sub-menu">
					<li> <a href="http://sherman.library.nova.edu/sites/services/circulation/" class="link">Borrowing &amp; Circulation</a> </li>
					<li> <a href="http://sherman.library.nova.edu/sites/services/collection-development/" class="link">Collection Development</a> </li>
					<li> <a class="link" href="http://sherman.library.nova.edu/sites/dils/library-workshops/">Library Workshops</a> </li>
					<li> <a href="http://sherman.library.nova.edu/sites/services/docdel/" class="link">Interlibrary Loan</a> </li>
					<li> <a href="http://sherman.library.nova.edu/sites/services/electronic-course-reserves-ecr/" class="link">Electronic Course Reserves</a> </li>
					<li> <a href="http://sherman.library.nova.edu/sites/services/public-library-services/" class="link">Public Library Services</a> </li>
					<li> <a href="http://sherman.library.nova.edu/sites/services/reference-department/" class="link">Reference</a> </li>
					<li> <a href="//www.nova.edu/library/serv/facilities" class="link">Conference Rooms</a> </li>
					<li> <a href="http://sherman.library.nova.edu/sites/services/other-services/" class="link">Other Services</a> </li>
				</ul>
			</li>

			<li class="icon-library">
				<a class="link">About the Library</a>
				<ul class="sub-menu">
					<li> <a href="http://sherman.library.nova.edu/sites/about/overview/" class="link">About the Library</a> </li>
					<li> <a href="http://www.nova.edu/library/main/contact.html" class="link">Contact Information</a> </li>
					<li class="has-subnav"> 
						 <a href="#parent" rel="nofollow">General Information</a>
						 <ul class="children">
						 	<li> <a href="http://nova.edu/library/main/hours.html">Hours</a> </li>
						 	<li> <a href="//www.nova.edu/library/main/hours.html#directions">Map &amp; Directions</a> </li>
						 	<li> <a href="//www.nova.edu/library/main/hours.html#parking">Parking</a> </li>
						 	<li> <a href="//www.nova.edu/library/main/hours.html#transport">Buses &amp; Shuttles</a> </li>
						 </ul>
					 </li>
					<li> <a href="http://sherman.library.nova.edu/sites/about/exhibits/" class="link">Exhibits</a> </li>
					<li> <a href="http://sherman.library.nova.edu/helios" class="link">Events</a> </li>
					<li> <a href="http://sherman.library.nova.edu/sites/about/job-vacancies/" class="link">Job Vacancies</a> </li>
					<li> <a href="http://sherman.library.nova.edu/sites/about/library-collections/" class="link">Library Collections</a> </li>
					<li> <a href="http://sherman.library.nova.edu/sites/policies" class="link">Policies</a> </li>
					<li> <a href="http://www.nova.edu/library/about/news.html" class="link">News</a> </li>
					<li> <a href="http://www.nova.edu/community/libraries.html" class="link">NSU Libraries</a> </li>
					<li> <a href="http://www.nova.edu/library/main/directory.html" class="link" title="Staff Directory">Staff Directory</a> </li>				</ul>
			</li>

			<li class="icon-help">
				<a class="link">Help</a>
				<ul class="sub-menu">
					<li> <a href="http://www.nova.edu/library/main/ask.html" class="link">Ask a Librarian</a> </li>
					<li> <a href="http://www.nova.edu/library/main/a-z-help.html" class="link">A - Z Help Index</a> </li>
					<li> <a href="//nova.campusguides.com/index.php?gid=104" class="link">Library Guides</a> </li>
					<li> <a href="//nova.campusguides.com/cat.php?cid=20755&gid=104" class="link">How To ... </a> </li>
					<li> <a href="http://www.nova.edu/library/main/library-instruction.html" class="link">Library Instruction</a> </li>
					<li> <a href="//www.nova.edu/library/staffonly" class="link">For Library Staff Only</a> </li>
				</ul>

			</li>
		</ul>

	</nav><!--/.menu-->