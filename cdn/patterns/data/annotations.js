var comments = {
"comments" : [
	{
		"el": "header[role=banner]",
		"title" : "Masthead",
		"comment": "The main header of the site doesn't take up too much screen real estate in order to keep the focus on the core content. It's using a linear CSS gradient instead of a background image to give greater design flexibility and reduce HTTP requests."
	},

	{
		"el" : ".comment--hide-a11y",
		"title" : "Accessible but visually hidden text",
		"comment" : "Use `.hide-accessible` before for headings, form elements, or content to ensure that screen readers can navigate the page."
	},

	{
		"el" : ".comment--p",
		"title" : "Paragraph",
		"comment" : "The body font size is <code>21px</code> or <code>1.16668rem</code> with a bottom margin of <code>1.5em</code>."
	}
]
};