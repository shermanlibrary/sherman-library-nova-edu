<head>

  <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- OpenGraph data -->
  <meta property="og:locale" content="en_US" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="Home - Alvin Sherman Library" />
  <meta property="og:url" content="http://sherman.library.nova.edu/sites/" />
  <meta property="og:site_name" content="Alvin Sherman Library" />

  <title>Alvin Sherman Library</title>

  <link rel="canonical" href="http://sherman.library.nova.edu/" />
  <link rel="shortcut icon" href="#" />

  <!-- Stylesheets -->
  <link rel='stylesheet' href='<?=WEB_ROOT?>/cdn/styles/css/public-global/critical.css' type='text/css' media='all' />
  <noscript><link rel="stylesheet" href="/cdn/styles/css/public-global/uncritical.css"></noscript>

  <!-- loadCSS(); -->
  <script>
  !function(e){"use strict";e.loadCSS=function(t,n,r){var i,l=e.document,o=l.createElement("link");if(n)i=n;else{var a=(l.body||l.getElementsByTagName("head")[0]).childNodes;i=a[a.length-1]}var s=l.styleSheets;o.rel="stylesheet",o.href=t,o.media="only x",i.parentNode.insertBefore(o,n?i:i.nextSibling);var f=function(e){for(var t=o.href,n=s.length;n--;)if(s[n].href===t)return e();setTimeout(function(){f(e)})};return o.onloadcssdefined=f,f(function(){o.media=r||"all"}),o}}(this);
  loadCSS( '/cdn/styles/css/public-global/uncritical.css' );
  </script>

  <!-- Google Analytics -->
  <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-37110734-2', 'auto');
  </script>

  <style>
    .overlayContainer { display: none;}
    .overlayBackground { top:0px; left:0px; padding-left:100px;position:absolute;z-index:1000;height:100%;width:100%;background-color:#808080;opacity:0.3;}
    .overlayContent { position:absolute; border: 1px solid #000; background-color:#fff;font-weight: bold;height: 100px;width: 300px;z-index:1000;text-align:center;}
  </style>

  <base href="/">
</head>

<body class="home" data-ng-controller="ApplicationController as ac">

    <svg display="none" version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
      <defs>
        <g id="icon-search">
            <path class="path1" d="M31.008 27.231l-7.58-6.447c-0.784-0.705-1.622-1.029-2.299-0.998 1.789-2.096 2.87-4.815 2.87-7.787 0-6.627-5.373-12-12-12s-12 5.373-12 12c0 6.627 5.373 12 12 12 2.972 0 5.691-1.081 7.787-2.87-0.031 0.677 0.293 1.515 0.998 2.299l6.447 7.58c1.104 1.226 2.907 1.33 4.007 0.23s0.997-2.903-0.23-4.007zM12 20c-4.418 0-8-3.582-8-8s3.582-8 8-8 8 3.582 8 8-3.582 8-8 8z"></path>
        </g>

        <symbol id="icon-question" viewBox="0 0 1024 1024">
            <title>question</title>
            <path class="path1" d="M448 704h128v128h-128zM704 256c35.346 0 64 28.654 64 64v192l-192 128h-128v-64l192-128v-64h-320v-128h384zM512 96c-111.118 0-215.584 43.272-294.156 121.844s-121.844 183.038-121.844 294.156c0 111.118 43.272 215.584 121.844 294.156s183.038 121.844 294.156 121.844c111.118 0 215.584-43.272 294.156-121.844s121.844-183.038 121.844-294.156c0-111.118-43.272-215.584-121.844-294.156s-183.038-121.844-294.156-121.844zM512 0v0c282.77 0 512 229.23 512 512s-229.23 512-512 512c-282.77 0-512-229.23-512-512s229.23-512 512-512z"></path>
        </symbol>

        <symbol id="icon-facebook" viewBox="0 0 1024 1024">
            <title>Facebook</title>
            <path class="path1" d="M512 0c-282.77 0-512 229.23-512 512s229.23 512 512 512v-384h-128v-128h128v-96c0-88.366 71.632-160 160-160h160v128h-160c-17.674 0-32 14.328-32 32v96h176l-32 128h-144v367.87c220.828-56.838 384-257.3 384-495.87 0-282.77-229.23-512-512-512z"></path>
        </symbol>

        <symbol id="icon-twitter" viewBox="0 0 1024 1024">
            <title>Twitter</title>
            <path class="path1" d="M1024 194.418c-37.676 16.708-78.164 28.002-120.66 33.080 43.372-26 76.686-67.17 92.372-116.23-40.596 24.078-85.556 41.56-133.41 50.98-38.32-40.83-92.922-66.34-153.346-66.34-116.022 0-210.088 94.058-210.088 210.078 0 16.466 1.858 32.5 5.44 47.878-174.6-8.764-329.402-92.4-433.018-219.506-18.084 31.028-28.446 67.116-28.446 105.618 0 72.888 37.088 137.192 93.46 174.866-34.438-1.092-66.832-10.542-95.154-26.278-0.020 0.876-0.020 1.756-0.020 2.642 0 101.788 72.418 186.696 168.522 206-17.626 4.8-36.188 7.372-55.348 7.372-13.538 0-26.698-1.32-39.528-3.772 26.736 83.46 104.32 144.206 196.252 145.896-71.9 56.35-162.486 89.934-260.916 89.934-16.958 0-33.68-0.994-50.116-2.94 92.972 59.61 203.402 94.394 322.042 94.394 386.422 0 597.736-320.124 597.736-597.744 0-9.108-0.206-18.168-0.61-27.18 41.056-29.62 76.672-66.62 104.836-108.748z"></path>
        </symbol>

        <symbol id="icon-bubbles" viewBox="0 0 1152 1024">
            <title>Ask</title>
            <path class="path1" d="M1088 901.166c0 45.5 26.028 84.908 64 104.184v15.938c-10.626 1.454-21.472 2.224-32.5 2.224-68.008 0-129.348-28.528-172.722-74.264-26.222 6.982-54.002 10.752-82.778 10.752-159.058 0-288-114.616-288-256s128.942-256 288-256c159.058 0 288 114.616 288 256 0 55.348-19.764 106.592-53.356 148.466-6.824 14.824-10.644 31.312-10.644 48.7zM230.678 221.186c-66.214 53.798-102.678 122.984-102.678 194.814 0 40.298 11.188 79.378 33.252 116.15 22.752 37.92 56.982 72.586 98.988 100.252 30.356 19.992 50.78 51.948 56.176 87.894 1.8 11.984 2.928 24.088 3.37 36.124 7.47-6.194 14.75-12.846 21.88-19.976 24.154-24.152 56.78-37.49 90.502-37.49 5.368 0 10.762 0.336 16.156 1.024 20.948 2.662 42.344 4.016 63.594 4.020v128c-27.128-0.002-53.754-1.738-79.742-5.042-109.978 109.978-241.25 129.7-368.176 132.596v-26.916c68.536-33.578 128-94.74 128-164.636 0-9.754-0.758-19.33-2.164-28.696-115.796-76.264-189.836-192.754-189.836-323.304 0-229.75 229.23-416 512-416 278.458 0 504.992 180.614 511.836 405.52-41.096-18.316-85.84-29.422-132.262-32.578-11.53-56.068-45.402-108.816-98.252-151.756-35.322-28.698-76.916-51.39-123.628-67.444-49.706-17.080-102.76-25.742-157.694-25.742-54.932 0-107.988 8.662-157.694 25.742-46.712 16.054-88.306 38.744-123.628 67.444z"></path>
        </symbol>

        <symbol id="icon-sign-out" viewBox="0 0 28 32">
          <title>sign-out</title>
          <path class="path1" d="M11.429 25.714q0 0.071 0.018 0.357t0.009 0.473-0.054 0.42-0.179 0.348-0.366 0.116h-5.714q-2.125 0-3.634-1.509t-1.509-3.634v-12.571q0-2.125 1.509-3.634t3.634-1.509h5.714q0.232 0 0.402 0.17t0.17 0.402q0 0.071 0.018 0.357t0.009 0.473-0.054 0.42-0.179 0.348-0.366 0.116h-5.714q-1.179 0-2.018 0.839t-0.839 2.018v12.571q0 1.179 0.839 2.018t2.018 0.839h5.571t0.205 0.018 0.205 0.054 0.143 0.098 0.125 0.161 0.036 0.241zM28 16q0 0.464-0.339 0.804l-9.714 9.714q-0.339 0.339-0.804 0.339t-0.804-0.339-0.339-0.804v-5.143h-8q-0.464 0-0.804-0.339t-0.339-0.804v-6.857q0-0.464 0.339-0.804t0.804-0.339h8v-5.143q0-0.464 0.339-0.804t0.804-0.339 0.804 0.339l9.714 9.714q0.339 0.339 0.339 0.804z"></path>
        </symbol>
      </defs>
    </svg>

    <a class="hide-accessible" href="#content">Skip the menu to the main content</a>

    <div id="container">

        <div class="menu menu--banner universal tinsley-gradient">
            <div class="wrap clearfix">

              <a class="link" href="http://www.nova.edu/" title="Nova Southeastern University">
                <picture class="logo logo--nsu">
                  <source media="(max-width: 617px)" srcset="//sherman.library.nova.edu/cdn/media/images/logos/nsu-small.png" alt="Alvin Sherman Library, Research, and Information Technology Center">
                  <img
                    alt="Alvin Sherman Library, Research, and Information Technology Center"
                    srcset="//sherman.library.nova.edu/cdn/media/images/logos/nsu.png"
                    data-src="//sherman.library.nova.edu/cdn/media/images/logos/nsu.png">
                </picture>
              </a>

              <a href="<?=WEB_ROOT?>" title="Home - Alvin Sherman Library">
                <picture class="logo logo--asl">
                  <source media="(min-width: 618px)" srcset="//sherman.library.nova.edu/cdn/media/images/logos/sherman-library.png" alt="Alvin Sherman Library, Research, and Information Technology Center">
                  <img
                    alt="Alvin Sherman Library, Research, and Information Technology Center"
                    srcset="//sherman.library.nova.edu/cdn/media/images/logos/sherman-library.png"
                    data-src="//sherman.library.nova.edu/cdn/media/images/logos/sherman-library.png">
                </picture>
              </a>

            </div>
        </div><!--/.universal-->

        <input type="checkbox" class="checkbox-toggle" id="top-menu" aria-describedby="top-menu-toggle-description">
        <span class="hide-accessible" id="top-menu-toggle-description">Checking this box will open the top menu</span>

        <header class="header" role="banner">

            <div id="inner-header" class="wrap clearfix">

                <label class="label label--hamburger" for="top-menu" title="Open the Menu">
                  <svg class="svg svg--menu" fill="#000000" height="32" viewBox="0 0 24 24" width="32" xmlns="http://www.w3.org/2000/svg">
                      <path d="M0 0h24v24H0z" fill="none"/>
                      <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"/>
                  </svg>
                  <span>Menu</span>
                </label>

                <span class="pill-menu__title" ng-if="ac.title" ng-cloak ng-bind-html="ac.title"></span>

                <ul ng-cloak class="menu--actions--public menu--actions--context">

                  <li class="menu--actions--public__menu-item icon">
                    <a href="//sherman.library.nova.edu/sites/hours" target="_self">
                      <svg class="svg" style="position: relative; width: 22px; height: 24px; fill:#444; "viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                          <path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z"/>
                          <path d="M0 0h24v24H0z" fill="none"/>
                          <path d="M12.5 7H11v6l5.25 3.15.75-1.23-4.5-2.67z"/>
                      </svg>
                      <span class="icon__label">
                        <span data-ng-if="ac.hours.open">Today</span>
                        <span data-ng-if="ac.hours.message">Closed</span>
                      </span>
                    </a>
                  </li>

                  <li class="menu--actions--public__menu-item hide-accessible--sm" data-ng-if="ac.hours.open" ng-cloak style="background-color: rgba( 0, 0, 0, 0.05 );">
                      <span data-ng-if="ac.hours.open">{{ ac.hours.open }} &ndash; {{ ac.hours.close }}</span>
                  </li>

                  <li class="menu--actions--public__menu-item icon">
                    <button class="button button--link link--drop-down" href="top" data-ng-click="librariesMenu.toggle()">

                      <svg class="svg svg--activity" xmlns="http://www.w3.org/2000/svg" viewBox="10 15 75 75" style="width: 22px; height: 24px; fill: #444;">
          						  <path d="M70.907 48.925c3.273-.662 6.546-1.866 9.82-2.528.064-.014.124-.05.168-.104.045-.054.07-.12.07-.182v-.667c0-.13-.107-.218-.24-.19-3.272.662-6.545 1.865-9.82 2.527-.132.027-.24.154-.24.287v.666c.002.064.025.12.07.156.046.035.108.048.172.035zM80.895 47.77c-.044-.037-.104-.05-.168-.037-3.273.663-6.546 1.866-9.82 2.528-.133.028-.24.155-.24.288v.666c0 .063.024.12.07.156.045.035.105.05.168.036 3.274-.662 6.548-1.867 9.822-2.53.066-.013.126-.05.168-.103.044-.05.07-.116.07-.182v-.667c0-.063-.025-.118-.07-.154zM70.907 53.887c3.273-.663 6.546-1.866 9.82-2.53.065-.013.125-.05.168-.103.043-.05.07-.115.07-.182v-.667c0-.063-.025-.12-.07-.155-.044-.036-.104-.05-.168-.036-3.273.663-6.546 1.867-9.82 2.53-.127.025-.24.16-.24.286v.666c0 .13.107.217.24.19z"></path>
          						  <path d="M83.424 39.514c-5.058.438-10.116 2.877-15.174 3.326-5.058-.45-10.116-2.888-15.174-3.326-.404-.035-.783.103-1.07.38-.284.28-.442.66-.442 1.064v21.736c0 .404.158.782.443 1.075.286.29.666.468 1.07.503 4.85.42 9.7 2.678 14.552 3.258.18.067.376.093.58.074.013 0 .027-.003.04-.004.015 0 .03.003.043.004.203.02.4-.007.58-.073 4.85-.58 9.702-2.836 14.553-3.257.404-.035.783-.212 1.07-.504.284-.294.442-.672.442-1.076V40.958c0-.403-.158-.785-.443-1.063-.288-.28-.667-.416-1.07-.38zM66.697 64.38c-4.037-.65-8.073-2.307-12.11-2.943l.002-18.712c4.035.636 8.07 2.293 12.107 2.942V64.38zm3.106 0V45.667c4.037-.65 8.072-2.307 12.108-2.942l.002 18.712c-4.036.636-8.072 2.292-12.11 2.943z"></path>
          						  <path d="M55.773 46.396c3.273.662 6.546 1.866 9.82 2.528.064.013.125 0 .17-.036.045-.035.07-.092.07-.156v-.666c0-.133-.106-.26-.24-.287-3.273-.663-6.546-1.866-9.82-2.528-.13-.027-.24.06-.24.19v.668c0 .062.026.128.07.182.046.055.106.09.17.104zM55.773 48.877c3.273.662 6.548 1.867 9.822 2.53.063.012.123-.002.168-.037.045-.037.07-.093.07-.156v-.666c0-.133-.107-.26-.24-.287-3.273-.66-6.546-1.864-9.82-2.527-.064-.014-.124 0-.168.036-.045.035-.07.09-.07.153v.668c0 .066.027.132.07.183.042.052.103.09.168.104zM65.593 53.887c.133.026.24-.06.24-.19v-.667c0-.127-.113-.26-.24-.287-3.273-.662-6.546-1.866-9.82-2.53-.064-.012-.124 0-.168.037-.045.036-.07.092-.07.155v.667c0 .066.027.13.07.182.043.054.103.09.168.104 3.274.663 6.547 1.866 9.82 2.53zM70.907 56.25c3.273-.662 6.546-1.866 9.82-2.53.064-.012.124-.05.168-.103.045-.054.07-.12.07-.183v-.667c0-.13-.107-.22-.24-.19-3.272.66-6.545 1.865-9.82 2.527-.132.026-.24.155-.24.287v.667c.002.064.025.12.07.156.046.036.108.05.172.037zM80.895 55.093c-.044-.036-.104-.048-.168-.035-3.273.662-6.546 1.866-9.82 2.527-.133.028-.24.155-.24.287v.667c0 .06.024.12.07.155.045.035.105.048.168.035 3.274-.662 6.548-1.866 9.822-2.527.066-.015.126-.054.168-.104.044-.054.07-.117.07-.184v-.667c0-.064-.025-.122-.07-.157zM80.727 57.54c-3.273.66-6.546 1.864-9.82 2.527-.127.025-.24.16-.24.286v.667c0 .13.107.22.24.19 3.273-.662 6.546-1.866 9.82-2.53.065-.01.125-.05.168-.103.043-.05.07-.116.07-.182v-.666c0-.065-.025-.12-.07-.157-.044-.034-.104-.047-.168-.034zM55.535 53.435c0 .063.025.13.07.183.044.053.104.09.168.104 3.273.663 6.546 1.867 9.82 2.53.064.012.125-.002.17-.037.045-.036.07-.092.07-.156v-.667c0-.132-.106-.26-.24-.287-3.273-.662-6.546-1.866-9.82-2.528-.13-.027-.24.06-.24.19.002.222.002.445.002.667zM55.773 56.203c3.273.66 6.548 1.865 9.822 2.527.063.013.123 0 .168-.035.045-.036.07-.094.07-.156v-.668c0-.132-.107-.26-.24-.287-3.273-.66-6.546-1.865-9.82-2.527-.064-.013-.124 0-.168.035-.045.035-.07.093-.07.156v.666c0 .066.027.13.07.183.042.05.103.088.168.103zM65.593 60.067c-3.273-.663-6.546-1.866-9.82-2.528-.064-.014-.124 0-.168.034-.045.036-.07.092-.07.156v.666c0 .065.027.13.07.182.043.053.103.092.168.104 3.273.663 6.546 1.867 9.82 2.53.133.027.24-.06.24-.19v-.668c0-.127-.113-.26-.24-.287z"></path>
          						  <path d="M76.422 68.066c-2.296.653-4.67 1.33-7.09 1.64-.296.078-.602.118-.912.118-.056 0-.113-.002-.17-.004-.056.002-.112.004-.168.004-.31 0-.616-.04-.912-.12-2.42-.31-4.794-.984-7.09-1.638-2.504-.713-4.87-1.386-7.195-1.588-.933-.08-1.806-.494-2.462-1.164-.564-.58-.908-1.31-1.02-2.092H47.1V44.44h2.25v-3.482c0-.994.405-1.958 1.112-2.648.67-.654 1.556-1.016 2.49-1.016.106 0 .21.005.316.014 2.748.238 5.43 1.003 8.025 1.742 2.42.69 4.708 1.342 6.958 1.566 2.174-.217 4.388-.835 6.717-1.498l-9.746-6.603-13.196-8.94c-.61-.413-1.316-.622-2.022-.622s-1.412.21-2.023.623l-13.196 8.94-13.197 8.938c-1.312.89-1.89 2.53-1.426 4.043.466 1.516 1.865 2.547 3.448 2.547h3.27V59.65c-1.68.235-2.99 1.628-3.096 3.347h-.174c-1.99 0-3.605 1.613-3.605 3.605v6.836c0 1.992 1.615 3.607 3.605 3.607h52.788c1.99 0 3.605-1.615 3.605-3.607v-6.336c-1.168.28-2.356.617-3.578.964zm-34.73-4.843H35.89V44.44h5.8v18.783z"></path>
          						</svg>
                      <span class="icon__label">Libraries</span>
                    </button>

                    <div class="menu__sub-menu" data-ng-class="{ 'menu__sub-menu--open' : librariesMenuToggle }" style="border-left: none;">

                      <h4 class="hide-accessible">
                        Nova Southeastern University Libraries
                      </h4>

                      <nav class="menu__sub-menu__content no-padding" role="navigation">

                        <a style="display: block; padding: .5em 1em; border-left: 3px solid #313547;" href="//sherman.library.nova.edu" target="_self">Alvin Sherman Library</a>
                        <ul class="list list--alternate">
                        <li style="border-left: 3px solid #ffae3d; background-color:#f5f5f5;" >&ndash; <a style="padding: .5em 1em;" href="//public.library.nova.edu">Public</a></li>
                        </ul>
                        <a style="display: block; padding: .5em 1em; border-left: 3px solid #006699;" href="http://nova.edu/hpdlibrary">Health Professions Division Library</a>
                        <a style="display: block; padding: .5em 1em; border-left: 3px solid #42b6f0;" href="//nova.campusguides.com/oclibrary">Oceanographic Library</a>
                        <a style="display: block; padding: .5em 1em; border-left: 3px solid #3366CC;" href="https://www.law.nova.edu/library/">Panza Maurer Law Library</a>

                      </nav>

                    </div>
                  </li>

                  <li class="menu--actions--public__menu-item icon">
                    <label for="search-hero" class="button button--link link--drop-down">
                      <svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg" style="width: 22px; height: 24px; fill: #444;">
                          <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/>
                          <path d="M0 0h24v24H0z" fill="none"/>
                      </svg>

                      <span class="icon__label">Search</span>
                    </label>
                  </li>

                  <?php if(!isset($_SESSION[AUTH]['user'])) : ?>
                    <li data-ng-if="!ac.info" class="menu--actions--public__menu-item icon">
                      <a href="/auth/" target="_self">
                        <svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg" style="width: 22px; height: 24px; fill: #444;">
                            <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"/>
                            <path d="M0 0h24v24H0z" fill="none"/>
                        </svg>
                        <span class="icon__label">Account</span>
                      </a>
                    </li>
                   <?php endif; ?>

                  <li data-ng-if="ac.info" class="menu--actions--public__menu-item menu icon" ng-cloak style="background-color: white;">

                      <button class="button button--link link--drop-down" href="top" data-ng-click="userMenu.toggle()">

                        <svg class="svg svg--user" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" style="width: 22px; height: 24px; fill: #21aabd;">
                             <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 3c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm0 14.2c-2.5 0-4.71-1.28-6-3.22.03-1.99 4-3.08 6-3.08 1.99 0 5.97 1.09 6 3.08-1.29 1.94-3.5 3.22-6 3.22z"/>
                             <path d="M0 0h24v24H0z" fill="none"/>
                         </svg>

                        <span class="icon__label">Account</span>
                      </button>

                      <div class="menu__sub-menu" data-ng-class="{ 'menu__sub-menu--open' : userMenuToggle }">

                          <section ng-if="ac.activity.fees" class="alert alert--warning no-margin">
                              <p class="align-center small-text no-margin">
                                  You have <a target="_self" href="/fees/">{{ac.activity.fees}} {{ac.activity.feeNumber}} ({{ac.activity.balance}})</a>
                              </p>
                          </section>

                          <section class="menu__sub-menu__content" ng-if="ac.activity">
                            <h4 class="no-margin zeta">{{ac.info.display_name}}</h4>
                              <p class="small-text" ng-if="ac.info.email">{{ac.info.email}}</p>
                              <p class="small-text no-margin" ng-if="ac.activity.checkouts">You have <a ng-href="checkouts">{{ac.activity.checkouts}} {{ac.activity.checkoutNumber}}</a> checked out.</p>
                              <p class="small-text no-margin" ng-if="ac.activity.holds">You have <a ng-href="holds">{{ac.activity.holds}} {{ac.activity.holdNumber}}</a> on hold.</p>
                          </section>

                          <section class="menu__sub-menu__content" ng-if="!ac.activity">
                              Welcome back, {{ac.info.display_name}}!
                          </section>

                          <footer class="clearfix menu__sub-menu__footer" ng-cloak>

                              <div class="col-sm--sixcol">
                                  <a ng-href="/account"  class="button button--flat button--small button--primary small-text" style="color: white;" onclick="ga( 'send', 'event', 'Context Menu', 'click', 'My Account' );">My Account</a>
                              </div>

                              <div class="col-sm--sixcol align-right">
                                  <a href="logout.php" class="button button--flat button--small button--default small-text" onclick="ga( 'send', 'event', 'Context Menu', 'click', 'Log Out' );">Log Out</a>
                              </div>
                          </footer>
                      </div>

                  </li>

                  <li data-ng-if="ac.info" class="menu--actions--public__menu-item menu menu--expanded hide-accessible--sm" ng-cloak style="min-width: 4em; border-left: 1px solid #ddd;">
                    <span class="menu--expanded__text">
                      {{ ac.info.display_name }}
                      <span class="menu--expanded__menu" style="position: absolute; left: 10px; bottom: 5px; font-family: Arial, 'sans-serif'; font-size: 10px;">
                        <a class="link link--undecorated" href="/auth/logout.php" style="color: #21aabd;" target="_self">
                          <svg class="svg" style="width: 12px; height: 12px; top: 3px; position: relative;"><use xlink:href="#icon-sign-out"></use></svg>
                          Log out
                        </a>
                      </span>
                    </span>
                  </li>

      					</ul>

            </div><!--/.inner-header-->

        </header><!--/.header-->

      <!-- Main menu -->
      <nav class="top-menu" role="navigation">
        <a href="#content" class="hide-accessible">Skip the menu to the main content</a>
        <div id="inner-menu" class="wrap clearfix">

              <div class="menu-new-main-menu-container">
                  <ul class="menu">
                      <li class="menu-item   menu-item-has-children"><a href="http://sherman.library.nova.edu/sites/about/" target="_self">About</a>
                          <ul class="sub-menu">
                            <li class="menu-item">
                              <a href="https://sherman.library.nova.edu/friends/" target="_self">Circle of Friends</a>
                              <ul class="sub-menu">
                                <li class="menu-item menu-item-has-children">
                                  <a href="https://sherman.library.nova.edu/friends/">Support Your Library</a>
                                </li>
                              </ul>
                            </li>
                              <li class="menu-item   "><a href="http://sherman.library.nova.edu/sites/hours/" target="_self">Hours</a></li>
                              <li class="menu-item   "><a href="http://sherman.library.nova.edu/sites/directions/" target="_self">Directions and Parking</a></li>
                              <li class="menu-item   "><a href="http://sherman.library.nova.edu/sites/policies/" target="_self">Policies</a></li>
                          </ul>
                      </li>

                      <li class="menu-item   menu-item-has-children "><a href="#">Research</a>
                      <ul class="sub-menu">
                          <li class="menu-item menu-item-has-children">
                            <a href="http://novacat.nova.edu">Catalog</a>
                            <ul class="sub-menu">
                              <li class="menu-item">
                                <a href="http://novacat.nova.edu/search/X#refined">Advanced Search</a>
                              </li>
                            </ul>
                          </li>
                          <li class="menu-item" style="color: #91cdeb;">Citation Tools
                            <ul class="sub-menu">
                              <li class="menu-item   ">
                                  <a href="http://sherman.library.nova.edu/sites/apa/" target="_self">APA</a>
                              </li>
                                <li class="menu-item   ">
                                    <a href="http://sherman.library.nova.edu/doi/" alt="Document Object Idenfitier" target="_self">DOI</a>
                                </li>
                                <li class="menu-item   ">
                                    <a href="http://nova.campusguides.com/endnote-libguide">EndNote</a>
                                </li>
                            </ul>

                          </li>
                          <li class="menu-item   "><a href="http://sherman.library.nova.edu/e-library" target="_self">Databases</a></li>
                          <li class="menu-item   menu-item-has-children ">
                            <a href="http://sherman.library.nova.edu/sites/interlibrary-loan/" target="_self">Interlibrary Loan</a>
                              <ul class="sub-menu">
                                  <li class="menu-item   ">
                                      <a href="http://illiad.library.nova.edu">ILLiad</a>
                                  </li>
                                  <li class="menu-item   ">
                                      <a href="http://sherman.library.nova.edu/sites/interlibrary-loan/#docdel" target="_self">Document Delivery</a>
                                  </li>
                              </ul>
                          </li>
                           <li class="menu-item   "><a href="http://lib.nova.edu/ftf">Full Text Finder</a></li>
                      </ul>
                      </li>
                  <li class="menu-item   menu-item-has-children "><a href="#">Services</a>
                  <ul class="sub-menu">
                      <li class="menu-item   "><a href="http://sherman.library.nova.edu/sites/policies/circulation/" target="_self">Circulation</a></li>
                      <li class="menu-item   "><a href="http://nova.campusguides.com/c.php?g=112162&#038;p=724231">Course Reserves</a></li>
                      <li class="menu-item   "><a href="http://sherman.library.nova.edu/sites/spotlight/events" target="_self">Programs and Events</a></li>
                      <li class="menu-item   "><a href="http://public.library.nova.edu">Public Library Services</a></li>
                      <li class="menu-item   children menu-item-2517"><a href="http://sherman.library.nova.edu/sites/rooms/" target="_self">Reserve a Room</a>
                      <ul class="sub-menu">
                          <li class="menu-item   "><a href="//sherman.library.nova.edu/sites/rooms/#reserve" target="_self">Conference Rooms</a></li>
                          <li class="menu-item   "><a href="http://sherman.library.nova.edu/rooms" target="_self">Study Rooms</a></li>
                      </ul>
                  </li>
                      <li class="menu-item   "><a href="http://sherman.library.nova.edu/sites/workshops-and-instruction/" target="_self">Workshops and Instruction</a></li>
                  </ul>
                  </li>
                  <li class="menu-item   menu-item-has-children "><a href="#">Help</a>
                  <ul class="sub-menu">
                      <li class="menu-item"><a href="//sherman.library.nova.edu/sites/ask-a-librarian/" target="_self">Ask a Librarian</a></li>
                      <li class="menu-item   menu-item-has-children"><a href="http://sherman.library.nova.edu/sites/contact/" target="_self">Contact</a>
                      <ul class="sub-menu">
                          <li  class="menu-item"><a href="//sherman.library.nova.edu/sites/directory/" target="_self">Staff Directory</a></li>
                          <li  class="menu-item"><a href="http://systems.library.nova.edu/form/view.php?id=10">Suggest a Purchase</a></li>
                      </ul>
                  </li>
                      <li  class="menu-item"><a href="http://public.library.nova.edu/card/">Get a Library Card</a></li>
                      <li  class="menu-item"><a href="http://nova.campusguides.com/main">Library Guides</a></li>
                  </ul>
                  </li>
              </ul>
            </div>
          </div>

        </nav>

        <ng-include src="'../views/forms/search.html'"></ng-include>
