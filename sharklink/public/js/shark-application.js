// Application 7/20/16
(function(){
    'use strict';

    var app = angular.module('sharklinkApp', [ 'ngSanitize', 'angularUtils.directives.dirPagination']);

    app.constant('API_URL', '//sherman.library.nova.edu');
})();

//Data Service
(function() {
    'use strict';

    var dataService = function($http, $q) {
        this.$http = $http;
        this.$q = $q;
    };

    dataService.$inject = ['$http', '$q'];

    dataService.prototype.getFeatures  = function( audience ) {
        var _this = this;
        var audience = typeof audience !== 'undefined' ? audience : 'all';

        return _this.$http.get('http://sherman.library.nova.edu/sites/wp-json/features/v2/ads/?audience=' + audience )
            .then(function(response){
                if (typeof response.data === 'object') {
                    return response.data;
                } else {
                    return response.data;
                }

            }, function(response) {
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.checkSession = function() {
        var _this = this;
        return _this.$http.post('api/endpoint.php', { job : 'getUserInfo'})
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response);
                }

            }, function(response) {
                // something went wrong
                return 'public';
            })
    };

    dataService.prototype.getUserInfo = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getUserInfo'})
            .then(function(response){
                if (response.data.status === 'public') {
                    // console.log(response.data);
                    return _this.$q.reject('AUTH_REQUIRED');
                } else {
                    // console.log(response.data);
                    return response.data;
                }
            }, function(response) {
                //console.log(response);
                return 'public';
            })
    };




    dataService.prototype.getApplication  = function() {
        var _this = this;
        return _this.$http.get('../auth/api/endpoint.php?job=getApplication')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);

                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getHome  = function() {
        var _this = this;
        return _this.$http.get('../auth/api/endpoint.php?job=getHome')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);

                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getTitles  = function( query ) {
        var _this = this;
        return _this.$http.get('../ftf/endpoint.php?query='+query )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };


    dataService.prototype.getAccount  = function() {
        var _this = this;
        return _this.$http.get('sharklink/public/account')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);
                return _this.$q.reject(response.data);
            })
    };


    dataService.prototype.getASLBuildingHours  = function() {
        var _this = this;
        return _this.$http.get('../auth/api/endpoint.php?job=getASLBuildingHours')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getAllDbsMenuUnlog  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getAllDbsMenuUnlog'})
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);

                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getAllSubjectsMenuUnlog  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getAllSubjectsMenuUnlog'})
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);

                return _this.$q.reject(response.data);
            })
    };


    dataService.prototype.updateEmail  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.updateHomeLibrary  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };


    dataService.prototype.getGuides  = function() {
        var _this = this;
        return _this.$http.get( '//lgapi.libapps.com/1.1/subjects?site_id=142&key=ea1d92be94311f8f083ebed28a0b5f10&guide_published=2' )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };



    dataService.prototype.getCheckouts = function() {
        var _this = this;
        return _this.$http.get('sharklink/public/checkouts')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.fillCheckoutCart  = function( object ) {
        var _this = this;
        return _this.$http.post('sharklink/public/checkouts/fill', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.emptyCheckoutCart  = function() {
        var _this = this;
        return _this.$http.get('sharklink/public/checkouts/empty')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.addToCheckoutCart  = function( object ) {
        var _this = this;
        return _this.$http.post('sharklink/public/checkouts/add', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.removeFromCheckoutCart  = function( object ) {
        console.log(object);
        var _this = this;
        return _this.$http.post('sharklink/public/checkouts/remove', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.checkoutCartSort  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };


    dataService.prototype.getCheckoutsCart  = function() {
        var _this = this;
        return _this.$http.get('sharklink/public/checkouts/cart')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.submitRenewal = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'submitRenewal'} )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getHolds = function() {
        var _this = this;
        return _this.$http.get('sharklink/public/holds')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };


    dataService.prototype.addToHoldCart  = function( object ) {
        var _this = this;
        return _this.$http.post('sharklink/public/holds/add', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.removeFromHoldCart  = function( object ) {
        var _this = this;
        return _this.$http.post('sharklink/public/holds/remove', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.fillHoldCart  = function( object ) {
        var _this = this;
        return _this.$http.post('sharklink/public/holds/fill', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.emptyHoldCart  = function() {
        var _this = this;
        return _this.$http.get('sharklink/public/holds/empty')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.holdCartSort  = function( object ) {
        var _this = this;
        return _this.$http.post('sharklink/public/holds/sort', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getHoldsCart  = function() {
        var _this = this;
        return _this.$http.get('sharklink/public/holds/cart')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.cancelHolds  = function() {
        var _this = this;
        return _this.$http.get('sharklink/public/holds/cancel')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getFees = function() {
        var _this = this;
        return _this.$http.get('sharklink/public/fees')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getFeeDisplay = function() {
        var _this = this;
        return _this.$http.get('sharklink/public/fees')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getEventsAPI  = function( audience, series ) {
        var _this = this;
        var audience = typeof audience !== 'undefined' ? audience : 'all';
        var series = typeof series !== 'undefined' ? series : '';

        return _this.$http.get('//sherman.library.nova.edu/sites/spotlight/wp-json/events/v2/upcoming/?audience=' + audience + '&series=' + series )
            .then(function(response){
                if (typeof response.data === 'object') {
                    return response.data;
                } else {
                    return response.data;
                }

            }, function(response) {
                return _this.$q.reject(response.data);
            })
    };


    angular.module('sharklinkApp')
        .service('dataService', dataService);

}());
//Paging Controller
// CONTROLLERS
(function(){
    'use strict';

    var PagingController = function($scope ){
        $scope.pageChangeHandler = function(num) {
            //console.log('going to page ' + num);
        }
    };

    PagingController.$inject = [ '$scope' ];

    angular.module('sharklinkApp')
        .controller('PagingController', PagingController);

})();

(function(){

    'use strict';

    var ApplicationController = function(  dataService ){

        var vm = this;
        vm.col = 'n';
        vm.info = 'jim';
        vm.libraries = [
            { code: 'main', label: 'Alvin Sherman Library'},
            { code: 'nmb', label: 'North Miami Beach'},
            { code: 'ocean', label: 'Oceanographic Center Library'}
        ];

        vm.changers = ['main', 'nmb', 'ocean'];

        /*

        function getApplication(){
            dataService.getApplication().then(function(data){
                console.log(data);
                if(data) {
                    vm.hours  = data.response.hours;
                    vm.status = data.response.AppStatus;

                    if(vm.status !== 'public'){
                        vm.info             = data.response.info;
                        //console.log(data.response.info);
                        vm.email_update     = data.response.info.email;
                        vm.activity         = data.response.activity;
                        vm.col              = vm.info.col;
                        vm.selectedLibrary =  vm.info.home_lib;
                    }
                }
            });
        }

        */




    };

    ApplicationController .$inject = [ 'dataService' ];

    angular.module('sharklinkApp')
        .controller('ApplicationController', ApplicationController);

})();

//Checkouts Controller
(function(){

    'use strict';

    var CheckoutsController = function( $location, dataService ){

        var vm = this;
        vm.checkouts        = null;
        vm.cart             = null;
        vm.currentPage      = 1;
        vm.pageSize         = 5;
        vm.sortType			= 'duedate_stamp';
        vm.showbutton		= false;
        vm.hidestatus		= false;
        vm.available		= 0;
        vm.submitted        = false;


        var getCheckouts = function (){

            dataService.getCheckouts().then(function(data){
                console.log(data);
                if( data.error){
                    vm.error = data.error;
                    return;
                }
                if( !data.checkouts){
                    //'No items available';
                }
                if( data.show_button){
                    vm.showbutton = data.show_button;

                }
                vm.checkouts = data.checkouts;
                vm.selected  = data.selected;

                if( vm.checkouts.length >= 0) {
                    vm.available = parseInt(vm.showbutton);
                    if( vm.available === vm.checkouts.length ){
                        vm.hidestatus = true;
                    }
                    vm.pageSize = vm.checkouts.length;
                } else {
                    vm.pageSize = vm.checkouts.length;
                }
            })
        };
        getCheckouts();

        var checkClick = function () {
            //alert('Hello');
            getCheckouts();
        };


        var addToCart = function(checkout){
            var object = {
                job             : 'addToCheckoutCart',
                checkout        : checkout

            };
            console.log(object);
            dataService.addToCheckoutCart(object).then(function(data){
                if(data){
                    vm.cart      = data.cart;
                    vm.selected  = data.checkouts;
                    console.log(data);
                }
            })
        };

        var removeFromCart = function(item){
            console.log(item);
            var object = {
                job    : 'removeFromCheckoutCart',
                item   :  item
            };
            dataService.removeFromCheckoutCart(object).then(function(data){
                if(data){
                    vm.cart      = data.cart;
                    vm.selected  = data.checkouts;
                }
            })
        };


        var fillCart = function(){
            var object = {
                job : 'fillCheckoutCart',
                checkouts: vm.checkouts
            };
            dataService.fillCheckoutCart(object).then(function(data){
                if(data){
                    vm.cart      = data.cart;
                    vm.selected  = data.checkouts;
                }
            })
        };

        var emptyCart = function(){
            dataService.emptyCheckoutCart().then(function(data){
                if(data){
                    vm.cart      = data.cart;
                    vm.selected  = data.checkouts;
                }
            });
        };

        var cartSort = function(){
            vm.duedate = vm.duedate === true  ? null : true;

            if(vm.duedate){
                var object = {
                    job : 'checkoutCartSort',
                    sortByDueDate: 1
                };
            } else {
                var object = {
                    job : 'checkoutCartSort',
                    sortByCheckoutDate: 1
                };
            }

            dataService.checkoutCartSort(object).then(function(data){
                //console.log(data);
                if(data.checkouts){
                    vm.checkouts = data.checkouts;
                    vm.selected  = data.selected;
                }
            });

        };

        var formSubmit = function(){
            //console.log('hello');
           vm.submitted = true;
        } ;


        vm.addToCart        = addToCart;
        vm.removeFromCart   = removeFromCart;
        vm.fillCart         = fillCart;
        vm.emptyCart        = emptyCart;
        vm.cartSort         = cartSort;
        vm.formSubmit       = formSubmit;
        vm.checkClick       = checkClick;

    };
    CheckoutsController.$inject = [ '$location','dataService' ];

    angular.module('sharklinkApp')
        .controller('CheckoutsController', CheckoutsController);
})();

//Checkout Results Directive
(function(){

    'use strict';

    var checkoutResult = function() {
        var controller = [function(){
            var vm = this;
            vm.link	= 'https://novacat.nova.edu/record=b';
            //console.log(vm.checkout);
            //console.log(vm.selected);


            var checkCart = function(item){

                var check = vm.selected.indexOf(item);
                if( check < 0) {

                    return false;
                } else{

                    return true;
                }
            };

            var addToCart = function( checkout ){
                vm.add()(checkout);
            };

            var removeFromCart = function( item ){
                vm.remove()(item );
            };

            vm.checkCart      = checkCart;
            vm.addToCart      = addToCart;
            vm.removeFromCart = removeFromCart;

        }];
        return {
            scope: {
                checkout     : '=',
                selected     : '=',
                showbutton	 : '=',
                hidestatus	 : '=',
                add          : '&',
                remove       : '&'
            },
            controller: controller,
            controllerAs: 'vm',
            bindToController: true,
            templateUrl: 'assets/js/templates/checkoutResult.html',
            replace: true
        }
    };
    angular.module('sharklinkApp')
        .directive('checkoutResult', checkoutResult );
})();


//Holds Controller
(function(){

    'use strict';

    var HoldsController = function( dataService ){
        var vm = this;

        vm.requestdate      = true;
        vm.holds            = null;
        vm.cart             = null;
        vm.currentPage      = 1;
        vm.pageSize         = 5;
        vm.submitted        = false;


        var getHolds = function(){

            dataService.getHolds().then(function(data){
                //console.log(data);
                if( data.error){
                    vm.error = data.error;
                    return;
                }
                //console.log(data);
                vm.holds        = data.holds;
                vm.selected     = data.selected;
                if( vm.holds.length >= 5) {
                    vm.pageSize = 5;
                } else {
                    vm.pageSize = vm.holds.length;
                }
            });
        };

        getHolds();

        var getHoldsCart = function getHoldsCart() {
            dataService.getHoldsCart().then(function(data){
                console.log(data);
                vm.cart       = data.holdscart;
            });
        };


        var addToCart = function(hold){
            var object = {
                job      : 'addToHoldCart',
                hold     : hold.hold,
                title    : hold.title,
                status   : hold.status,
                pickup   : hold.pickup
            };
            dataService.addToHoldCart(object).then(function(data){
                if(data){
                    vm.cart      = data.holdscart;
                    vm.selected  = data.holds;
                    //console.log(data);
                }
            });
        };

        var removeFromCart = function(hold){
            var object = {
                job    : 'removeFromHoldCart',
                hold   :  hold
            };
            dataService.removeFromHoldCart(object).then(function(data){
                if(data){
                    vm.cart      = data.holdscart;
                    vm.selected  = data.holds;
                    if(vm.selected.length == 0){
                        vm.submitted = false;
                    }

                }
            });
        };

        var fillCart = function(){
            var object = {
                job : 'fillHoldCart',
                holds: vm.holds
            };
            dataService.fillHoldCart(object).then(function(data){
                if(data){
                    vm.cart      = data.holdscart;
                    vm.selected  = data.holds;
                }

            });
        };

        var emptyCart = function(){
            dataService.emptyHoldCart().then(function(data){
                vm.cart      = data.holdscart;
                vm.selected  = data.holds;
            });
        };

        var cartSort = function(){
            vm.requestdate = vm.requestdate === true  ? null : true;

            if(vm.requestdate){
                var object = {
                    job : 'holdCartSort',
                    sortByDueDate: 1
                };
            } else {
                var object = {
                    job : 'holdCartSort',
                    sortByCheckoutDate: 1
                };
            }

            dataService.holdCartSort(object).then(function(data){
                //console.log(data);
                if(data.checkouts){
                    vm.holds        = data.holds;
                    vm.selected     = data.selected;
                }
            });
        };

        var formSubmit = function(){
            getHoldsCart();
            vm.submitted = true;
        } ;

        var updateHolds = function(){
            getHolds();
            //vm.submitted = true;
        } ;

        var cancelHolds = function(){
            dataService.cancelHolds().then(function(data){
                if(data.cancelerror) {
                    vm.cancelerror = data.cancelerror;
                    if(data.errorlist){
                        hc.errorlist = data.errorlist;
                    }
                    vm.cart = null;

                }
                vm.submitted = false;
                vm.holds = data.holds;
                vm.selected  = data.selected;
                if( vm.holds.length >= 5) {
                    vm.pageSize = 5;
                } else {
                    vm.pageSize = vm.holds.length;
                }

            });
        } ;

        vm.getHoldsCart     = getHoldsCart;
        vm.addToCart        = addToCart;
        vm.removeFromCart   = removeFromCart;
        vm.fillCart         = fillCart;
        vm.emptyCart        = emptyCart;
        vm.cartSort         = cartSort;
        vm.formSubmit       = formSubmit;
        vm.updateHolds      = updateHolds;
        vm.cancelHolds      = cancelHolds;

    };

    HoldsController.$inject = [ 'dataService' ];
    angular.module('sharklinkApp')
        .controller('HoldsController', HoldsController);
})();

//Holds Results Directive
(function(){

    'use strict';

    var holdResult = function() {
        var controller = [function(){
            var vm = this;

            var checkCart = function(hold){
                var check = vm.selected.indexOf(hold);
                if( check < 0) {
                    return false;
                } else{
                    return true;
                }
            };

            var addToCart = function( hold ){
                vm.add()(hold);
            };

            var removeFromCart = function( hold ){
                vm.remove()(hold);
            };

            vm.checkCart      = checkCart;
            vm.addToCart      = addToCart;
            vm.removeFromCart = removeFromCart;

        }]
        return {
            scope: {
                hold         : '=',
                selected     : '=',
                add          : '&',
                remove       : '&'
            },
            controller: controller,
            controllerAs: 'vm',
            bindToController: true,
            templateUrl: 'assets/js/templates/holdResult.html',
            replace: true
        }
    };
    angular.module('sharklinkApp')
        .directive('holdResult', holdResult );
})();

//Fees Controller


(function(){

    'use strict';

    var FeesController = function(  dataService ){
        var fc = this;
        fc.currentCart      = [];
        fc.cartItems        = [];
        fc.cartTotal        = '$0.00';
        fc.currentInvoices  = [];
        fc.currentPage = 1;
        fc.pageSize = 5;


        getFeeDisplay();

        function getFeeDisplay() {
            "use strict";
            dataService.getFeeDisplay().then(function(data){
                //console.log(data);
                fc.currentFees        = data.fees;
                fc.currentCart        = data.cart;
                fc.cartItems          = data.cartItems;
                fc.feeTotal          = data.feeTotal;
                fc.cartTotal          = data.cartTotal;
                fc.currentInvoices    = data.invoices;
                fc.limits             = data.limits;
                if( fc.currentFees.length >= 5) {
                    fc.pageSize = 5;
                } else {
                    fc.pageSize = fc.currentFees.length;
                }

            })
        }




        fc.pageChangeHandler = function(num) {
            //console.log('page changed to ' + num);
        };


        fc.fillCart = function(){
            console.log('filling');
            dataService.fillCart().then(function(data){
                fc.currentFees      = []
                fc.currentCart      = data.cart;
                fc.cartItems        = data.cartItems;
                fc.cartTotal        = data.cartTotal;
                fc.currentInvoices  = data.invoices;
                fc.currentFees      = data.fees
                //$route.reload();
            })
        };

        fc.emptyCart = function(){

            dataService.emptyCart().then(function(data){
                fc.currentFees      = []
                fc.currentCart      = data.cart;
                fc.currentInvoices  = data.invoices;
                fc.currentFees      = data.fees
                fc.cartItems       = data.cartItems;
                //$route.reload();

            })
        };

        fc.addToCart = function(fee){
            //console.log(fee);
            var invoice = fee.invoice, amount  = fee.amount,
                charge =  fee.charge, title   = fee.title;

            var object = {
                job    : 'addToCart',
                invoice:  invoice,
                amount :  amount,
                charge :  charge,
                title  :  title
            }

            //console.log(object);
            dataService.addToCart(object).then(function(data){
                fc.currentCart      = data.cart;
                fc.cartItems        = data.cartItems;
                fc.cartTotal        = data.cartTotal;
                fc.currentInvoices  = data.invoices;
                //console.log(data);
            })

        };

        fc.removeFromCart = function(fee){
            //console.log(fee);
            var invoice = fee;

            var object = {
                job    : 'removeFromCart',
                invoice:  invoice
            }

            dataService.removeFromCart(object).then(function(data){
                fc.currentCart      = data.cart;
                fc.cartItems        = data.cartItems;
                fc.cartTotal        = data.cartTotal;
                fc.currentInvoices  = data.invoices;
                //console.log(data);
            })
        };


    }
    FeesController.$inject = [ 'dataService' ];
    angular.module('sharklinkApp')
        .controller('FeesController', FeesController);
})();

//console.log('test');
(function(){
    var feeResult = function() {
        var controller = [function(){
            var vm = this;

            console.log(vm.invoices);

            vm.checkCart = function(invoice){
                console.log(invoice);
                var check = vm.invoices.indexOf(invoice);
                //console.log(check);
                if( check < 0) {
                    // console.log('nope');
                    return false;
                } else{
                    // console.log('yep');
                    return true;
                }
            };

            vm.showDetails = function(fee){
                if( vm.fee.detailState  == 'hide-accessible') {
                    vm.fee.detailState = '';
                } else {
                    vm.fee.detailState = 'hide-accessible';
                }
            };

            vm.addToCart = function( fee ){
                vm.add()(fee);
                vm.fee.addState = fee.invoice;
            };

            vm.removeFromCart = function( fee ){
                // console.log(fee);
                vm.remove()(fee);
                vm.fee.addState = '';
            };

        }]
        return {
            scope: {
                fee       : '=',
                invoices  : '=',
                user      : '=',
                add       : '&',
                remove    : '&'

            },
            controller: controller,
            controllerAs: 'vm',
            bindToController: true,
            templateUrl: 'assets/js/templates/feeResult.html',
            replace: true
        }
    };
    angular.module('sharklinkApp')
        .directive('feeResult', feeResult );
})();


//Account Controller
// CONTROLLERS
(function(){

    'use strict';

    var AccountController = function( dataService ){

        var vm    = this;
        vm.dbAcc  = false;
        vm.subAcc = false;

        function  getAccount(){
            dataService. getAccount().then(function(data){
                console.log(data);
                if(data) {
                    vm.databases  = data.databases;
                    vm.subjects   = data.subjects;
                }
            });
        }

        getAccount();

        var dbAccToggle = function(){
            vm.dbAcc =  vm.dbAcc == false ? true : false;
        };

        var subAccToggle = function(){
            vm.subAcc = vm.subAcc == false ? true : false;
        };

        var startsWith = function (actual, expected) {
            var lowerStr = (actual + "").toLowerCase();
            return lowerStr.indexOf(expected.toLowerCase()) === 0;
        };

        vm.dbAccToggle  = dbAccToggle;
        vm.subAccToggle = subAccToggle;
        vm.startsWith   = startsWith;
    };

    AccountController.$inject = ['dataService'];

    angular.module('sharklinkApp')
        .controller('AccountController', AccountController);

})();

// CONTROLLERS
(function(){

    'use strict';


var EventController = function( $attrs, dataService ) {

    var vm    = this;
    vm.audience = $attrs.audience;
    vm.series = $attrs.series;
    /**
     * Uses dataService.prototype.getEvents - be sure to remove if this controller is removed too
     */
    function  getEventsAPI( audience, series ){

        dataService.getEventsAPI( audience, series ).then(function(data){
            if( data ) {
                vm.events = data;
            }
        });
    }

    getEventsAPI( vm.audience, vm.series );

};

EventController.$inject = ['$attrs', 'dataService'];
    angular.module('sharklinkApp')
        .controller('EventController', EventController);

})();

//Pagination Directive
/**
 * dirPagination - AngularJS module for paginating (almost) anything.
 *
 *
 * Credits
 * =======
 *
 * Daniel Tabuenca: https://groups.google.com/d/msg/angular/an9QpzqIYiM/r8v-3W1X5vcJ
 * for the idea on how to dynamically invoke the ng-repeat directive.
 *
 * I borrowed a couple of lines and a few attribute names from the AngularUI Bootstrap project:
 * https://github.com/angular-ui/bootstrap/blob/master/src/pagination/pagination.js
 *
 * Copyright 2014 Michael Bromley <michael@michaelbromley.co.uk>
 */

(function() {

    /**
     * Config
     */
    var moduleName = 'angularUtils.directives.dirPagination';
    var DEFAULT_ID = '__default';

    /**
     * Module
     */
    var module;
    try {
        module = angular.module(moduleName);
    } catch(err) {
        // named module does not exist, so create one
        module = angular.module(moduleName, []);
    }

    module
        .directive('dirPaginate', ['$compile', '$parse', 'paginationService', dirPaginateDirective])
        .directive('dirPaginateNoCompile', noCompileDirective)
        .directive('dirPaginationControls', ['paginationService', 'paginationTemplate', dirPaginationControlsDirective])
        .filter('itemsPerPage', ['paginationService', itemsPerPageFilter])
        .service('paginationService', paginationService)
        .provider('paginationTemplate', paginationTemplateProvider);

    function dirPaginateDirective($compile, $parse, paginationService) {

        return  {
            terminal: true,
            multiElement: true,
            compile: dirPaginationCompileFn
        };

        function dirPaginationCompileFn(tElement, tAttrs){

            var expression = tAttrs.dirPaginate;
            // regex taken directly from https://github.com/angular/angular.js/blob/master/src/ng/directive/ngRepeat.js#L211
            var match = expression.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?\s*$/);

            var filterPattern = /\|\s*itemsPerPage\s*:[^|]*/;
            if (match[2].match(filterPattern) === null) {
                throw 'pagination directive: the \'itemsPerPage\' filter must be set.';
            }
            var itemsPerPageFilterRemoved = match[2].replace(filterPattern, '');
            var collectionGetter = $parse(itemsPerPageFilterRemoved);

            addNoCompileAttributes(tElement);

            // If any value is specified for paginationId, we register the un-evaluated expression at this stage for the benefit of any
            // dir-pagination-controls directives that may be looking for this ID.
            var rawId = tAttrs.paginationId || DEFAULT_ID;
            paginationService.registerInstance(rawId);

            return function dirPaginationLinkFn(scope, element, attrs){

                // Now that we have access to the `scope` we can interpolate any expression given in the paginationId attribute and
                // potentially register a new ID if it evaluates to a different value than the rawId.
                var paginationId = $parse(attrs.paginationId)(scope) || attrs.paginationId || DEFAULT_ID;
                paginationService.registerInstance(paginationId);

                var repeatExpression = getRepeatExpression(expression, paginationId);
                addNgRepeatToElement(element, attrs, repeatExpression);

                removeTemporaryAttributes(element);
                var compiled =  $compile(element);

                var currentPageGetter = makeCurrentPageGetterFn(scope, attrs, paginationId);
                paginationService.setCurrentPageParser(paginationId, currentPageGetter, scope);

                if (typeof attrs.totalItems !== 'undefined') {
                    paginationService.setAsyncModeTrue(paginationId);
                    scope.$watch(function() {
                        return $parse(attrs.totalItems)(scope);
                    }, function (result) {
                        if (0 <= result) {
                            paginationService.setCollectionLength(paginationId, result);
                        }
                    });
                } else {
                    scope.$watchCollection(function() {
                        return collectionGetter(scope);
                    }, function(collection) {
                        if (collection) {
                            paginationService.setCollectionLength(paginationId, collection.length);
                        }
                    });
                }

                // Delegate to the link function returned by the new compilation of the ng-repeat
                compiled(scope);
            };
        }

        /**
         * If a pagination id has been specified, we need to check that it is present as the second argument passed to
         * the itemsPerPage filter. If it is not there, we add it and return the modified expression.
         *
         * @param expression
         * @param paginationId
         * @returns {*}
         */
        function getRepeatExpression(expression, paginationId) {
            var repeatExpression,
                idDefinedInFilter = !!expression.match(/(\|\s*itemsPerPage\s*:[^|]*:[^|]*)/);

            if (paginationId !== DEFAULT_ID && !idDefinedInFilter) {
                repeatExpression = expression.replace(/(\|\s*itemsPerPage\s*:[^|]*)/, "$1 : '" + paginationId + "'");
            } else {
                repeatExpression = expression;
            }

            return repeatExpression;
        }

        /**
         * Adds the ng-repeat directive to the element. In the case of multi-element (-start, -end) it adds the
         * appropriate multi-element ng-repeat to the first and last element in the range.
         * @param element
         * @param attrs
         * @param repeatExpression
         */
        function addNgRepeatToElement(element, attrs, repeatExpression) {
            if (element[0].hasAttribute('dir-paginate-start') || element[0].hasAttribute('data-dir-paginate-start')) {
                // using multiElement mode (dir-paginate-start, dir-paginate-end)
                attrs.$set('ngRepeatStart', repeatExpression);
                element.eq(element.length - 1).attr('ng-repeat-end', true);
            } else {
                attrs.$set('ngRepeat', repeatExpression);
            }
        }

        /**
         * Adds the dir-paginate-no-compile directive to each element in the tElement range.
         * @param tElement
         */
        function addNoCompileAttributes(tElement) {
            angular.forEach(tElement, function(el) {
                if (el.nodeType === Node.ELEMENT_NODE) {
                    angular.element(el).attr('dir-paginate-no-compile', true);
                }
            });
        }

        /**
         * Removes the variations on dir-paginate (data-, -start, -end) and the dir-paginate-no-compile directives.
         * @param element
         */
        function removeTemporaryAttributes(element) {
            angular.forEach(element, function(el) {
                if (el.nodeType === Node.ELEMENT_NODE) {
                    angular.element(el).removeAttr('dir-paginate-no-compile');
                }
            });
            element.eq(0).removeAttr('dir-paginate-start').removeAttr('dir-paginate').removeAttr('data-dir-paginate-start').removeAttr('data-dir-paginate');
            element.eq(element.length - 1).removeAttr('dir-paginate-end').removeAttr('data-dir-paginate-end');
        }

        /**
         * Creates a getter function for the current-page attribute, using the expression provided or a default value if
         * no current-page expression was specified.
         *
         * @param scope
         * @param attrs
         * @param paginationId
         * @returns {*}
         */
        function makeCurrentPageGetterFn(scope, attrs, paginationId) {
            var currentPageGetter;
            if (attrs.currentPage) {
                currentPageGetter = $parse(attrs.currentPage);
            } else {
                // if the current-page attribute was not set, we'll make our own
                var defaultCurrentPage = paginationId + '__currentPage';
                scope[defaultCurrentPage] = 1;
                currentPageGetter = $parse(defaultCurrentPage);
            }
            return currentPageGetter;
        }
    }

    /**
     * This is a helper directive that allows correct compilation when in multi-element mode (ie dir-paginate-start, dir-paginate-end).
     * It is dynamically added to all elements in the dir-paginate compile function, and it prevents further compilation of
     * any inner directives. It is then removed in the link function, and all inner directives are then manually compiled.
     */
    function noCompileDirective() {
        return {
            priority: 5000,
            terminal: true
        };
    }

    function dirPaginationControlsDirective(paginationService, paginationTemplate) {

        var numberRegex = /^\d+$/;

        return {
            restrict: 'AE',
            templateUrl: function(elem, attrs) {
                return attrs.templateUrl || paginationTemplate.getPath();
            },
            scope: {
                maxSize: '=?',
                onPageChange: '&?',
                paginationId: '=?'
            },
            link: dirPaginationControlsLinkFn
        };

        function dirPaginationControlsLinkFn(scope, element, attrs) {

            // rawId is the un-interpolated value of the pagination-id attribute. This is only important when the corresponding dir-paginate directive has
            // not yet been linked (e.g. if it is inside an ng-if block), and in that case it prevents this controls directive from assuming that there is
            // no corresponding dir-paginate directive and wrongly throwing an exception.
            var rawId = attrs.paginationId ||  DEFAULT_ID;
            var paginationId = scope.paginationId || attrs.paginationId ||  DEFAULT_ID;

            if (!paginationService.isRegistered(paginationId) && !paginationService.isRegistered(rawId)) {
                var idMessage = (paginationId !== DEFAULT_ID) ? ' (id: ' + paginationId + ') ' : ' ';
                throw 'pagination directive: the pagination controls' + idMessage + 'cannot be used without the corresponding pagination directive.';
            }

            if (!scope.maxSize) { scope.maxSize = 9; }
            scope.directionLinks = angular.isDefined(attrs.directionLinks) ? scope.$parent.$eval(attrs.directionLinks) : true;
            scope.boundaryLinks = angular.isDefined(attrs.boundaryLinks) ? scope.$parent.$eval(attrs.boundaryLinks) : false;

            var paginationRange = Math.max(scope.maxSize, 5);
            scope.pages = [];
            scope.pagination = {
                last: 1,
                current: 1
            };
            scope.range = {
                lower: 1,
                upper: 1,
                total: 1
            };

            scope.$watch(function() {
                return (paginationService.getCollectionLength(paginationId) + 1) * paginationService.getItemsPerPage(paginationId);
            }, function(length) {
                if (0 < length) {
                    generatePagination();
                }
            });

            scope.$watch(function() {
                return (paginationService.getItemsPerPage(paginationId));
            }, function(current, previous) {
                if (current != previous && typeof previous !== 'undefined') {
                    goToPage(scope.pagination.current);
                }
            });

            scope.$watch(function() {
                return paginationService.getCurrentPage(paginationId);
            }, function(currentPage, previousPage) {
                if (currentPage != previousPage) {
                    goToPage(currentPage);
                }
            });

            scope.setCurrent = function(num) {
                if (isValidPageNumber(num)) {
                    num = parseInt(num, 10);
                    paginationService.setCurrentPage(paginationId, num);
                }
            };

            function goToPage(num) {
                if (isValidPageNumber(num)) {
                    scope.pages = generatePagesArray(num, paginationService.getCollectionLength(paginationId), paginationService.getItemsPerPage(paginationId), paginationRange);
                    scope.pagination.current = num;
                    updateRangeValues();

                    // if a callback has been set, then call it with the page number as an argument
                    if (scope.onPageChange) {
                        scope.onPageChange({ newPageNumber : num });
                    }
                }
            }

            function generatePagination() {
                var page = parseInt(paginationService.getCurrentPage(paginationId)) || 1;

                scope.pages = generatePagesArray(page, paginationService.getCollectionLength(paginationId), paginationService.getItemsPerPage(paginationId), paginationRange);
                scope.pagination.current = page;
                scope.pagination.last = scope.pages[scope.pages.length - 1];
                if (scope.pagination.last < scope.pagination.current) {
                    scope.setCurrent(scope.pagination.last);
                } else {
                    updateRangeValues();
                }
            }

            /**
             * This function updates the values (lower, upper, total) of the `scope.range` object, which can be used in the pagination
             * template to display the current page range, e.g. "showing 21 - 40 of 144 results";
             */
            function updateRangeValues() {
                var currentPage = paginationService.getCurrentPage(paginationId),
                    itemsPerPage = paginationService.getItemsPerPage(paginationId),
                    totalItems = paginationService.getCollectionLength(paginationId);

                scope.range.lower = (currentPage - 1) * itemsPerPage + 1;
                scope.range.upper = Math.min(currentPage * itemsPerPage, totalItems);
                scope.range.total = totalItems;
            }

            function isValidPageNumber(num) {
                return (numberRegex.test(num) && (0 < num && num <= scope.pagination.last));
            }
        }

        /**
         * Generate an array of page numbers (or the '...' string) which is used in an ng-repeat to generate the
         * links used in pagination
         *
         * @param currentPage
         * @param rowsPerPage
         * @param paginationRange
         * @param collectionLength
         * @returns {Array}
         */
        function generatePagesArray(currentPage, collectionLength, rowsPerPage, paginationRange) {
            var pages = [];
            var totalPages = Math.ceil(collectionLength / rowsPerPage);
            var halfWay = Math.ceil(paginationRange / 2);
            var position;

            if (currentPage <= halfWay) {
                position = 'start';
            } else if (totalPages - halfWay < currentPage) {
                position = 'end';
            } else {
                position = 'middle';
            }

            var ellipsesNeeded = paginationRange < totalPages;
            var i = 1;
            while (i <= totalPages && i <= paginationRange) {
                var pageNumber = calculatePageNumber(i, currentPage, paginationRange, totalPages);

                var openingEllipsesNeeded = (i === 2 && (position === 'middle' || position === 'end'));
                var closingEllipsesNeeded = (i === paginationRange - 1 && (position === 'middle' || position === 'start'));
                if (ellipsesNeeded && (openingEllipsesNeeded || closingEllipsesNeeded)) {
                    pages.push('...');
                } else {
                    pages.push(pageNumber);
                }
                i ++;
            }
            return pages;
        }

        /**
         * Given the position in the sequence of pagination links [i], figure out what page number corresponds to that position.
         *
         * @param i
         * @param currentPage
         * @param paginationRange
         * @param totalPages
         * @returns {*}
         */
        function calculatePageNumber(i, currentPage, paginationRange, totalPages) {
            var halfWay = Math.ceil(paginationRange/2);
            if (i === paginationRange) {
                return totalPages;
            } else if (i === 1) {
                return i;
            } else if (paginationRange < totalPages) {
                if (totalPages - halfWay < currentPage) {
                    return totalPages - paginationRange + i;
                } else if (halfWay < currentPage) {
                    return currentPage - halfWay + i;
                } else {
                    return i;
                }
            } else {
                return i;
            }
        }
    }

    /**
     * This filter slices the collection into pages based on the current page number and number of items per page.
     * @param paginationService
     * @returns {Function}
     */
    function itemsPerPageFilter(paginationService) {

        return function(collection, itemsPerPage, paginationId) {
            if (typeof (paginationId) === 'undefined') {
                paginationId = DEFAULT_ID;
            }
            if (!paginationService.isRegistered(paginationId)) {
                throw 'pagination directive: the itemsPerPage id argument (id: ' + paginationId + ') does not match a registered pagination-id.';
            }
            var end;
            var start;
            if (collection instanceof Array) {
                itemsPerPage = parseInt(itemsPerPage) || 9999999999;
                if (paginationService.isAsyncMode(paginationId)) {
                    start = 0;
                } else {
                    start = (paginationService.getCurrentPage(paginationId) - 1) * itemsPerPage;
                }
                end = start + itemsPerPage;
                paginationService.setItemsPerPage(paginationId, itemsPerPage);

                return collection.slice(start, end);
            } else {
                return collection;
            }
        };
    }

    /**
     * This service allows the various parts of the module to communicate and stay in sync.
     */
    function paginationService() {

        var instances = {};
        var lastRegisteredInstance;

        this.registerInstance = function(instanceId) {
            if (typeof instances[instanceId] === 'undefined') {
                instances[instanceId] = {
                    asyncMode: false
                };
                lastRegisteredInstance = instanceId;
            }
        };

        this.isRegistered = function(instanceId) {
            return (typeof instances[instanceId] !== 'undefined');
        };

        this.getLastInstanceId = function() {
            return lastRegisteredInstance;
        };

        this.setCurrentPageParser = function(instanceId, val, scope) {
            instances[instanceId].currentPageParser = val;
            instances[instanceId].context = scope;
        };
        this.setCurrentPage = function(instanceId, val) {
            instances[instanceId].currentPageParser.assign(instances[instanceId].context, val);
        };
        this.getCurrentPage = function(instanceId) {
            var parser = instances[instanceId].currentPageParser;
            return parser ? parser(instances[instanceId].context) : 1;
        };

        this.setItemsPerPage = function(instanceId, val) {
            instances[instanceId].itemsPerPage = val;
        };
        this.getItemsPerPage = function(instanceId) {
            return instances[instanceId].itemsPerPage;
        };

        this.setCollectionLength = function(instanceId, val) {
            instances[instanceId].collectionLength = val;
        };
        this.getCollectionLength = function(instanceId) {
            return instances[instanceId].collectionLength;
        };

        this.setAsyncModeTrue = function(instanceId) {
            instances[instanceId].asyncMode = true;
        };

        this.isAsyncMode = function(instanceId) {
            return instances[instanceId].asyncMode;
        };
    }

    /**
     * This provider allows global configuration of the template path used by the dir-pagination-controls directive.
     */
    function paginationTemplateProvider() {

        var templatePath = 'assets/js/templates/dirPagination.html';

        this.setPath = function(path) {
            templatePath = path;
        };

        this.$get = function() {
            return {
                getPath: function() {
                    return templatePath;
                }
            };
        };
    }
})();