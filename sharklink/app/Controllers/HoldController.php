<?php
/**
 * Created by PhpStorm.
 * User: jamesh
 * Date: 3/12/2017
 * Time: 10:42 AM
 */

namespace App\Controllers;
use Interop\Container\ContainerInterface;

use App\APIs\PatronAPI\PatronAPI;
use App\APIs\SierraCURL\SierraCURL;
use App\AccountFunctions\Checkouts\Checkouts;
use App\AccountFunctions\Fines\Fines;
use App\AccountFunctions\Holds\Holds;
use App\Workshops\Workshops;
use App\Events\Events;
use App\ElibResponse\ElibResponse;


class HoldController extends Controller
{


    protected $_c;


    public function __construct(ContainerInterface $c )
    {
        parent::__construct( $c );
    }


    public function holds($request, $response)
    {
        $data = Holds::getHoldsCurl();
        return $response->withJson($data, 200);
    }

    public function holdscartsort($request, $response)
    {
        $params = $request->getParams();
        //print_r($params);die();
        $data = Holds::getHoldsCurl($params);
        return $response->withJson($data, 200);
    }

    public function addhold($request, $response)
    {
        $params = $request->getParams();
        $holds = new Holds();
        $data = $holds->addtoCart($params);
        return $response->withJson($data, 200);
    }

    public function removehold($request, $response)
    {
        $params = $request->getParams();
        $holds = new Holds();
        $data = $holds->removeFromCart($params);
        return $response->withJson($data, 200);
    }

    public function fillholdcart($request, $response)
    {
        $params = $request->getParams();
        $holds = new Holds();
        $data = $holds->fillCart($params);
        return $response->withJson($data, 200);
    }

    public function emptyholdcart($request, $response)
    {
        $holds = new Holds();
        $data = $holds->emptyCart();
        return $response->withJson($data, 200);
    }


    public function getholdscart($request, $response)
    {
        $holds = new Holds();
        $data = $holds->getItemsInCart();
        return $response->withJson($data, 200);
    }

    public function cancelholds($request, $response)
    {
        $holds = new Holds();
        $data = $holds->cancelHolds();
        return $response->withJson($data, 200);
    }


}