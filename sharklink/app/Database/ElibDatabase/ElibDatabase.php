<?php
namespace App\Database\ElibDatabase;

use PDO;
use PDOException;


class ElibDatabase {
        private static $dsn = 'mysql:host=127.0.0.1;dbname=eresources';
        private static $username =  'presser';
        private static $password = 'XqHTDLHwLGxGUvq5';
        private static $auth_db;

        public function __construct() {}

        public static function getDB () {
            if(!isset(self::$auth_db)) :
                try {
                     self::$auth_db = new PDO(self::$dsn,
                     self::$username,
                     self::$password);
                }catch(PDOException $e){
                    $error_message = $e->getMessage();
                    include '(../errors/database_error.php)';
                exit;
                }
            endif;

            return self::$auth_db;
        }
}