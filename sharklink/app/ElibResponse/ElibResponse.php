<?php


namespace App\ElibResponse;

use PDO;
use PDOException;
use App\Database\DatabaseObject;
use App\Database\ElibDatabase\ElibDatabase;
use App\Elibrary\Databases\Database;
use App\Elibrary\Subjects\Subject;

class ElibResponse extends DatabaseObject {
    //public $url_elements;
    protected $_parameters = array();
    protected $_cat = 'gen';
    protected $_group = 'db_nsu_access';
    protected $_location = " != 'N'";
    protected $_allow = array('job', 'callback', 'col', 'cat', 'group', 'letter', 'location');


    public function __construct( $parameters = array() ) {
        foreach($parameters as $key => $value ) :
            if(in_array($key, $this->_allow)):
                $this->_parameters[$key] = trim($value);
            endif;
        endforeach;

        $this->getGroup();
        $this->_setLocation();
    }

    public static function getAllDbsMenuUnlog( $params = []){
        $response = new ElibResponse( $params);

        $sql = $response->_setDbSQL('all_dbs_menu_unlog');
        $records = static::find_by_sql( $sql);
        $collection = $response->_group;
        $out = array();
        $outs = array();
        foreach($records as $record) :
            $out['title'] = htmlentities($record['title']);
            $out['aid'] = $record['db_id'];
            $access = $record[$collection];
            if($access == 'R' || $access == 'P') :
                $out['access'] = 'remote';
            else:
                $out['access'] = 'local';
            endif;
            $outs[] = $out;
        endforeach;
        return $outs;
    }

    public static function getAllSubjectsMenuUnlog( $params = []){
        $response = new ElibResponse( $params );
        $sql = $response->_setDbSQL('all_dbs_subjects_unlog');
        $records = static::find_by_sql( $sql);

        $out = array();
        $outs = array();

        foreach($records as $record) :
            $out['name'] = htmlentities($record['name']);
            $out['subj_id'] = $record['subj_id'];
            $outs[] = $out;
        endforeach;

        return $records;

    }

    protected function _setGroup(){
        if(array_key_exists('group', $this->_parameters)) :
           return $this->_group = $this->_parameters['group'];
        endif;
        return $this->_group;
    }

    public function getGroup(){
        return $this->_setGroup();
    }



    protected function _setLocation(){

        if(array_key_exists('location', $this->_parameters)) :
            return $this->_location = $this->_parameters['location'];
        endif;
        return $this->_location;
    }

    public function getLocation(){
        return $this->_setLocation();
    }



    public function _setDbSQL( $var ){
        $collection = $this->_setGroup();
        switch( $var ) :
            case 'all_dbs_menu_unlog':
                return $sql ="
                   	SELECT `db_id`, `$collection`,
                    `db_name` AS `title`
                    FROM
                    `db_info`
                    WHERE `db_display` = 'Y'
                    AND `$collection` != 'N'
                    AND `db_status` != 'I'
                    AND `db_trial_status` != 'S'
                    UNION
                    SELECT `db_id`, `$collection`,
                    `db_alt_name` AS `title`
                    FROM
                    `db_info`
                    WHERE `db_display` = 'Y'
                    AND `$collection` != 'N'
                    AND `db_status` != 'I'
                    AND `db_trial_status` != 'S'
                    AND `db_alt_name` !=''
                    ORDER BY `title`";

            case 'all_subjects_menu_unlog':
            default:
                $location = $this->_group. $this->_location;
                return  $sql = "
                 SELECT  DISTINCT(`db_subject`.`subj_id`), `subject`.`subj_name` as name
                 FROM `db_info`
                 INNER JOIN `db_subject` ON `db_info`.`db_id` = `db_subject`.`db_id`
                 INNER JOIN `subject` ON `db_subject`.`subj_id` = `subject`.`subj_id`
                 WHERE `db_display` = 'Y'
                 AND `db_status` != 'I'
                 AND `db_trial_status` != 'S'
                 AND  $location
                 ORDER BY name";

        endswitch;

    }

    public static function find_by_sql( $sql = '', $params = array() ){
        $db = ElibDatabase::getDB();
        $result = $db->prepare($sql);
        !empty($params ) ? $result->execute($params) :  $result->execute();
        static::$rowcount = $result->rowCount();
        $results = $result->fetchALL(PDO::FETCH_ASSOC);
        $errorInfo = $result->errorInfo();
        if(isset($errorInfo[2])) :
            //Email::send_SQLError( $errorInfo[2], $sql );
            echo $errorInfo[2];
            die();
        endif;
        $result->closeCursor();
        return !empty($results) ? $results : false;
    }

    public static function getDatabasesIndex( $params = [] ){
        $databases = ElibResponse::getAllDbsMenuUnlog( $params );
        $databases = new Database($databases);
        return $databases->instantiate();
    }

    public static function getSubjectsIndex( $params = [] ){
        $subjects = ElibResponse::getAllSubjectsMenuUnlog( $params );
        $subjects = new Subject($subjects);
        return $subjects->instantiate();
    }


}