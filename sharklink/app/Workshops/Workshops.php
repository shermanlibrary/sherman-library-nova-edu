<?php


namespace App\Workshops;

use PDO;
use App\Database\DatabaseObject;
use App\Database\SitesDatabase\SitesDatabase;



class Workshops extends DatabaseObject {
    protected static $table_name = 'reservations';
    protected static $table_id = 'id';


    public $id;


    public function __construct(){
        global $status;
    }

    /*
    * @param string $name
    *
    * @return mixed
    */
    public function __get($name)
    {
        $method = ucfirst($name);
        if (method_exists(__CLASS__, $method)) {
            return $this->$method();
        } else {
            return $this->$name;
        }
    }

    /**
     * Overloaded set method
     *
     * @param string $name
     * @param mixed $value
     */

    public function __set($name, $value)
    {
        $method = ucfirst($name);
        if (method_exists(__CLASS__, $method)) {
            $this->$method($value);
        } else {
            $this->$name = $value;
        }
    }

    public static function find_by_sql( $sql = '', $params = array() ){
        $db = SitesDatabase::getDB();
        $result = $db->prepare($sql);
        !empty($params ) ? $result->execute($params) :  $result->execute();
        static::$rowcount = $result->rowCount();
        $results = $result->fetchALL(PDO::FETCH_ASSOC);
        $errorInfo = $result->errorInfo();
        if(isset($errorInfo[2])) :
            //Email::send_SQLError( $errorInfo[2], $sql );
            //echo $errorInfo[2];
            //die();
        endif;
        $result->closeCursor();
        return !empty($results) ? $results : false;
    }


    public static function getWorkshops(){
        $sql =
            "SELECT p.post_title,p.guid,m1.meta_value as 'event_start_time', m2.meta_value as 'event_start', m3.meta_value as 'scheduling_options'
            FROM wp_17_terms AS t
            INNER JOIN wp_17_term_taxonomy AS tt ON (tt.term_id = t.term_id)
            INNER JOIN wp_17_term_relationships AS tr ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
            INNER JOIN wp_17_posts AS p ON (tr.object_id = p.ID)
            LEFT JOIN wp_17_postmeta m1
                ON p.id = m1.post_id AND m1.meta_key = 'event_start_time'
            LEFT JOIN wp_17_postmeta m2
                ON p.id = m2.post_id AND m2.meta_key = 'event_start'
            LEFT JOIN wp_17_postmeta m3
                ON p.id = m3.post_id AND m3.meta_key = 'scheduling_options'
            WHERE p.post_type = 'spotlight_events'
                    AND p.post_status = 'publish'
                    AND t.term_id = 320
            ORDER BY event_start
            LIMIT 0,100";

            $outs = array();

            $today = (int)date('Ymd');
            $workshops = static::find_by_sql( $sql );
            if(!empty($workshops)):
                foreach( $workshops as $workshop ):
                    if(strpos($workshop['scheduling_options'],'multiday') !== false) :
                        $out = array();
                        if((int)$workshop['event_end'] >= $today ) :
                            $out['title'] = htmlentities($workshop['post_title']);
                            $out['start'] = date('F j', strtotime($workshop['event_start']));
                            $out['end']   = date('F j', strtotime($workshop['event_end']));
                            $out['url']   = $workshop['guid'];
                            $out['url']   = str_replace('#038;','',$workshop['guid']);
                            $outs[] = $out;
                        endif;
                    else :
                        $out = array();
                        if((int)$workshop['event_start'] >= $today ) :
                            $out['title'] = htmlentities($workshop['post_title']);
                            $out['start'] = date('F j', strtotime($workshop['event_start']));
                            $out['time']  = $workshop['event_start_time'];
                            $out['url']   = str_replace('#038;','',$workshop['guid']);
                            $outs[] = $out;
                        endif;
                    endif;
                    if(count($outs) === 5):
                        break;
                    endif;
                endforeach;
            endif;

            return $outs;

    }

    public static function workshopsIndex(){
        $workshops = static::getWorkshops();
        $workshops = new Workshop($workshops);
        return $workshops->instantiate();
    }


}