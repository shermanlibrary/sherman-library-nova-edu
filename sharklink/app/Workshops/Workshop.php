<?php

namespace App\Workshops;

use \DateTime;
use App\ObjectFactory\ObjectFactory;


class Workshop extends ObjectFactory{

    public $_objectFields = [
        'title', 'start', 'time', 'url'
    ];

    public $title;
    public $start;
    public $time;
    public $url;


}
