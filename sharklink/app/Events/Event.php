<?php

namespace App\Events;

use \DateTime;
use App\ObjectFactory\ObjectFactory;


class Event extends ObjectFactory{

    public $_objectFields = [
        'title', 'start', 'time', 'url'
    ];

    public $title;
    public $start;
    public $time;
    public $url;


}