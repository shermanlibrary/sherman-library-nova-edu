<?php

namespace App\Events;

use PDO;
use App\Database\DatabaseObject;
use App\Database\SitesDatabase\SitesDatabase;

class Events extends DatabaseObject {

    public function __construct(){
        global $status;
    }

    /*
    * @param string $name
    *
    * @return mixed
    */
    public function __get($name)
    {
        $method = ucfirst($name);
        if (method_exists(__CLASS__, $method)) {
            return $this->$method();
        } else {
            return $this->$name;
        }
    }

    /**
     * Overloaded set method
     *
     * @param string $name
     * @param mixed $value
     */

    public function __set($name, $value)
    {
        $method = ucfirst($name);
        if (method_exists(__CLASS__, $method)) {
            $this->$method($value);
        } else {
            $this->$name = $value;
        }
    }

    public static function find_by_sql( $sql = '', $params = array() ){
        $db = SitesDatabase::getDB();
        $result = $db->prepare($sql);
        !empty($params ) ? $result->execute($params) :  $result->execute();
        static::$rowcount = $result->rowCount();
        $results = $result->fetchALL(PDO::FETCH_ASSOC);
        $errorInfo = $result->errorInfo();
        if(isset($errorInfo[2])) :
            Email::send_SQLError( $errorInfo[2], $sql );
            echo $errorInfo[2];
            die();
        endif;
        $result->closeCursor();
        return !empty($results) ? $results : false;
    }

    public static function getEvents(){
        $today = (int)date('Ymd');
        $sql =
            "  SELECT p.post_title,p.guid,
                m1.meta_value as 'event_start_time',
                m2.meta_value as 'event_start',
                CAST(m3.meta_value AS UNSIGNED) as 'event_end',
                m4.meta_value as 'scheduling_options'
                FROM wp_17_posts AS p
                LEFT JOIN wp_17_postmeta m1
                    ON p.id = m1.post_id AND m1.meta_key = 'event_start_time'
                LEFT JOIN wp_17_postmeta m2
                    ON p.id = m2.post_id AND m2.meta_key = 'event_start'
                LEFT JOIN wp_17_postmeta m3
                    ON p.id = m3.post_id AND m3.meta_key = 'event_end'
                LEFT JOIN wp_17_postmeta m4
                    ON p.id = m4.post_id AND m4.meta_key = 'scheduling_options'
                INNER JOIN wp_17_term_relationships AS tr
                  ON p.id = tr.object_id
                INNER JOIN wp_17_term_taxonomy AS tt
                  ON tr.term_taxonomy_id = tt.term_taxonomy_id
                INNER JOIN wp_17_terms AS t
                  ON tt.term_id = t.term_id
                WHERE p.post_type = 'spotlight_events'
					AND p.post_status = 'publish'
					AND t.slug = 'public'
					AND (m3.meta_value >= $today OR  m3.meta_value IS NULL)
                    ORDER BY event_start
                    LIMIT 0, 500";

        $events = static::find_by_sql( $sql );
        //echo '<pre>';
        //print_r($events);die();
        $outs = array();
        $tomorrow = (int)date('Ymd');
        if(!empty($events)):
            foreach( $events as $event ):
                if(strpos($event['scheduling_options'],'multiday') !== false) :
                    $out = array();
                    if((int)$event['event_end'] >= $today ) :
                        $out['title'] = htmlentities($event['post_title']);
                        $out['start'] = date('F j', strtotime($event['event_start']));
                        $out['end']   = date('F j', strtotime($event['event_end']));
                        $out['url']   = $event['guid'];
                        $out['url']   = str_replace('#038;','',$event['guid']);
                        $outs[] = $out;
                    endif;
                else :
                    $out = array();
                    if((int)$event['event_start'] >= $today ) :
                        $out['title'] = htmlentities($event['post_title']);
                        $out['start'] = date('F j', strtotime($event['event_start']));
                        $out['time']  = $event['event_start_time'];
                        $out['url']   = str_replace('#038;','',$event['guid']);
                        $outs[] = $out;
                    endif;
                endif;
                if(count($outs) === 5):
                    break;
                endif;
            endforeach;
        endif;

        //print_r($outs);die();
        return $outs;
    }

    public static function eventsIndex(){
        $events = static::getEvents();
        $events = new Event($events);
        return $events->instantiate();
    }


}
