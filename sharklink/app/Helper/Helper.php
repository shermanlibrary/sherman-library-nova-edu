<?php

namespace App\Helper;

class Helper {

    public static function set_action(){
        $action = 'home';
        if(isset ($_POST['action']) && !empty($_POST['action'])) :
            return $action = $_POST['action'];
        elseif(isset ($_GET['action']) && !empty($_GET['action'])):
            return $action = $_GET['action'];
        elseif(isset ($_SESSION[APPLICATION]['action']) && !empty($_SESSION[APPLICATION]['action'])) :
            return $action = $_SESSION[APPLICATION]['action'];
        else :return $action ; endif;
    }
	
	public static function secureRedirect(){
		if(!isset($_SERVER['HTTPS'])) :
			$url = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			header('Location:'.$url);
			exit;
		endif;
	}

    public static function illiadRedirect(){
        header('REMOTE_USER: jamesh');
        header('Location: https://illiad.library.nova.edu');
    }

    public static function post_to_session( $level, $code, $message = '', $location = '' ){
        if(!($_POST)) :
            setAlert ( $level, $code, $custom='', $location = '');
        else:
            foreach ($_POST as $key => $val):
                $_SESSION['form'][$key] = $val;
            endforeach;
            unset($_POST);
            $record = $_SESSION['form'];
            return $record;
        endif;
    }

    public static function fieldname_as_text($fieldname) {
        $fieldname = str_replace("_", " ", $fieldname);
        $fieldname = str_replace("id", "ID", $fieldname);
        $fieldname = ucfirst($fieldname);
        return $fieldname;
    }

    public static function getActive( $page = null ){
        if(!empty($page)):
            if(is_array($page)) :
                $error = array();
                foreach ($page as $key => $value ) :
                    if( Url::getParam( $key ) != $value ):
                        array_push( $error, $key );
                    endif;
                endforeach;
                return empty($error) ? ' class="act"' : null;
            endif;
            return $page == Url::cPage() ? ' class="act"' : null;
        endif;
    }
	
	public static function encodeHTML($string, $case = 2) {
		switch($case) {
			case 1:
			return htmlentities($string, ENT_NOQUOTES, 'UTF-8', false);
			break;			
			case 2:
			$pattern = '<([a-zA-Z0-9\.\, "\'_\/\-\+~=;:\(\)?&#%![\]@]+)>';
			// put text only, devided with html tags into array
			$textMatches = preg_split('/' . $pattern . '/', $string);
			// array for sanitised output
			$textSanitised = array();			
			foreach($textMatches as $key => $value) {
				$textSanitised[$key] = htmlentities(html_entity_decode($value, ENT_QUOTES, 'UTF-8'), ENT_QUOTES, 'UTF-8');
			}			
			foreach($textMatches as $key => $value) {
				$string = str_replace($value, $textSanitised[$key], $string);
			}			
			return $string;			
			break;
		}
	}

    public static function getImgSize( $image, $case){
        if(is_file($image)) :
            // 0 => width,  1 => height, 2 => type, 3 => attributes
            $size = getimagesize( $image);
            return $size[$case];
        endif;
    }

    public static function shortenString( $string, $len = 150 ){
        if( strlen($string) > $len) :
            $string = trim(substr($string, 0, $len));
            $string = substr($string, 0, strrpos($string, ' ')).'&hellip;';
        else:
            $string .= '&hellip;';
        endif;
        return $string;
    }

    public static function redirect($url = null){
        if(!empty($url)) :
            header("Location: " . $url, true, 303);
            exit;
        endif;
    }

    public static function formatCurrency($number) {
        $cents = substr($number, -2, 2);
        // place decimal before the last two digits of the number
        $number = substr_replace($number, '.', -2, 2) . $cents;

        $number = number_format($number,2);

        return '$'.$number;
    }

    public static function output_message ( $message =''){
        if(isset($_SESSION[APPLICATION]['message'])) :
            $message = $_SESSION[APPLICATION]['message'];
            unset($_SESSION[APPLICATION]['message']);
        endif;
        if( !empty($message)):
            return "<p class=\"message\">{$message}</p>";
        else :
            return '';
        endif;
    }

    public static function output_alert ( $alert =''){
        if(isset($_SESSION[APPLICATION]['alert']) && count($_SESSION[APPLICATION]['alert']) == 1) :
            $session = $_SESSION[APPLICATION]['alert'];
            unset($_SESSION[APPLICATION]['alert']);
            foreach($session as $key => $value) :
                $alert = $session[$key];
                return '<div class="alert alert--'.$key.'" role="alert">'.$alert.'</div>';
            endforeach;
        else:
        endif;
    }

    public static function upay_currency( $number = '' ) {
        // get last two digits of number
        $cents = substr($number, -2, 2);
        // place decimal before the last two digits of the number
        $number = substr_replace($number, '.', -2, 2) . $cents;
        return $number;
    }

    public static function fines_pp_max(){
        if(isset($_SESSION[APPLICATION]['pp_max'])):
            $pp_max = $_SESSION[APPLICATION]['pp_max'];
        else:
            $pp_max = 5;
        endif;
        return $pp_max;
    }

    public static function setDate($case = null, $date = null) {

        $date = empty($date) ? time() : strtotime($date);

        switch($case) :
            case 1:
                // 01/01/2010
                return date('d/m/Y', $date);
            case 2:
                // 012/01/2014
                return date('m/d/Y', $date);
            case 3:
                // Monday, 1st January 2010, 09:30:56
                return date('l, jS F Y, H:i:s', $date);
            case 4:
                // 2010-01-01-09-30-56
                return date('Y-m-d-H-i-s', $date);
            default:
                return date('Y-m-d H:i:s', $date);
        endswitch;

    }


    public static function get_ip() {
        return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
    }

    public static function getProxyIp() {
        $ip_keys = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED');
        foreach ($ip_keys as $key) :
            if (array_key_exists($key, $_SERVER) === true) :
                foreach (explode(',', $_SERVER[$key]) as $ip) :
                    // trim for safety measures
                    $ip = trim($ip);
                    // attempt to validate IP
                    if (static::validate_ip($ip)) :
                        return $ip;
                    endif;
                endforeach;
            endif;
        endforeach;
        return '';
    }
    /**
     * Ensures an ip address is both a valid IP and does not fall within
     * a private network range.
     */
    public static function validate_ip($ip){
        if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) === false) :
            return false;
        endif;
        return true;
    }
	
    public static function get_location() {
        $octets = explode( '.', static::get_ip() );
        $location = REMOTE;
        if (($octets[0] == '137' && $octets[1] == '52')
            || ($octets[0] == '10' && $octets[1] ==  '232')
            || $octets[0] == '10' && $octets[1] == '254' ) :
            $location = LOCAL;
        endif;
        return $location;
    }

    public static function cleanString($name = null) {
        if (!empty($name)) :
            return strtolower(preg_replace('/[^a-zA-Z0-9.]/', '-', $name));
        endif;
    }

    public static function isGetVar() {
        if(isset($_GET['action']) && ($_GET['action'] == 'receipt')) :
            if (isset($_GET['id']) && !empty($_GET['id'])) :
               return $receipt_id = $_GET['id'];
            else:
                return false;
            endif;
        else :
            return false;
        endif;
    }

    public static function unparse_url($parsed_url) {
        $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
        $host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
        $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
        $user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
        $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
        $pass     = ($user || $pass) ? "$pass@" : '';
        $path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
        $query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
        $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
        return $scheme.$user.$pass.'0-'.$host.'.novacat.nova.edu'.$port.$path.$query.$fragment;
    }

    public static function marps_url($parsed_url) {
        $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
        $host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
        $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
        $port     = ':8080';
        $user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
        $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
        $pass     = ($user || $pass) ? "$pass@" : '';
        $path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
        $query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
        $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
        return $scheme.$user.$pass.$host.$port.$path.$query.$fragment;
    }

    public static function prep_form_vars($form_values) {
        $i = 0;
        $fmt_postvar = '';
        foreach ($form_values as $key => $val) :
            if ($i != 0) :
                $fmt_postvar .= "&";
            endif;
            //$fmt_postvar .= $key . "=" . urlencode($val);
            $fmt_postvar .= $key . "=" . $val;
            $i++;
        endforeach;
        return $fmt_postvar;
    }


}