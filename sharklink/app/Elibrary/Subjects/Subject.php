<?php

namespace App\Elibrary\Subjects;

use \DateTime;
use App\ObjectFactory\ObjectFactory;

class Subject extends ObjectFactory{

    public $_objectFields = [
        'subj_id', 'name'
    ];

    public $subj_id;
    public $name;


}