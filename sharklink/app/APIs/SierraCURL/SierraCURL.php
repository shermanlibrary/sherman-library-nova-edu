<?php

namespace App\APIs\SierraCURL;

use App\Helper\Helper;


class SierraCURL {
    protected $_agent       = 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6 (.NET CLR 3.5.30729)';
    protected $_url         = 'https://novacat.nova.edu/patroninfo';
    protected $_response    = array();


    public function __construct($post_fields = 0, $id = NULL, $url_override = NULL, $no_loop = NULL, $curl_timeout = 6){
        $user = $_SESSION['auth'];


        $this->_setCookieName(($user['patronid']));

        $ch = curl_init();
        if ($post_fields) :
            curl_setopt ($ch, CURLOPT_POST, 1);
            curl_setopt ($ch, CURLOPT_POSTFIELDS, $post_fields);
        endif;
        curl_setopt($ch, CURLOPT_URL, $this->_url);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->_agent);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEJAR, ROOT_PATH.DS.SESSIONS.md5($user['patronid']).'.txt');
        curl_setopt($ch, CURLOPT_COOKIEFILE, ROOT_PATH.DS.SESSIONS.md5($user['patronid']).'.txt');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $curl_response = curl_exec($ch);
        //var_dump($curl_response);die();
        $errormsg = curl_error($ch);
        $errno = curl_errno($ch);
        curl_close($ch);

        $pattern = '/patActionBar/';
        preg_match($pattern, $curl_response, $matches);
        if($matches):
            return $curl_response;
        else:
            return false;
        endif;
    }

    protected function _setCookieName($patronid) {
        $patronid = md5($patronid);
        $fp = fopen(ROOT_PATH.DS.SESSIONS.$patronid.'.txt', 'a');
        fclose($fp);
        return ROOT_PATH.DS.SESSIONS.$patronid .'.txt';
    }

    public static function curlSession($url_suffix, $post_fields = 0, $id = NULL, $url_override = NULL, $no_loop = NULL, $curl_timeout = 6){

        $user = $_SESSION['auth'];

        $agent       = 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6 (.NET CLR 3.5.30729)';

        if ($url_suffix[0] == '/') :
            $url_suffix = substr($url_suffix, 1);
        endif;
        $url_suffix = str_replace('crlslash', '%2F', $url_suffix);
        if ($url_override) :
            $curl_url = $url_override . '/' . $url_suffix;
        else:
            $curl_url = 'https://novacat.nova.edu/'. $url_suffix;
        endif;

         $ch = curl_init();
        if ( $post_fields ) :
            curl_setopt ($ch, CURLOPT_POST, 1);
            curl_setopt ($ch, CURLOPT_POSTFIELDS, $post_fields);
        endif;

        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_URL, $curl_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_COOKIEJAR, ROOT_PATH.DS.SESSIONS.md5($user['patronid']).'.txt');
        curl_setopt($ch, CURLOPT_COOKIEFILE, ROOT_PATH.DS.SESSIONS.md5($user['patronid']).'.txt');
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $curl_timeout);
        curl_setopt($ch, CURLE_OPERATION_TIMEOUTED, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        $curl_response = curl_exec($ch);
        //var_dump($curl_response);die();
        $errormsg = curl_error($ch);

        $errno = curl_errno($ch);
        curl_close($ch);
        return $curl_response;
    }



    public static function getHoldsNumber( $parameters= array() ){
        if(!isset($_SESSION[APPLICATION]['user'])):
            return false;
        endif;

        $user = $_SESSION[APPLICATION]['user'];

        $vars = array( 'code' => $user['univ_id'], 'name' => $user['lastname']);
        $post_fields =  Helper::prep_form_vars($vars);
        new SierraCURL($post_fields);

        unset($parameters['job']);
        $parameters = Helper::prep_form_vars($parameters);

        $results = SierraCURL::curlSession('patroninfo/'.$user['patronid'].'/holds', $parameters );
        $html = new simple_html_dom();
        $html->load($results);
        if(isset($html->find('.errormessage  [!style]',0)->plaintext)):
            $output['renewerror'] = $html->find('#hold_form .errormessage',0)->plaintext;
            if(isset($html->find('#hold_form ul',0)->plaintext)):
                $output['errorlist'] = $html->find('#hold_form ul',0)->innertext;
            endif;
        endif;
        $entries   = $html->find('.patFuncEntry');
        if(!$entries) :
            $html->clear();
            unset($html);
            return false;
        endif;
        $out = array();
        $html->clear();
        unset($html);
        $out['total'] = count($entries);
        return $out;
    }

    public static function placeHold( $string){

        $user = Session::getSession('user');
        $vars = array( 'code' => $user['univ_id'], 'name' => $user['lastname']);
        $post_fields =  Helper::prep_form_vars($vars);
        new SierraCURL($post_fields);

        $output = array();
        $results = SierraCURL::curlSession($string);
        //var_dump($results);die();
        $html = new simple_html_dom();
        $html->load($results);

        $return = null;

        if(isset($html->find('a[href*=frameset~]',0)->href)):
            $return = $html->find('a[href*=frameset~]',0)->href;
        endif;

        $title = null;
        if(isset($html->find('strong',0)->plaintext)):
            $title = $html->find('strong',0)->plaintext;
        endif;

        if(isset($html->find('#errormsg  [!style]', 0)->plaintext)):
            $error = $html->find('#errormsg', 0)->plaintext;
            $output['error']  = static::_setHoldError($error);
            $output['return'] = $return;
            $output['title']  = $title;
        endif;

        if(isset($html->find('.request',0)->outertext)):
            $output['success']  = $html->find('.request',0)->outertext;
            $output['return'] = $return;
            $output['title']  = $title;
        endif;


        if(isset($html->find('#bib_items',0)->plaintext)):
            $entries   = $html->find('.bibItemsEntry');

            $outs = array();
            $out = array();
            if($entries) :
                foreach ($entries as $entry):
                    $out['item'] = $entry->find('input', 0)->value;
                    $out['location'] =  $entry->find('td',1)->plaintext;
                    $out['callnumber'] = $entry->find('td',2)->plaintext;
                    $out['status'] = $entry->find('td',3)->plaintext;
                    $outs[] = $out;
                endforeach;
                $output['return'] = $return;
                $output['items'] = $outs;

            endif;
        endif;
        $html->clear();
        unset($html);
        return $output;
    }

    public static function requestItem( $parameters ){

        $user = Session::getSession('user');
        $vars = array( 'code' => $user['univ_id'], 'name' => $user['lastname']);
        $post_fields =  Helper::prep_form_vars($vars);
        new SierraCURL($post_fields);

        unset($parameters['job']);
        $url = $parameters['url'];
        $string =   'radio='.$parameters['item'];

        $results = SierraCURL::curlSession($url, $string);
        //var_dump($results);die();
        $html = new simple_html_dom();
        $html->load($results);
        $output = array();
        $return = null;

        if(isset($html->find('a[href*=frameset~]',0)->href)):
            $return = $html->find('a[href*=frameset~]',0)->href;
        endif;

        $title = null;
        if(isset($html->find('strong',0)->plaintext)):
            $title = $html->find('strong',0)->plaintext;
        endif;

        if(isset($html->find('#errormsg  [!style]', 0)->plaintext)):
            $error = $html->find('#errormsg', 0)->plaintext;
            $output['error']  = static::_setHoldError($error);
            $output['return'] = $return;
            $output['title']  = $title;
        endif;

        if(isset($html->find('.request',0)->outertext)):
            $output['success']  = $html->find('.request',0)->outertext;
            $output['return'] = $return;
            $output['title']  = $title;
        endif;


        if(isset($html->find('#bib_items',0)->plaintext)):
            $entries   = $html->find('.bibItemsEntry');

            $outs = array();
            $out = array();
            if($entries) :
                foreach ($entries as $entry):
                    $out['item'] = $entry->find('input', 0)->value;
                    $out['location'] =  $entry->find('td',1)->plaintext;
                    $out['callnumber'] = $entry->find('td',2)->plaintext;
                    $out['status'] = $entry->find('td',3)->plaintext;
                    $outs[] = $out;
                endforeach;
                $output['return'] = $return;
                $output['items'] = $outs;

            endif;
        endif;
        $html->clear();
        unset($html);
        return $output;

    }


    protected static function _setHoldError($error){
        if(strpos($error, 'already') !== false):
            return 'You already have a hold or checkout on this item.';
        elseif(strpos($error, 'problem') !== false ):
            $message = 'You may have reached your hold limit or there is an issue with your library record ';
            $message .= 'that is preventing you from placing the hold. ';
            $message .= 'Please contact the library for more information.';
            return $message;
        elseif(strpos($error, 'requestable') !== false ):
            return 'You are not permitted to put a hold on this item.';
        elseif(strpos($error, 'requestable') !== false ):
            return 'The "Cancel if not filled" by date has already passed.';
        else:
            return 'An error occurred that prevented you from placing this hold.';
        endif;
    }



    public static function getCheckouts( $parameters = array() ){


        $user = Session::getSession('user');
        $vars = array( 'code' => $user['univ_id'], 'name' => $user['lastname']);
        $post_fields =  Helper::prep_form_vars($vars);
        new SierraCURL($post_fields);

        unset($parameters['job']);
        $parameters = Helper::prep_form_vars($parameters);


        $results = SierraCURL::curlSession('patroninfo/'.$user['patronid'].'/items', $parameters);

        var_dump($results);
        $html = new simple_html_dom();

        $html->load($results);

        if(isset($html->find('#renewfailmsg [!style]',0)->plaintext)):

            if(isset($html->find('#checkout_form h2',0)->plaintext)):
                $form = $html->find('#checkout_form',0)->innertext;
                if(strpos($form, 'color="red"') !== false ||
                    strpos($form, 'You cannot renew items') !== false ) :
                    $output['renewerror'] = $html->find('#checkout_form h2',0)->plaintext;
               endif;
            endif;

            if(isset($html->find('#checkout_form ul',0)->plaintext)):
                $output['errorlist'] = $html->find('#checkout_form ul',0)->innertext;
            endif;
        endif;

        $entries   = $html->find('.patFuncEntry');
        $outs = array();
        $out = array();
        if($entries) :
            foreach ($entries as $entry):
                if(isset($entry->find('input',0)->value)):
                    $out['item'] = $entry->find('input', 0)->value;
                else:
                    $out['item'] = 'norenew';
                endif;
                $link               = $entry->find('a', 0)->href;
                $title              = $entry->find('.patFuncTitle',0)->plaintext;
                $out['title']       =  '<a href="https://novacat.nova.edu'.$link.'">'.$title.'</a>';
                $out['barcode']     =  $entry->find('.patFuncBarcode',0)->plaintext;
                $out['status']      =  $entry->find('.patFuncStatus',0)->innertext;
                $out['callnumber']  =  $entry->find('.patFuncCallNo',0)->plaintext;
                $outs[] = $out;
            endforeach;
            $output['checkouts'] = $outs;
        else:
            $output['error'] = 'No Checkouts Found';
        endif;

        $checkouts           = Session::getSession('checkouts');
        if(is_array($checkouts)):
            $output['selected']    = array_unique($checkouts);
        else:
            $output['selected']   = array();
        endif;
        $html->clear();
        unset($html);
        return $output;
    }

    public static function getCheckoutsNumber( $parameters = array() ){

        if(!isset($_SESSION[APPLICATION]['user'])):
            return false;
        endif;

        $user = $_SESSION[APPLICATION]['user'];

        $vars = array( 'code' => $user['univ_id'], 'name' => $user['lastname']);
        $post_fields =  Helper::prep_form_vars($vars);
        new SierraCURL($post_fields);

        unset($parameters['job']);
        $parameters = Helper::prep_form_vars($parameters);

        $results = SierraCURL::curlSession('patroninfo/'.$user['patronid'].'/items', $parameters);
        $html = new simple_html_dom();
        $html->load($results);
        if(isset($html->find('#renewfailmsg [!style]',0)->plaintext)):
            $output['renewerror'] = $html->find('#checkout_form .errormessage',0)->plaintext;
            if(isset($html->find('#checkout_form ul',0)->plaintext)):
                $output['errorlist'] = $html->find('#checkout_form ul',0)->innertext;
            endif;
        endif;

        $entries   = $html->find('.patFuncEntry');

        if(!$entries) :
            $html->clear();
            unset($html);
            return false;
        endif;
        $out = array();
        $html->clear();
        unset($html);
        $out['total'] = count($entries);
        return $out;
    }

    public static function getFeesInfo( $parameters = array() ){

        if(!isset($_SESSION[APPLICATION]['user'])):
            return false;
        endif;

        $user = $_SESSION[APPLICATION]['user'];

        $vars = array( 'code' => $user['univ_id'], 'name' => $user['lastname']);
        $post_fields =  Helper::prep_form_vars($vars);
        new SierraCURL($post_fields);

        unset($parameters['job']);
        $parameters = Helper::prep_form_vars($parameters);

        $results = SierraCURL::curlSession('patroninfo/'.$user['patronid'].'/overdues', $parameters);
        $html = new simple_html_dom();
        $html->load($results);
        if(isset($html->find('#renewfailmsg [!style]',0)->plaintext)):
            $output['renewerror'] = $html->find('#checkout_form .errormessage',0)->plaintext;
            if(isset($html->find('#checkout_form ul',0)->plaintext)):
                $output['errorlist'] = $html->find('#checkout_form ul',0)->innertext;
            endif;
        endif;

        $out = array();
        $entries   = $html->find('.patFuncFinesDetailAmt');

        foreach($entries as $entry):
            $entry = $entry->plaintext;
            trim($entry);
            $out[] = $entry = str_replace(array('$','.'), '', $entry);
        endforeach;

        if(!$entries) :
            return false;
        else:
            $outs = array();
            $outs['total'] = count($out);
            $balance = array_sum($out);
            $outs['balance'] = Helper::formatCurrency($balance);
            return $outs;
        endif;
    }

    public static function novacatForm( $curl_timeout = 6){
        $user = Session::getSession('user');
        $agent       = 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.6) Gecko/2009011913 Firefox/3.0.6 (.NET CLR 3.5.30729)';
        $curl_url = 'https://systemsdev.library.nova.edu/auth/api/novacat.php';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_URL, $curl_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $curl_timeout);
        curl_setopt($ch, CURLE_OPERATION_TIMEOUTED, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        $curl_response = curl_exec($ch);
       //var_dump($curl_response);die();
        $errormsg = curl_error($ch);
        $errno = curl_errno($ch);
        curl_close($ch);
        return $curl_response;
    }

    public static function saveSearch( $string){
        $user = Session::getSession('user');
        $vars = array( 'code' => $user['univ_id'], 'name' => $user['lastname']);
        $post_fields =  Helper::prep_form_vars($vars);
        new SierraCURL($post_fields);
        $output = array();
        $results = SierraCURL::curlSession($string);
        if($results):
            $string = str_replace('&SUBPREF=t','', $string);
            $string = '//novacat.nova.edu/'.$string;
            Helper::redirect($string);
        endif;
        //
       // var_dump($results);die();
       // $html = new simple_html_dom();
       // $html->load($results);
        //

    }

}

