<?php
namespace App\Session;

class Session{
    private $logged_in = false;
    private static $sessid;
    private $status = 'public';
    public  $user_id;
    public  $message;
    private $_fine_count = 0;
    private $_subtotal   = 0;
    private $_paytotal   = 0;

    function __construct(){
        //$this->check_status();
		//$this->setSessionTimer();
        //$this->setPayment();
        //$this->checkConfirm();
        //$this->_setFineInfo();
    }

    public static function session_id(){
        return self::$sessid = session_id();
    }



	private function setSessionTimer(){
		if(isset($_SESSION)) :
			if (isset($_SESSION[APPLICATION]['LAST_ACTIVITY']) && (time() - $_SESSION[APPLICATION]['LAST_ACTIVITY'] > 1800)) :
				// last request was more than 30 minutes ago
				session_unset();     // unset $_SESSION variable for the run-time
				session_destroy();   // destroy session data in storage
			endif;
			$_SESSION[APPLICATION]['LAST_ACTIVITY'] = time(); // update last activity time stamp
		endif;
	}

    public function message( $msg = ""){
        if(!empty($msg)) :
            $_SESSION[APPLICATION]['message'] = $msg;
        else :
            return $this->message;
        endif;

        return true;
    }

    public function check_status(){
        return isset($_SESSION[APPLICATION]['status']) ? $_SESSION[APPLICATION]['status'] : 'public';
    }

    public static function getSession($name = null) {
        if (!empty($name)) :
            return isset($_SESSION[APPLICATION][$name]) ? $_SESSION[APPLICATION][$name] : null;
        endif;

        return true;
    }

    public static function setSession($name = null, $value = null) {
        if (!empty($name) && !empty($value)) :
            $_SESSION[APPLICATION][$name] = $value;
        endif;
    }

    public static function clear($id = null) {
        if (!empty($id) && isset($_SESSION[$id])) :
            $_SESSION = array();
            $_SESSION[$id] = null;
            unset($_SESSION[$id]);
        else :
            $_SESSION = array();
            session_destroy();
        endif;
    }
}


$session = new Session();
//$status  = $session->check_status();



