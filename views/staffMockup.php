<div  data-ng-controller="AccountController as my" id="account">

<div class="has-background background-base">
	<div class="clearfix hero wrap">
		<h1 class="col-md--eightcol col--centered">Welcome, {{ac.info.display_name}}!</h1>
		<p class="col-md--eightcol col--centered">
	    From here you can manage your library account as well as watch the status
			of any holds, renewals, and other services.
		</p>
	</div>
</div>

<div class="alert alert--warning no-margin">
  <div class="clearfix wrap">
    <p class="zeta no-margin"><svg class="svg" fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg" style="position: relative; height: 24px; top: 5px;">
      <path d="M12 22c1.1 0 2-.9 2-2h-4c0 1.1.89 2 2 2zm6-6v-5c0-3.07-1.64-5.64-4.5-6.32V4c0-.83-.67-1.5-1.5-1.5s-1.5.67-1.5 1.5v.68C7.63 5.36 6 7.92 6 11v5l-2 2v1h16v-1l-2-2z"/>
    </svg>
    If you have an alert related to your account it would be here.</p>
  </div>
</div>

<div class="has-cards">
	<div class="clearfix hero wrap">

		<div class="col-md--fourcol">
			<nav data-ng-include="'views/sidebar.html'"></nav>
		</div>

		<div class="col-md--eightcol">

      Content here.

	  </div>
</div>
</div>
