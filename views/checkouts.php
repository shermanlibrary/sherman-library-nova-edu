<div ng-controller="CheckoutsController as cc" ng-cloak>
	

    <header class="align-center col-md--tencol col--centered">	
        <h1>My Library Checkouts</h1>
		
		
		<div wc-overlay wc-overlay-delay="300">
					We are loading the items you have checked out ....
		</div>	
    </header>
   
      <div class="clearfix hero wrap">
        <div class="col-md--twelvecol col--centered">			

            <section ng-if="cc.checkouts">  
		
				
					<form class="form" role="form">
						<input class="form__input" type="text" placeholder="Search for checkouts" ng-model="query" autofocus>
						<label for="paging"> Items per page:</label>
						<input id="paging" type="number" min="1" max="{{cc.checkouts.length}}" class="form-control form__input" ng-model="cc.pageSize">
					</form>
				
				<div ng-if="cc.showbutton">	
					<button class="button button--small button--default no-margin small-text"
							ng-show="cc.selected.length <  cc.showbutton"
							ng-click="cc.fillCart()">
						Select All
					</button>
				
					<button class="button button--small button--default no-margin small-text"
							ng-show="cc.selected.length == cc.showbutton"
							ng-click="cc.emptyCart()">
						Deselect All
					</button>
					

					<button class="button button--small base no-margin small-text"
							ng-show="cc.selected.length > 0"
							ng-click="cc.formSubmit()">
						Submit
					</button>
				</div>
	
                <table class="table table--responsive">
                    <thead class="has-cards">
						<tr>
							<th ng-if="cc.showbutton"> RENEW </th>
							<th><a ng-class="cc.sortType == 'title' ? 'delta' : ''" href ng-click="cc.sortType = 'title'; cc.sortReverse = !cc.sortReverse"> TITLE</a></th>
							<th><a ng-class="cc.sortType == 'barcode' ? 'delta' : ''" href ng-click="cc.sortType = 'barcode'; cc.sortReverse = !cc.sortReverse"> BARCODE</a></th>
							<th ng-show="!cc.hidestatus"><a ng-class="cc.sortType == 'status' ? 'delta' : ''" href ng-click="cc.sortType = 'status'; cc.sortReverse = !cc.sortReverse"> STATUS</a></th>
							<th><a ng-class="cc.sortType == 'renewals' ? 'delta' : ''" href ng-click="cc.sortType = 'renewals'; cc.sortReverse = !cc.sortReverse"> RENEWED </a></th>
							<th><a ng-class="cc.sortType == 'checkout_stamp' ? 'delta' : ''" href ng-click="cc.sortType = 'checkout_stamp'; cc.sortReverse = !cc.sortReverse"> CHECKOUT DATE</a></th>
							<th><a ng-class="cc.sortType == 'duedate_stamp' ? 'delta' : ''" href ng-click="cc.sortType = 'duedate_stamp'; cc.sortReverse = !cc.sortReverse"> DUE DATE</a></th>
							<th><a ng-class="cc.sortType == 'callnumber' ? 'delta' : ''" href ng-click="cc.sortType = 'callnumber'; cc.sortReverse = !cc.sortReverse"> CALL NUMBER</a></th>

						</tr>	
                    </thead>

                    <tbody dir-paginate="checkout in cc.checkouts | filter:query  | orderBy:cc.sortType:cc.sortReverse| itemsPerPage: cc.pageSize" current-page="cc.currentPage">
                    <tr checkout-result checkout="checkout"
                        selected = "cc.selected"
                        add="cc.addToCart"
                        remove="cc.removeFromCart"
						showbutton="cc.showbutton"
						hidestatus="cc.hidestatus"
                        >
                    </tr>
                    </tbody>
                </table>
                <div ng-controller="PagingController" class="paging-controller">
                    <div class="text-center">
                        <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="assets/js/templates/dirPagination.html"></dir-pagination-controls>
                    </div>
                </div>
            </section>
            <div ng-if="cc.error" ng-cloak>
                {{cc.error}}
            </div>
    </div>
      </div><!--/.hero-->

</div>
