<div ng-controller="CheckoutsConfirmController as cc" ng-cloak>

    <header class="align-center col-md--eightcol col--centered">
        <h1>My Library Checkouts</h1>
    </header>

    <div class="col-md--eightcol col--centered">

        <section id="has-cart" ng-if="cc.cart">
                <div ng-if="cc.cart.length === 0">
                    No Items Found.
                  <div class="align-right">
                      <a class="button button--small button--default" ng-href="/checkouts">Go Back</a>
                  </div>
                </div>
                <div ng-if="cc.cart.length !== 0">
				
					<form class="form" role="form">
						<input class="form__input" type="text" placeholder="Search for checkouts" ng-model="query" autofocus>
						<label for="paging"> Items per page:</label>
						<input id="paging" type="number" min="1" max="{{cc.cart.length}}" class="form-control form__input" ng-model="cc.pageSize">
					</form>
				
				
                    <table class="table table--responsive">
                    <thead class="has-cards">
						<tr>
							<th></th>
							<th><a ng-class="cc.sortType == 'title' ? 'delta' : ''" href ng-click="cc.sortType = 'title'; cc.sortReverse = !cc.sortReverse"> TITLE </a></th>
							<th><a ng-class="cc.sortType == 'barcode' ? 'delta' : ''" href ng-click="cc.sortType = 'barcode'; cc.sortReverse = !cc.sortReverse"> BARCODE </a></th>
							<th><a ng-class="cc.sortType == 'renewals' ? 'delta' : ''" href ng-click="cc.sortType = 'renewals'; cc.sortReverse = !cc.sortReverse"> RENEWALS </a></th>
							<th><a ng-class="cc.sortType == 'checkout_stamp' ? 'delta' : ''" href ng-click="cc.sortType = 'checkout_stamp'; cc.sortReverse = !cc.sortReverse"> CHECKOUT DATE</a></th>
							<th><a ng-class="cc.sortType == 'duedate_stamp' ? 'delta' : ''" href ng-click="cc.sortType = 'duedate_stamp'; cc.sortReverse = !cc.sortReverse"> DUE DATE </a></th>
							<th><a ng-class="cc.sortType == 'callnumber' ? 'delta' : ''" href ng-click="cc.sortType = 'callnumber'; cc.sortReverse = !cc.sortReverse"> CALL NUMBER </a></th>	
						</tr>	
                    </thead>

                    <tbody dir-paginate="checkout in cc.cart | filter:query  | orderBy:cc.sortType:cc.sortReverse| itemsPerPage: cc.pageSize" current-page="cc.currentPage">
                    <tr checkout-cart checkout="checkout"  
						cart = "cc.selected"					
                        remove="cc.removeFromCart"					
                        >
                    </tr>
                    </tbody>
                </table>
                    <div class="align-right">

                       <a class="button button--small button--default" ng-href="/checkouts">Go Back</a>

                        <button input type="submit" class="button button--small button--primary"
                                ng-click="cc.checkoutsRenew()">
                            Renew {{ cc.cart.length  === 1 ? 'Item' : 'Items' }}
                        </button>
                    </div>
                </div>
        </section>

        <section ng-if="cc.checkouts">
            <div class="alert alert--warning" role="alert" ng-if="cc.renewerror">
                {{cc.renewerror}}
                <ul ng-bind-html="cc.errorlist">
                </ul>
            </div>

            <form class="form" role="form">
                <input class="form__input" type="text" placeholder="Search for checkouts" ng-model="query" autofocus>
                <label for="paging"> Items per page:</label>
                <input id="paging" type="number" min="1" max="{{cc.checkouts.length}}" class="form-control form__input" ng-model="cc.pageSize">
            </form>
            <button class="button button--small button--default no-margin small-text"
                    ng-show="cc.selected.length < cc.checkouts.length"
                    ng-click="cc.fillCart()">
                Select All
            </button>
            <button class="button button--small button--default no-margin small-text"
                    ng-show="cc.selected.length === cc.checkouts.length"
                    ng-click="cc.emptyCart()">
                Deselect All
            </button>
            <button class="button button--small button--default no-margin small-text"
                    ng-click="cc.cartSort()">
                Sort by {{ cc.duedate ? ' Due Date' : ' Date Checked Out'}}
            </button>

            <button class="button button--small base no-margin small-text"
                    ng-show="cc.selected.length > 0"
                    ng-click="cc.formSubmit()">
                Submit
            </button>


            <table class="table table--responsive">
                <thead class="has-cards">
                <tr>
                    <th> RENEW </th>
                    <th> TITLE  </th>
                    <th> BARCODE </th>
                    <th> STATUS </th>
                    <th> CALL NUMBER </th>
                </tr>
                </thead>

                <tbody dir-paginate="checkout in cc.checkouts | filter:query |  itemsPerPage: cc.pageSize" current-page="cc.currentPage">
                <tr checkout-result checkout="checkout"
                    selected = "cc.selected"
                    add="cc.addToCart"
                    remove="cc.removeFromCart"
                    >
                </tr>
                </tbody>
            </table>
            <div ng-controller="PagingController" class="paging-controller">
                <div class="text-center">
                    <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="assets/js/templates/dirPagination.html"></dir-pagination-controls>
                </div>
            </div>
        </section>
    </div>

</div>




    


