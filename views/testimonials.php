<style>
  .columns {
    -webkit-column-break-inside: avoid; /* Chrome, Safari, Opera */
    page-break-inside: avoid; /* Firefox */
    break-inside: avoid; /* IE 10+ */
    column-count: 3;
  }

  .columns > div {
    -webkit-column-break-inside: avoid; /* Chrome, Safari, Opera */
    page-break-inside: avoid; /* Firefox */
    break-inside: avoid; /* IE 10+ */
  }

  .columns--3 {
    column-width: 33.333333%;

  }

  blockquote {
    margin: auto;
  }
</style>

<h1 class="hide-accessible">Testimonials about the Alvin Sherman Library</h1>

<header class="has-background background-base hero">
  <p class="col-lg--eightcol col--centered">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, et, expedita! Delectus ducimus id illo laudantium quam quas quos sint, voluptates. Alias dolores ducimus ea maiores nostrum sequi temporibus voluptas.</p>
</header>

<div class="has-cards hero" data-ng-controller="AdController as adc" data-audience="testimonials">

  <div class="columns columns--3 clearfix wrap">

    <div data-ng-repeat="ad in adc.ads" data-ng-cloak>
      <blockquote class="card">
        <section class="card__content">
          <p class="type-sm--zeta type-lg--epsilon no-margin">{{ ad.excerpt }}</p>
        </section>
      </blockquote>
    </div>


  </div>

</div>