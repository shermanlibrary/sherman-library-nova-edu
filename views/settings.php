<div  data-ng-controller="AccountController as my" id="account">

  <div class="has-background background-base">
  	<div class="clearfix hero wrap">
  		<h1 class="col-md--eightcol col--centered">Welcome, {{ac.info.display_name}}!</h1>
  		<p class="col-md--eightcol col--centered">
  	    From here you can manage your library account as well as watch the status
  			of any holds, renewals, and other services.
  		</p>
  	</div>
  </div>


  <div class="has-cards">
  	<div class="clearfix hero wrap">

  		<div class="col-md--fourcol">
  			<nav data-ng-include="'views/sidebar.html'"></nav>
  		</div>

      <div class="col-md--eightcol">
        <div class="card" style="margin-bottom: 1em;">
          <header class="card__header">
            <h2 class="delta">Personal Information</h2>
          </header>

          <section class="content">
            <p ng-if="!ac.info.invalid_address">
                          <span ng-if="ac.info.billname">{{ac.info.billname}} <br></span>
                          <span ng-if="!ac.info.billname">{{ac.info.display_name}} <br></span>
              <span ng-if="ac.info.email">{{ac.info.email}} <br></span>
                          <span ng-if="ac.info.telephone">{{ac.info.telephone}} <br></span>
                          <span ng-if="ac.info.address_line1">{{ac.info.address_line1}} <br></span>
                          <span ng-if="ac.info.address_line2">{{ac.info.address_line2}} <br></span>
                          <span ng-if="ac.info.city">{{ac.info.city}}<span ng-if="ac.info.state">, {{ac.info.state}} <span ng-if="ac.info.zip">{{ac.info.zip}}<br></span>
            </p>

                      <span ng-if="ac.info.col === 'n'">
                          <span ng-bind-html="ac.update_message"></span>
                          Update Home Library  <a href class="small-text" data-ng-click="ac.showModalWindow({element: 'homelib'})">(What's this?)</a>
                          <select ng-model="ac.selectedLibrary" ng-options="library.code as library.label for library in ac.libraries">
                          </select>
                          <button
                              class="button button--small small-text"
                              ng-class="ac.selectedLibrary === ac.info.home_lib ? 'button--disabled' : 'button--default'"
                              ng-disabled="ac.selectedLibrary === ac.info.home_lib"
                              ng-click="ac.updateHomeLibrary()">Update</button>
                      </span>


                      <span ng-if="ac.info.col === 'p'">
                          <span ng-bind-html="ac.update_message"></span>
                          Update Email
                          <input
                              ng-model="ac.email_update"
                              ng-change="ac.update_message = ''" />
                          <button
                              class="button button--small small-text"
                              ng-class="ac.email_update === ac.info.email ? 'button--disabled' : 'button--default'"
                              ng-disabled="ac.email_update === ac.info.email"
                              ng-click="ac.updateEmail()">Update</button>
                      </span>
          </section>
        </div>
      </div>

  	</div>
  </div>

</div>
