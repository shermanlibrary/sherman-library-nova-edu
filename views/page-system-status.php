<style>

.card-list__item {
  padding: .5em 1em;
  border-bottom: 1px solid #ddd;
}

.list--inline dt {
  font-weight: normal;
}

.card--no-border {
  border: none;
}

.card--gray {
  background-color: #f5f5f5;
  margin-bottom: 1em;

}

.card.bug {
}

.card.proposal {
}

.card.enhancement {
}

.valert {
  background-color: #f5f5f5;
  border-bottom-right-radius: 2px;
  border-left: 4px solid #ddd;
  border-top-right-radius: 2px;
  padding: .5em 1em;
  position: relative;
  color: #444;
}

.card--gray .valert { background-color: #fff; }

.valert::before {
  background-color: #ddd;
  border-radius: 100%;
  color: white;
  content: "!";
  font-family: Arial, sans-serif;
  font-size: 14px;
  font-weight: bold;
  height: 20px;
  left: -12px;
  line-height: 20px;
  position: absolute;
  text-align: center;
  top: 14px;
  width: 20px;
}
.valert.bug {
  border-left-color: #e2624f;
}
.valert.bug::before {
  background-color: #e2624f;
}
.valert.bug.trivial::before {
  background-color: #efa89e;
}
.valert.bug.trivial {
  border-left-color: #efa89e;
}
.valert.bug.minor::before {
  background-color: #e88576;
}
.valert.bug.minor {
  border-left-color: #e88576;
}
.chip {
  background-color: #f5f5f5;
  border-radius: 1em;
  display: inline-block;
  color: #444;
  margin: .25em 0;
  padding: 0 1em 0 0;
  white-space: nowrap;
}

.chip__badge {
  background-color: #aaa;
  color: white;
  display: inline-block;
  border-radius: 2em;
  height: 2em;
  margin-right: .25em;
  line-height: 2em;
  vertical-align: middle;
  overflow: hidden;
  text-align: center;
  width: 2em;
}

.chip__badge--bug {
  background-color: #e2624f;
}

.chip__badge--proposal {
  background-color: #ffae3d;
}

.chip__badge--enhancement {
  background-color: #21aabd;
}

.chip__text {
  display: inline-block;
  font-family: Arial, 'sans-serif';
  font-size: 90%;
  vertical-align: middle;
}

@media screen and ( min-width: 768px ) {
  .masonry {
    column-count: 2;
  }

  .masonry__item {
    -webkit-column-break-inside: avoid;
    page-break-inside: avoid;
    break-inside: avoid;
  }
}
</style>

<div class="clearfix has-cards">
  <div class="clearfix hero--small wrap">

    <div class="clearfix">
      <div class="col-md--fourcol">

        <h2 class="gamma"><span style="color: #e2624f;">❤</span> Dashboard</h2>
        <p>
          Some of our applications and systems impact our whole ecosystem,
          and when one isn't feeling well they all suffer. Here, you can
          get a feel for the general well being of our web services.
        </p>
        <p>Curious about <a href="https://trello.com/b/5wflMskO/web-services-dashboard">what's next for web services?</a></p>

      </div>
      <div class="col-md--fourcol">
        <ng-controller data-ng-controller="SystemsStatusController as sc" data-repository="content">
          <a data-ng-href="status/content" class="link link--undecorated">
            <div class="card">
              <h3 class="card__title no-margin epsilon">Content</h3>
              <p class="small-text">Where we do our best to keep content up to date, accurate, and good.</p>
              <span class="chip small-text" data-ng-if="sc.tasks">
                <span class="chip__badge">{{ sc.tasks }}</span>
                <span class="chip__text">Tasks</span>
              </span>
              <span class="chip small-text" data-ng-if="sc.proposals">
                <span class="chip__badge chip__badge--proposal">{{ sc.proposals }}</span>
                <span class="chip__text">Ideas</span>
              </span>
              <span class="chip small-text" data-ng-if="sc.enhancements">
                <span class="chip__badge chip__badge--enhancement">{{ sc.enhancements }}</span>
                <span class="chip__text">Enhancements</span>
              </span>
              <span class="chip small-text" data-ng-if="sc.bugs">
                <span class="chip__badge chip__badge--enhancement">{{ sc.bugs }}</span>
                <span class="chip__text">Bugs</span>
              </span>
            </div>
          </a>
        </ng-controller>

        <ng-controller data-ng-controller="SystemsStatusController as sc" data-repository="electronic-resources">
          <a data-ng-href="status/electronic-resources" class="link link--undecorated">
            <div class="card">
              <h3 class="card__title no-margin epsilon">Electronic Resources</h3>
              <p class="small-text">Databases, Full Text Finder</p>
              <span class="chip small-text" data-ng-if="sc.tasks">
                <span class="chip__badge">{{ sc.tasks }}</span>
                <span class="chip__text">Tasks</span>
              </span>
              <span class="chip small-text" data-ng-if="sc.proposals">
                <span class="chip__badge chip__badge--proposal">{{ sc.proposals }}</span>
                <span class="chip__text">Ideas</span>
              </span>
              <span class="chip small-text" data-ng-if="sc.enhancements">
                <span class="chip__badge chip__badge--enhancement">{{ sc.enhancements }}</span>
                <span class="chip__text">Enhancements</span>
              </span>
              <span class="chip small-text" data-ng-if="sc.bugs">
                <span class="chip__badge chip__badge--bug">{{ sc.bugs }}</span>
                <span class="chip__text">Bugs</span>
              </span>
            </div>
          </a>
        </ng-controller>


        <ng-controller data-ng-controller="SystemsStatusController as sc" data-repository="sherman-library-nova-edu">
          <a data-ng-href="status/sherman-library-nova-edu" class="link link--undecorated">
            <div class="card">
              <h3 class="card__title no-margin epsilon">Design System</h3>
              <p class="small-text">Generally the look-and-feel of our applications and sites.</p>
              <span class="chip small-text" data-ng-if="sc.tasks">
                <span class="chip__badge">{{ sc.tasks }}</span>
                <span class="chip__text">Tasks</span>
              </span>
              <span class="chip small-text" data-ng-if="sc.proposals">
                <span class="chip__badge chip__badge--proposal">{{ sc.proposals }}</span>
                <span class="chip__text">Ideas</span>
              </span>
              <span class="chip small-text" data-ng-if="sc.enhancements">
                <span class="chip__badge chip__badge--enhancement">{{ sc.enhancements }}</span>
                <span class="chip__text">Enhancements</span>
              </span>
              <span class="chip small-text" data-ng-if="sc.bugs">
                <span class="chip__badge chip__badge--bug">{{ sc.bugs }}</span>
                <span class="chip__text">Bugs</span>
              </span>
            </div>
          </a>
        </ng-controller>

      </div>
      <div class="col-md--fourcol">

      <ng-controller data-ng-controller="SystemsStatusController as sc" data-repository="libguides">
        <a data-ng-href="status/libguides" class="link link--undecorated">
          <div class="card">
            <h3 class="card__title no-margin epsilon">LibApps</h3>
            <p class="small-text">LibGuides, LibWizard</p>
            <span class="chip small-text" data-ng-if="sc.tasks">
              <span class="chip__badge">{{ sc.tasks }}</span>
              <span class="chip__text">Tasks</span>
            </span>
            <span class="chip small-text" data-ng-if="sc.proposals">
              <span class="chip__badge chip__badge--proposal">{{ sc.proposals }}</span>
              <span class="chip__text">Ideas</span>
            </span>
            <span class="chip small-text" data-ng-if="sc.enhancements">
              <span class="chip__badge chip__badge--enhancement">{{ sc.enhancements }}</span>
              <span class="chip__text">Enhancements</span>
            </span>
            <span class="chip small-text" data-ng-if="sc.bugs">
              <span class="chip__badge chip__badge--bug">{{ sc.bugs }}</span>
              <span class="chip__text">Bugs</span>
            </span>
          </div>
        </a>
      </ng-controller>

      <ng-controller data-ng-controller="SystemsStatusController as sc" data-repository="novacat">
        <a data-ng-href="status/novacat" class="link link--undecorated">
          <div class="card">
            <h3 class="card__title no-margin epsilon">NovaCat</h3>
            <p class="small-text">Catalog, Find It</p>
            <span class="chip small-text" data-ng-if="sc.tasks">
              <span class="chip__badge">{{ sc.tasks }}</span>
              <span class="chip__text">Tasks</span>
            </span>
            <span class="chip small-text" data-ng-if="sc.proposals">
              <span class="chip__badge chip__badge--proposal">{{ sc.proposals }}</span>
              <span class="chip__text">Ideas</span>
            </span>
            <span class="chip small-text" data-ng-if="sc.enhancements">
              <span class="chip__badge chip__badge--enhancement">{{ sc.enhancements }}</span>
              <span class="chip__text">Enhancements</span>
            </span>
            <span class="chip small-text" data-ng-if="sc.bugs">
              <span class="chip__badge chip__badge--bug">{{ sc.bugs }}</span>
              <span class="chip__text">Bugs</span>
            </span>
          </div>
        </a>
      </ng-controller>

      <ng-controller data-ng-controller="SystemsStatusController as sc" data-repository="asl-wp-theme">
        <a data-ng-href="status/asl-wp-theme" class="link link--undecorated">
          <div class="card">
            <h3 class="card__title no-margin epsilon">WordPress</h3>
            <p class="small-text">Our pretty robust WordPress network</p>
            <span class="chip small-text" data-ng-if="sc.tasks">
              <span class="chip__badge">{{ sc.tasks }}</span>
              <span class="chip__text">Tasks</span>
            </span>
            <span class="chip small-text" data-ng-if="sc.proposals">
              <span class="chip__badge chip__badge--proposal">{{ sc.proposals }}</span>
              <span class="chip__text">Ideas</span>
            </span>
            <span class="chip small-text" data-ng-if="sc.enhancements">
              <span class="chip__badge chip__badge--enhancement">{{ sc.enhancements }}</span>
              <span class="chip__text">Enhancements</span>
            </span>
            <span class="chip small-text" data-ng-if="sc.bugs">
              <span class="chip__badge chip__badge--bug">{{ sc.bugs }}</span>
              <span class="chip__text">Bugs</span>
            </span>
          </div>
        </a>
      </ng-controller>

    </div>
    </div>

    <div class="clearfix">
      <div class="col-md--fourcol"></div>
      <div class="col-md--eightcol">
        <h3 class="hide-accessible">Leave Feedback</h3>
        <form action="//sherman.library.nova.edu/reporter/index.php" method="post" role="form" name="reporter" class="form">

              <input type="hidden" name="action" value="status_submit">
              <input type="hidden" name="url" value="http://systemsdev.library.nova.edu/status/">
              <input type="hidden" name="status" value="Submitted">
              <input type="hidden" name="application_url" value="https://systemsdev.library.nova.edu/status">
              <input type="hidden" name="user" value="Guest">
              <input type="hidden" name="full_name" value="{{ ac.info.billname }}">

              <ul>
                <li class="form__field">
                  <input class="checkbox-toggle" type="radio" name="sort" id="idea" data-ng-model="sort" value="idea">
                  <label class="button button--small orange" for="idea" data-ng-class="{ 'button--flat' : sort === 'idea'}">Share an idea</label>

                  <input class="checkbox-toggle" type="radio" name="sort" id="question" data-ng-model="sort" value="question">
                  <label class="button button--small green" for="question" data-ng-class="{ 'button--flat' : sort === 'question'}">Ask a question</label>

                  <input class="checkbox-toggle" type="radio" name="sort" id="report" data-ng-model="sort" value="report">
                  <label class="button button--small coral" for="report" data-ng-class="{ 'button--flat' : sort === 'report'}">Report a problem</label>
                </li>

                <li class="form__field" data-ng-if="sort === 'report'">
                  <label class="form__label" for="issue_type">Can this issue fit nicely in one of these categories?</label>
                  <div class="form__select">
                    <select name="issue_type" id="category" data-ng-model="category" data-ng-init="category = 'content'">
                      <option value="idea" data-ng-if="sort !== 'report'">I have a neat idea ...</option>
                      <option value="content">Content Correction or Update</option>
                      <option value="copiers" disabled>Computers, Copiers, or Printers</option>
                      <option value="electronic-resources">Databases or Electronic Resources</option>
                      <option value="asl-wp-event-plugin">Event Management</option>
                      <option value="electronic-resources">Full Text Finder</option>
                      <option value="libguides">LibApps</option>
                      <option value="asl-wp-event-plugin">List Maker</option>
                      <option value="novacat">NovaCat</option>
                      <option value="other">Hmm. No, none of these.</option>
                    </select>
                  </div>
                  <input type="hidden" name="application" value="{{ category ? category : 'Services Status Dashboard'}}">
                </li>

                <li class="form__field" data-ng-if="category && sort !== 'idea' && sort !== 'question'">
                  <label for="issue_type" class="form__label" data-ng-if="sort === 'report'">Tell us about it.</label>
                  <div class="form__select">
                    <select name="issue_type" id="issue_type">
                      <option value="typo" data-ng-if="sort === 'report'">I found a typo</option>
                      <option value="cataloging" data-ng-if="category === 'novacat'">It's showing the wrong cover</option>
                      <option value="functionality" data-ng-if="sort === 'report'">Something's broken or not displaying correctly</option>
                      <option value="cataloging" data-ng-if="category === 'novacat' || sort === 'suggestion'">I want to suggest a purchase</option>
                      <option value="other">Something else</option>
                    </select>
                  </div>
                </li>

                <li class="form__field" data-ng-if="sort">
                  <label for="comment">
                    <ng-if data-ng-if="sort === 'idea'">What's your idea? We can't wait to hear.</ng-if>
                    <ng-if data-ng-if="sort === 'question'">How can we help?</ng-if>
                    <ng-if data-ng-if="sort === 'suggestion'">What's on your mind?</ng-if>
                    <ng-if data-ng-if="sort === 'report'">Yikes! Tell us about it.</ng-if>
                  </label>
                  <textarea name="comment" id="" cols="30" rows="10" class="form__input textarea form__input--full-width"></textarea>
                </li>

                <li data-ng-if="sort">
                  <button class="primary button button--primary button--small small-text" type="submit">Submit</button>
                </li>
              </ul>
            </form>
      </div>
    </div>
  </div>
</div>

<ng-controller data-ng-controller="IssuesController as ic">

  <ng-if data-ng-if="ic.issues">
    <div class="hero">
      <div class="clearfix wrap">
      <div class="col-md--fourcol">
        <nav role="navigation">
          <ul class="list">
            <li>
              <a ng-href="/status/{{ ic.repository }}/?state=new">New</a>
              <p class="small-text no-margin"><b>New</b> issues have been reported but haven't yet been addressed.</p>
            </li>
            <li>
              <a ng-href="/status/{{ ic.repository }}/?state=open">Open</a>
              <p class="small-text no-margin"><b>Open</b> issues are currently being investigated or worked on.</p>
            </li>
            <li>
              <a ng-href="/status/{{ ic.repository }}/?state=resolved">Resolved</a>
              <p class="small-text no-margin"><b>Resolved</b> issues should be good to go.</p>
            </li>
          </ul>
        </nav>
      </div>
      <div class="col-md--eightcol clearfix">

        <ul class="list">
          <a href="{{ issue.links.html.href }}" class="link link--undecorated" data-ng-repeat="issue in ic.issues" data-ng-if="ic.issues">
            <li class="card" data-ng-class="[ issue.kind, issue.priority ]">
              <p class="no-margin">{{ issue.content.raw ? issue.title + issue.content.raw : issue.title }}</p>
              <p class="small-text" style="margin-top: 1em;"><b> {{ issue.created_on === issue.updated_on ? 'Created' : 'Updated' }} on</b> {{ issue.updated_on | date : 'longDate' }}. This <b>{{ issue.kind }}</b> is <b>{{ issue.state }}</b>.</p>
            </li>
          </a>
        </ul>

        <article data-ng-if="ic.issues.length == 0">
          Looks like everything is running smoothly.
        </article>
      </div>
      </div>
    </div>
  </ng-if>
</ng-controller>
