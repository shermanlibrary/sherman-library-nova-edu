<div ng-controller="CheckoutsRenewController  as cc" ng-cloak>

		<header class="align-center col-md--eightcol col--centered">
			<h1>My Library Checkouts</h1>
		</header>
		
		<div wc-overlay wc-overlay-delay="300">
				We are trying to renew the items you selected now ....
	     </div>

		<div class="col-md--eightcol col--centered">
			
						<form class="form" role="form">
							<input class="form__input" type="text" placeholder="Search for checkouts" ng-model="query" autofocus>
							<label for="paging"> Items per page:</label>
							<input id="paging" type="number" min="1" max="{{cc.renewal.length}}" class="form-control form__input" ng-model="cc.pageSize">
						</form>
					
					
						<table class="table table--responsive">
							<thead class="has-cards">
								<tr>								
									<th><a ng-class="cc.sortType == 'title' ? 'delta' : ''" href ng-click="cc.sortType = 'title'; cc.sortReverse = !cc.sortReverse"> TITLE</a></th>
									<th><a ng-class="cc.sortType == 'barcode' ? 'delta' : ''" href ng-click="cc.sortType = 'barcode'; cc.sortReverse = !cc.sortReverse"> BARCODE</a></th>
									<th><a ng-class="cc.sortType == 'newduedate_stamp' ? 'delta' : ''" href ng-click="cc.sortType = 'newduedate_stamp'; cc.sortReverse = !cc.sortReverse"> NEW DUE DATE</a></th>
									<th><a ng-class="cc.sortType == 'description' ? 'delta' : ''" href ng-click="cc.sortType = 'description'; cc.sortReverse = !cc.sortReverse"> STATUS</a></th>
									
								</tr>	
							</thead>

							<tbody dir-paginate="renewal in cc.renewals | filter:query  | orderBy:cc.sortType:cc.sortReverse| itemsPerPage: cc.pageSize" current-page="cc.currentPage">
								<tr checkout-renew renewal="renewal">
								</tr>
							</tbody>
						</table>
						
						<div ng-controller="PagingController" class="paging-controller">
						<div class="text-center">
							<dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="assets/js/templates/dirPagination.html"></dir-pagination-controls>
						</div>
					</div>
		</div>	
</div>




    


