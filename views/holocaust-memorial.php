<div class="has-background background-base hero">
    <div class="clearfix wrap">
        <p class="col--centered col-md--eightcol">
            The Craig and Barbara Weiner Holocaust Reflection and Resource Room offers Nova Southeastern University
            students and the general public a place to learn about, and to contemplate, the horrendous acts that result
            from intolerance and hate.
        </p>
    </div>
</div>

<blockquote class="col--centered col-md--sixcol hero">
    <p class="delta no-margin">Those who cannot remember the past are condemned to repeat it.</p>
    <cite class="align-right" style="display: block;">George Santayana</cite>
</blockquote>

<h2 class="align-center delta no-margin hero--small" style="border-top: 1px solid #ddd;">United States Holocaust Memorial Museum Links</h2>

<nav class="clearfix" role="navigation">
    <a href="https://www.ushmm.org/online/hsv/person_advance_search.php" class="col-sm--sixcol col-lg--fourcol card media menu__item">
        <img ng-src="//sherman.library.nova.edu/cdn/media/images/holocaust/women-from-auschwitz.jpg" alt="Jewish women from Subcarpathian Rus who have been selected for forced labor at Auschwitz-Birkenau, march toward their barracks after disinfection and headshaving.">
        <span class="menu__item__content">
      <h3 class="type-sm--zeta type-md--delta no-margin">Survivors and Victims Database</h3>
    </span>
    </a>

    <a href="http://collections.ushmm.org/search/?f%5Brecord_type_facet%5D%5B%5D=Photograph" class="col-sm--sixcol col-lg--fourcol card media menu__item">
        <img ng-src="//sherman.library.nova.edu/cdn/media/images/holocaust/ss-troops.jpg" alt="SS troops and officers search the Jewish department heads of the Brauer armaments factory during the suppression of the Warsaw ghetto uprising.">
        <span class="menu__item__content">
      <h3 class="type-sm--zeta type-md--delta no-margin">Holocaust Images and Films</h3>
    </span>
    </a>

    <a href="http://collections.ushmm.org/search/?f%5Bfnd_type%5D%5B%5D=sound&f%5Bfnd_type%5D%5B%5D=video&f%5Brecord_type_facet%5D%5B%5D=Oral+History" class="col-sm--sixcol col-lg--fourcol card media menu__item">
        <img ng-src="//sherman.library.nova.edu/cdn/media/images/holocaust/strasser.jpg" alt="Portrait of Serafina Strasser.">
        <span class="menu__item__content">
      <h3 class="type-sm--zeta type-md--delta no-margin">Oral Testimonies</h3>
    </span>
    </a>

    <a href="https://www.ushmm.org/learn/timeline-of-events/before-1933" class="col-sm--sixcol col-lg--fourcol card media menu__item">
        <img ng-src="//sherman.library.nova.edu/cdn/media/images/holocaust/clock-tower.jpg" alt="View of the main entrance to the Buchenwald concentration camp.">
        <span class="menu__item__content">
      <h3 class="type-sm--zeta type-md--delta no-margin">Timeline of Events</h3>
    </span>
    </a>

    <a href="https://www.ushmm.org/learn/mapping-initiatives" class="col-sm--twelvecol col-md--sixcol col-lg--fourcol card media menu__item">
        <img ng-src="//sherman.library.nova.edu/cdn/media/images/holocaust/concentration-camps.jpg" alt="Map of the major Nazi concentration and extermination camps.">
        <span class="menu__item__content">
      <h3 class="type-sm--zeta type-md--delta no-margin">Maps</h3>
    </span>
    </a>

    <a href="https://www.ushmm.org/learn/holocaust-encyclopedia" class="col-sm--sixcol col-lg--fourcol card media menu__item">
        <img ng-src="//sherman.library.nova.edu/cdn/media/images/holocaust/confiscated-books.jpg" alt="A member of the SA throws confiscated books into the bonfire during the public burning of "un-German" books on the Opernplatz in Berlin.">
        <span class="menu__item__content">
      <h3 class="type-sm--zeta type-md--delta no-margin">Holocaust Encyclopedia</h3>
    </span>
    </a>


    <a href="http://www.wollheim-memorial.de/en/home" class="col-sm--sixcol col-lg--threecol card media menu__item">
        <img ng-src="//sherman.library.nova.edu/cdn/media/images/holocaust/wollheimmemorial.jpg" alt="">
        <span class="menu__item__content">
      <h3 class="type-sm--zeta type-md--delta no-margin">Wollheim Memorial</h3>
    </span>
    </a>


    <a href="//auschwitz.org" class="col-sm--sixcol col-lg--threecol card media menu__item">
        <img ng-src="//sherman.library.nova.edu/cdn/media/images/holocaust/child-survivors.jpg" alt="">
        <span class="menu__item__content">
        <h3 class="type-sm--zeta type-md--delta no-margin">Auschwitz-Birkenau Memorial</h3>
        <span class="zeta">Auschwitz was the largest of the German Nazi concentration camps and extermination centers. Lost their lives here more than 1.1 million men, women and children.</span>
      </span>
    </a>

    <a href="//buchenwald.de/en/" class="col-sm--sixcol col-lg--threecol card media menu__item">
        <img ng-src="//sherman.library.nova.edu/cdn/media/images/holocaust/buchenwald.jpg" alt="">
        <span class="menu__item__content">
        <h3 class="type-sm--zeta type-md--delta no-margin">Buchenwald and Mittlebau-Dora Memorials</h3>
        <span class="zeta">Preserving the sites of the crimes as sites of mourning and commemoration, to make them accessible to the public in an appropriate manner.</span>
      </span>
    </a>

    <a href="//kz-gedenkstaette-dachau.de/index-e.html" class="col-sm--sixcol col-lg--threecol card media menu__item">
        <img ng-src="//sherman.library.nova.edu/cdn/media/images/holocaust/dachau.jpg" alt="">
        <span class="menu__item__content">
        <h3 class="type-sm--zeta type-md--delta no-margin">Dachau Memorial Museum</h3>
        <span class="zeta">This camp served as a model for all later concentration camps and as a "school of violence" for the SS men under whose command it stood.</span>
      </span>
    </a>

    <a href="http://yadvashem.org" class="col-sm--sixcol col-lg--threecol card media menu__item">
        <img ng-src="//sherman.library.nova.edu/cdn/media/images/holocaust/yadvashem.jpg" alt="">
        <span class="menu__item__content">
      <h3 class="type-sm--zeta type-md--delta no-margin">Yad Vashem</h3>
      <span class="zeta">The World Holocaust Remembrace Center</span>
    </span>
    </a>

    <a href="https://sfi.usc.edu/" class="col-sm--sixcol col-lg--threecol card media menu__item">
        <img ng-src="//sherman.library.nova.edu/cdn/media/images/holocaust/gerson-adler.jpg" alt="">
        <span class="menu__item__content">
      <h3 class="type-sm--zeta type-md--delta no-margin">USC Shoah Foundation</h3>
      <span class="zeta">The USC Shoah Foundation is an educational institute , part of the University of Southern California. </span>
    </span>
    </a>

    <a href="http://www.iwm.org.uk/history/the-holocaust" class="col-sm--sixcol col-lg--threecol card media menu__item">
        <img ng-src="//sherman.library.nova.edu/cdn/media/images/holocaust/war-museum.jpg" alt="">
        <span class="menu__item__content">
      <h3 class="type-sm--zeta type-md--delta no-margin">Imperial War Museum - London</h3>
      <span class="zeta">Collection of people�s experiences of the Holocaust.</span>
    </span>
    </a>

    <a href="http://yahadmap.org/#map/" class="col-sm--sixcol col-lg--threecol card media menu__item">
        <img ng-src="//sherman.library.nova.edu/cdn/media/images/holocaust/yahadamap.jpg" alt="">
        <span class="menu__item__content">
      <h3 class="type-sm--zeta type-md--delta no-margin">Execution Sites of Jewish Victims</h3>
      <span class="zeta">Investigated by Yahad-In Unum</span>
    </span>
    </a>

</nav>

<main class="clearfix" role="main">
    <div class="clearfix">
        <div class="col-md--fourcol">
            <div class="has-cards">

                <div class="clearfix hero wrap">

                    <div class="carousel__container">
                    
            <span class="card media carousel__item" href="#">
              <img ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/covers/miami-beach.jpg" alt="">
              <span class="carousel__item__content">
                <h3 class="carousel__item__title">Miami Beach Survivors (Miami)</h3>
              </span>
            </span>

            <span class="card media carousel__item" href="#">
              <img ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/covers/life-after-holocaust.jpg" alt="">
              <span class="carousel__item__content">
                <h3 class="carousel__item__title">Life After the Holocaust</h3>
              </span>
            </span>
            
            

            <span class="card media carousel__item" href="#">
              <img ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/testimony-glance.jpg" alt="">
              <span class="carousel__item__content">
                <h3 class="carousel__item__title">Testimony of Baruch Glance - May 19, 2016</h3>
              </span>
            </span>

                        <span class="card media carousel__item" href="#">
              <img ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/testimony-yegerman.jpg" alt="">
              <span class="carousel__item__content">
                <h3 class="carousel__item__title">Testimony of Elfrieda Yegerman - May 19, 2016</h3>
              </span>
            </span>

                        <span class="card media carousel__item" href="#">
              <img ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/testimony-harber.jpg" alt="">
              <span class="carousel__item__content">
                <h3 class="carousel__item__title">Testimony of Thomas Harber - US Army Veteran</h3>
              </span>
            </span>

                        <span class="card media carousel__item" href="https://novacat.nova.edu:443/record=b3027099~S13">
              <img ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/covers/town-marked.jpg" alt="">
              <span class="carousel__item__content">
                <h3 class="carousel__item__title">A Town Marked by Tragedy: Chapters from Terezin's History</h3>
              </span>
            </span>

                        <span class="card media carousel__item" href="#">
                <img ng-src="https://secure.syndetics.com/index.php?upc=786936161434/mc.gif&client=novaseu" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">Anne Frank: The Whole Story</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://secure.syndetics.com/index.php?upc=794051211323/mc.gif&client=novaseu" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">Auschwitz: Inside the Nazi State</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://secure.syndetics.com/index.php?upc=025192470028/mc.gif&client=novaseu" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">Broken Silence</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://secure.syndetics.com/index.php?isbn=0026359178320/mc.gif&client=novaseu" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">Conspiracy</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/covers/echoes.jpg" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">Echoes of the Holocaust</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://secure.syndetics.com/index.php?isbn=9781784040529/mc.gif&client=novaseu" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">D-Day to Berlin</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://secure.syndetics.com/index.php?upc=881482309498/mc.gif&client=novaseu" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">Holocaust: Ravensbruck and Buchenwald</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://secure.syndetics.com/index.php?upc=767685111932/mc.gif&client=novaseu" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">Inheritance</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://secure.syndetics.com/index.php?upc=748252681003/mc.gif&client=novaseu" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">La Rafle</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://secure.syndetics.com/index.php?upc=850700001896/mc.gif&client=novaseu" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">Lodz Ghetto</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://secure.syndetics.com/index.php?upc=841887019170/mc.gif&client=novaseu" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">Never Forget to Lie</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://secure.syndetics.com/index.php?upc=748252999702/mc.gif&client=novaseu" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">Nicky's Family</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/covers/kz.jpg" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">KZ Buchenwald / Post Weimar</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/covers/one.jpg" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">One Survivor Remembers</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://secure.syndetics.com/index.php?upc=795975108331/mc.gif&client=novaseu" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">Sophie Scholl: The Final Days</h3>
                </span>
              </span>

                      <span href="#" class="card media carousel__item">
                <img ng-src="https://secure.syndetics.com/index.php?upc=715515105415/mc.gif&client=novaseu" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">Shoah</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://secure.syndetics.com/index.php?upc=786936188387/mc.gif&client=novaseu" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">Swing Kids</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/covers/testimony.jpg" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">Testimony of the Human Spirit</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/covers/absence.jpg" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">The Absence - La Ausencia</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/covers/last.jpg" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">The Last Days</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/covers/nazis.jpg" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">The Nazis: A Warning from History</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/covers/genocide.jpg" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">The Path to Nazi Genocide</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://secure.syndetics.com/index.php?upc=025193088529/mc.gif&client=novaseu" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">The Pianist</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://secure.syndetics.com/index.php?upc=718122355419/mc.gif&client=novaseu" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">The Rape of Europa</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/covers/rose.jpg" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">The Search for the White Rose</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/covers/long-way-home.jpg" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">The Long Way Home</h3>
                </span>
              </span>

                        <span href="#" class="card media carousel__item">
                <img ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/covers/arms-of-strangers.jpg" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">Into the Arms of Strangers</h3>
                </span>
              </span>

                        <span class="card media carousel__item">
                <img ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/covers/schindlers-list.jpg" alt="">
                <span class="carousel__item__content">
                  <h3 class="carousel__item__title">Schindler's List</h3>
                </span>
              </span>


                        <span class="card media carousel__item">
                <img alt=""
                     ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/covers/memory-camps.jpg">
                <span class="carousel__item__content">
                    <h3 class="carousel__item__title">Memory of the Camps (<b>Caution:</b> video contains disturbing images)</h3>
                </span>
            </span>

             <span class="card media carousel__item">
                <img alt="" ng-src="https://sherman.library.nova.edu/cdn/media/images/holocaust/covers/valkyrie.jpg">
                <span class="carousel__item__content">
                    <h3 class="carousel__item__title">Valkyrie</h3>
                </span>
            </span>

            <span class="card media carousel__item">
              <img ng-src="http://sherman.library.nova.edu/cdn/media/images/holocaust/covers/group.jpg " alt="">
              <span class="carousel__item__content">
                <h3 class="carousel__item__title">The Auschwitz Album</h3>
              </span>
            </span>

                    </div>

                    <button class="carousel__button carousel__button--prev" role="button" aria-hidden="true">Left</button>

                    <button class="carousel__button carousel__button--next" role="button" aria-hidden="true">Right</button>


                </div>

            </div>
        </div>
        <div class="col-md--eightcol">
            <article class="clearfix hero wrap" role="article" style="border-top: 1px solid #ddd;">
                <h2 class="hide-accessible">About</h2>
                <p>The <b>Craig and Barbara Weiner Holocaust Reflection and Resource Room</b>
                    offers Nova Southeastern University students and the general public a
                    place to learn about, and to contemplate, the horrendous acts that result
                    from intolerance and hate.</p>

                <p>One of the two rooms contains a number of computers with headphones
                    for NSU students, members of the faculty, staff, and the public at large to
                    research and watch <a href="http://collections.ushmm.org/search/?f%5Brecord_type_facet%5D%5B%5D=Oral+History">survivor testimonies</a>,
                    utilize a <a href="https://www.ushmm.org/learn/holocaust-encyclopedia">Holocaust encyclopedia</a>,
                    and research <a href="http://collections.ushmm.org/search/?f%5Brecord_type_facet%5D%5B%5D=Photograph">images and films</a>
                    linked to the <a href="https://www.ushmm.org/">United States Holocaust Memorial
                        Museum</a> in Washington, D.C., <a href="http://yadvashem.org">Yad Vashem</a>, and other important memorials.</p>

                <p>In addition, the center contains several important images including research
                    maps depicting the location of the major labor and extermination camps, examples of Nazi
                    propaganda as well as prisoner hand drawings and artifacts.</p>

                <p>The large room houses a large flat screen which plays continuous
                    video from the Holocaust period, many of which are actual archival
                    films made with home movie cameras.
                    This room contains comfortable seating for participants to review
                    a number of historical newspapers from the 1930�s and 1940�s;
                    propaganda material used by the Nazis; book cases filled
                    with Holocaust research books, as well as display cases
                    containing original artifacts illustrating both the horrors
                    of the Nazi regime, as well as artifacts from their
                    prisoner victims.</p>

                <blockquote class="col-md--tencol col--centered">
                    <p class="epsilon">It's important that we do not forget the lessons from the past. With
                        so few survivors of the Holocaust remaining, resources like this one
                        are vital to help tomorrow's generation avoid the atrocities of past generations.
                    </p>
                    <cite class="align-right" style="display: block;">NSU President Dr. George Hanbury</cite>
                </blockquote>

                <blockquote class="col-md--tencol col--centered hero">
                    <p class="epsilon">Our hope is that current and future generations will
                        have a meaningful experience in this Holocaust Reflection and Resource Room.
                        Our greatest prayer is that we all learn from this horrific event in human history.
                        We hope that this will inspire you to live honestly, live righteously,
                        live respectfully, and treat all others in the same manner as we would like to be
                        treated ourselves.
                    </p>
                    <cite class="align-right" style="display: block;">Craig and Barbara Weiner</cite>
                </blockquote>

            </article>

        </div
    </div>
</main>
