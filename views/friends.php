<style>
  .card--no-border {
    border: none;
  }

  .card--gray {
    background-color: #f5f5f5;
    margin-bottom: 1em;
  }

  .s-friends--chihuly {
    height: auto !important;
  }

  .s-friends--chihuly::before {
    opacity: .6;
    filter: brightness( 40% ) !important;
  }

  @media screen and ( min-width: 1024px ) {
    .s--friends__mission {
      padding-top: 6em;
    }

    .s-friends--background {
      padding-bottom: 6em;
    }

    .s--friends__buttons {
      position: absolute;
      left: 9%;
      margin-top: -5em;
    }
  }

</style>

<h1 class="hide-accessible">Circle of Friends</h1>

<div class="hero--small has-background s-friends--background">
  <div class="align-center clearfix wrap">
    <img class="s-friends__logo" src="//sherman.library.nova.edu/cdn/media/images/logos/circle-of-friends.png" alt="">
  </div>
</div>


<div class="col-md--tencol col--centered clearfix s--friends__buttons" >

  <div class="col-md--fourcol">
    <a href="https://www.nova.edu/cof/membership/" class="link link--undecorated">
      <div class="align-center card">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class ="icon-bar" width="52.559" height="73.954" viewBox="0 0 52.559 73.954"><path fill-rule="evenodd" clip-rule="evenodd" fill="#F7A800" d="M10.482 20.59c.33 0 .727-.066 1.057-.198.064 0 2.312-1.123 4.495-2.116-1.984-.53-3.572-1.786-4.563-3.44-1.123.53-1.984.927-2.048.927-1.324.663-1.854 2.116-1.258 3.374.463.924 1.39 1.453 2.315 1.453zm31.672-9.654c0-3.836-3.106-7.01-7.01-7.01s-7.01 3.174-7.01 7.01c0 3.9 3.108 7.01 7.01 7.01s7.01-3.11 7.01-7.01z"/><path fill-rule="evenodd" clip-rule="evenodd" fill="#00AFAA" d="M25.228 10.936c0-3.836-3.11-7.01-7.01-7.01S11.21 7.1 11.21 10.936c0 3.9 3.11 7.01 7.01 7.01s7.008-3.11 7.008-7.01z"/><path fill-rule="evenodd" clip-rule="evenodd" fill="#EA2A22" d="M42.55 36.194l.2-8.066c.924 1.786 1.32 4.298-.2 8.066z"/><path fill-rule="evenodd" clip-rule="evenodd" fill="#F7A800" d="M45.394 22.574l-.66-.795-.597-.597c-.463.198-.925.33-1.388.33s-.93-.063-1.325-.263c-.066 0-2.843-1.32-5.224-2.446-1.056-.132-2.313-.198-3.304-.528-.598.264-1.32.66-2.05 1.057.66-3.57-.926-1.255-2.844-5.157-.396.264-.86.528-1.257.793-.594-.33-1.19-.663-1.654-.927-.723 1.654-2.047 2.975-3.7 3.77 1.323.728 4.63 2.513 5.555 3.174.196.132.396.198.592.33v13.16c0 1.388.53 2.51.53 3.57V67.21c0 1.785 1.456 3.238 3.24 3.238 1.72 0 3.173-1.454 3.173-3.24V40.957h.466v26.25c0 1.785 1.453 3.238 3.24 3.238s3.238-1.454 3.238-3.24V41.088s-.066-.066-.066-.132h.132c.198.528.598.925 1.126 1.19.396.2.858.332 1.255.332.995 0 1.92-.53 2.448-1.456 4.895-9.06 2.184-15.01-.923-18.446z"/><path fill-rule="evenodd" clip-rule="evenodd" fill="#EA2A22" d="M10.548 28.26v1.323l.132 6.016c-1.19-3.307-.927-5.62-.132-7.34z"/><path fill-rule="evenodd" clip-rule="evenodd" fill="#00AFAA" d="M20.334 18.275c-.99.264-2.183.396-3.242.528-2.378 1.125-5.157 2.447-5.22 2.447-.4.2-.862.264-1.324.264s-.927-.132-1.39-.33L8.1 22.304c0 .07-.065.07-.065.07-3.107 3.437-5.818 9.39-.86 18.512.464.86 1.39 1.39 2.382 1.39.396 0 .86-.066 1.255-.33.462-.2.792-.598 1.06-.994V67.14c0 1.783 1.453 3.24 3.238 3.24 1.786 0 3.24-1.457 3.24-3.24V40.955h.46V67.14c0 1.783 1.388 3.24 3.174 3.24s3.242-1.457 3.242-3.24V37.715c.33-.993.528-2.05.528-3.24V20.92c-1.454 0-3.832-1.984-5.42-2.645zm16.86 0c2.182.993 4.495 2.05 4.562 2.116.333.134.73.2 1.06.2.925 0 1.852-.53 2.314-1.454.594-1.257.065-2.71-1.19-3.374-.134 0-1.06-.46-2.25-.99-.99 1.655-2.58 2.91-4.497 3.505z"/><defs><path id="a" d="M247.553-60.802h329.192v374.128H247.553z"/></defs><clipPath id="b"><use xlink:href="#a" overflow="visible"/></clipPath><g clip-path="url(#b)"><defs><path id="c" d="M247.553-60.802h329.192v374.128H247.553z"/></defs><clipPath id="d"><use xlink:href="#c" overflow="visible"/></clipPath><g clip-path="url(#d)"><defs><path id="e" d="M247.553-60.802h329.192v374.128H247.553z"/></defs></g></g></svg>
        <header class="card__header align-center">Join / Renew</header>
      </div>
    </a>
  </div>

  <div class="col-md--fourcol">
    <a href="https://www.nova.edu/giving/" class="link link--undecorated">
      <div class="align-center card">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class ="icon-bar" x="0px" y="0px"
             width="52.559" height="73.954" viewBox="0 0 422.859 576.734" enable-background="new 0 0 422.859 576.734"
             xml:space="preserve"><g><path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M0,0c140.924,0,281.849,0,422.859,0
		c0,192.169,0,384.371,0,576.734c-140.895,0-281.877,0-422.859,0C0,384.556,0,192.295,0,0z M227.195,157.051
		c0,2.56,0,4.549,0.001,6.539c0.035,123.811,0.069,247.62,0.123,371.43c0.001,2.826,0.005,5.681,0.373,8.474
		c2.163,16.396,17.853,26.744,33.683,22.304c11.892-3.335,19.368-13.93,19.366-27.691c-0.011-65.155-0.062-130.31-0.079-195.463
		c0-2.074,0.209-4.148,0.317-6.167c7.521,0,14.431,0,21.801,0c0,2.142,0,3.943,0,5.745c0,16.163-0.001,32.327,0,48.49
		c0.001,49.49-0.037,98.981,0.024,148.473c0.021,16.642,12.848,28.88,28.657,27.605c14.447-1.164,24.517-12.738,24.517-28.316
		c0.004-117.978-0.011-235.955-0.02-353.934c-0.001-2.083,0-4.168,0-6.156c5.866,0,10.952,0,16.43,0c0,2.171,0,3.987,0,5.803
		c0,43.825-0.007,87.65,0.019,131.475c0.002,2.827,0.075,5.677,0.431,8.475c1.234,9.716,8.412,16.439,17.956,16.998
		c10.234,0.598,18.584-5.263,20.795-14.919c0.66-2.886,0.718-5.953,0.72-8.938c0.042-46.157,0.049-92.315-0.008-138.473
		c-0.005-4.152-0.167-8.358-0.83-12.446c-4.695-28.939-30.425-54.356-64.599-53.997c-71.146,0.747-142.304,0.226-213.457,0.226
		c-2.092,0-4.184,0-7.165,0c4.821-12.34,9.291-23.783,13.943-35.692c-27.834,0-54.82-0.002-81.806,0.013
		c-0.612,0-1.224,0.18-2.261,0.342c4.593,11.755,9.082,23.242,13.913,35.605c-6.044,0-11.04-0.247-16.002,0.058
		c-7.637,0.471-13.088,4.49-16.359,11.351c-7.545,15.829,2.285,31.979,19.822,32.521c5.621,0.173,11.251,0.027,16.877,0.027
		C28.583,171.12,0.223,215.269,7.945,262.978c6.749,41.691,44.04,74.844,87.096,76.805c44.09,2.009,83.05-27.26,94.37-70.275
		c10.265-39.01-8.004-94.449-65.652-112.456C158.47,157.051,192.52,157.051,227.195,157.051z M342.025,58.054
		c0.027-24.794-20.104-44.936-44.781-44.808c-24.557,0.128-44.375,20.06-44.403,44.658c-0.028,24.798,20.11,44.968,44.752,44.819
		C322.146,102.575,341.999,82.615,342.025,58.054z"/><path fill-rule="evenodd" clip-rule="evenodd" d="M175.553,246.825c-0.241-42.335-34.613-76.139-77.156-75.881
		c-41.326,0.252-75.351,34.79-75.217,76.351c0.136,42.062,34.515,76.05,76.687,75.816
		C141.78,322.878,175.791,288.599,175.553,246.825z"/><path fill-rule="evenodd" clip-rule="evenodd" d="M117.136,92.836c-12.967,0-25.195,0-38.02,0c2.455,6.19,4.601,11.931,7.068,17.53
		c0.452,1.027,2.186,2.062,3.361,2.101c5.825,0.193,11.667,0.177,17.489-0.073c1.125-0.049,2.729-1.239,3.192-2.304
		C112.611,104.625,114.694,99.028,117.136,92.836z"/><g><path fill-rule="evenodd" clip-rule="evenodd" fill="#B6BD00" d="M227.195,157.051c-34.676,0-68.726,0-103.436,0
			c57.647,18.007,75.917,73.446,65.652,112.456c-11.32,43.016-50.28,72.284-94.37,70.275c-43.056-1.961-80.347-35.113-87.096-76.805
			C0.223,215.269,28.583,171.12,74.379,156.81c-5.626,0-11.257,0.146-16.877-0.027c-17.537-0.542-27.368-16.692-19.822-32.521
			c3.271-6.861,8.722-10.88,16.359-11.351c4.962-0.305,9.958-0.058,16.002-0.058c-4.831-12.363-9.32-23.85-13.913-35.605
			c1.037-0.162,1.648-0.342,2.261-0.342c26.986-0.015,53.972-0.013,81.806-0.013c-4.652,11.909-9.122,23.352-13.943,35.692
			c2.98,0,5.073,0,7.165,0c71.153,0,142.311,0.521,213.457-0.226c34.174-0.359,59.903,25.058,64.599,53.997
			c0.663,4.087,0.825,8.294,0.83,12.446c0.057,46.157,0.05,92.315,0.008,138.473c-0.002,2.985-0.06,6.053-0.72,8.938
			c-2.211,9.656-10.561,15.517-20.795,14.919c-9.544-0.559-16.722-7.282-17.956-16.998c-0.355-2.798-0.429-5.647-0.431-8.475
			c-0.025-43.824-0.019-87.65-0.019-131.475c0-1.816,0-3.631,0-5.803c-5.478,0-10.563,0-16.43,0c0,1.988-0.001,4.073,0,6.156
			c0.009,117.979,0.023,235.956,0.02,353.934c0,15.578-10.069,27.152-24.517,28.316c-15.81,1.274-28.637-10.964-28.657-27.605
			c-0.062-49.491-0.023-98.982-0.024-148.473c-0.001-16.163,0-32.327,0-48.49c0-1.802,0-3.604,0-5.745c-7.37,0-14.279,0-21.801,0
			c-0.108,2.019-0.317,4.093-0.317,6.167c0.018,65.153,0.068,130.308,0.079,195.463c0.002,13.762-7.475,24.356-19.366,27.691
			c-15.83,4.44-31.52-5.907-33.683-22.304c-0.368-2.793-0.372-5.647-0.373-8.474c-0.054-123.81-0.088-247.619-0.123-371.43
			C227.195,161.601,227.195,159.611,227.195,157.051z"/><path fill-rule="evenodd" clip-rule="evenodd" fill="#B6BD00" d="M342.025,58.054c-0.026,24.561-19.88,44.521-44.433,44.669
			c-24.642,0.148-44.78-20.021-44.752-44.819c0.028-24.598,19.847-44.53,44.403-44.658C321.922,13.118,342.053,33.26,342.025,58.054
			z"/></g><path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M175.553,246.825c0.238,41.773-33.773,76.053-75.686,76.286
		c-42.172,0.233-76.551-33.755-76.687-75.816c-0.134-41.561,33.891-76.099,75.217-76.351
		C140.939,170.687,175.312,204.49,175.553,246.825z M105.073,294.773c2.084-0.33,3.875-0.6,5.66-0.9
		c13.886-2.334,23.398-11.286,25.03-23.551c1.585-11.913-4.471-21.566-17.603-27.766c-3.45-1.628-7.147-2.737-10.597-4.367
		c-1.073-0.507-2.393-1.919-2.43-2.958c-0.226-6.274-0.112-12.562-0.112-19.564c6.71,2.223,12.794,4.237,19.106,6.327
		c2.896-4.735,5.708-9.332,8.833-14.442c-5.109-1.959-9.366-3.82-13.77-5.218c-4.561-1.448-9.265-2.447-14.413-3.774
		c0-4.101,0-8.175,0-12.22c-4.818,0-9.077,0-13.79,0c0,4.031,0,7.785,0,11.781c-2.205,0.338-4.001,0.582-5.786,0.89
		c-13.292,2.293-22.272,10.794-23.981,22.685c-1.685,11.718,3.998,21.188,16.608,27.264c4.178,2.013,8.564,3.592,12.84,5.364
		c0,7.432,0,14.813,0,23.045c-7.94-3.085-15.318-5.953-23.126-8.989c-2.731,4.343-5.608,8.917-9.275,14.747
		c11.252,4.134,21.744,7.99,32.869,12.079c0,3.144,0,7.077,0,11.101c4.831,0,9.213,0,13.936,0
		C105.073,302.302,105.073,298.675,105.073,294.773z"/><path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M117.136,92.836c-2.442,6.192-4.525,11.789-6.909,17.253
		c-0.463,1.064-2.067,2.255-3.192,2.304c-5.822,0.25-11.664,0.267-17.489,0.073c-1.175-0.039-2.909-1.073-3.361-2.101
		c-2.467-5.599-4.613-11.339-7.068-17.53C91.941,92.836,104.169,92.836,117.136,92.836z"/><path fill-rule="evenodd" clip-rule="evenodd" fill="#CCD14D" d="M105.073,294.773c0,3.901,0,7.528,0,11.532
		c-4.723,0-9.105,0-13.936,0c0-4.023,0-7.957,0-11.101c-11.125-4.089-21.617-7.944-32.869-12.079
		c3.667-5.831,6.543-10.404,9.275-14.747c7.809,3.036,15.186,5.903,23.126,8.989c0-8.232,0-15.613,0-23.045
		c-4.276-1.772-8.663-3.351-12.84-5.364c-12.61-6.077-18.292-15.546-16.608-27.264c1.709-11.89,10.688-20.391,23.981-22.685
		c1.784-0.308,3.581-0.552,5.786-0.89c0-3.996,0-7.75,0-11.781c4.713,0,8.972,0,13.79,0c0,4.045,0,8.119,0,12.22
		c5.148,1.327,9.852,2.326,14.413,3.774c4.404,1.398,8.661,3.258,13.77,5.218c-3.126,5.11-5.938,9.707-8.833,14.442
		c-6.313-2.09-12.396-4.104-19.106-6.327c0,7.002-0.114,13.29,0.112,19.564c0.038,1.039,1.357,2.451,2.43,2.958
		c3.45,1.63,7.147,2.738,10.597,4.367c13.132,6.2,19.188,15.854,17.603,27.766c-1.632,12.265-11.144,21.217-25.03,23.551
		C108.948,294.174,107.157,294.443,105.073,294.773z M105.315,259.359c0,6.239,0,12.134,0,18.088c6.379-1.4,9.507-4.292,9.613-8.674
		C115.032,264.469,112.049,261.497,105.315,259.359z M90.797,215.857c-5.782,0.811-8.895,3.815-8.752,8.291
		c0.133,4.208,3.51,7.291,8.752,8.004C90.797,226.729,90.797,221.305,90.797,215.857z"/><path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M105.315,259.359c6.734,2.138,9.717,5.11,9.613,9.414
		c-0.106,4.382-3.233,7.274-9.613,8.674C105.315,271.493,105.315,265.598,105.315,259.359z"/><path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M90.797,215.857c0,5.448,0,10.872,0.006,16.296
		c-5.248-0.714-8.625-3.797-8.758-8.005C81.902,219.672,85.016,216.668,90.797,215.857z"/>
		</svg>
        <header class="card__header align-center">Donate</header>
      </div>
    </a>
  </div>

  <div class="col-md--fourcol">
    <a href="https://systemsdev.library.nova.edu/testimonials" class="link link--undecorated" target="_self">
      <div class="align-center card">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class ="icon-bar" x="0px" y="0px" width="52.559" height="73.954" viewBox="0 0 587.926 582.927" enable-background="new 0 0 587.926 582.927"
             xml:space="preserve"><path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M587.926,582.927c-195.976,0-391.951,0-587.926,0
		C0,388.618,0,194.309,0,0c195.975,0,391.95,0,587.926,0C587.926,194.309,587.926,388.618,587.926,582.927z M147.191,570.692
		c98.039,0,195.233,0,292.9,0c0-28.489,0-56.533,0-85.266c5.28,2.465,9.925,4.604,14.545,6.794
		c18.942,8.979,37.834,18.067,56.828,26.938c17.58,8.211,38.729,3.894,51.75-10.319c13.226-14.436,15.609-35.887,5.433-52.856
		c-10.703-17.846-21.78-35.466-32.696-53.185c-31.352-50.886-62.767-101.732-94.01-152.684
		c-7.361-12.005-17.629-19.382-31.539-21.253c-6.742-0.907-13.599-1.328-20.404-1.338c-63.82-0.098-127.641-0.07-191.461-0.036
		c-5.329,0.003-10.671,0.119-15.982,0.52c-16.104,1.215-28.738,8.004-37.439,22.193c-41.772,68.126-83.781,136.107-125.663,204.167
		c-11.046,17.95-9.321,39.153,4.223,54.172c13.44,14.904,34.69,18.897,53.481,9.995c21.5-10.187,42.979-20.418,64.472-30.62
		c1.598-0.759,3.235-1.434,5.563-2.459C147.191,514.291,147.191,542.36,147.191,570.692z M392.523,111.854
		c-0.07-54.805-44.396-99.023-99.246-99.005c-54.771,0.018-99.205,44.423-99.174,99.112c0.03,54.656,44.557,99.084,99.282,99.059
		C348.152,210.994,392.593,166.542,392.523,111.854z"/><g>
            <path fill-rule="evenodd" clip-rule="evenodd" fill="#E90006" d="M147.191,570.692c0-28.332,0-56.401,0-85.238
		c-2.327,1.025-3.964,1.7-5.563,2.459c-21.493,10.202-42.972,20.434-64.472,30.62c-18.791,8.902-40.041,4.909-53.481-9.995
		c-13.543-15.019-15.269-36.222-4.223-54.172c41.881-68.06,83.89-136.04,125.663-204.167c8.701-14.189,21.334-20.979,37.439-22.193
		c5.311-0.4,10.653-0.517,15.982-0.52c63.82-0.034,127.641-0.062,191.461,0.036c6.806,0.01,13.662,0.431,20.404,1.338
		c13.91,1.871,24.178,9.248,31.539,21.253c31.243,50.952,62.658,101.798,94.01,152.684c10.916,17.719,21.993,35.339,32.696,53.185
		c10.177,16.97,7.793,38.421-5.433,52.856c-13.021,14.213-34.17,18.53-51.75,10.319c-18.994-8.87-37.886-17.958-56.828-26.938
		c-4.62-2.189-9.265-4.329-14.545-6.794c0,28.732,0,56.776,0,85.266C342.425,570.692,245.23,570.692,147.191,570.692z
		 M201.627,453.429c30.998,22.007,61.755,43.841,92.049,65.348c31.075-22.315,61.445-44.124,91.739-65.879
		c-10.796-19.776-12.998-38.559,2.062-55.725c14.128-16.103,32.061-18.912,52.159-11.654c4.154-10.652,5.557-21.845,5.777-33.17
		c0.455-23.452-6.204-44.012-25.07-59.365c-33.395-27.177-93.442-24.377-119.171,19.525c-2.417,4.123-4.827,8.251-7.374,12.604
		c-26.743-57.434-84.664-56.964-116.466-39.067c-35.559,20.011-43.825,63.536-29.234,99.889
		c20.103-7.625,38.297-4.604,52.562,11.869C215.493,414.93,213.321,433.518,201.627,453.429z"/>
            <path fill-rule="evenodd" clip-rule="evenodd" fill="#E90006" d="M392.523,111.854c0.069,54.688-44.371,99.14-99.139,99.165
		c-54.725,0.025-99.252-44.402-99.282-99.059c-0.03-54.688,44.403-99.094,99.174-99.112
		C348.127,12.831,392.453,57.049,392.523,111.854z"/></g>
          <path fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" d="M201.627,453.429c11.694-19.911,13.866-38.499-0.966-55.625 c-14.265-16.474-32.459-19.494-52.562-11.869c-14.591-36.353-6.325-79.878,29.234-99.889
		c31.802-17.897,89.723-18.367,116.466,39.067c2.547-4.354,4.957-8.481,7.374-12.604c25.729-43.902,85.776-46.703,119.171-19.525
		c18.866,15.354,25.525,35.913,25.07,59.365c-0.221,11.325-1.623,22.518-5.777,33.17c-20.099-7.258-38.031-4.448-52.159,11.654
		c-15.06,17.166-12.857,35.948-2.062,55.725c-30.294,21.755-60.664,43.563-91.739,65.879
		C263.382,497.27,232.625,475.436,201.627,453.429z"/>
</svg>
        <header class="card__header align-center">Love</header>
      </div>
    </a>
  </div>

</div>

<div class="clearfix hero wrap s--friends__mission">
  <div class="col-md--tencol col-lg--sixcol col--centered">
    <p class="no-margin">
      The Circle of Friends for the Nova Southeastern University Alvin Sherman Library is a not for profit
      organization founded in 1999 as a diverse group of mutually concerned and avid library supporters.
    </p>
  </div>
</div>


<div class="hero has-background s-friends--chihuly">
  <div class="clearfix wrap">
    <div class="col-md--tencol col-lg--sixcol col--centered align-center">
      <p class="epsilon">
        I believe the library's programming helps bridge cultural divides ... helps
        bridge our differences, while showing how alike we truly are. <cite>Fallen H.</cite>
      </p>
    </div>
  </div>
</div>

<section class="has-cards hero">
  <div class="clearfix wrap">

    <div class="col-md--fourcol">

      <nav role="navigation" class="menu menu--sidebar" style="margin-top: 1em;">
        <a class="link link--undecorated" href="https://sherman.library.nova.edu/sites/friends-new/about-the-circle-of-friends/">About Us</a>
        <a class="link link--undecorated" href="https://sherman.library.nova.edu/sites/friends-new/cof-our-mission/">Our Mission</a>
        <a class="link link--undecorated" href="https://sherman.library.nova.edu/sites/friends-new/board-of-directors/">Board of Directors</a>
        <a class="link link--undecorated" href="https://sherman.library.nova.edu/sites/friends-new/cof-bylaws/">Bylaws</a>
        <a class="link link--undecorated" href="https://sherman.library.nova.edu/sites/policies/gift-policy/">Donation/Collection Policy</a>
        <a class="link link--undecorated" href="https://sherman.library.nova.edu/sites/friends-new/cof-contact/">Contact Us</a>
        <a class="link link--undecorated" href="https://sherman.library.nova.edu/ ">Back to Library's Webpage</a>
      </nav>

      <div ng-controller="AdController as adc">
        <div style="align-items: center; display: flex; justify-content: center;" ng-if="!adc.ads">
          <blockquote>
            <p>“She sounds like someone who spends a lot of time in libraries, which are the best sorts of people.”</p>
            <cite>Catherynne M. Valente</cite>
          </blockquote>
        </div>

        <div data-ng-controller="EventController as ec" data-audience="all" data-series="friends">

          <a href="http://sherman.library.nova.edu/sites/workshops-and-instruction" class="link link--undecorated">
            <h2 class="align-center delta no-margin hero--small">Circle of Friends Events</h2>
          </a>
          <ng-repeat data-ng-repeat="event in ec.events">
            <article role="article">
              <a ng-href="{{ event.url }}" class="link link--undecorated">
                <div class="card">
                  <span class="card__color-code"></span>
                  <header class="card__header">
                    <h3 class="card__title zeta no-margin">{{ event.title }}</h3>
                    <div class="small-text">
                      <time class="time">
                        <b>{{ event.start }}</b>
                        <span class="time__hours" style="color:#999;">{{ event.from }} {{ event.until }}</span>
                      </time>
                    </div>
                  </header>
                </div>
              </a>
            </article>
          </ng-repeat>
        </div>

        <div class="align-right">
          <a class="link small-text" href="http://sherman.library.nova.edu/sites/spotlight/events" target="_self">More events</a>
        </div>

        <div data-ng-repeat="ad in adc.ads | limitTo : 1" data-ng-cloak>
          <a class="link link--undecorated" ng-href="{{ ad.link }}?utm_source=aclib&utm_media=card&utm_campaign=ad-manager" target="_self">
            <article class="card">
              <div class="card__media">
                <img ng-src="{{ad.media}}">
              </div>
              <header class="card__header">
                <h2 class="card__title type-sm--delta type-lg--gamma">{{ ad.title }}</h2>
              </header>
              <section class="card__content">
                <p class="type-sm--zeta type-lg--epsilon">{{ ad.excerpt }}</p>
              </section>
            </article>
          </a>
        </div>

      </div>

    </div>


    <div class="col-md--eightcol">
      <div class="card card--gray card--no-border">

        <ng-controller data-ng-controller="BlogController as bc" data-audience="49">
          <ng-repeat data-ng-repeat="post in bc.posts">

            <article class="col-md--sixcol">
              <a href="{{ post.link }}" class="link link--undecorated">
                <div class="card">
                  <div class="card__media" data-ng-if="post.better_featured_image">
                    <img alt="" ng-src="{{post.better_featured_image.source_url}}">
                  </div>
                  <header class="card__header">
                    <h2 class="card__title type-sm--delta type-lg--gamma" data-ng-class="{'no-margin' : !post.better_featured_image}">{{ post.title.rendered }}</h2>
                  </header>
                  <section class="card__content post__content" ng-bind-html="post.excerpt.rendered"></section>
                </div>
              </a>
            </article>
          </ng-repeat>
        </ng-controller>

        <!--
        <div class="col-md--sixcol">
          <a href="" class="link link--undecorated">
            <div class="card">
              <div class="card__media">
                <img src="//placekitten.com/500/281" alt="">
              </div>
              <div class="card__header">
                <h2 class="card__title delta">Showcasing the Collaborative Study Room</h2>
              </div>
              <section class="card__content">
                <p class="zeta">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias asperiores, aut consequuntur</p>
              </section>
            </div>
          </a>
        </div>
        -->

      </div>
    </div>
  </div>
</section>