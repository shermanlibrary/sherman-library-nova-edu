<?php require_once '..\inc\autoload.php'; ?>
<?php

if( isset($_COOKIE['pid'] ) ) :
 $pid = $_COOKIE['pid'];
 $pid  = base64_decode($pid);
 $pieces = explode( '|',  $pid);
endif;

 ?>

 <div  data-ng-controller="HomeController as hc">

 	<div class="has-cards" id="main">

 		<main class="hero wrap clearfix" role="main">

      <ng-include src="'../views/forms/search-widget.html'"></ng-include>

 			<div class="col-md--twelvecol col-lg--fourcol clearfix">

 				<div class="col-lg--twelvecol">
 					<section class="accordion" style="margin-bottom: 1em;">
 						<section class="accordion__section" id="ask">
 							<a class="link link--undecorated" href="#ask" onclick="ga( 'send', 'event', 'Ask a Librarian', 'click', 'Toggle Open' );">
 								<h2 class="accordion__section__title zeta" style="border-color: #069;">
 									<svg class="svg" viewBox="0 0 32 32" style="width: 21px; float: right; margin-right: .5em; bottom: 5px; position: relative; fill: #069;">
 										<use xlink:href="#icon-bubbles"></use>
 									</svg>

 									Ask a Librarian
 								</h2>
 							</a>

 							<div class="accordion__section__content">
 								<ul class="list list--alternate">
 									<li><b>Reference desk</b> is open from 10 a.m. - 8 p.m.
                     <br><small>(Friday until 6 p.m. / Sunday until 7 p.m. )</small>. <a target="_self" class="link link--undecorated small-text" href="/sites/hours">See hours.</a></li>
 									<li><b>Call</b> <a class="link link--undecorated" tel="9542624613">(954) 262-4613</a> <small>(<a target="_self" href="/sites/contact/" class="align-right">Toll-Free</a>)</small></li>
 									<li><b>Email</b> <mark>refdesk@nova.edu</mark> or <a href="http://systems.library.nova.edu/form/view.php?id=6" class="link">use our form</a>.
 									<li><b>Chat</b> with <a class="link link--undecorated" href="http://sherman.library.nova.edu/sites/ask-a-librarian/#chat" target="_self">a Librarian</a></li>
                                    <li><b>Appointment</b> request <a href="http://systems.library.nova.edu/form/view.php?id=22">form</a> | <a href="http://lib.nova.edu/cita">En Español</a></li>
 									<li><b>Text</b> (954) 372-3505 <br><small>Start your question with "NSU".</small></li>
 								</ul>
 							</div>
 						</section>
 					</section>

 					<section class="accordion">

 						<section class="accordion__section" id="databases-by-name">
 							<a class="link link--undecorated" href="#databases-by-name" onclick="ga( 'send', 'event', 'Database by Name', 'click', 'Toggle Open' );">
 								<h2 class="accordion__section__title zeta">Databases by Name</h2>
 							</a>

 							<div class="accordion__section__content">
 								<input class="form__input form__input--full-width" placeholder="Start typing a database title" ng-model="filter"/>
 								<div class="align-right small-text">
 									or <a href="http://sherman.library.nova.edu/e-library/index.php?action=all&col=n" target="_self">see all databases</a>
 								</div>

 								<ul class="list list--alternate">

 									<li data-ng-repeat="database in hc.databases | filter: filter:hc.startsWith | orderBy: 'title'" data-ng-cloak>
 										<a target="_self" ng-href="/auth/index.php?aid={{database.aid}}" data-ng-click="ga( 'send', 'event', 'Databases by Name', 'click', '{{database.title}}' );"><span ng-bind-html="database.title"></span></a>
 									</li>

 								</ul>
 							</div>

 					    </section>
 					<section class="accordion" style="margin-bottom: 1em;">
 						<section class="accordion__section" id="databases-by-subject">
 							<a href="#databases-by-subject" class="link link--undecorated" onclick="ga( 'send', 'event', 'Database by Subject', 'click', 'Toggle Open' );">
 								<h2 class="accordion__section__title zeta">Databases by Subject</h2>
 							</a>
 							<div class="accordion__section__content">
 								<input class="form__input form__input--full-width" placeholder="Start typing a subject area" ng-model="query"/>
 								<div class="align-right small-text">
 									or <a href="//sherman.library.nova.edu/e-library/" target="_self">see all subjects</a>
 								</div>

 								<ul class="list list--alternate">

 									<li ng-repeat="subject in hc.subjects | filter: query | orderBy: 'name'">
 										<a target="_self" ng-href="/e-library/index.php?action=subject&col={{ac.col}}&cat={{subject.subj_id}}" data-ng-click="ga( 'send', 'event', 'Databases by Subject', 'click', '{{subject.name}}' );"><span ng-bind-html="subject.name"></span></a>
 									</li>

 								</ul>
 							</div>
 						</section>
 					</section>

 					</section>
 				</div>


 			</div>
 		</main>

 	</div>

 	<div class="has-cards hero">

    <div class="clearfix wrap">

 		<div class="col-md--twelvecol col-lg--eightcol">

 				<div class="col-md--fourcol">
 					<h3 class="delta no-margin">Borrow</h3>
 					<ul class="no-bullets">

 						<li><a class="link link--undecorated" onclick="ga( 'send', 'event', 'Homepage Quick Links', 'http://nova.campusguides.com/c.php?g=112162&p=724231', 'Course Reserves');" href="http://nova.campusguides.com/c.php?g=112162&p=724231">Course Reserves</a></li>
 						<li><a class="link link--undecorated" onclick="ga( 'send', 'event', 'Homepage Quick Links', 'http://sherman.library.nova.edu/sites/interlibrary-loan/', 'Interlibrary Loan');" target="_self" href="http://sherman.library.nova.edu/sites/interlibrary-loan/" target="_self">Interlibrary Loan</a></li>
 						<li><a class="link link--undecorated" onclick="ga( 'send', 'event', 'Homepage Quick Links', 'http://sherman.library.nova.edu/sites/policies/circulation/#borrowing', 'Loan Periods');" target="_self" href="/sites/policies/circulation/#borrowing">Loan Periods</a></li>
 						<li><a class="link link--undecorated" onclick="ga( 'send', 'event', 'Homepage Quick Links', 'http://sherman.library.nova.edu/new/', 'New Materials');" target="_self" href="/new/">New Materials</a></li>
 						<li><a class="link link--undecorated" onclick="ga( 'send', 'event', 'Homepage Quick Links', 'http://sherman.library.nova.edu/rooms', 'Study Rooms');" target="_self" href="/rooms/">Study Rooms</a></li>
 					</ul>
 				</div>

 				<div class="col-md--fourcol">
 					<h3 class="delta no-margin">Research</h3>
 					<ul class="no-bullets">
 						<li><a class="link link--undecorated" onclick="ga( 'send', 'event', 'Homepage Quick Links', 'http://nova.campusguides.com/nsu-alumni', 'Alumni');" href="http://nova.campusguides.com/nsu-alumni">Alumni</a></li>
 						<li><a class="link link--undecorated" onclick="ga( 'send', 'event', 'Homepage Quick Links', 'http://copyright.nova.edu', 'Copyright');" href="http://copyright.nova.edu">Copyright</a></li>
 						<li><a class="link link--undecorated" onclick="ga( 'send', 'event', 'Homepage Quick Links', 'http://nova.campusguides.com/aslfaculty', 'Faculty');" href="http://nova.campusguides.com/aslfaculty">Faculty</a></li>
 						<li><a class="link link--undecorated" onclick="ga( 'send', 'event', 'Homepage Quick Links', 'http://www.nova.edu/library/', 'NSU Libraries');" href="http://www.nova.edu/library/">NSU Libraries</a></li>
 						<li><a class="link link--undecorated" onclick="ga( 'send', 'event', 'Homepage Quick Links', 'http://nsuworks.nova.edu', 'NSUWorks');" href="http://nsuworks.nova.edu">NSUWorks</a></li>
 					</ul>
 				</div>


 				<div class="col-md--fourcol">
 					<h3 class="delta no-margin">Help</h3>
 					<ul class="no-bullets">
 						<li><a class="link link--undecorated"  onclick="ga( 'send', 'event', 'Homepage Quick Links', 'http://sherman.library.nova.edu/sites/contact', 'Contact Us' );" class="link link--undecorated" href data-ng-click="ac.showModalWindow({element:'contact'})">Contact Us</a></li>
 						<li>
 							<a  class="link link--undecorated" onclick="ga( 'send', 'event', 'Homepage Quick Links', 'http://sherman.library.nova.edu/sites/learn', 'LibraryLearn' );" target="_self" href="/sites/learn">LibraryLearn Videos</a> <br>
 						</li>
 						<li><a  class="link link--undecorated" onclick="ga( 'send', 'event', 'Homepage Quick Links', 'http://systems.library.nova.edu/form/view.php?id=22', 'Make an Appointment' );" href="http://systems.library.nova.edu/form/view.php?id=22">Make an Appointment</a></li>
 						<li><a  class="link link--undecorated" onclick="ga( 'send', 'event', 'Homepage Quick Links', 'http://nova.campusguides.com/razors-research-bytes', 'Razors Research Bytes' );" href="http://nova.campusguides.com/razors-research-bytes">Razor's Research Bytes</a></li>
 						<li><a class="link link--undecorated"  onclick="ga( 'send', 'event', 'Homepage Quick Links', 'http://sherman.library.nova.edu/sites/workshops-and-instruction', 'Workshops' );" target="_self" href="/sites/workshops-and-instruction">Workshops</a></li>

 					</ul>
 				</div>

 			</div>

 			<div class="col-lg--fourcol">

 				<a class="hide-accessible" href="#programs-and-events">Skip this long list of library guides to the next section</a>
 		    <h3 class="delta no-margin">Library Guides <a class="small-text" data-ng-click="ac.showModalWindow({element: 'guides'})">(What's this?)</a></h3>
        <p class="zeta">Try searching for a subject or course, or browse by subject.</p>

        <form action="//nova.campusguides.com/srch.php" class="form" role="form">
          <ul>
            <li class="form__field">
              <label for="guide-search" class="form__label hide-accessible">Search for a subject or course</label>

                <input id="guide-search" class="form__input form__input--full-width" name="q" placeholder="Endnote, BIOL 1500" data-ng-model="narrow">
                <button class="button button--small form__submit form__submit--inside no-margin" data-ng-class="{ 'button--primary--alt' : narrow, 'button--disabled' : !narrow }">
                  <svg xmlns="http://www.w3.org/2000/svg" height="18" viewBox="0 0 24 24" width="18" fill="#fff">
                    <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/>
                    <path d="M0 0h24v24H0z" fill="none"/>
                  </svg>
                  <span class="button__text hide-accessible">Search</span>
                </button>

            </li>
          </ul>

        </form>

        <div class="card no-padding">
    	    <ul class="list list--alternate list--datalist no-margin" id="mylist">
   		    	<li data-ng-repeat="guide in hc.guides | orderBy: 'name'" data-ng-cloak>
   		    		<a ng-href="//nova.campusguides.com/sb.php?subject_id={{ guide.id }}" data-ng-click="ga( 'send', 'event', 'Library Guides', 'click', '{{guide.name}}' );">
   		    			{{ guide.name }}
   		    		</a>
   		    	</li>
   		    </ul>
        </div>

 			</div>

    </div>
  </div>

<article class="ad ad--hero ad--transparent card clearfix s--holocaust-ad no-margin">

  <div class="clearfix wrap">
    <div class="col-md--sixcol align-center ad__media">
      <a href="//sherman.library.nova.edu/holocaust?utm_source=aclib&utm_medium=banner&utm_campaign=holocaust-room" target="_self">
        <img src="//sherman.library.nova.edu/cdn/media/images/ads/boys.png" alt="Two young boys wearing jewish stars" />
      </a>
    </div>

    <div class="col-md--sixcol">

      <div class="col-md--tencol col--centered ad__copy">

        <header class="card__header">
          <a class="link link--undecorated _link--blue" href="//sherman.library.nova.edu/holocaust?utm_source=aclib&utm_medium=banner&utm_campaign=holocaust-room" target="_self">
            <h2 class="menu__item__title hide-accessible">Now Open</h2>
          </a>
        </header>

        <section class="no-margin">
          <p class="epsilon">
            Now open on the 2nd floor, our <b>Holocaust Reflection and Resource Room</b> is a place
            to learn about and to contemplate the horrendous acts that result from intolerance and hate.
          </p>
        </section>

        </div>
    </div>

    </div>
  </article>

 	<div class="has-cards clearfix" id="programs-and-events">
 		<div class="clearfix hero wrap" ng-controller="AdController as adc">
      <div class="col-md--fourcol" style="align-items: center; display: flex; justify-content: center;" ng-if="!adc.ads">
 				<blockquote>
 					<p>“She sounds like someone who spends a lot of time in libraries, which are the best sorts of people.”</p>
 					<cite>Catherynne M. Valente</cite>
 				</blockquote>
 			</div>

 			<div class="col-md--fourcol" data-ng-repeat="ad in adc.ads | limitTo : 1" data-ng-cloak>

        <a class="link link--undecorated" ng-href="{{ ad.link }}?utm_source=aclib&utm_media=card&utm_campaign=ad-manager" target="_self">
   				<article class="card">
   					<div class="card__media">
   							<img ng-src="{{ad.media}}">
   					</div>
   					<header class="card__header">
   						<h2 class="card__title type-sm--delta type-lg--gamma">{{ ad.title }}</h2>
   					</header>
   					<section class="card__content">
   						<p class="type-sm--zeta type-lg--epsilon">{{ ad.excerpt }}</p>
   					</section>
   				</article>
        </a>

 			</div>

       <div class="col-md--fourcol" data-ng-controller="EventController as ec" data-audience="all" data-series="academic-workshops">

          <a href="http://sherman.library.nova.edu/sites/workshops-and-instruction" class="link link--undecorated" target="_self">
            <h2 class="align-center delta no-margin hero--small separator separator--top">Upcoming Workshops</h2>
          </a>
         <ng-repeat data-ng-repeat="event in ec.events">
           <article role="article">
             <a ng-href="{{ event.url }}" class="link link--undecorated" target="_self">
               <div class="card">
                 <span class="card__color-code"></span>
                 <header class="card__header">
                   <h3 class="card__title zeta no-margin">{{ event.title }}</h3>
                    <div class="small-text">
                     <time class="time">
                       <b>{{ event.start }}</b>
                       <span class="time__hours" style="color:#999;">{{ event.from }} {{ event.until }}</span>
                     </time>
                   </div>
                 </header>
               </div>
             </a>
           </article>
         </ng-repeat>

         <a class="link" href="http://sherman.library.nova.edu/sites/workshops-and-instruction" target="_self">More workshops</a>
       </div>

       <div class="col-md--fourcol" data-ng-controller="EventController as ec" data-audience="public">
         <a class="link link--undecorated" href="http://sherman.library.nova.edu/sites/spotlight/events" target="_self">
           <h2 class="align-center delta no-margin hero--small separator separator--top">Programs and Events</h2>
         </a>
         <ng-repeat data-ng-repeat="event in ec.events">
           <article role="article">
             <a ng-href="{{ event.url }}" class="link link--undecorated" target="_self">
               <div class="card">
                 <span class="card__color-code" data-ng-class="{ 'card__color-code--kids' : event.audience === 'kids', 'card__color-code--teens' : event.audience === 'teens' }"></span>
                 <header class="card__header">
                   <h3 class="card__title zeta no-margin">{{ event.title }}</h3>
                    <div class="small-text">
                     <time class="time">
                       <b>{{ event.start }}</b>
                       <span class="time__hours" style="color:#999;">{{ event.from }} {{ event.until }}</span>
                     </time>
                   </div>
                 </header>
               </div>
             </a>
           </article>
         </ng-repeat>

         <a class="link" href="http://sherman.library.nova.edu/sites/spotlight/events" target="_self">More events</a>
       </div>

 		</div>


     </div>

 </div>
