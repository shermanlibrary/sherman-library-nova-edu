<div  data-ng-controller="AccountController as my" id="account">

<header class="align-center col-md--eightcol col--centered">
	<h1>My Library Account</h1>
	<p>
        Welcome, {{ac.info.display_name}}
	</p>
</header>

<div class="has-cards">
	<div class="clearfix hero wrap">
		<div class="col-md--fourcol">
			<div class="card" style="margin-bottom: 1em;">
				<header class="card__header">
					<h2 class="delta">Personal Information</h2>
				</header>

				<section class="content">
					<p ng-if="!ac.info.invalid_address">
                        <span ng-if="ac.info.billname">{{ac.info.billname}} <br></span>
                        <span ng-if="!ac.info.billname">{{ac.info.display_name}} <br></span>
						<span ng-if="ac.info.email">{{ac.info.email}} <br></span>
                        <span ng-if="ac.info.telephone">{{ac.info.telephone}} <br></span>
                        <span ng-if="ac.info.address_line1">{{ac.info.address_line1}} <br></span>
                        <span ng-if="ac.info.address_line2">{{ac.info.address_line2}} <br></span>
                        <span ng-if="ac.info.city">{{ac.info.city}}<span ng-if="ac.info.state">, {{ac.info.state}} <span ng-if="ac.info.zip">{{ac.info.zip}}<br></span>
					</p>

                    <span ng-if="ac.inArray(ac.info.home_lib, ac.changers)  && ac.info.col === 'n'">
                        <span ng-bind-html="ac.update_message"></span>
                        Update Home Library  <a href class="small-text" data-ng-click="ac.showModalWindow({element: 'homelib'})">(What's this?)</a>
                        <select ng-model="ac.selectedLibrary" ng-options="library.code as library.label for library in ac.libraries">
                        </select>
                        <button
                            class="button button--small small-text"
                            ng-class="ac.selectedLibrary === ac.info.home_lib ? 'button--disabled' : 'button--default'"
                            ng-disabled="ac.selectedLibrary === ac.info.home_lib"
                            ng-click="ac.updateHomeLibrary()">Update</button>
                    </span>


                    <span ng-if="ac.info.col === 'p'">
                        <span ng-bind-html="ac.update_message"></span>
                        Update Email
                        <input
                            ng-model="ac.email_update"
                            ng-change="ac.update_message = ''" />
                        <button
                            class="button button--small small-text"
                            ng-class="ac.email_update === ac.info.email ? 'button--disabled' : 'button--default'"
                            ng-disabled="ac.email_update === ac.info.email"
                            ng-click="ac.updateEmail()">Update</button>
                    </span>
				</section>
			</div>

			<div class="card">
				<header class="card__header">
					<h2 class="delta no-margin"><span ng-if="ac.info.col !== 'p'">SharkCard</span> <span ng-if="ac.info.col === 'p'">Alvin Sherman Library Card</span></h2>
					<p class="small-text">Library Card Type</p>
				</header>
				<section class="content media" ng-cloak>

					<img ng-if="ac.info.col !== 'p'" ng-src="http://www.nova.edu/nsucard/images/nsucard_new.gif" alt="NSU Shark Card" ng-cloak>
					<img ng-if="ac.info.col === 'p' && ac.info.pcode1 !== 'e'" ng-src="http://public.library.nova.edu/wp-content/uploads/2013/10/library-card.jpg" alt="Alvin Sherman Library Card" ng-cloak>

				</section>
			</div>
		</div>

		<div class="col-md--fourcol">
			<div class="card">
				<header class="card__header">
					<h2 class="delta">Activity</h2>
				</header>

				<ul class="list list--alternate" ng-if="ac.activity">


					<li ng-if="ac.activity.checkouts"><a ng-href="/checkouts">Checkouts</a>: {{ac.activity.checkouts}}</li>
                    <li ng-if="ac.activity.webs">Electronic documents available: {{ac.activity.webs}}</li>
                    <li ng-if="ac.activity.fees"><a target="_self" href="/fees/">Fees:</a> {{ac.activity.fees}} ( {{ac.activity.balance}} )</li>
					<li ng-if="ac.activity.holds"><a ng-href="/holds">Holds</a>: {{ac.activity.holds}}</li>
					<li ng-if="ac.activity.rooms"><a target="_self" href="/rooms/">Room reservations</a>: {{ac.activity.rooms}}</li>
				</ul>

                <p class="no-margin" ng-if="!ac.activity">
                    It's quiet here. Need <a href="//sherman.library.nova.edu/sites/spotlight/lists/" target="_self">something to read</a>?
                </p>

				</section>
			</div>
		</div>

		<div class="col-md--fourcol">
				<section class="accordion">

                <section class="accordion__section" id="databases-by-name"
                         data-ng-class="{'accordion__section--open' : my.dbAcc }">

                    <a class="link link--undecorated"
                        data-ng-click="my.dbAccToggle()">
                        <h2 class="accordion__section__title zeta">Databases by Name</h2>
                    </a>

                    <div class="accordion__section__content">
                        <input class="form__input form__input--full-width" placeholder="Start typing a database title" ng-model="filter"/>
                        <ul class="list list--alternate">

                            <li data-ng-repeat="database in my.databases | filter: filter:my.startsWith | orderBy: 'title'" data-ng-cloak>
                                <a href="//sherman.library.nova.edu/auth/index.php?aid={{database.aid}}">{{database.title}}</a>
                            </li>

                        </ul>
                    </div>

                </section>
                <section class="accordion" style="margin-bottom: 1em;">
                    <section class="accordion__section" id="databases-by-subject"
                             data-ng-class="{'accordion__section--open' : my.subAcc }">


                        <a  class="link link--undecorated"
                            data-ng-click="my.subAccToggle()">
                            <h2 class="accordion__section__title zeta">Databases by Subject</h2>
                        </a>
                        <div class="accordion__section__content">
                            <input class="form__input form__input--full-width" placeholder="Start typing a subject area" ng-model="query"/>
                            <ul class="list list--alternate">

                                <li ng-repeat="subject in my.subjects | filter: query | orderBy: 'name'">
                                    <a href="//sherman.library.nova.edu/e-library/index.php?action=subject&col={{ac.col}}&cat={{subject.subj_id}}">{{subject.name}}</a>
                                </li>

                            </ul>
                        </div>
                    </section>
                </section>
			<section class="card">
                <header class="card__header">
                    <h2 class="delta">Services</h2>
                </header>
				<ul class="list list--alternate">
                    <li><a href="//novacat.nova.edu">Catalog</a></li>
                    <li><a target="_self" href="/linker">Durable Linker</a></li>
                    <li><a target="_self" href="/e-library">E-Library</a></li>
                    <li><a target="_self" href="/auth/index.php?token">ILLiad</a></li>
                    <li><a target="_self" href="/fees/">Library Fees</a></li>
                    <li><a target="_self" href="/rooms/">Study Room Reservation</a></li>
				</ul>
			</section>
		</div>
	</div>
</div>
</div>
