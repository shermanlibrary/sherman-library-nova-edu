<style>
    .menu.menu--banner.universal.tinsley-gradient,
    header.header,
    footer#footer {
        display: none;
    }
</style>

<div class="container-fluid sharklink">
    <div class="row">
        <div class="col-md-12">
            <portlet>
                <header class="portlet-topper nsu-icon-user">
                    <h2 class="portlet-title">My Library Account</h2>
                </header>
                <div class="portlet-content">
                    <div class="panel panel-default" data-ng-controller="CheckoutsController as cc">
                        <div class="panel-heading" id="headingOne" role="tab">
                            <h4 class="panel-title text-uppercase"><a aria-controls="collapseThree" aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseThree" role="button">Checkouts</a></h4>
                        </div>
                        <div aria-labelledby="headingThree" class="panel-collapse collapse" id="collapseThree" role="tabpanel">
                            <div class="panel-body">
                                <div class="alert alert-info no-margin text-center" role="alert" data-ng-if="!cc.checkouts">You currently do not have anything checked out</div>
                                <div class="table-responsive" data-ng-if="cc.checkouts">

                                    <table class="table no-border table-condensed">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1" data-ng-if="cc.showbutton">RENEW</th>
                                            <th class="col-md-4" data-ng-click="cc.sortType = 'title'; cc.sortReverse = !cc.sortReverse">TITLE</th>
                                            <th class="col-md-1" data-ng-click="cc.sortType = 'barcode'; cc.sortReverse = !cc.sortReverse">BARCODE</th>
                                            <th class="col-md-1" data-ng-click="cc.sortType = 'status'; cc.sortReverse = !cc.sortReverse" data-ng-show="!cc.hidestatus">STATUS</th>
                                            <th class="col-md-1" data-ng-click="cc.sortType = 'renewals'; cc.sortReverse = !cc.sortReverse">RENEWED</th>
                                            <th class="col-md-1" data-ng-click="cc.sortType='checkout_stamp'; cc.sortReverse = !cc.sortReverse">CHECKOUT DATE</th>
                                            <th class="col-md-1" data-ng-click="cc.sortType = 'duedate_stamp'; cc.sortReverse = !cc.sortReverse">DUE DATE</th>
                                            <th class="col-md-1" data-ng-click="cc.sortType='callnumber'; cc.sortReverse = !cc.sortReverse">CALL NUMBER</th>
                                        </tr>
                                        </thead>
                                        <tbody dir-paginate="checkout in cc.checkouts | filter:query | orderBy:cc.sortType:cc.sortReverse | itemsPerPage: cc.pageSize" current-page="cc.currentPage">
                                        <tr checkout-result checkout="checkout"
                                            selected = "cc.selected"
                                            add="cc.addToCart"
                                            remove="cc.removeFromCart"
                                            showbutton="cc.showbutton"
                                            hidestatus="cc.hidestatus"
                                        >
                                        </tr>
                                        </tbody>
                                    </table>

                                    <ng-if data-ng-if="cc.checkouts">
                                    <form class="form-inline" role="form">
                                        <div class="form-group">
                                            <label for="paging">Items per page:</label>
                                            <input id="paging" type="number" min="1" max="{{cc.checkouts.length}}" class="form-control" ng-model="cc.pageSize">
                                            <input class="form-control" type="text" placeholder="Search for checkouts" ng-model="query" autofocus>
                                        </div>
                                    </form>

                                    <div ng-if="cc.showbutton">
                                        <button class="btn btn-default"
                                                ng-show="cc.selected.length <  cc.showbutton"
                                                ng-click="cc.fillCart()">
                                            Select All
                                        </button>

                                        <button class="btn btn-default"
                                                ng-show="cc.selected.length == cc.showbutton"
                                                ng-click="cc.emptyCart()">
                                            Deselect All
                                        </button>


                                        <button class="btn btn-primary"
                                                ng-show="cc.selected.length > 0"
                                                ng-click="cc.formSubmit()">
                                            Submit
                                        </button>
                                    </div>
                                    </ng-if>

                                    <div ng-controller="PagingController" class="paging-controller">
                                        <div class="text-center">
                                            <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="assets/js/templates/dirPagination.html"></dir-pagination-controls>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default" data-ng-controller="HoldsController as hc" data-ng-cloak>
                        <div class="panel-heading" id="headingFour" role="tab">
                            <h4 class="panel-title text-uppercase"><a aria-controls="collapsefour" aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" ng-href="#collapsefour" role="button">Holds </a></h4>
                        </div>
                        <div aria-labelledby="headingfour" class="panel-collapse collapse" id="collapsefour" role="tabpanel">
                            <div class="panel-body">
                                <div class="alert alert-info no-margin text-center" role="alert" data-ng-if="!hc.holds">You currently do not have anything on hold</div>
                                <ng-if data-ng-if="hc.holds">
                                    <form class="form-inline" role="form">
                                        <label for="paging">Holds per page:</label>
                                        <input id="paging" type="number" min="1" max="{{hc.holds.length}}" class="form-control" ng-model="hc.pageSize">
                                        <input class="form-control" type="text" placeholder="Search for holds" ng-model="query" autofocus>
                                    </form>

                                    <button class="btn btn-default"
                                            ng-show="hc.selected.length < hc.holds.length"
                                            ng-click="hc.fillCart()">
                                        Select All
                                    </button>
                                    <button class="btn btn-default"
                                            ng-show="hc.selected.length === hc.holds.length"
                                            ng-click="hc.emptyCart()">
                                        Deselect All
                                    </button>

                                    <button class="btn btn-default"
                                            ng-click="hc.cartSort()">
                                        Sort by {{  hc.requestdate ? ' Hold Requested Date' : ' Pickup Expiration Date'}}
                                    </button>


                                    <button class="btn btn-primary"
                                            ng-show="hc.selected.length > 0"
                                            ng-click="hc.formSubmit()">
                                        Submit
                                    </button>
                                </ng-if>

                                <div class="table-responsive" data-ng-if="hc.holds">
                                    <table class="table no-border table-condensed">
                                        <thead>
                                        <tr>
                                            <th>CANCEL</th>
                                            <th>TITLE</th>
                                            <th>STATUS</th>
                                            <th>PICKUP LOCATION</th>
                                            <th>CANCEL IF NOT FILLED BY</th>
                                        </tr>
                                        </thead>
                                        <tbody tbody dir-paginate="hold in hc.holds | filter:query |  itemsPerPage: hc.pageSize" current-page="hc.currentPage">
                                        <tr hold-result
                                            hold="hold"
                                            selected = "hc.selected"
                                            add="hc.addToCart"
                                            remove="hc.removeFromCart"
                                        >
                                        </tr>
                                        </tbody>
                                    </table>

                                    <div ng-controller="PagingController" class="paging-controller">
                                        <div class="text-center">
                                            <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="assets/js/templates/dirPagination.html"></dir-pagination-controls>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" id="headingfive" role="tab">
                            <h4 class="panel-title text-uppercase"><a aria-controls="collapsefive" aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" href="#collapsefive" role="button">Electronic Documents</a></h4>
                        </div>
                        <div aria-labelledby="headingfive" class="panel-collapse collapse" id="collapsefive" role="tabpanel">
                            <div class="panel-body">
                                <div class="alert alert-info no-margin text-center" role="alert" data-ng-if="!ac.activity.web">You currently do not have any electronic pending</div>
                                <!--<div class="table-responsive">
                                    <table class="table no-border table-condensed">
                                        <thead>
                                        <tr>
                                            <th class="col-md-1">ITEM</th>
                                            <th class="col-md-8">TITLE</th>
                                            <th class="col-md-3">URL</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>A book title</td>
                                            <td><a href="">http://www.somelink.com</a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>-->
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" id="headingsix" role="tab">
                            <h4 class="panel-title text-uppercase"><a aria-controls="collapsesix" aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" href="#collapsesix" role="button">Fees Payments and Reciepts</a></h4>
                        </div>
                        <div aria-labelledby="headingsix" class="panel-collapse collapse" id="collapsesix" role="tabpanel">
                            <div class="panel-body">
                                <div class="alert alert-info no-margin text-center" role="alert" data-ng-if="!ac.activity.fees">You currently do not have any fees or reciepts</div>
                                {{ ac.activity.fees }} {{ac.activity.feeNumber}} {{ ac.activity.balance }}
                                <!--<div class="table-responsive">
                                    <table class="table no-border table-condensed">
                                        <thead>
                                        <tr>
                                            <th class="col-md-5">TITLE</th>
                                            <th class="col-md-5">TYPE</th>
                                            <th class="col-md-2">AMOUNT</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>A book title</td>
                                            <td>Library Fee</td>
                                            <td>$1.75</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>-->
                            </div>
                        </div>
                    </div>
                    <div class="portlet-width"><a class="btn btn-block btn-primary" href="" role="button" target="_blank">Go To My Library Account</a></div>
                </div>
            </portlet>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <portlet>
                <header class="portlet-topper nsu-icon-globe">
                    <h2 class="portlet-title">My Library Account</h2>
                </header>
                <div class="portlet-content sameheight">
                    <div>
                        <div class="list-group"><a class="list-group-item" href="http://sherman.library.nova.edu" target="_blank">Alvin Sherman Library</a> <a class="list-group-item" href="http://sherman.library.nova.edu/sites/ask-a-librarian/" target="_blank">Ask a Librarian</a> <a class="list-group-item" href="http://www.nova.edu/hpdlibrary/" target="_blank">Health Professions Division Library</a> <a class="list-group-item" href="http://nova.campusguides.com/Help" target="_blank">Contact an HPD Librarian</a> <a class="list-group-item" href="http://nova.campusguides.com/oclibrary" target="_blank">Oceanography Library</a> <a class="list-group-item" href="http://nova.campusguides.com/law/library" target="_blank">Panza Maurer Law Library</a> <a class="list-group-item" href="http://copyright.nova.edu" target="_blank">Office of Copyright</a></div>
                    </div>
                </div>
            </portlet>
        </div>
        <div class="col-md-4">
            <portlet>
                <header class="portlet-topper nsu-icon-calendar2">
                    <h2 class="portlet-title">Programs and Events</h2>
                </header>
                <div class="portlet-content sameheight">
                    <ng-controller data-ng-controller="EventController as ec" data-audience="all">

                        <ng-repeat data-ng-repeat="event in ec.events">
                            <article role="article">
                                <a ng-href="{{ event.url }}" class="link link--undecorated" target="_self">
                                    <div class="card">
                                        <span class="card__color-code"></span>
                                        <header class="card__header">
                                            <h3 class="card__title zeta no-margin">{{ event.title }}</h3>
                                            <div class="small-text">
                                                <time class="time">
                                                    <b>{{ event.start }}</b>
                                                    <span class="time__hours" style="color:#999;">{{ event.from }} {{ event.until }}</span>
                                                </time>
                                            </div>
                                        </header>
                                    </div>
                                </a>
                            </article>
                        </ng-repeat>

                    </ng-controller>

                </div>
            </portlet>
        </div>
        <div class="col-md-4">
            <portlet>
                <header class="portlet-topper nsu-icon-resources">
                    <h2 class="portlet-title">Services and Resources</h2>
                </header>
                <div class="portlet-content sameheight">
                    <div>
                        <div class="list-group"><a class="list-group-item" href="https://novacat.nova.edu" target="_blank">Catalog</a> <a class="list-group-item" href="http://sherman.library.nova.edu/linker" target="_blank">Durable Linker</a> <a class="list-group-item" href="http://sherman.library.nova.edu/e-library" target="_blank">E-Library</a> <a class="list-group-item" href="https://sherman.library.nova.edu/auth/index.php?token=illiad" target="_blank">ILLiad</a> <a class="list-group-item" href="http://sherman.library.nova.edu/rooms" target="_blank">Study Room Reservation</a> <a class="list-group-item" href="http://nova.campusguides.com" target="_blank">Subject and Course Guides</a> <a class="list-group-item" href="http://sherman.library.nova.edu/sites/workshops-and-instruction" target="_blank">Workshops and Instruction</a></div>

                    </div>
                    <div class="portlet-width"><a class="btn btn-block btn-primary" href="#" role="button" target="_blank">BUTTON TEXT</a></div>

                </div>

            </portlet>
        </div>
    </div>
</div>