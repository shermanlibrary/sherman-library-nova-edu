 <div ng-controller="HoldsController as hc" ng-cloak>
     <header class="align-center col-md--eightcol col--centered">
         <h1>My Library Holds</h1>
     </header>

     <div class="col-md--eightcol col--centered">

            <section ng-if="hc.holds">

                <div wc-overlay wc-overlay-delay="300">
                    Loading.........
                </div>


                <form class="form" role="form">
                    <input class="form__input" type="text" placeholder="Search for holds" ng-model="query" autofocus>
                    <label for="paging"> Holds per page:</label>
                    <input id="paging" type="number" min="1" max="{{hc.holds.length}}" class="form-control form__input" ng-model="hc.pageSize">
                </form>
                <button class="button button--small button--default no-margin small-text"
                        ng-show="hc.selected.length < hc.holds.length"
                        ng-click="hc.fillCart()">
                    Select All
                </button>
                <button class="button button--small button--default no-margin small-text"
                        ng-show="hc.selected.length === hc.holds.length"
                        ng-click="hc.emptyCart()">
                    Deselect All
                </button>

                <button class="button button--small button--default no-margin small-text"
                        ng-click="hc.cartSort()">
                    Sort by {{  hc.requestdate ? ' Hold Requested Date' : ' Pickup Expiration Date'}}
                </button>


                <button class="button button--small base no-margin small-text"
                        ng-show="hc.selected.length > 0"
                        ng-click="hc.formSubmit()">
                    Submit
                </button>

                <br />
                <table class="table table--responsive">
                    <thead class="has-cards">
                    <tr>
                        <th> CANCEL </th>
                        <th> TITLE  </th>
                        <th> STATUS </th>
                        <th>PICKUP LOCATION</th>
                        <th> CANCEL IF NOT FILLED BY </th>
                    </tr>
                    </thead>


                    <tbody dir-paginate="hold in hc.holds | filter:query |  itemsPerPage: hc.pageSize" current-page="hc.currentPage">
                    <tr hold-result
                        hold="hold"
                        selected = "hc.selected"
                        add="hc.addToCart"
                        remove="hc.removeFromCart"
                        >
                    </tr>
                    </tbody>

                </table>

                <div ng-controller="PagingController" class="paging-controller">
                    <div class="text-center">
                        <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="assets/js/templates/dirPagination.html"></dir-pagination-controls>
                    </div>
                </div>
            </section>
            <section ng-if="hc.error" ng-cloak>
                {{hc.error}}
            </section>

    </div>
</div>

