<div ng-controller="HoldsConfirmController as hc">

    <header class="align-center col-md--eightcol col--centered">
        <h1>My Library Holds</h1>
    </header>

    <div class="col-md--eightcol col--centered">

        <section id="has-cart" ng-if="hc.cart">
            <span ng-if="hc.cart.length === 0">
                No Holds Found.

                 <div class="align-right">
                     <a class="button button--small button--default" ng-href="/holds">Go Back</a>
                 </div>
            </span>

            <div ng-if="hc.cart.length !== 0">
                <table class="table table--responsive">
                    <thead class="has-cards">
                        <tr>
                            <th> CANCEL </th>
                            <th> TITLE  </th>
                            <th> STATUS </th>
                            <th>PICKUP LOCATION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="hold in hc.cart">
                            <td>
                                <a href ng-click="hc.removeFromCart(hold.hold)">Remove</a>
                            </td>
                            <td ng-bind-html="hold.title"></td>
                            <td>{{ hold.status }}</td>
                            <td>{{ hold.pickup }}</td>
                        </tr>
                    </tbody>
                </table>
                <div class="align-right">
                    <button input type="submit" class="button button--small button--primary paypal"
                            ng-if="hc.cart.length !== 0" ng-click="hc.cancelHolds()">
                        Cancel {{ hc.cart.length  === 1 ? 'Hold' : 'Holds' }}
                    </button>
                </div>
            </div>
    </section>
        <section ng-if="hc.holds">
        <div class="alert alert--warning" role="alert" ng-if="hc.cancelerror">
            {{hc.cancelerror}}
            <ul ng-bind-html="hc.errorlist">
            </ul>
        </div>

        <form class="form" role="form">
            <input class="form__input" type="text" placeholder="Search for holds" ng-model="query" autofocus>
            <label for="paging"> Holds per page:</label>
            <input id="paging" type="number" min="1" max="{{hc.holds.length}}" class="form-control form__input" ng-model="hc.pageSize">
        </form>

        <button class="button button--small button--default no-margin small-text"
                ng-show="hc.selected.length < hc.holds.length"
                ng-click="hc.fillCart()">
            Select All
        </button>
        <button class="button button--small button--default no-margin small-text"
                ng-show="hc.selected.length === hc.holds.length"
                ng-click="hc.emptyCart()">
            Deselect All
        </button>

        <button class="button button--small button--default no-margin small-text"
                ng-click="hc.cartSort()">
            Sort by {{  hc.requestdate ? ' Hold Requested Date' : ' Pickup Expiration Date'}}
        </button>


        <button class="button button--small base no-margin small-text"
                ng-show="hc.selected.length > 0"
                ng-click="hc.formSubmit()">
            Submit
        </button>

        <br />
        <table class="table table--responsive">
            <thead class="has-cards">
                <tr>
                    <th> CANCEL </th>
                    <th> TITLE  </th>
                    <th> STATUS </th>
                    <th>PICKUP LOCATION</th>
                    <th> CANCEL IF NOT FILLED BY </th>
                </tr>
            </thead>

            <tbody dir-paginate="hold in hc.holds | filter:query |  itemsPerPage: hc.pageSize" current-page="hc.currentPage"">
                <tr hold-result
                    hold="hold"
                    selected = "hc.selected"
                    add="hc.addToCart"
                    remove="hc.removeFromCart"
                    >
                </tr>
            </tbody>
        </table>
        <div ng-controller="PagingController" class="paging-controller">
            <div class="text-center">
                <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="assets/js/templates/dirPagination.html"></dir-pagination-controls>
            </div>
        </div>

    </section>
    </div>

</div>





    


