<?php
/**
 * Created by PhpStorm.
 * User: jamesh
 * Date: 6/22/14
 * Time: 7:18 PM
 */
class DatabaseObject {
    public $id;
    protected static $record = array();
    protected static $object = null;
    public static $rowcount;

    // Common Database Methods
    public static function find_all(){
        return static::find_by_sql("SELECT * FROM ".static::$table_name);
    }

    public static function find_by_id( $id = 0 ){
        $db = Database::getDB();
        $sql = "SELECT * FROM ".static::$table_name." WHERE ".static::$table_id." = :id LIMIT 1";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id);
        $result->execute();
        $results = $result->fetch(PDO::FETCH_ASSOC);
        $errorInfo = $result->errorInfo();
        //echo $sql;die();
        if(isset($errorInfo[2])) :
            Email::send_SQLError( $errorInfo[2], $sql );
            //echo $errorInfo[2];
            //die();
        endif;
        $result->closeCursor();
        return !empty($results) ? $results : false;
    }

    protected static function get_sql($action){}

    public static function find_by_sql( $sql = '', $params = array() ){
        $db = Database::getDB();
        $result = $db->prepare($sql);
        !empty($params ) ? $result->execute($params) :  $result->execute();
        static::$rowcount = $result->rowCount();
        $results = $result->fetchALL(PDO::FETCH_ASSOC);
        $errorInfo = $result->errorInfo();
        if(isset($errorInfo[2])) :
            Email::send_SQLError( $errorInfo[2], $sql );
            //echo $errorInfo[2];
            //die();
        endif;
        $result->closeCursor();
        return !empty($results) ? $results : false;
    }

    public static function count_all(){
        $db = Database::getDB();
        $sql = "SELECT * FROM ".static::$table_name;
        $result = $db->prepare($sql);
        $result->execute();
        $rowcount = $result->rowCount();
        $result->closeCursor();
        return $rowcount;
    }

    public static function instantiate($record){
        $class_name = get_called_class();
        $object = new $class_name;
        foreach( $record as $attribute => $value ):
            if($object->has_attribute($attribute)):
                $object->$attribute = trim($value);
            endif;
        endforeach;
        return $object;
    }

    protected function has_attribute($attribute){
        $object_vars = $this->attributes();
        return(array_key_exists($attribute,$object_vars));
    }

    protected function attributes(){
        $attributes = array();
        foreach ( static::$db_fields as $field ):
            if( property_exists( $this, $field)):
                $attributes[$field] = $this->$field;
            endif;
        endforeach;
        return $attributes;
    }

    public function save(){
       /* $found = static::find_by_id($this->id);

        if($found):
            echo 'update';die();
        else:
            echo 'create';die();
        endif;*/

        return static::find_by_id($this->id)? $this->update() : $this->create();
    }

    protected function create(){
        $db = Database::getDB();
        $attributes = $this->attributes();
        $sql  = "INSERT INTO ".static::$table_name." (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES (:";
        $sql .= join(",  :", array_keys($attributes));
        $sql .= ")";
        //echo $sql;die();
        $create  = $db->prepare($sql);
        $created = $create->execute($attributes);
        $errorInfo = $create->errorInfo();
        if(isset($errorInfo[2])) :
            Email::send_SQLError( $errorInfo[2], $sql );
            //echo $errorInfo[2];
            //die();
        endif;
        $create->closeCursor();
        if($created) :
           $this->id = $db->lastInsertId();
           return true;
        else :
            return false;
        endif;
    }

    protected function update(){
        $db = Database::getDB();
        $attributes = $this->attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value ) :
            $attribute_pairs[] = "$key = :$key";
        endforeach;
        $attributes['id'] = $this->id;
        $sql  = "UPDATE ".static::$table_name." SET ";
        $sql .= join(", ", $attribute_pairs);
        $sql .= " WHERE ".static::$table_id." =:id";
        $update = $db->prepare($sql);
        $updated = $update->execute($attributes);
        $errorInfo = $update->errorInfo();
        if(isset($errorInfo[2])) :
            Email::send_SQLError( $errorInfo[2], $sql );
            //echo $errorInfo[2];
            //die();
        endif;
        $update->closeCursor();
        return $updated == 1 ? true : false;
    }

    public static function delete( $id=0 ){
        $db = Database::getDB();
        $sql  = "DELETE FROM ".static::$table_name;
        $sql .= " WHERE ".static::$table_id." = :id";
        $sql .= " LIMIT 1";
        $delete = $db->prepare($sql);
        $delete->bindValue(':id', $id);
        $deleted = $delete->execute();
        $errorInfo = $delete->errorInfo();
        if(isset($errorInfo[2])) :
            Email::send_SQLError( $errorInfo[2], $sql );
            //echo $errorInfo[2];
            //die();
        endif;
        $delete->closeCursor();
        return $deleted == 1 ? true : false;
    }

    public function get_id(){
        return isset($this->id) ? $this->id : false;
    }

    protected static function set_records($records) {
        if(is_array( $records )) :
            $objects = array();
            foreach($records as $record):
                $class_name = get_called_class();
                $object = new $class_name;
                foreach ( $record as $key => $value ):
                    $object->$key = $value;
                endforeach;
                $objects[] = $object;
            endforeach;
            return $objects;
        else:
            return false;
        endif;
    }

    public static function get_records($action, $params ){
        $sql    = static::get_sql($action, $params);
        $record = static::find_by_sql( $sql, $params);
        $objects = static::set_records( $record );
        return $objects;
    }


}