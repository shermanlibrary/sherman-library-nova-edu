<?php
class Patron extends DatabaseObject {
    protected static $table_name = 'patrons';
    protected static $table_id = 'RECORD_#';

    protected static $db_fields = array(
        'patronid', 'univ_id', 'ptype','pcode3', 'home_lib', 'moneyowed',
        'fullname', 'billname', 'proper_name',
        'address_line1', 'address_line2', 'city', 'state', 'zip',
        'telephone', 'email', 'sessid'
    );

    public $id;
    protected $patronid;
    protected $univ_id;
    protected $ptype;
    protected $pcode3;
    protected $home_lib;
	protected $moneyowed;
    protected $fullname;
    protected $billname;
    protected $proper_name;
    protected $address_line1;
    protected $address_line2;
    protected $city;
    protected $state;
    protected $zip;
    protected $telephone = '';
    protected $email = '';
    public $sessid;

    public function __construct(){}



    public static function instantiate($patron){


        $record = static::convertAttributes($patron);
        $temp = $record;
        $temp['firstname']        = isset($patron['PATRN_NAME']) ? $patron['PATRN_NAME'] : '';
        if(isset($patron['country']))         : $temp['country']         =  $patron['country']; endif;
        if(isset($patron['invalid_email']))   : $temp['invalid_email']   =  $patron['invalid_email']; endif;
        if(isset($patron['invalid_address'])) : $temp['invalid_address'] =  $patron['invalid_address']; endif;
        if(isset($patron['payments']))        : $temp['payments']        =  $patron['payments']; endif;

        $_SESSION[APPLICATION]['user'] = $temp;

        unset($_SESSION[APPLICATION]['patron']);

        $class_name = get_called_class();
        $object = new $class_name;
        foreach( $record as $attribute => $value ):
            if($object->has_attribute($attribute)):
                $object->$attribute = $value;
                if($attribute == 'patronid'):
                    $object->id = $value;
                endif;
            endif;
        endforeach;
        $object->sessid = session_id();
        //var_dump($object);die();
        return $object;
    }

    public static function convertAttributes( $patron ) {


            $record['patronid']      = $patron['RECORD_#'];
            $record['univ_id']       = $patron['UNIV_ID'];
            $record['ptype']         = isset($patron['P_TYPE']) ? (int)$patron['P_TYPE'] : 200;

            $col = ElibResponse::getPatronType($record['ptype']);
            $record['col']            = $col;

			$record['option']        = isset($patron['option']) ? $patron['option'] : 'shark';
			$record['pass']          = isset($patron['pass']) ? $patron['pass'] : null;
            if($record['ptype'] === 35 ):
                $record['p35dept'] = $patron['DEPT'];
            endif;
            $record['pcode1']        = isset($patron['PCODE1']) ?  $patron['PCODE1'] : '';
            $record['pcode3']        = isset($patron['PCODE3']) ? (int)$patron['PCODE3'] : 200;
            $record['home_lib']      = isset($patron['HOME_LIBR']) ? $patron['HOME_LIBR'] : 'main';
            $record['moneyowed']     = isset($patron['MONEY_OWED']) ? $patron['MONEY_OWED'] : null;
            $record['fullname']      = isset($patron['PATRN_NAME']) ? $patron['PATRN_NAME'] : '';
            $record['billname']      = isset($patron['billname']) ? $patron['billname'] : '';
            $record['proper_name']   = isset($patron['proper_name']) ? $patron['proper_name'] : '';
            $record['address_line1']   = isset($patron['address_line1']) ? $patron['address_line1'] : '';
            $record['address_line2']   = isset($patron['address_line2']) ? $patron['address_line2'] : '';
            $record['city']          = isset($patron['city']) ? $patron['city'] : '';
            $record['state']         = isset($patron['state']) ? $patron['state'] : '';
            $record['zip']           = isset($patron['zip']) ? $patron['zip'] : '';
            $record['address2_line1'] = isset($patron['address2_line1']) ? $patron['address2_line1'] : '';
            $record['address2_line2'] = isset($patron['address2_line2']) ? $patron['address2_line2'] : '';
            $record['city2']          = isset($patron['city2']) ? $patron['city2'] : '';
            $record['state2']         = isset($patron['state2']) ? $patron['state2'] : '';
            $record['zip2']           = isset($patron['zip2']) ? $patron['zip2'] : '';
            $record['telephone']     =  isset($patron['TELEPHONE']) ? $patron['TELEPHONE'] : '';
            $record['email']         =  isset($patron['EMAIL_ADDR']) ? $patron['EMAIL_ADDR'] : '';
            $record['exp_date']      = isset($patron['EXP_DATE']) ? $patron['EXP_DATE'] : '';
            $record['barcode']       = isset($patron['P_BARCODE']) ? $patron['P_BARCODE'] : '';
            $record['nsu_id']        = isset($patron['UNIV_ID_EXP']) ? $patron['UNIV_ID_EXP'] : '';
            $record['checkouts']     = isset($patron['checkouts']) ? $patron['checkouts'] : '';
            $record['fine_count']    = isset($patron['fine_count']) ? $patron['fine_count'] : '';
            $record['hold_count']    = isset($patron['hold_count']) ? $patron['hold_count'] : '';
            $record['lastname']      = isset($patron['lastname']) ? $patron['lastname'] : '';
            $record['firstname']     = isset($patron['firstname']) ? $patron['firstname'] : '';
            $record['middlename']    = isset($patron['middlename']) ? $patron['middlename'] : '';

            $display_name = isset($record['fullname']) ? $record['fullname'] : 'Guest';
            if(isset($record['firstname']) && !empty($record['firstname'])) :
                $display_name = $record['firstname'];

            elseif(isset($record['proper_name']) && !empty($record['proper_name'])) :
                $display_name = $record['proper_name'];

            elseif(isset($record['billname']) && !empty($record['billname'])) :
                $display_name = $record['proper_name'];
            endif;

            $record['display_name'] = $display_name;

            /*
            $location = Helper::get_location();

            if($location):
                $record['location'] = $location;
            endif;
            */

            //print_r($record);die();
            return $record;
    }

    public static function clearAttributes($patron){
        unset($patron['REC_INFO']);
        unset($patron['EXP_DATE']);
        unset($patron['PCODE']);
        unset($patron['PCODE1']);
        unset($patron['PCODE2']);
        unset($patron['PCODE3']);
        unset($patron['P_TYPE']);
        unset($patron['TOT_CHKOUT']);
        unset($patron['TOT_RENWAL']);
        unset($patron['BIRTH_DATE']);
        unset($patron['HOME_LIBR']);
        unset($patron['PMESSAGE']);
        unset($patron['RECORD_#']);
        unset($$patron['REC_LENG']);
        unset($patron['CREATED']);
        unset($patron['UPDATED']);
        unset($patron['REVISIONS']);
        unset($patron['AGENCY']);
        unset($patron['CUR_CHKOUT']);
        unset($patron['MBLOCK']);
        unset($patron['REC_TYPE']);
        unset($patron['REC_LENG']);
        unset($patron['CL_RTRND']);
        unset($patron['CUR_ITEMA']);
        unset($patron['CUR_ITEMB']);
        unset($patron['PIUSE']);
        unset($patron['ILL_REQUES']);
        unset($patron['CUR_ITEMC']);
        unset($patron['CUR_ITEMD']);
        unset($patron['NOTICEPREF']);
        unset($patron['TOTAL_REG']);
        unset($patron['TOT_ATTEND']);
        unset($patron['WAIT_ON_REC']);
        unset($patron['CIRCACTIVE']);
        unset($patron['FINE']);
        unset($patron['PATRN_NAME']);
        unset($patron['ADDRESS']);
        unset($patron['TELEPHONE']);
        unset($patron['P_BARCODE']);
        unset($patron['NOTE']);
        unset($patron['UNIV_ID']);
        unset($patron['EMAIL_ADDR']);
        unset($patron['GENDER']);
        unset($patron['UNIV_ID_EXP']);
    }


}