<?php
/**
 * Created by PhpStorm.
 * User: jamesh
 * Date: 4/14/15
 * Time: 8:25 PM
 */
class ApplicationHelper{


    protected static $_allowed = array('display_name','fullname', 'billname',
        'address_line1', 'address_line2', 'city', 'state','zip',
        'telephone', 'email',
        'ptype', 'pcode1', 'pcode3', 'home_lib',
        'col', 'location',
        'invalid_address'
    );



    public static function setLoginForm(){
        $output = '';
        if(isset($_SERVER['QUERY_STRING'])) :
            $output = '<input type="hidden" name="string" value="'.$_SERVER['QUERY_STRING'].'">';
        endif;
        return $output;
    }

    public static function set_action(){

        if(isset ($_GET['action']) && !empty($_GET['action'])):
            return $action = $_GET['action'];
        elseif(isset ($_SESSION[APPLICATION]['action'])):			
          $action = $_SESSION[APPLICATION]['action'];
        else:
            if (array_key_exists('aid', $_GET)) :
               $action = 'redirect';
            elseif (array_key_exists('package_atoz', $_GET)) :
                $action = 'aid_lookup';
            elseif (array_key_exists('qurl', $_GET)) :
                $action = 'bookmark';
            elseif (array_key_exists('token', $_GET)) :
                $action = 'service';
            elseif (array_key_exists('hold', $_GET)) :
                $action = 'hold';
            elseif (array_key_exists('saved', $_GET)) :
                $action = 'saved';
            elseif (array_key_exists('logup', $_GET)) :
                $_SESSION[APPLICATION]['auth_error'] = 'NR';
                Helper::redirect('.');
                exit;
            else:
                $action = 'home';
            endif;
       endif;

       return $action;
    }

    public static function getAid(){
        return  $aid = (isset($_GET['aid'])) ?  $aid = $_GET['aid'] : 9999;
    }

    public static function getUrl(){
	    $pattern   = "/\/".APPLICATION."\/index.php\?aid=\d+&url=/";
		$az   = "/\&issn_atoz.*/";	
		
        $url = (isset($_GET['url'])) ? $_SERVER['REQUEST_URI'] : null;
		
	    $url = preg_replace( $pattern,'', $url);
		$url = preg_replace( $az,'', $url);
		
		$wrong = array('rft_atitle','rft_title','rft_jtitle','rft_stitle','rft_date','rft_volume','rft_issue','rft_spage','rft_epage','rft_pages','rft_artnum','rft_issn','rft_eissn','rft_aulast','rft_aufirst','rft_auinit','rft_auinit1','rft_auinitm','rft_ausuffix','rft_au','rft_aucorp','rft_isbn','rft_coden','rft_sici','rft_genre','rft_chron','rft_ssn','rft_quarter','rft_part');
		$right = array('rft.atitle','rft.title','rft.jtitle','rft.stitle','rft.date','rft.volume','rft.issue','rft.spage','rft.epage','rft.pages','rft.artnum','rft.issn','rft.eissn','rft.aulast','rft.aufirst','rft.auinit','rft.auinit1','rft.auinitm','rft.ausuffix','rft.au','rft.aucorp','rft.isbn','rft.coden','rft.sici','rft.genre','rft.chron','rft.ssn','rft.quarter','rft.part');
		$url = str_replace( $wrong, $right, $url);
		
        return $url;
    }

    public static function getAZISSN(){
        return $issn_atoz = (isset($_GET['issn_atoz'])) ? $_GET['issn_atoz'] : '';
    }

    public static function getAZTitle(){
        return $title_atoz = (isset($_GET['title_atoz'])) ? $_GET['title_atoz'] : '';
    }

    public static function getToken(){
        return $token = (isset($_GET['token'])) ? $_GET['token'] : 'illiad';
    }

    public static function getQurl(){

        if(isset($_GET['qurl'])):
           return $qurl = $_GET['qurl'];
        else:
            $_SESSION[APPLICATION]['auth_error']['code'] = 'UE';
            $_SESSION[APPLICATION]['action'] = 'auth_error';
            Helper::redirect('.');
        endif;

    }

    public static function APIRoute( $parameters ){

        $out = array();
        if(!isset($parameters['job'])) :
            $out['url'] = '';
            $text = 'ShortURL service is not currently available. Please try again.';
            $out['message'] = '<div class="alert alert--danger" role="alert">'.$text.'</div>';
            return $out;
        endif;

       $job = $parameters['job'];
       //echo $job;die();
   
        switch( $job ) :
            case 'userLookup':
                $session = Session::getSession('user');
                return $session;
            case 'getHolds':
                return SierraCURL::getHolds();
            case 'getCheckouts':
                return SierraCURL::getCheckouts();
            case 'getMenuDbs':
                return static::getMenuDbs($parameters);
            case 'getMenuSubs':
                return static::getMenuSubs($parameters);
            case 'getMenus':
                return static::getMenus($parameters);
            case 'addToHoldCart':
                $cart = new HoldCart( $parameters );
                return $cart->addtoCart();
            case 'removeFromHoldCart':
                $cart = new HoldCart( $parameters );
                return $cart->removeFromCart();
            case 'requestItem':
               return $item = SierraCURL::requestItem( $parameters );

            case 'fillHoldCart':
                $cart = new HoldCart( $parameters );
                return $cart->fillCart();
            case 'emptyHoldCart':
                $out = HoldCart::emptyCart();
                return $out;
            case 'holdCartSort':
                return SierraCURL::getHolds($parameters);
            case 'getHoldsCart':
                $cart = new HoldCart( $parameters );
                return $cart->getItemsInCart();
            case 'cancelHolds':
                $cart = new HoldCart( $parameters );
                return $cart->cancelHolds();

            case 'addToCheckoutCart':
                $cart = new CheckoutCart( $parameters );
                 return $cart->addtoCart();
            case 'removeFromCheckoutCart':
                $cart = new CheckoutCart( $parameters );
                return $cart->removeFromCart();
            case 'fillCheckoutCart':
                $cart = new CheckoutCart( $parameters );
                return $cart->fillCart();
            case 'emptyCheckoutCart':
                $out = CheckoutCart::emptyCart();
                return $out;
            case 'checkoutCartSort':
                return SierraCURL::getCheckouts($parameters);
            case 'getCheckoutsCart':
                $cart = new CheckoutCart( $parameters );
                return $cart->getItemsInCart();
            case 'cancelCheckouts':
                $cart = new CheckoutCart( $parameters );
                return $cart->cancelCheckouts();

            case 'getUserInfo':
                $status = isset($_SESSION[APPLICATION]['status']) ? $_SESSION[APPLICATION]['status'] : 'public';
                $out['status'] = $status;
                if($status === 'public'):
                    return $out;
                endif;
                $session = $_SESSION[APPLICATION];
                $out['info'] = $session;
                return $out;


            case 'getApplication':
                $out['response'] = static::_getApplication();
                return $out;

            case 'getHome':
                $out['response'] = static::_getHome();
                return $out;

            case 'getAccount':
                $out['response'] = static::_getAccount();
                return $out;

            case 'updateEmail':
                return $update = ModPinInfo::updateEmail( $parameters);

            case 'updateHomeLibrary':
                return ModPinInfo::updateHomeLibrary( $parameters);

            case 'novaCatApplication';
                $out['response'] = static::_getNovaCatApplication( $parameters );
                return $out;
            default:
                return 'here now';
        endswitch;
    }


    protected static function _setMenuDBs($params = array()){
        $dbs = PublicDisplay::get_display('menuDbs', $params);
        $outs = array();
        $out  = array();
        foreach($dbs as $db) :
            foreach($db as $key => $value):
                if( $key === 'db_id' || $key === 'title' ) :
                    if($key ==='db_id') :
                        $aid = $value;
                    endif;
                    $out[$key] = trim(htmlentities(strip_tags($value)));
                endif;
            endforeach;
            $outs[] = $out;
        endforeach;
        $output = $outs;
        return $output;
    }

    protected static function getMenuDbs($params = array()){
        $output = array();
        $output['parameter'] = static::_setCol();
        $output['databases'] = static::_setMenuDBs($params);
        return $output;
    }


    protected static function _setMenuSubs($params = array() ){
        $subjects = PublicDisplay::get_display( 'menuSubs', $params);
        $outs = array();
        $out  = array();
        foreach($subjects as $subject) :
            foreach($subject as $key => $value):
                if( $key === 'subj_id' || $key === 'subj_name') :
                    $out[$key] = trim(utf8_encode($value));
                endif;
                if( $key === 'subj_id') :
                    $remotes  = PublicDisplay::get_display('subsCount', $params = array ('cat' => $value));
                    $remote_count   = PublicDisplay::$rowcount;
                    $out['count'] = $remote_count ;
                endif;
            endforeach;
            $outs[] = $out;
        endforeach;
        $output = $outs;
        return $output;
    }

    protected static function getMenuSubs($params = array()){
        $output = array();
        $output['parameter'] = static::_setCol();
        $output['subjects']  = static::_setMenuSubs($params);
        return $output;
    }


    public static function getMenus( $params = array() ){
        $output = array();
        $output['parameter'] = static::_setCol();
        $output['databases'] = static::_setMenuDBs($params);
        $output['subjects']  = static::_setMenuSubs($params);
        return $output;
    }

    protected static function _setCol(){

        $auth = new Authentication();
        $group = $auth->getGroup();
        switch($group):

        case 'db_public_access':
            return 'p';

        case 'db_hpd_access':
            return 'h';

        case 'db_law_access':
            return 'l';

        case 'db_alumni_access':
            return 'a';

        case 'db_ocean_access':
        case 'db_nsu_access':
        default:
            return 'n';

        endswitch;
    }


    protected static function _getApplication(){
        global $status;
        $out = array();
        $out['AppStatus']   = static::_getStatus();
        $out['info']        = static::_getPatronInfo();
        $activity           = static::_getSettings();
        //var_dump($activity);die();
        if(!empty($activity)) :
            $out['activity']  = $activity;
        endif;
        $out['hours']       = Hours::getASLHours();
        return $out;
    }

    protected static function _getNovaCatApplication( $parameters ){
        //global $status;
        $out = array();
        $out['databases']   = ElibResponse::getAllDbsMenuUnlog();
        $out['subjects']    = ElibResponse::getAllSubjectsMenuUnlog();

        $out['AppStatus']   = 'auth_user';
        //$out['AppStatus']   = static::_getStatus();
        $out['info']        = Login::IIIAPILookupNC( $parameters );
        $activity           = static::_getSettings();
        //var_dump($activity);die();
        if(!empty($activity)) :
            $out['activity']  = $activity;
        endif;
        //$out['hours']       = Hours::getASLHours();
        return $out;
    }


    protected static function _getHome(){
        $out = array();
        $out['databases']   = ElibResponse::getAllDbsMenuUnlog();
        $out['subjects']    = ElibResponse::getAllSubjectsMenuUnlog();
        $out['workshops']   = Workshop::getWorkshops();
        $out['programs']    = Event::getEvents();
        return $out;
    }

    protected static function _getStatus(){
        return isset($_SESSION[APPLICATION]['status']) ? $_SESSION[APPLICATION]['status'] : 'public';
    }

    protected static function _getPatronInfo(){

        if(!isset($_SESSION[APPLICATION]['user'])) :
            return false;
        endif;

        $user = $_SESSION[APPLICATION]['user'];
        //var_dump($user);die();
        $allow = array();
        foreach( $user as $key => $value):
            if(in_array($key, static::$_allowed)):
                $allow[$key] = $value;
            endif;
        endforeach;

        return $allow;
        //return $user;
    }

    protected function _getSettings(){
        if(!isset($_SESSION[APPLICATION]['user'])) :
            return false;
        endif;

        $webs = ILLiad::getDeliveredToWeb();
        $rooms = static::getReservations();

        $settings = array();
        $api = new SierraAPI();

        $holds = $api->patronAPICall('holds');
        if($holds === 'API failed'):
            $holds  = SierraCURL::getHoldsNumber();
        endif;

        $checkouts = $api->patronAPICall('checkouts');
        if($checkouts === 'API failed'):
            $checkouts = SierraCurl::getCheckoutsNumber();
        endif;

        $fees = $api->patronAPICall('fines');
        if($fees === 'API failed'):
            $fees = SierraCurl::getFeesInfo();
        endif;

        if($fees) :
            $settings['fees'] = $fees['total'];
            $settings['feeNumber'] = $fees['total'] == 1 ? ' fee': ' fees';
            $settings['balance'] = $fees['balance'];
        endif;

        if($checkouts):
            $settings['checkouts']   = $checkouts['total'];
            $settings['checkoutNumber'] =  $checkouts['total'] == 1 ? ' item': ' items';
        endif;

        if($holds):
            $settings['holds'] = $holds['total'];
            $settings['holdNumber'] = $holds['total'] == 1 ? ' item': ' items';
        endif;

        if($webs):
            $settings['webs'] = $webs;
        endif;

        if($rooms):
            $settings['rooms'] = $rooms;
        endif;


        return $settings;
    }

    protected static function _getAccount(){
        $out['databases']   = ElibResponse::getAllDbsMenuUnlog();
        $out['subjects']    = ElibResponse::getAllSubjectsMenuUnlog();
        return $out;
    }

    public static function setHomeLib( $homelib ){
        switch($homelib):
            case 'hp':
                return 'HPD Library';
            case 'nmb':
                return 'North Miami Beach';
            case 'ocean':
                return 'Oceanographic Center Library';
            case 'main':
            default:
            return  'Alvin Sherman Library';
        endswitch;
    }

    public static function getReservations() {
        if(!isset($_SESSION[APPLICATION]['user'])) :
            return false;
        endif;
        $user = $_SESSION[APPLICATION]['user'];


        $patronid = $user['patronid'];
        $db = RoomsDatabase::getDB();
        $query = "SELECT `reservationid` FROM `reservations` WHERE `patronid` = :patronid
	    AND `end` > Now()
	    ORDER by `start`";
        $result = $db->prepare($query);
        $result->bindValue(':patronid', $patronid);
        $result->execute();
        $results = $result->fetch(PDO::FETCH_ASSOC);
        $rowcount = $result->rowCount();
        $result->closeCursor();
        return $rowcount > 0 ? $rowcount : false;
    }

}