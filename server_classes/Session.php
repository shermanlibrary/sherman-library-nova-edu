<?php
class Session{

    private $status = 'public';
    public  $user_id;
    public  $message;

    function __construct(){
        $this->check_status();
		$this->setSessionTimer();
    }

    public static function session_id(){
        return session_id();
    }


	
	private function setSessionTimer(){
		if(isset($_SESSION)) :
			if (isset($_SESSION[APPLICATION]['LAST_ACTIVITY']) && (time() - $_SESSION[APPLICATION]['LAST_ACTIVITY'] > 1800)) :
				// last request was more than 30 minutes ago
				session_unset();     // unset $_SESSION variable for the run-time 
				session_destroy();   // destroy session data in storage
			endif;
			$_SESSION[APPLICATION]['LAST_ACTIVITY'] = time(); // update last activity time stamp
		endif;
	}

    public function message( $msg = ""){
        if(!empty($msg)) :
            return $_SESSION[APPLICATION]['message'] = $msg;
        else :
            return $this->message;
        endif;
    }

    public function check_status(){
        return isset($_SESSION[APPLICATION]['status']) ? $_SESSION[APPLICATION]['status'] : 'public';
    }


    public static function getSession($name = null) {
        if (!empty($name)) :
            return isset($_SESSION[APPLICATION][$name]) ? $_SESSION[APPLICATION][$name] : null;
        else:
            return null;
        endif;
    }

    public static function setSession($name = null, $value = null) {
        if (!empty($name) && !empty($value)) :
            $_SESSION[APPLICATION][$name] = $value;
        endif;
    }

    public static function clear($id = null) {
        if (!empty($id) && isset($_SESSION[$id])) :
            $_SESSION[$id] = null;
            unset($_SESSION[$id]);
        else :
            session_destroy();
        endif;
    }
}


$session = new Session();
$status  = $session->check_status();



