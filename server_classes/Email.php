<?php


class Email {
    public $email;
    public $subject;
    public $message;
    public $headers;
    private $staff_mail = 'jamesh@nova.edu, jamesh0577@gmail.com';

    public function construct(){
        $headers  = "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $headers .= 'From: Library Fees <libsys2@nova.edu>' . "\n";
        $headers .= 'Cc: jamesh0577@gmail.com'. "\n";
        $this->headers = $headers;
    }

       public function errorReport($username, $label, $aid=NULL, $url=NULL )	{
        $patron = Session::getSession('user');
        $this->email = $this->staff_mail;
        $this->subject = 'Authentication Error - '.$label;
        $msg = 'The following issue was encountered by '. $patron['billname'] . '('.$username.') on ' .date('m/d/y h:i' ,time()). '.'."<br /><br />\r\n";
        if(!is_null($aid)):
            $msg .= 'This link (<strong>'. $url . '</strong>) was a mismatch for AID: '.$aid.'.'."<br /><br />\r\n";
        endif;
        $msg .= 'Error Message: '.$label. "<br /><br />\r\n";
        $msg .= 'Sherman Library Application';
        $this->message = $msg;
        $headers  = "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $headers .= 'From: Sherman Library Application <libsys2@nova.edu>' . "\n";
        $headers .= 'Cc: jamesh@nova.edu'. "\n";
        $this->headers = $headers;
        //$headers .= 'X-Mailer: PHP/' . phpversion();
        $sent = $this->send_mail();
        return $sent ? true : false;
    }

    public static function send_SQLError( $errorInfo, $sql)	{
        $user = Session::getSession('user');
        if(empty($user)):
            $user['billname'] = 'undefined';
            $user['univ_id']  = 'undefined';
        endif;
        $class_name = get_called_class();
        $object = new $class_name;
        $object->email = $object->staff_mail;
        $object->subject = 'SQL Error Sherman Library Portal';
        $message = '';
        $message .= 'Report at '.date('m-d-Y h:i').'.<br />';
        $message .= "An an error occurred for the following SQL statement: <br /> ";
        $message .= 'SQL: '.$sql."<br />";
        $message .= 'Error: '.$errorInfo."<br /><br />";
        $message .= 'Error encountered by: '.$user['billname'].' - '.$user['univ_id'].'.';
        $message .= '<br /><br />E-Card Portal';
        $object->message = $message;
        $headers  = "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $headers .= 'From: Sherman Library Portal<libsys2@nova.edu>' . "\n";
        $headers .= 'Cc: jamesh@nova.edu'. "\n";
        $object->headers = $headers;
        $sent = $object->send_mail();
        return $sent ? true : false;
    }

    public static function send_connectionError( $errorInfo, $dsn)	{
        $user = Session::getSession('user');
        if(empty($user)):
            $user['billname'] = 'undefined';
            $user['univ_id']  = 'undefined';
        endif;
        $class_name = get_called_class();
        $object = new $class_name;
        $object->email = $object->staff_mail;
        $object->subject = 'SQL Error Sherman Library Portal';
        $message = '';
        $message .= 'Report at '.date('m-d-Y h:i').'.<br />';
        $message .= "An an error occurred for the following SQL statement: <br /> ";
        $message .= 'DSN: '.$dsn."<br />";
        $message .= 'Error: '.$errorInfo."<br /><br />";
        $message .= 'Error encountered by: '.$user['billname'].' - '.$user['univ_id'].'.';
        $message .= '<br /><br />E-Card Portal';
        $object->message = $message;
        $headers  = "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $headers .= 'From: Sherman Library Portal<libsys2@nova.edu>' . "\n";
        $headers .= 'Cc: jamesh@nova.edu'. "\n";
        $object->headers = $headers;
        $sent = $object->send_mail();
        return $sent ? true : false;
    }

    private function setEmail(){
        $patron = Session::getSession('user');
        return $email = $patron['email'];
    }

    private function getEmail(){
        return $this->setEmail();
    }

    public function send_mail(){
        $mail = mail($this->email, $this->subject, $this->message, $this->headers);
       // echo $mail;die();
        return $mail ? true : false;
    }


}
