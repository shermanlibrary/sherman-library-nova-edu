<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('log_errors', 1);
ini_set('log_errors_max_len', 0);
ini_set('error_log', 'log\error_log.txt');

require_once 'inc\autoload.php';

//print_r($_SESSION[APPLICATION]);
switch( $status ) :
    case APPLICATION.'_user':
        include CONTROLLER.'user.php';
        break;
    case APPLICATION.'_staff':
        include CONTROLLER.'staff.php';
        break;
    default:
        //Helper::SecureRedirect();
        include CONTROLLER.'public.php';
        break;
endswitch;