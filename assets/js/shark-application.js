// Application 7/20/16
(function(){
    'use strict';
    var app = angular.module('sharklinkApp',[]);
	
})();

//Data Service
(function() {
    'use strict';

    var dataService = function($http, $q) {
        this.$http = $http;
        this.$q = $q;
    };

    dataService.$inject = ['$http', '$q'];

    dataService.prototype.getFeatures  = function( audience ) {
        var _this = this;
        var audience = typeof audience !== 'undefined' ? audience : 'all';

        return _this.$http.get('http://sherman.library.nova.edu/sites/wp-json/features/v2/ads/?audience=' + audience )
            .then(function(response){
                if (typeof response.data === 'object') {
                    return response.data;
                } else {
                    return response.data;
                }

            }, function(response) {
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.checkSession = function() {
        var _this = this;
        return _this.$http.post('api/endpoint.php', { job : 'getUserInfo'})
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response);
                }

            }, function(response) {
                // something went wrong
                return 'public';
            })
    };

    dataService.prototype.getUserInfo = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getUserInfo'})
            .then(function(response){
                if (response.data.status === 'public') {
                    // console.log(response.data);
                    return _this.$q.reject('AUTH_REQUIRED');
                } else {
                    // console.log(response.data);
                    return response.data;
                }
            }, function(response) {
                //console.log(response);
                return 'public';
            })
    };




    dataService.prototype.getApplication  = function() {
        var _this = this;
        return _this.$http.get('../auth/api/endpoint.php?job=getApplication')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);

                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getHome  = function() {
        var _this = this;
        return _this.$http.get('../auth/api/endpoint.php?job=getHome')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);

                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getTitles  = function( query ) {
        var _this = this;
        return _this.$http.get('../ftf/endpoint.php?query='+query )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };


    dataService.prototype.getAccount  = function() {
        var _this = this;
        return _this.$http.get('../auth/api/endpoint.php?job=getAccount')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);
                return _this.$q.reject(response.data);
            })
    };


    dataService.prototype.getASLBuildingHours  = function() {
        var _this = this;
        return _this.$http.get('../auth/api/endpoint.php?job=getASLBuildingHours')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getAllDbsMenuUnlog  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getAllDbsMenuUnlog'})
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);

                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getAllSubjectsMenuUnlog  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getAllSubjectsMenuUnlog'})
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);

                return _this.$q.reject(response.data);
            })
    };


    dataService.prototype.updateEmail  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.updateHomeLibrary  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };


    dataService.prototype.getGuides  = function() {
        var _this = this;
        return _this.$http.get( '//lgapi.libapps.com/1.1/subjects?site_id=142&key=ea1d92be94311f8f083ebed28a0b5f10&guide_published=2' )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getEvents  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getEvents'})
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);

                return _this.$q.reject(response.data);
            })
    };



    dataService.prototype.getWorkshops  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getWorkshops'})
            .then(function(response){
                if (typeof response.data === 'object') {
                    console.log(response.data);
                    return response.data;
                } else {
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getCheckouts = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getCheckouts'})
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.fillCheckoutCart  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.emptyCheckoutCart  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'emptyCheckoutCart'} )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.addToCheckoutCart  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.removeFromCheckoutCart  = function( object ) {
		console.log(object);
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.checkoutCartSort  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getCheckoutsCart  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getCheckoutsCart'} )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.submitRenewal = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'submitRenewal'} )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getHolds = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getHolds'})
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };


    dataService.prototype.addToHoldCart  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.removeFromHoldCart  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.fillHoldCart  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.emptyHoldCart  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'emptyHoldCart'} )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.holdCartSort  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getHoldsCart  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getHoldsCart'} )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.cancelHolds  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'cancelHolds'} )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getBitbucketIssues = function( repository, state = 'open' ) {

      var _this = this,
          query;

      switch ( state ) {

        case 'open' :
          query = '?q=state!="resolved"';
          break;

        case 'resolved' :
          query = '?q=state="resolved"';
          break;

        default :
          query = '?q=state!="resolved"';
          break;

      }

      return _this.$http.get('https://api.bitbucket.org/2.0/repositories/shermanlibrary/' + repository + '/issues' + query)
          .then(function(response){
              if (typeof response.data === 'object') {
                  return response.data;
              } else {
                  return response.data;
              }

          }, function(response) {
              return _this.$q.reject(response.data);
          })


    };

    angular.module('sharklinkApp')
        .service('dataService', dataService);

}());

// CONTROLLERS
(function(){

    'use strict';

    var ApplicationController = function( $rootScope, $window, $interval, dataService ){

        var vm = this;
            vm.col = 'n';
            vm.libraries = [
                { code: 'main', label: 'Alvin Sherman Library'},
                { code: 'nmb', label: 'North Miami Beach'},
                { code: 'ocean', label: 'Oceanographic Center Library'}
            ];

            vm.changers = ['main', 'nmb', 'ocean'];

        $rootScope.$on('$routeChangeSuccess', function() {
            vm.title = $route.current.title;
            routeService.requireConditions();
        });

        function getApplication(){
            dataService.getApplication().then(function(data){
                console.log(data);
                if(data) {
                    vm.hours  = data.response.hours;
                    vm.status = data.response.AppStatus;

                    if(vm.status !== 'public'){
                        vm.info             = data.response.info;
                        //console.log(data.response.info);
                        vm.email_update     = data.response.info.email;
                        vm.activity         = data.response.activity;
                        vm.col              = vm.info.col;
                        vm.selectedLibrary =  vm.info.home_lib;
                    }
                }
            });
        }

        function checkSession(){
            dataService.checkSession().then(function(data) {
                //console.log(data);
                if(data) {
                    if(data.status === 'public') {
                        //console.log('go now');
                        $window.location.href = "./logout.php";
                    }else{
                        //console.log('nope');
                    }
                }
            });
        }

        //25 minutes to logout
        //$interval(checkSession, 1500000);
        //20 minutes to logout
        $interval(checkSession, 1200000);
        //$interval(checkSession, 90);

        getApplication();

        /*var userMenuToggle = function(){
            $rootScope.display =   $rootScope.display == 'none' ? 'block' : 'none';
        };*/

        var userMenu = (function() {

          $rootScope.userMenuToggle = false;

          function toggle() {

            $rootScope.userMenuToggle = !$rootScope.userMenuToggle;
          }

          return {
            toggle: toggle
          }

        })();

        var librariesMenu = (function() {

          $rootScope.librariesMenuToggle = false;

          function toggle() {
            $rootScope.librariesMenuToggle = !$rootScope.librariesMenuToggle;
          }

          return {
            toggle: toggle
          };

        })();

        var showModalWindow = function( object ){
            object.show = true;
            vm.modal = object;
        };

        var closeModalWindow = function(){
            vm.modal = {};
        };

        var updateEmail = function(){
            console.log(vm.email_update);
            var object = {
                job: 'updateEmail',
                email: vm.email_update
            };
            console.log(object);
            dataService.updateEmail(object).then(function(data){
                console.log(data);

                if(data.update_error){
                    vm.email_update =  vm.info.email;
                    vm.update_message = '<div class="alert alert--warning">' + data.update_error + '</div>';
                }

                if(data.response){
                    vm.email_update = data.response;
                    vm.info.email = data.response;
                    vm.update_message = '<div class="alert alert--success">Email updated!</div>';
                }

            });
        };

        var updateHomeLibrary = function(){

            vm.update_message = null;

            var object = {
                job: 'updateHomeLibrary',
                field: 'fixedFields',
                locx00 : vm.selectedLibrary,
                original : vm.info.home_lib

            };

            dataService.updateHomeLibrary(object).then(function(data){
                console.log(data);

                if(data.update_error){
                    vm.update_message = '<div class="alert alert--warning">' + data.update_error + '</div>';
                }

                if(data.response){

                    vm.info.home_lib = data.response;
                    vm.update_message = '<div class="alert alert--success">Home Library updated!</div>';
                }

            });
        };

        var inArray =   function(needle, haystack) {
            for(var i in haystack) {
                if(haystack[i] == needle) return true;
            }
            return false;
        };

        $rootScope.librariesMenu    = librariesMenu;
        $rootScope.userMenu         = userMenu;
        vm.showModalWindow          = showModalWindow;
        vm.closeModalWindow         = closeModalWindow;
        vm.updateEmail              = updateEmail;
        vm.updateHomeLibrary        = updateHomeLibrary;
        vm.inArray                  = inArray;
    };

    ApplicationController .$inject = [ '$rootScope', '$window', '$interval', 'dataService' ];

    angular.module('sharklinkApp')
        .controller('ApplicationController', ApplicationController);

})();