// Application 7/20/16
(function(){
    'use strict';

    var app = angular.module('shermanApp', ['ngRoute', 'ngSanitize', 'angularUtils.directives.dirPagination', 'wc.Directives']);

    app.constant('API_URL', '//sherman.library.nova.edu');

    app.run(['$rootScope', '$window', '$location', function($rootScope, $window, $location ) {

        $rootScope.$on( "$routeChangeStart", function(event, next, current, error) {
        });

        // Track the route as a pageview with Google Analytics
        $rootScope.$on( '$routeChangeSuccess', function() {

            $window.ga( 'send', 'pageview', { page: $location.url() });

        });

        $rootScope.$on( "$routeChangeError", function(event, next, current, error) {
            if(error === 'AUTH_REQUIRED'){
                var path = $location.path();
                $window.location = '/auth/index.php?action=login&route='+path;
            }
        });

    }]);

    app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {



        $routeProvider
            .when('/account', {
                templateUrl: 'views/account.php',
                title : 'My Library Account',
                resolve: {
                    auth: [ 'dataService', function( dataService) {

                        return dataService.getUserInfo();
                    }],

                    routeRules: ['routeService', function(routeService){

                        return routeService.setNonHome();
                    }]
                }
            })

            .when('/accountMS', {
                templateUrl: 'views/page-account.php',
                title : 'Library Account',
                resolve: {
                    auth: [ 'dataService', function( dataService) {

                        return dataService.getUserInfo();
                    }],

                    routeRules: ['routeService', function(routeService){

                        return routeService.setNonHome();
                    }]
                }
            })

            .when('/checkouts', {
                templateUrl: 'views/checkouts.php',
                title: 'Checkouts',
                resolve: {
                    auth: [ 'dataService', function(dataService) {
                       return dataService.getUserInfo();
                    }],

                    routeRules: ['routeService', function(routeService){
                        return routeService.setNonHome();
                    }]
                }
            })

            .when('/checkoutsconfirm', {
                templateUrl: 'views/checkoutsconfirm.php',
                resolve: {
                    auth: [ 'dataService', function(dataService) {
                        return dataService.getUserInfo();
                    }],

                    routeRules: ['routeService', function(routeService){
                        return routeService.setNonHome();
                    }]
                }
            })

            .when('/friends', {
              templateUrl : 'views/friends.php',
              resolve : {
                routeRules : [ 'routeService', function( routeService ) {
                    return routeService.loadCSS( 'http://sherman2.library.nova.edu/cdn/styles/css/public-global/s--friends.css');
                }]
              },
              title : 'Circle of Friends'

            })

      			.when('/checkoutsrenew', {
      				  templateUrl: 'views/checkoutsrenew.php',
                        resolve: {
                          auth: [ 'dataService', function(dataService) {
      						console.log('checkoutsrenew');
                              return dataService.getUserInfo();
                          }],

                          routeRules: ['routeService', function(routeService){
                              return routeService.setNonHome();
                          }]
                      }
                  })

            .when('/holds', {
                templateUrl: 'views/holds.php',
                resolve: {
                    auth: [ 'dataService', function(dataService) {
                        return dataService.getUserInfo();
                    }],

                    routeRules: ['routeService', function(routeService){
                        return routeService.setNonHome();
                    }]
                }
            })

            .when('/holdsconfirm', {
                templateUrl: 'views/holdsconfirm.php',
                resolve: {
                    auth: [ 'dataService', function(dataService) {
                        return dataService.getUserInfo();
                    }],

                    routeRules: ['routeService', function(routeService){
                        return routeService.setNonHome();
                    }]
                }
            })

            .when('/holocaust', {
              templateUrl : 'views/holocaust-memorial.php',
              resolve : {
                routeRules : [ 'routeService', function( routeService ) {
                    return routeService.loadCSS( 'http://sherman2.library.nova.edu/cdn/styles/css/public-global/s--holocaust.css');
                }]
              },
              title : 'Holocaust Reflection and Resource Room'

            })

            .when('/holds', {
                templateUrl: 'views/holds.php',
                resolve: {
                    auth: [ 'dataService', function(dataService) {
                        return dataService.getUserInfo();
                    }],

                    routeRules: ['routeService', function(routeService){
                        return routeService.setNonHome();
                    }]
                }
            })

            .when( '/settings', {
              templateUrl: 'views/settings.php',
              title: 'My Library Account – Settings',
              resolve: {
                  auth: [ 'dataService', function( dataService) {
                      return dataService.getUserInfo();
                  }],

                  routeRules: ['routeService', function(routeService){
                      return routeService.setNonHome();
                  }]
              }
            })


            .when( '/staffMockup', {
              templateUrl: 'views/staffMockup.php',
              title: 'Library Account – Staff',
              resolve: {
                  auth: [ 'dataService', function( dataService) {
                      return dataService.getUserInfo();
                  }],

                  routeRules: ['routeService', function(routeService){
                      return routeService.setNonHome();
                  }]
              }
            })

            .when( '/status', {
              templateUrl: 'views/page-system-status.php',
              title: 'Libraries Systems Status',
              resolve: {
                  auth: [ 'dataService', function( dataService) {
                      return dataService.getUserInfo();
                  }],

                  routeRules: ['routeService', function(routeService){
                      return routeService.setNonHome();
                  }]
              }
            })


            .when( '/status/:repository', {
              controller : 'IssuesController',
              templateUrl: 'views/page-system-status.php',
              title: 'Libraries Systems Status',
              resolve: {
                  auth: [ 'dataService', function( dataService) {
                      return dataService.getUserInfo();
                  }],

                  routeRules: ['routeService', function(routeService){
                      return routeService.setNonHome();
                  }]
              }
            })

            .when( '/', {
                templateUrl: 'views/home.php',
                resolve: {
                    routeRules: ['routeService', function( routeService ){
                        routeService.loadCSS( '//sherman.library.nova.edu/cdn/styles/css/public-global/s--home.css');
                        routeService.setHome();
                    }]
                }
            })

            .otherwise({
                redirectTo: '/'
            });

        $locationProvider.html5Mode(true);
    }]);

})();

//Route Service
(function() {
    'use strict';

    var routeService = function ($rootScope) {

        function requireConditions(){
            var script = document.createElement("script");
            script.type = "text/javascript";
            script.src = "//sherman.library.nova.edu/cdn/scripts/config.js";
            document.querySelector("body").appendChild(script);
        }

        function setHome() {
            $rootScope.display = 'none';
            $rootScope.message = 'Public Library Services';
            $rootScope.messageURL = 'http://public.library.nova.edu';
            return true;
        }

        function setNonHome() {
            $rootScope.display = 'none';
            $rootScope.message = 'Ask a Librarian';
            $rootScope.messageURL = 'http://lib.nova.edu/ask';
            return true;
        }

        return {
            loadCSS     : loadCSS,
            requireConditions : requireConditions,
            setHome     : setHome,
            setNonHome  : setNonHome
        };
    };

    routeService.$inject=['$rootScope'];

    angular
      .module('shermanApp')
      .factory('routeService', routeService);
}());

//Data Service
(function() {
    'use strict';

    var dataService = function($http, $q) {
        this.$http = $http;
        this.$q = $q;
    };

    dataService.$inject = ['$http', '$q'];

    dataService.prototype.getFeatures  = function( audience ) {
        var _this = this;
        var audience = typeof audience !== 'undefined' ? audience : 'all';

        return _this.$http.get('http://sherman.library.nova.edu/sites/wp-json/features/v2/ads/?audience=' + audience )
            .then(function(response){
                if (typeof response.data === 'object') {
                    return response.data;
                } else {
                    return response.data;
                }

            }, function(response) {
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.checkSession = function() {
        var _this = this;
        return _this.$http.post('api/endpoint.php', { job : 'getUserInfo'})
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response);
                }

            }, function(response) {
                // something went wrong
                return 'public';
            })
    };

    dataService.prototype.getUserInfo = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getUserInfo'})
            .then(function(response){
                if (response.data.status === 'public') {
                    // console.log(response.data);
                    return _this.$q.reject('AUTH_REQUIRED');
                } else {
                    // console.log(response.data);
                    return response.data;
                }
            }, function(response) {
                //console.log(response);
                return 'public';
            })
    };




    dataService.prototype.getApplication  = function() {
        var _this = this;
        return _this.$http.get('../auth/api/endpoint.php?job=getApplication')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);

                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getHome  = function() {
        var _this = this;
        return _this.$http.get('../auth/api/endpoint.php?job=getHome')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);

                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getTitles  = function( query ) {
        var _this = this;
        return _this.$http.get('../ftf/endpoint.php?query='+query )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };


    dataService.prototype.getAccount  = function() {
        var _this = this;
        return _this.$http.get('../auth/api/endpoint.php?job=getAccount')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);
                return _this.$q.reject(response.data);
            })
    };


    dataService.prototype.getASLBuildingHours  = function() {
        var _this = this;
        return _this.$http.get('../auth/api/endpoint.php?job=getASLBuildingHours')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getAllDbsMenuUnlog  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getAllDbsMenuUnlog'})
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);

                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getAllSubjectsMenuUnlog  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getAllSubjectsMenuUnlog'})
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);

                return _this.$q.reject(response.data);
            })
    };


    dataService.prototype.updateEmail  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.updateHomeLibrary  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };


    dataService.prototype.getGuides  = function() {
        var _this = this;
        return _this.$http.get( '//lgapi.libapps.com/1.1/subjects?site_id=142&key=ea1d92be94311f8f083ebed28a0b5f10&guide_published=2' )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getEvents  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getEvents'})
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);

                return _this.$q.reject(response.data);
            })
    };



    dataService.prototype.getWorkshops  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getWorkshops'})
            .then(function(response){
                if (typeof response.data === 'object') {
                    console.log(response.data);
                    return response.data;
                } else {
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getCheckouts = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getCheckouts'})
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.fillCheckoutCart  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.emptyCheckoutCart  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'emptyCheckoutCart'} )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.addToCheckoutCart  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.removeFromCheckoutCart  = function( object ) {
		console.log(object);
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.checkoutCartSort  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getCheckoutsCart  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getCheckoutsCart'} )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.submitRenewal = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'submitRenewal'} )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getHolds = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getHolds'})
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };


    dataService.prototype.addToHoldCart  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.removeFromHoldCart  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.fillHoldCart  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.emptyHoldCart  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'emptyHoldCart'} )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.holdCartSort  = function( object ) {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', object )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getHoldsCart  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getHoldsCart'} )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.cancelHolds  = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'cancelHolds'} )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getBitbucketIssues = function( repository, state = 'open' ) {

      var _this = this,
          query;

      switch ( state ) {

        case 'open' :
          query = '?q=state!="resolved"';
          break;

        case 'resolved' :
          query = '?q=state="resolved"';
          break;

        default :
          query = '?q=state!="resolved"';
          break;

      }

      return _this.$http.get('https://api.bitbucket.org/2.0/repositories/shermanlibrary/' + repository + '/issues' + query)
          .then(function(response){
              if (typeof response.data === 'object') {
                  return response.data;
              } else {
                  return response.data;
              }

          }, function(response) {
              return _this.$q.reject(response.data);
          })


    };

    angular.module('shermanApp')
        .service('dataService', dataService);

}());
//Pagination Directive
/**
 * dirPagination - AngularJS module for paginating (almost) anything.
 *
 *
 * Credits
 * =======
 *
 * Daniel Tabuenca: https://groups.google.com/d/msg/angular/an9QpzqIYiM/r8v-3W1X5vcJ
 * for the idea on how to dynamically invoke the ng-repeat directive.
 *
 * I borrowed a couple of lines and a few attribute names from the AngularUI Bootstrap project:
 * https://github.com/angular-ui/bootstrap/blob/master/src/pagination/pagination.js
 *
 * Copyright 2014 Michael Bromley <michael@michaelbromley.co.uk>
 */

(function() {

    /**
     * Config
     */
    var moduleName = 'angularUtils.directives.dirPagination';
    var DEFAULT_ID = '__default';

    /**
     * Module
     */
    var module;
    try {
        module = angular.module(moduleName);
    } catch(err) {
        // named module does not exist, so create one
        module = angular.module(moduleName, []);
    }

    module
        .directive('dirPaginate', ['$compile', '$parse', 'paginationService', dirPaginateDirective])
        .directive('dirPaginateNoCompile', noCompileDirective)
        .directive('dirPaginationControls', ['paginationService', 'paginationTemplate', dirPaginationControlsDirective])
        .filter('itemsPerPage', ['paginationService', itemsPerPageFilter])
        .service('paginationService', paginationService)
        .provider('paginationTemplate', paginationTemplateProvider);

    function dirPaginateDirective($compile, $parse, paginationService) {

        return  {
            terminal: true,
            multiElement: true,
            compile: dirPaginationCompileFn
        };

        function dirPaginationCompileFn(tElement, tAttrs){

            var expression = tAttrs.dirPaginate;
            // regex taken directly from https://github.com/angular/angular.js/blob/master/src/ng/directive/ngRepeat.js#L211
            var match = expression.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?\s*$/);

            var filterPattern = /\|\s*itemsPerPage\s*:[^|]*/;
            if (match[2].match(filterPattern) === null) {
                throw 'pagination directive: the \'itemsPerPage\' filter must be set.';
            }
            var itemsPerPageFilterRemoved = match[2].replace(filterPattern, '');
            var collectionGetter = $parse(itemsPerPageFilterRemoved);

            addNoCompileAttributes(tElement);

            // If any value is specified for paginationId, we register the un-evaluated expression at this stage for the benefit of any
            // dir-pagination-controls directives that may be looking for this ID.
            var rawId = tAttrs.paginationId || DEFAULT_ID;
            paginationService.registerInstance(rawId);

            return function dirPaginationLinkFn(scope, element, attrs){

                // Now that we have access to the `scope` we can interpolate any expression given in the paginationId attribute and
                // potentially register a new ID if it evaluates to a different value than the rawId.
                var paginationId = $parse(attrs.paginationId)(scope) || attrs.paginationId || DEFAULT_ID;
                paginationService.registerInstance(paginationId);

                var repeatExpression = getRepeatExpression(expression, paginationId);
                addNgRepeatToElement(element, attrs, repeatExpression);

                removeTemporaryAttributes(element);
                var compiled =  $compile(element);

                var currentPageGetter = makeCurrentPageGetterFn(scope, attrs, paginationId);
                paginationService.setCurrentPageParser(paginationId, currentPageGetter, scope);

                if (typeof attrs.totalItems !== 'undefined') {
                    paginationService.setAsyncModeTrue(paginationId);
                    scope.$watch(function() {
                        return $parse(attrs.totalItems)(scope);
                    }, function (result) {
                        if (0 <= result) {
                            paginationService.setCollectionLength(paginationId, result);
                        }
                    });
                } else {
                    scope.$watchCollection(function() {
                        return collectionGetter(scope);
                    }, function(collection) {
                        if (collection) {
                            paginationService.setCollectionLength(paginationId, collection.length);
                        }
                    });
                }

                // Delegate to the link function returned by the new compilation of the ng-repeat
                compiled(scope);
            };
        }

        /**
         * If a pagination id has been specified, we need to check that it is present as the second argument passed to
         * the itemsPerPage filter. If it is not there, we add it and return the modified expression.
         *
         * @param expression
         * @param paginationId
         * @returns {*}
         */
        function getRepeatExpression(expression, paginationId) {
            var repeatExpression,
                idDefinedInFilter = !!expression.match(/(\|\s*itemsPerPage\s*:[^|]*:[^|]*)/);

            if (paginationId !== DEFAULT_ID && !idDefinedInFilter) {
                repeatExpression = expression.replace(/(\|\s*itemsPerPage\s*:[^|]*)/, "$1 : '" + paginationId + "'");
            } else {
                repeatExpression = expression;
            }

            return repeatExpression;
        }

        /**
         * Adds the ng-repeat directive to the element. In the case of multi-element (-start, -end) it adds the
         * appropriate multi-element ng-repeat to the first and last element in the range.
         * @param element
         * @param attrs
         * @param repeatExpression
         */
        function addNgRepeatToElement(element, attrs, repeatExpression) {
            if (element[0].hasAttribute('dir-paginate-start') || element[0].hasAttribute('data-dir-paginate-start')) {
                // using multiElement mode (dir-paginate-start, dir-paginate-end)
                attrs.$set('ngRepeatStart', repeatExpression);
                element.eq(element.length - 1).attr('ng-repeat-end', true);
            } else {
                attrs.$set('ngRepeat', repeatExpression);
            }
        }

        /**
         * Adds the dir-paginate-no-compile directive to each element in the tElement range.
         * @param tElement
         */
        function addNoCompileAttributes(tElement) {
            angular.forEach(tElement, function(el) {
                if (el.nodeType === Node.ELEMENT_NODE) {
                    angular.element(el).attr('dir-paginate-no-compile', true);
                }
            });
        }

        /**
         * Removes the variations on dir-paginate (data-, -start, -end) and the dir-paginate-no-compile directives.
         * @param element
         */
        function removeTemporaryAttributes(element) {
            angular.forEach(element, function(el) {
                if (el.nodeType === Node.ELEMENT_NODE) {
                    angular.element(el).removeAttr('dir-paginate-no-compile');
                }
            });
            element.eq(0).removeAttr('dir-paginate-start').removeAttr('dir-paginate').removeAttr('data-dir-paginate-start').removeAttr('data-dir-paginate');
            element.eq(element.length - 1).removeAttr('dir-paginate-end').removeAttr('data-dir-paginate-end');
        }

        /**
         * Creates a getter function for the current-page attribute, using the expression provided or a default value if
         * no current-page expression was specified.
         *
         * @param scope
         * @param attrs
         * @param paginationId
         * @returns {*}
         */
        function makeCurrentPageGetterFn(scope, attrs, paginationId) {
            var currentPageGetter;
            if (attrs.currentPage) {
                currentPageGetter = $parse(attrs.currentPage);
            } else {
                // if the current-page attribute was not set, we'll make our own
                var defaultCurrentPage = paginationId + '__currentPage';
                scope[defaultCurrentPage] = 1;
                currentPageGetter = $parse(defaultCurrentPage);
            }
            return currentPageGetter;
        }
    }

    /**
     * This is a helper directive that allows correct compilation when in multi-element mode (ie dir-paginate-start, dir-paginate-end).
     * It is dynamically added to all elements in the dir-paginate compile function, and it prevents further compilation of
     * any inner directives. It is then removed in the link function, and all inner directives are then manually compiled.
     */
    function noCompileDirective() {
        return {
            priority: 5000,
            terminal: true
        };
    }

    function dirPaginationControlsDirective(paginationService, paginationTemplate) {

        var numberRegex = /^\d+$/;

        return {
            restrict: 'AE',
            templateUrl: function(elem, attrs) {
                return attrs.templateUrl || paginationTemplate.getPath();
            },
            scope: {
                maxSize: '=?',
                onPageChange: '&?',
                paginationId: '=?'
            },
            link: dirPaginationControlsLinkFn
        };

        function dirPaginationControlsLinkFn(scope, element, attrs) {

            // rawId is the un-interpolated value of the pagination-id attribute. This is only important when the corresponding dir-paginate directive has
            // not yet been linked (e.g. if it is inside an ng-if block), and in that case it prevents this controls directive from assuming that there is
            // no corresponding dir-paginate directive and wrongly throwing an exception.
            var rawId = attrs.paginationId ||  DEFAULT_ID;
            var paginationId = scope.paginationId || attrs.paginationId ||  DEFAULT_ID;

            if (!paginationService.isRegistered(paginationId) && !paginationService.isRegistered(rawId)) {
                var idMessage = (paginationId !== DEFAULT_ID) ? ' (id: ' + paginationId + ') ' : ' ';
                throw 'pagination directive: the pagination controls' + idMessage + 'cannot be used without the corresponding pagination directive.';
            }

            if (!scope.maxSize) { scope.maxSize = 9; }
            scope.directionLinks = angular.isDefined(attrs.directionLinks) ? scope.$parent.$eval(attrs.directionLinks) : true;
            scope.boundaryLinks = angular.isDefined(attrs.boundaryLinks) ? scope.$parent.$eval(attrs.boundaryLinks) : false;

            var paginationRange = Math.max(scope.maxSize, 5);
            scope.pages = [];
            scope.pagination = {
                last: 1,
                current: 1
            };
            scope.range = {
                lower: 1,
                upper: 1,
                total: 1
            };

            scope.$watch(function() {
                return (paginationService.getCollectionLength(paginationId) + 1) * paginationService.getItemsPerPage(paginationId);
            }, function(length) {
                if (0 < length) {
                    generatePagination();
                }
            });

            scope.$watch(function() {
                return (paginationService.getItemsPerPage(paginationId));
            }, function(current, previous) {
                if (current != previous && typeof previous !== 'undefined') {
                    goToPage(scope.pagination.current);
                }
            });

            scope.$watch(function() {
                return paginationService.getCurrentPage(paginationId);
            }, function(currentPage, previousPage) {
                if (currentPage != previousPage) {
                    goToPage(currentPage);
                }
            });

            scope.setCurrent = function(num) {
                if (isValidPageNumber(num)) {
                    num = parseInt(num, 10);
                    paginationService.setCurrentPage(paginationId, num);
                }
            };

            function goToPage(num) {
                if (isValidPageNumber(num)) {
                    scope.pages = generatePagesArray(num, paginationService.getCollectionLength(paginationId), paginationService.getItemsPerPage(paginationId), paginationRange);
                    scope.pagination.current = num;
                    updateRangeValues();

                    // if a callback has been set, then call it with the page number as an argument
                    if (scope.onPageChange) {
                        scope.onPageChange({ newPageNumber : num });
                    }
                }
            }

            function generatePagination() {
                var page = parseInt(paginationService.getCurrentPage(paginationId)) || 1;

                scope.pages = generatePagesArray(page, paginationService.getCollectionLength(paginationId), paginationService.getItemsPerPage(paginationId), paginationRange);
                scope.pagination.current = page;
                scope.pagination.last = scope.pages[scope.pages.length - 1];
                if (scope.pagination.last < scope.pagination.current) {
                    scope.setCurrent(scope.pagination.last);
                } else {
                    updateRangeValues();
                }
            }

            /**
             * This function updates the values (lower, upper, total) of the `scope.range` object, which can be used in the pagination
             * template to display the current page range, e.g. "showing 21 - 40 of 144 results";
             */
            function updateRangeValues() {
                var currentPage = paginationService.getCurrentPage(paginationId),
                    itemsPerPage = paginationService.getItemsPerPage(paginationId),
                    totalItems = paginationService.getCollectionLength(paginationId);

                scope.range.lower = (currentPage - 1) * itemsPerPage + 1;
                scope.range.upper = Math.min(currentPage * itemsPerPage, totalItems);
                scope.range.total = totalItems;
            }

            function isValidPageNumber(num) {
                return (numberRegex.test(num) && (0 < num && num <= scope.pagination.last));
            }
        }

        /**
         * Generate an array of page numbers (or the '...' string) which is used in an ng-repeat to generate the
         * links used in pagination
         *
         * @param currentPage
         * @param rowsPerPage
         * @param paginationRange
         * @param collectionLength
         * @returns {Array}
         */
        function generatePagesArray(currentPage, collectionLength, rowsPerPage, paginationRange) {
            var pages = [];
            var totalPages = Math.ceil(collectionLength / rowsPerPage);
            var halfWay = Math.ceil(paginationRange / 2);
            var position;

            if (currentPage <= halfWay) {
                position = 'start';
            } else if (totalPages - halfWay < currentPage) {
                position = 'end';
            } else {
                position = 'middle';
            }

            var ellipsesNeeded = paginationRange < totalPages;
            var i = 1;
            while (i <= totalPages && i <= paginationRange) {
                var pageNumber = calculatePageNumber(i, currentPage, paginationRange, totalPages);

                var openingEllipsesNeeded = (i === 2 && (position === 'middle' || position === 'end'));
                var closingEllipsesNeeded = (i === paginationRange - 1 && (position === 'middle' || position === 'start'));
                if (ellipsesNeeded && (openingEllipsesNeeded || closingEllipsesNeeded)) {
                    pages.push('...');
                } else {
                    pages.push(pageNumber);
                }
                i ++;
            }
            return pages;
        }

        /**
         * Given the position in the sequence of pagination links [i], figure out what page number corresponds to that position.
         *
         * @param i
         * @param currentPage
         * @param paginationRange
         * @param totalPages
         * @returns {*}
         */
        function calculatePageNumber(i, currentPage, paginationRange, totalPages) {
            var halfWay = Math.ceil(paginationRange/2);
            if (i === paginationRange) {
                return totalPages;
            } else if (i === 1) {
                return i;
            } else if (paginationRange < totalPages) {
                if (totalPages - halfWay < currentPage) {
                    return totalPages - paginationRange + i;
                } else if (halfWay < currentPage) {
                    return currentPage - halfWay + i;
                } else {
                    return i;
                }
            } else {
                return i;
            }
        }
    }

    /**
     * This filter slices the collection into pages based on the current page number and number of items per page.
     * @param paginationService
     * @returns {Function}
     */
    function itemsPerPageFilter(paginationService) {

        return function(collection, itemsPerPage, paginationId) {
            if (typeof (paginationId) === 'undefined') {
                paginationId = DEFAULT_ID;
            }
            if (!paginationService.isRegistered(paginationId)) {
                throw 'pagination directive: the itemsPerPage id argument (id: ' + paginationId + ') does not match a registered pagination-id.';
            }
            var end;
            var start;
            if (collection instanceof Array) {
                itemsPerPage = parseInt(itemsPerPage) || 9999999999;
                if (paginationService.isAsyncMode(paginationId)) {
                    start = 0;
                } else {
                    start = (paginationService.getCurrentPage(paginationId) - 1) * itemsPerPage;
                }
                end = start + itemsPerPage;
                paginationService.setItemsPerPage(paginationId, itemsPerPage);

                return collection.slice(start, end);
            } else {
                return collection;
            }
        };
    }

    /**
     * This service allows the various parts of the module to communicate and stay in sync.
     */
    function paginationService() {

        var instances = {};
        var lastRegisteredInstance;

        this.registerInstance = function(instanceId) {
            if (typeof instances[instanceId] === 'undefined') {
                instances[instanceId] = {
                    asyncMode: false
                };
                lastRegisteredInstance = instanceId;
            }
        };

        this.isRegistered = function(instanceId) {
            return (typeof instances[instanceId] !== 'undefined');
        };

        this.getLastInstanceId = function() {
            return lastRegisteredInstance;
        };

        this.setCurrentPageParser = function(instanceId, val, scope) {
            instances[instanceId].currentPageParser = val;
            instances[instanceId].context = scope;
        };
        this.setCurrentPage = function(instanceId, val) {
            instances[instanceId].currentPageParser.assign(instances[instanceId].context, val);
        };
        this.getCurrentPage = function(instanceId) {
            var parser = instances[instanceId].currentPageParser;
            return parser ? parser(instances[instanceId].context) : 1;
        };

        this.setItemsPerPage = function(instanceId, val) {
            instances[instanceId].itemsPerPage = val;
        };
        this.getItemsPerPage = function(instanceId) {
            return instances[instanceId].itemsPerPage;
        };

        this.setCollectionLength = function(instanceId, val) {
            instances[instanceId].collectionLength = val;
        };
        this.getCollectionLength = function(instanceId) {
            return instances[instanceId].collectionLength;
        };

        this.setAsyncModeTrue = function(instanceId) {
            instances[instanceId].asyncMode = true;
        };

        this.isAsyncMode = function(instanceId) {
            return instances[instanceId].asyncMode;
        };
    }

    /**
     * This provider allows global configuration of the template path used by the dir-pagination-controls directive.
     */
    function paginationTemplateProvider() {

        var templatePath = 'assets/js/templates/dirPagination.html';

        this.setPath = function(path) {
            templatePath = path;
        };

        this.$get = function() {
            return {
                getPath: function() {
                    return templatePath;
                }
            };
        };
    }
})();
//WC Angular Over Directive
(function () {

    var wcOverlayDirective = function ($q, $timeout, $window, httpInterceptor, wcOverlayConfig) {
            return {
                restrict: 'EA',
                transclude: true,
                scope: {
                    wcOverlayDelay: "@"
                },
                template: '<div id="overlay-container" class="overlayContainer">' +
                    '<div id="overlay-background" class="overlayBackground"></div>' +
                    '<div id="overlay-content" class="overlayContent" data-ng-transclude>' +
                    '</div>' +
                    '</div>',
                link: function (scope, element, attrs) {
                    var overlayContainer = null,
                        timerPromise = null,
                        timerPromiseHide = null,
                        inSession = false,
                        queue = [],
                        overlayConfig = wcOverlayConfig.getConfig();

                    init();

                    function init() {
                        wireUpHttpInterceptor();
                        if (window.jQuery) wirejQueryInterceptor();
                        overlayContainer = document.getElementById('overlay-container');
                    }

                    //Hook into httpInterceptor factory request/response/responseError functions
                    function wireUpHttpInterceptor() {

                        httpInterceptor.request = function (config) {
                            //I want to have a condition to not show the overlay on specific calls
                            if(shouldShowOverlay(config.method, config.url))
                                processRequest();
                            return config || $q.when(config);
                        };

                        httpInterceptor.response = function (response) {
                            processResponse();
                            return response || $q.when(response);
                        };

                        httpInterceptor.responseError = function (rejection) {
                            processResponse();
                            return $q.reject(rejection);
                        };
                    }

                    //Monitor jQuery Ajax calls in case it's used in an app
                    function wirejQueryInterceptor() {
                        $(document).ajaxStart(function () {
                            processRequest();
                        });

                        $(document).ajaxComplete(function () {
                            processResponse();
                        });

                        $(document).ajaxError(function () {
                            processResponse();
                        });
                    }

                    function processRequest() {
                        queue.push({});
                        if (queue.length == 1) {
                            timerPromise = $timeout(function () {
                                if (queue.length) showOverlay();
                            }, scope.wcOverlayDelay ? scope.wcOverlayDelay : overlayConfig.delay); //Delay showing for 500 millis to avoid flicker
                        }
                    }

                    function processResponse() {
                        queue.pop();
                        if (queue.length == 0) {
                            //Since we don't know if another XHR request will be made, pause before
                            //hiding the overlay. If another XHR request comes in then the overlay
                            //will stay visible which prevents a flicker
                            timerPromiseHide = $timeout(function () {
                                //Make sure queue is still 0 since a new XHR request may have come in
                                //while timer was running
                                if (queue.length == 0) {
                                    hideOverlay();
                                    if (timerPromiseHide) $timeout.cancel(timerPromiseHide);
                                }
                            }, scope.wcOverlayDelay ? scope.wcOverlayDelay : overlayConfig.delay);
                        }
                    }

                    function showOverlay() {
                        var w = 0;
                        var h = 0;
                        if (!$window.innerWidth) {
                            if (!(document.documentElement.clientWidth == 0)) {
                                w = document.documentElement.clientWidth;
                                h = document.documentElement.clientHeight;
                            }
                            else {
                                w = document.body.clientWidth;
                                h = document.body.clientHeight;
                            }
                        }
                        else {
                            w = $window.innerWidth;
                            h = $window.innerHeight;
                        }
                        var content = document.getElementById('overlay-content');
                        if(content) {
                            var contentWidth = parseInt(getComputedStyle(content, 'width').replace('px', ''));
                            var contentHeight = parseInt(getComputedStyle(content, 'height').replace('px', ''));
                            content.style.top = h / 2 - contentHeight / 2 + 'px';
                            content.style.left = w / 2 - contentWidth / 2 + 'px';
                            overlayContainer.style.display = 'block';
                        }
                    }

                    function hideOverlay() {
                        if (timerPromise) $timeout.cancel(timerPromise);
                        overlayContainer.style.display = 'none';
                    }

                    var getComputedStyle = function () {
                        var func = null;
                        if (document.defaultView && document.defaultView.getComputedStyle) {
                            func = document.defaultView.getComputedStyle;
                        } else if (typeof (document.body.currentStyle) !== "undefined") {
                            func = function (element, anything) {
                                return element["currentStyle"];
                            };
                        }

                        return function (element, style) {
                            return func(element, null)[style];
                        }
                    }();

                    function shouldShowOverlay(method, url){
                        var searchCriteria = {
                            method: method,
                            url: url
                        }
                        return angular.isUndefined(findUrl(overlayConfig.exceptUrls, searchCriteria));
                    }

                    function findUrl(urlList, searchCriteria){
                        var retVal = undefined;
                        angular.forEach(urlList, function(url){
                            if(angular.equals(url, searchCriteria)){
                                retVal = true;
                                return false; //break out of forEach
                            }
                        });
                        return retVal;
                    }
                }
            }
        },

        httpProvider = function ($httpProvider) {
            $httpProvider.interceptors.push('httpInterceptor');
        },

        httpInterceptor = function () {
            return {}
        };

    var wcOverlayConfig = function(){

        //default config
        var config = {
            delay: 500,
            exceptUrls: []
        };

        //set delay
        this.setDelay = function(delayTime){
            config.delay = delayTime;
        };

        //set exception urls
        this.setExceptionUrls = function(urlList){
            config.exceptUrls = urlList;
        }

        this.$get = function(){
            return {
                getDelayTime: getDelayTime,
                getExceptUrls: getExceptUrls,
                getConfig: getConfig
            };

            function getDelayTime(){
                return config.delay;
            }

            function getExceptUrls(){
                return config.exceptUrls;
            }

            function getConfig(){
                return config;
            }
        };
    }

    var wcDirectivesApp = angular.module('wc.Directives', []);

    //provider service to setup overlay configuration in the app.config
    //this will configure the delay and the APIs which you don't want to show overlay on
    wcDirectivesApp.provider('wcOverlayConfig', wcOverlayConfig);

    //Empty factory to hook into $httpProvider.interceptors
    //Directive will hookup request, response, and responseError interceptors
    wcDirectivesApp.factory('httpInterceptor', httpInterceptor);

    //Hook httpInterceptor factory into the $httpProvider interceptors so that we can monitor XHR calls
    wcDirectivesApp.config(['$httpProvider', httpProvider]);

    //Directive that uses the httpInterceptor factory above to monitor XHR calls
    //When a call is made it displays an overlay and a content area
    //No attempt has been made at this point to test on older browsers
    wcDirectivesApp.directive('wcOverlay', ['$q', '$timeout', '$window', 'httpInterceptor','wcOverlayConfig', wcOverlayDirective]);

}());
//Paging Controller
// CONTROLLERS
(function(){
    'use strict';

    var PagingController = function($scope ){
        $scope.pageChangeHandler = function(num) {
            //console.log('going to page ' + num);
        }
    };

    PagingController.$inject = [ '$scope' ];

    angular.module('shermanApp')
        .controller('PagingController', PagingController);

})();
//Application Controller
// CONTROLLERS
(function(){

    'use strict';

    var ApplicationController = function( $rootScope, $window, $interval, dataService, $route, routeService ){

        var vm = this;
            vm.col = 'n';
            vm.libraries = [
                { code: 'main', label: 'Alvin Sherman Library'},
                { code: 'nmb', label: 'North Miami Beach'},
                { code: 'ocean', label: 'Oceanographic Center Library'}
            ];

            vm.changers = ['main', 'nmb', 'ocean'];

        $rootScope.$on('$routeChangeSuccess', function() {
            vm.title = $route.current.title;
            routeService.requireConditions();
        });

        function getApplication(){
            dataService.getApplication().then(function(data){
                console.log(data);
                if(data) {
                    vm.hours  = data.response.hours;
                    vm.status = data.response.AppStatus;

                    if(vm.status !== 'public'){
                        vm.info             = data.response.info;
                        //console.log(data.response.info);
                        vm.email_update     = data.response.info.email;
                        vm.activity         = data.response.activity;
                        vm.col              = vm.info.col;
                        vm.selectedLibrary =  vm.info.home_lib;
                    }
                }
            });
        }

        function checkSession(){
            dataService.checkSession().then(function(data) {
                //console.log(data);
                if(data) {
                    if(data.status === 'public') {
                        //console.log('go now');
                        $window.location.href = "./logout.php";
                    }else{
                        //console.log('nope');
                    }
                }
            });
        }

        //25 minutes to logout
        //$interval(checkSession, 1500000);
        //20 minutes to logout
        $interval(checkSession, 1200000);
        //$interval(checkSession, 90);

        getApplication();

        /*var userMenuToggle = function(){
            $rootScope.display =   $rootScope.display == 'none' ? 'block' : 'none';
        };*/

        var userMenu = (function() {

          $rootScope.userMenuToggle = false;

          function toggle() {

            $rootScope.userMenuToggle = !$rootScope.userMenuToggle;
          }

          return {
            toggle: toggle
          }

        })();

        var librariesMenu = (function() {

          $rootScope.librariesMenuToggle = false;

          function toggle() {
            $rootScope.librariesMenuToggle = !$rootScope.librariesMenuToggle;
          }

          return {
            toggle: toggle
          };

        })();

        var showModalWindow = function( object ){
            object.show = true;
            vm.modal = object;
        };

        var closeModalWindow = function(){
            vm.modal = {};
        };

        var updateEmail = function(){
            console.log(vm.email_update);
            var object = {
                job: 'updateEmail',
                email: vm.email_update
            };
            console.log(object);
            dataService.updateEmail(object).then(function(data){
                console.log(data);

                if(data.update_error){
                    vm.email_update =  vm.info.email;
                    vm.update_message = '<div class="alert alert--warning">' + data.update_error + '</div>';
                }

                if(data.response){
                    vm.email_update = data.response;
                    vm.info.email = data.response;
                    vm.update_message = '<div class="alert alert--success">Email updated!</div>';
                }

            });
        };

        var updateHomeLibrary = function(){

            vm.update_message = null;

            var object = {
                job: 'updateHomeLibrary',
                field: 'fixedFields',
                locx00 : vm.selectedLibrary,
                original : vm.info.home_lib

            };

            dataService.updateHomeLibrary(object).then(function(data){
                console.log(data);

                if(data.update_error){
                    vm.update_message = '<div class="alert alert--warning">' + data.update_error + '</div>';
                }

                if(data.response){

                    vm.info.home_lib = data.response;
                    vm.update_message = '<div class="alert alert--success">Home Library updated!</div>';
                }

            });
        };

        var inArray =   function(needle, haystack) {
            for(var i in haystack) {
                if(haystack[i] == needle) return true;
            }
            return false;
        };

        $rootScope.librariesMenu    = librariesMenu;
        $rootScope.userMenu         = userMenu;
        vm.showModalWindow          = showModalWindow;
        vm.closeModalWindow         = closeModalWindow;
        vm.updateEmail              = updateEmail;
        vm.updateHomeLibrary        = updateHomeLibrary;
        vm.inArray                  = inArray;
    };

    ApplicationController .$inject = [ '$rootScope', '$window', '$interval', 'dataService', '$route', 'routeService' ];

    angular.module('shermanApp')
        .controller('ApplicationController', ApplicationController);

})();
//Home Controller

// CONTROLLERS
(function(){

    'use strict';

    var HomeController = function( dataService, routeService ){
        var vm          = this;
        vm.showModal    = false;
        vm.databases    = [];

        function getGuides(){
            dataService.getGuides().then(function(data){
                if(data){
                    //console.log(data);
                    vm.guides = data;
                }
            });
        }

        function  getHome(){
            dataService. getHome().then(function(data){

                if(data){

                    vm.databases  = data.response.databases;
                    vm.subjects   = data.response.subjects;
                    vm.workshops  = data.response.workshops;
                    vm.programs   = data.response.programs;
                }
            })


        }

        function init(){
          getGuides();
          getHome();
        }

        init();

        var startsWith = function (actual, expected) {
            var lowerStr = (actual + "").toLowerCase();
            return lowerStr.indexOf(expected.toLowerCase()) === 0;
        };

        vm.startsWith   = startsWith;
    };

    HomeController.$inject = ['dataService', 'routeService'];

    angular.module('shermanApp')
        .controller('HomeController', HomeController);

})();

//FTF Controller
(function(){
    'use strict';
    var ftfController = function(  $timeout, dataService ){
        var vm = this;
        vm.selected = false;
        vm.filterTitle = 'startsWith';
        //vm.metadata = null;


        var getTitles = function (){
            vm.selected = false;
            if(vm.model !== undefined) {
                vm.model = vm.model.replace(/\s\s+/g, ' ');
                vm.model = vm.model.replace('.', ' ');
            }

            //console.log(vm.model);
            //vm.hits = null;
            //console.log(query);

            dataService.getTitles(vm.model).then(function(data){
                vm.items = data;
            });
        };

        var startsWith = function (actual, expected) {
            var lowerStr = (actual + "").toLowerCase();
            return lowerStr.indexOf(expected.toLowerCase()) === 0;
        };

        var setCurrent = function(index){
            vm.model = index.title;
            vm.selected = true;
            vm.metadata = index;
        };

        vm.getTitles    = getTitles;
        vm.startsWith   = startsWith;
        vm.setCurrent   = setCurrent;

    };

    ftfController .$inject = ['$timeout', 'dataService' ];
    angular.module('shermanApp')
        .controller('ftfController', ftfController);
})();

//Account Controller
// CONTROLLERS
(function(){

    'use strict';

    var AccountController = function( dataService ){

        var vm    = this;
        vm.dbAcc  = false;
        vm.subAcc = false;

        function  getAccount(){
            dataService. getAccount().then(function(data){
                //console.log(data);
                if(data) {
                    vm.databases  = data.response.databases;
                    vm.subjects   = data.response.subjects;
                }
            });
        }

        getAccount();

        var dbAccToggle = function(){
            vm.dbAcc =  vm.dbAcc == false ? true : false;
        };

        var subAccToggle = function(){
            vm.subAcc = vm.subAcc == false ? true : false;
        };

        var startsWith = function (actual, expected) {
            var lowerStr = (actual + "").toLowerCase();
            return lowerStr.indexOf(expected.toLowerCase()) === 0;
        };

        vm.dbAccToggle  = dbAccToggle;
        vm.subAccToggle = subAccToggle;
        vm.startsWith   = startsWith;
    };

    AccountController.$inject = ['dataService'];

    angular.module('shermanApp')
        .controller('AccountController', AccountController);

})();
//Checkouts Controller
(function(){

    'use strict';

    var CheckoutsController = function( $location, dataService ){

        var vm = this;
        vm.checkouts        = null;
        vm.cart             = null;
        vm.currentPage      = 1;
        vm.pageSize         = 5;
		vm.sortType			= 'duedate_stamp';
		vm.showbutton		= false;
		vm.hidestatus		= false;
		vm.available		= 0;

        var getCheckouts = function (){

            dataService.getCheckouts().then(function(data){
                console.log(data);
                if( data.error){
                    vm.error = data.error;
                    return;
                }
                if( !data.checkouts){
                    //'No items available';
                }
				if( data.show_button){
                    vm.showbutton = data.show_button;

                }
                vm.checkouts = data.checkouts;
                vm.selected  = data.selected;

                if( vm.checkouts.length >= 0) {
					vm.available = parseInt(vm.showbutton);
					if( vm.available === vm.checkouts.length ){
						vm.hidestatus = true;
					}
					 vm.pageSize = vm.checkouts.length;
                } else {
                    vm.pageSize = vm.checkouts.length;
                }
            })
        }
        getCheckouts();

        var addToCart = function(checkout){
            var object = {
                job             : 'addToCheckoutCart',
                checkout        : checkout

            };
			console.log(object);
            dataService.addToCheckoutCart(object).then(function(data){
                if(data){
                    vm.cart      = data.cart;
                    vm.selected  = data.checkouts;
                    console.log(data);
                }
            })
        };

        var removeFromCart = function(item){
            console.log(item);
            var object = {
                job    : 'removeFromCheckoutCart',
                item   :  item
            };
            dataService.removeFromCheckoutCart(object).then(function(data){
                if(data){
                    vm.cart      = data.cart;
                    vm.selected  = data.checkouts;
                }
            })
        };


        var fillCart = function(){
            var object = {
                job : 'fillCheckoutCart',
                checkouts: vm.checkouts
            };
            dataService.fillCheckoutCart(object).then(function(data){
                if(data){
                    vm.cart      = data.cart;
                    vm.selected  = data.checkouts;
                }
            })
        };

        var emptyCart = function(){
            dataService.emptyCheckoutCart().then(function(data){
                if(data){
                    vm.cart      = data.cart;
                    vm.selected  = data.checkouts;
                }
            });
        };

        var cartSort = function(){
            vm.duedate = vm.duedate === true  ? null : true;

            if(vm.duedate){
                var object = {
                    job : 'checkoutCartSort',
                    sortByDueDate: 1
                };
            } else {
                var object = {
                    job : 'checkoutCartSort',
                    sortByCheckoutDate: 1
                };
            }

            dataService.checkoutCartSort(object).then(function(data){
                //console.log(data);
                if(data.checkouts){
                    vm.checkouts = data.checkouts;
                    vm.selected  = data.selected;
                }
            });

        };

        var formSubmit = function(){
            //console.log('hello');
           $location.path('checkoutsconfirm');
        } ;


        vm.addToCart        = addToCart;
        vm.removeFromCart   = removeFromCart;
        vm.fillCart         = fillCart;
        vm.emptyCart        = emptyCart;
        vm.cartSort         = cartSort;
        vm.formSubmit       = formSubmit;

    };
    CheckoutsController.$inject = [ '$location','dataService' ];

    angular.module('shermanApp')
        .controller('CheckoutsController', CheckoutsController);
})();
//Checkouts Confirmation Controller
(function(){

    'use strict';

    var CheckoutsConfirmController = function(  $route, $location, dataService ){
        var vm = this;
        vm.checkouts        = null;
        vm.cart             = null;
        vm.renewerror       = null;
        vm.currentPage      = 1;
        vm.pageSize         = 5;
		vm.sortType			= 'duedate_stamp';

        var getCheckoutsCart = function (){
            dataService.getCheckoutsCart().then(function(data){
                //console.log(data);
				if(!data.checkoutscart) {
					$location.path('checkouts');
				} else {
				vm.cart       = data.checkoutscart;

					if( vm.cart.length >= 0) {
						//vm.pageSize = 5;
						 vm.pageSize = vm.cart.length;
					} else {
						vm.pageSize = vm.cart.length;
					}
			}
            });
        };

        getCheckoutsCart();

        var showDetails = function(){
            if( vm.detailState  == 'show') {
                vm.detailState = '';
            } else {
                vm.detailState = 'show';
            }
        };

        var addToCart = function(checkout){
            vm.renewerror       = null;
            var object = {
                job             : 'addToCheckoutCart',
				checkout        : checkout

            };
            dataService.addToCheckoutCart(object).then(function(data){
                vm.cart      = data.cart;
                vm.selected  = data.checkouts;
            })
        };

        var removeFromCart = function(item){
		console.log('jim');
		console.log(item);
            vm.renewerror       = null;
            var object = {
                job    : 'removeFromCheckoutCart',
                item   :  item
            };
            dataService.removeFromCheckoutCart(object).then(function(data){
				console.log(data);
                vm.cart  = data.checkoutscart;
            });
        };

        var fillCart = function(){
            vm.renewerror       = null;
            var object = {
                job : 'fillCheckoutCart',
                checkouts: vm.checkouts
            };
            dataService.fillCheckoutCart(object).then(function(data){
                vm.cart      = data.cart;
                vm.selected  = data.checkouts;
            })
        };

        var emptyCart = function(){
            vm.renewerror       = null;
            dataService.emptyCheckoutCart().then(function(data){
                vm.cart  = data.checkoutscart;
            });
        };

        var cartSort = function(){
            vm.renewerror       = null;
            vm.duedate = vm.duedate === true  ? null : true;

            if(vm.duedate){
                var object = {
                    job : 'checkoutCartSort',
                    sortByDueDate: 1
                };
            } else {
                var object = {
                    job : 'checkoutCartSort',
                    sortByCheckoutDate: 1
                };
            }

            dataService.checkoutCartSort(object).then(function(data){
                //console.log(data);
                if(data.checkouts){
                    vm.checkouts = data.checkouts;
                    vm.selected  = data.selected;
                }
            });

        };

        var cancelCheckouts = function(){

            dataService.cancelCheckouts().then(function(data){
                if(data.renewerror) {
                    vm.renewerror = data.renewerror;
                    if(data.errorlist){
                        vm.errorlist = data.errorlist;
                    }
                    vm.cart = null;
                    vm.checkouts = data.checkouts;
                    vm.selected  = data.selected;
                } else{
                    $location.path('checkouts');
                }
                //getCheckouts();
            });
        } ;


        var checkoutsRenew = function(){
			   //$location.path('checkoutsconfirm');

              $location.path('checkoutsrenew');
        };

        var formSubmit = function(){
			  $route.reload();
              //$location.path('checkoutsrenew');
			  $location.path('checkoutsconfirm');
        };


        vm.getCheckoutsCart = getCheckoutsCart;
        vm.showDetails      = showDetails;
        vm.addToCart        = addToCart;
        vm.removeFromCart   = removeFromCart;
        vm.fillCart         = fillCart;
        vm.emptyCart        = emptyCart;
        vm.cartSort         = cartSort;
        vm.cancelCheckouts  = cancelCheckouts;
		vm.checkoutsRenew	= checkoutsRenew;
        vm.formSubmit       = formSubmit;


    };

    CheckoutsConfirmController .$inject = [ '$route', '$location', 'dataService' ];
    angular.module('shermanApp')
        .controller('CheckoutsConfirmController', CheckoutsConfirmController );
})();

//Checkouts Renew Controller
(function(){

    'use strict';

    var CheckoutsRenewController = function(  $route, $location, dataService ){
		var vm    = this;
		vm.currentPage      = 1;
        vm.pageSize         = 5;
		vm.test   = 'Controller is controlling';

		    var submitRenewal = function(){

            dataService.submitRenewal().then(function(data){
				console.log(data);
                if(data.renewals) {
                    vm.renewals = data.renewals;
						if( vm.renewals.length >= 0) {
							//vm.pageSize = 5;
							 vm.pageSize = vm.renewals.length;
						} else {
							vm.pageSize = vm.renewals.length;
						}

                } else{
                   $location.path('checkouts');
                }
                //getCheckouts();
            });
        } ;
		submitRenewal();
    };

    CheckoutsRenewController.$inject = [ '$route', '$location', 'dataService' ];
    angular.module('shermanApp')
        .controller('CheckoutsRenewController', CheckoutsRenewController );
})();
//Checkout Results Directive
(function(){

    'use strict';

    var checkoutResult = function() {
        var controller = [function(){
            var vm = this;
			vm.link	= 'https://novacat.nova.edu/record=b';
            //console.log(vm.checkout);
            //console.log(vm.selected);


            var checkCart = function(item){

                var check = vm.selected.indexOf(item);
                if( check < 0) {

                    return false;
                } else{

                    return true;
                }
            };

            var addToCart = function( checkout ){
                vm.add()(checkout);
            };

            var removeFromCart = function( item ){
                vm.remove()(item );
            };

            vm.checkCart      = checkCart;
            vm.addToCart      = addToCart;
            vm.removeFromCart = removeFromCart;

        }];
        return {
            scope: {
                checkout     : '=',
                selected     : '=',
				showbutton	 : '=',
				hidestatus	 : '=',
                add          : '&',
                remove       : '&'
            },
            controller: controller,
            controllerAs: 'vm',
            bindToController: true,
            templateUrl: 'assets/js/templates/checkoutResult.html',
            replace: true
        }
    };
    angular.module('shermanApp')
        .directive('checkoutResult', checkoutResult );
})();

//Checkout Cart Directive
(function(){

    'use strict';

    var checkoutCart = function() {
        var controller = [function(){
            var vm = this;
			vm.link	= 'https://novacat.nova.edu/record=b';

			var removeFromCart = function( item ){
                vm.remove()(item );
            };

			vm.removeFromCart = removeFromCart;

        }];
        return {
            scope: {
                checkout     : '=',
				cart		 : '=',
                remove       : '&'
            },
            controller: controller,
            controllerAs: 'vm',
            bindToController: true,
            templateUrl: 'assets/js/templates/checkoutCart.html',
            replace: true
        }
    };
    angular.module('shermanApp')
        .directive('checkoutCart', checkoutCart );
})();

//Checkouts Renew Directive
(function(){

    'use strict';

    var checkoutRenew = function() {
        var controller = [function(){
            var vm = this;
			vm.link	= 'https://novacat.nova.edu/record=b';

			}];
        return {
            scope: {
                renewal     : '='
            },
            controller: controller,
            controllerAs: 'vm',
            bindToController: true,
            templateUrl: 'assets/js/templates/checkoutRenew.html',
            replace: true
        }
    };
    angular.module('shermanApp')
        .directive('checkoutRenew', checkoutRenew );
})();


//Holds Controller
(function(){

    'use strict';

    var HoldsController = function( $location, dataService ){
        var vm = this;

        vm.requestdate      = true;
        vm.holds            = null;
        vm.cart             = null;
        vm.currentPage      = 1;
        vm.pageSize         = 5;


        var getHolds = function(){

            dataService.getHolds().then(function(data){
                //console.log(data);
                if( data.error){
                    vm.error = data.error;
                    return;
                }
                //console.log(data);
                vm.holds        = data.holds;
                vm.selected     = data.selected;
                if( vm.holds.length >= 5) {
                    vm.pageSize = 5;
                } else {
                    vm.pageSize = vm.holds.length;
                }
            });
        };

        getHolds();

        var addToCart = function(hold){
            var object = {
                job      : 'addToHoldCart',
                hold     : hold.hold,
                title    : hold.title,
                status   : hold.status,
                pickup   : hold.pickup
            };
            dataService.addToHoldCart(object).then(function(data){
                if(data){
                    vm.cart      = data.holdscart;
                    vm.selected  = data.holds;
                    //console.log(data);
                }
            });
        };

        var removeFromCart = function(hold){
            var object = {
                job    : 'removeFromHoldCart',
                hold   :  hold
            };
            dataService.removeFromHoldCart(object).then(function(data){
                if(data){
                    vm.cart      = data.holdscart;
                    vm.selected  = data.holds;
                }
            });
        };

        var fillCart = function(){
            var object = {
                job : 'fillHoldCart',
                holds: vm.holds
            };
            dataService.fillHoldCart(object).then(function(data){
                if(data){
                    vm.cart      = data.holdscart;
                    vm.selected  = data.holds;
                }

            });
        };

        var emptyCart = function(){
            dataService.emptyHoldCart().then(function(data){
                vm.cart      = data.holdscart;
                vm.selected  = data.holds;
            });
        };

        var cartSort = function(){
            vm.requestdate = vm.requestdate === true  ? null : true;

            if(vm.requestdate){
                var object = {
                    job : 'holdCartSort',
                    sortByDueDate: 1
                };
            } else {
                var object = {
                    job : 'holdCartSort',
                    sortByCheckoutDate: 1
                };
            }

            dataService.holdCartSort(object).then(function(data){
                //console.log(data);
                if(data.checkouts){
                    vm.holds        = data.holds;
                    vm.selected     = data.selected;
                }
            });
        };

        var formSubmit = function(){
            //console.log('hello');
            $location.path('holdsconfirm');
        } ;

        vm.addToCart        = addToCart;
        vm.removeFromCart   = removeFromCart;
        vm.fillCart         = fillCart;
        vm.emptyCart        = emptyCart;
        vm.cartSort         = cartSort;
        vm.formSubmit       = formSubmit;

    };

    HoldsController.$inject = [ '$location', 'dataService' ];
    angular.module('shermanApp')
        .controller('HoldsController', HoldsController);
})();
//Holds Confirmation Controller
(function(){

    'use strict';

    var HoldsConfirmController = function( $route, $location, dataService ){
        var vm = this;
        vm.requestdate      = true;
        vm.holds            = null;
        vm.cart             = null;
        vm.cancelerror      = null;
        vm.currentPage      = 1;
        vm.pageSize         = 5;


        var getHoldsCart = function getHoldsCart() {
            dataService.getHoldsCart().then(function(data){
                console.log(data);
                vm.cart       = data.holdscart;
            });
        };

        getHoldsCart();

        var showDetails = function(){
            if( vm.detailState  == 'show') {
                vm.detailState = '';
            } else {
                vm.detailState = 'show';
            }
        };

        var addToCart = function(hold){
            var object = {
                job      : 'addToHoldCart',
                hold     : hold.hold,
                title    : hold.title,
                status   : hold.status,
                pickup   : hold.pickup
            };
            dataService.addToHoldCart(object).then(function(data){
                if(data){
                    vm.cart      = data.holdscart;
                    vm.selected  = data.holds;
                    //console.log(data);
                }
            });
        };

        var removeFromCart = function(hold){
            var object = {
                job    : 'removeFromHoldCart',
                hold   :  hold
            };
            dataService.removeFromHoldCart(object).then(function(data){
                if(data){
                    vm.cart      = data.holdscart;
                }
            });
        };

        var fillCart = function(){
            var object = {
                job : 'fillHoldCart',
                holds: vm.holds
            };
            dataService.fillHoldCart(object).then(function(data){
                if(data){
                    vm.cart      = data.holdscart;
                    vm.selected  = data.holds;
                }

            });
        };

        var emptyCart = function(){
            dataService.emptyHoldCart().then(function(data){
                if(data){
                    vm.cart      = data.holdscart;
                }
            });
        };

        var cartSort = function(){
            vm.requestdate = vm.requestdate === true  ? null : true;

            if(vm.requestdate){
                var object = {
                    job : 'holdCartSort',
                    sortByDueDate: 1
                };
            } else {
                var object = {
                    job : 'holdCartSort',
                    sortByCheckoutDate: 1
                };
            }

            dataService.holdCartSort(object).then(function(data){
                //console.log(data);
                if(data.checkouts){
                    vm.holds        = data.holds;
                    vm.selected     = data.selected;
                }
            });
        };

        var formSubmit = function(){
            //console.log('hello');
            $route.reload();
        } ;

        var cancelHolds = function(){
            dataService.cancelHolds().then(function(data){
                if(data.cancelerror) {
                    vm.cancelerror = data.cancelerror;
                    if(data.errorlist){
                        hc.errorlist = data.errorlist;
                    }
                    vm.cart = null;
                    vm.holds = data.holds;
                    vm.selected  = data.selected;
                } else{
                    $location.path('holds');
                }

                //getCheckouts();
            });
        } ;

        vm.getHoldsCart     = getHoldsCart;
        vm.showDetails      = showDetails;
        vm.addToCart        = addToCart;
        vm.removeFromCart   = removeFromCart;
        vm.fillCart         = fillCart;
        vm.emptyCart        = emptyCart;
        vm.cartSort         = cartSort;
        vm.formSubmit       = formSubmit;
        vm.cancelHolds      = cancelHolds;


    }
    HoldsConfirmController .$inject = [ '$route', '$location', 'dataService' ];
    angular.module('shermanApp')
        .controller('HoldsConfirmController', HoldsConfirmController );
})();
//Holds Results Directive
(function(){

    'use strict';

    var holdResult = function() {
        var controller = [function(){
            var vm = this;

            var checkCart = function(hold){
                var check = vm.selected.indexOf(hold);
                if( check < 0) {
                    return false;
                } else{
                    return true;
                }
            };

            var addToCart = function( hold ){
                vm.add()(hold);
            };

            var removeFromCart = function( hold ){
                vm.remove()(hold);
            };

            vm.checkCart      = checkCart;
            vm.addToCart      = addToCart;
            vm.removeFromCart = removeFromCart;

        }]
        return {
            scope: {
                hold         : '=',
                selected     : '=',
                add          : '&',
                remove       : '&'
            },
            controller: controller,
            controllerAs: 'vm',
            bindToController: true,
            templateUrl: 'assets/js/templates/holdResult.html',
            replace: true
        }
    };
    angular.module('shermanApp')
        .directive('holdResult', holdResult );
})();

(function(){

    'use strict';

    var AdController = function( dataService ) {

      var vm    = this;

      /**
       * Uses dataService.prototype.getFeatures - be sure to remove if this controller is removed too
       */
      function  getFeatures( audience ){

          dataService.getFeatures( audience ).then(function(data){
              if( data ) {
                vm.ads = data;
              }
          });
      }

      getFeatures( 'academic' );

    }

    AdController.$inject = ['dataService'];
    angular.module('shermanApp').controller('AdController', AdController);

})();

(function(){

    'use strict';

    var SystemsStatusController = function( $attrs, dataService ) {

      var repository = $attrs.repository;
      var vm = this;


      function  getBitbucketIssues( repository ){

          dataService.getBitbucketIssues( repository ).then(function( data ){
              if( data ) {

                vm.issues = data.values;
                vm.bugs       = 0;
                vm.tasks      = 0;
                vm.proposals  = 0;
                vm.enhancements = 0;

                for ( var i = 0; i < vm.issues.length; i++ ) {

                  var issue = vm.issues[i];
                  if ( issue[ 'kind' ] == 'bug' && issue['priority'] == 'minor' && issue[ 'state'] !== 'resolved' ) { vm.statusMinor = true; }
                  if ( issue[ 'kind' ] == 'bug' && issue[ 'priority'] == 'major' && issue[ 'state'] !== 'resolved' ) { vm.statusMajor = true; }

                  if ( issue[ 'state' ] !== 'resolved' ) {
                    if ( issue[ 'kind' ] == 'task' ) { vm.tasks++; }
                    if ( issue[ 'kind' ] == 'bug' ) { vm.bugs++; }
                    if ( issue[ 'kind' ] == 'proposal' ) { vm.proposals++; }
                    if ( issue[ 'kind' ] == 'enhancement' ) { vm.enhancements++; }
                  }
                }

              }
          });
      }

      getBitbucketIssues( repository );

    }

    SystemsStatusController.$inject = ['$attrs', 'dataService'];
    angular.module('shermanApp').controller('SystemsStatusController', SystemsStatusController);

})();


(function(){

    'use strict';

    var IssuesController = function( dataService, $routeParams ) {


        function  getBitbucketIssues( repository, state ){

            dataService.getBitbucketIssues( repository, state ).then(function( data ){
                if( data ) {
                    vm.issues = data.values;
                }
            });
        }


      if ( $routeParams.repository ) {

        var repository = $routeParams.repository;
        var state = ( $routeParams.state ? $routeParams.state :  'open' );
        var vm = this;
        vm.repository = repository;

        getBitbucketIssues( repository, state );

      }

      return false;

    }

    IssuesController.$inject = [ 'dataService', '$routeParams' ];
    angular.module('shermanApp').controller('IssuesController', IssuesController);

})();

(function() {

  angular.module( 'shermanApp' ).filter( 'trusted', ['$sce', function( $sce ) {
    return function( url ) {
      return $sce.trustAsResourceUrl( url );
    }
  }]);

})();
