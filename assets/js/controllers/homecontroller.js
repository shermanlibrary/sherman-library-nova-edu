// CONTROLLERS
(function(){

    'use strict';

    var HomeController = function( dataService ){
        var vm          = this;
        vm.showModal    = false;
        vm.databases    = [];

        function requireConditions(){
            var script = document.createElement("script"); script.type = "text/javascript"; script.src = "//sherman.library.nova.edu/cdn/scripts/config.js"; document.querySelector("body").appendChild(script);
        }

        function getGuides(){
            dataService.getGuides().then(function(data){
                if(data){
                    //console.log(data);
                    vm.guides = data;
                }
            });
        }

        function  getHome(){
            dataService. getHome().then(function(data){
                //console.log(data);
                if(data){
                    vm.databases  = data.response.databases;
                    vm.subjects   = data.response.subjects;
                    vm.workshops  = data.response.workshops;
                    vm.programs   = data.response.programs;
                }
            })


        }

        function init(){
            loadCSS( '//sherman.library.nova.edu/cdn/styles/css/public-global/s--home.css' );
            requireConditions();
            getGuides();
            getHome();
        }

        init();

        var startsWith = function (actual, expected) {
            var lowerStr = (actual + "").toLowerCase();
            return lowerStr.indexOf(expected.toLowerCase()) === 0;
        };

        vm.startsWith   = startsWith;
    };

    HomeController.$inject = ['dataService'];

    angular.module('shermanApp')
    .controller('HomeController', HomeController);

})();
