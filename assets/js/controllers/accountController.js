var dataService = require( '../services/dataService.js' );

dataService.prototype.getAccount  = function() {
    var _this = this;
    return _this.$http.get('../auth/api/endpoint.php?job=getAccount')
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                //console.log(response.data);
                return response.data;
                // invalid response
                //return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            //console.log(response);
            return _this.$q.reject(response.data);
        })
};


var AccountController = function( dataService ){

    var vm    = this;
    vm.dbAcc  = false;
    vm.subAcc = false;

    function  getAccount(){
        dataService. getAccount().then(function(data){
            //console.log(data);
            if(data) {
                vm.databases  = data.response.databases;
                vm.subjects   = data.response.subjects;
            }
        });
    }

    getAccount();

    var dbAccToggle = function(){
        vm.dbAcc =  vm.dbAcc == false ? true : false;
    };

    var subAccToggle = function(){
        vm.subAcc = vm.subAcc == false ? true : false;
    };

    var startsWith = function (actual, expected) {
        var lowerStr = (actual + "").toLowerCase();
        return lowerStr.indexOf(expected.toLowerCase()) === 0;
    };

    vm.dbAccToggle  = dbAccToggle;
    vm.subAccToggle = subAccToggle;
    vm.startsWith   = startsWith;
};

AccountController.$inject = ['dataService'];
module.exports = AccountController;
