/**
 * List Controller
 *
 * This passes data from a service into a Controller
 * intended for book and item lists - whether as carousels
 * or whatever.
 *
 * @param $attrs so the slug can be defined from the markup and passed
 - back to the controller. This way a librarian can embed their own
 - carousel.
 *
 * @requires services/listservice
 */
(function(){

    'use strict';

    var ListController = function( $attrs, dataService ) {

      var vm    = this;
      vm.slug = $attrs.slug;

      function  getList( slug ){

          dataService.getList( slug ).then(function(data){
              if( data ) {
                console.log( data );
                vm.list = data;
              }
          });
      }

      getList( slug );

    }

    ListController.$inject = [ '$attrs', 'dataService' ];
    angular.module( 'publicServices' ).controller('ListController', ListController);

})();
