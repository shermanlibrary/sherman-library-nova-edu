'use strict';

var SystemsStatusController = function( $attrs, dataService ) {

  var repository = $attrs.repository;
  var vm = this;


  function  getBitbucketIssues( repository ){

      dataService.getBitbucketIssues( repository ).then(function( data ){
          if( data ) {

            vm.issues = data.values;
            vm.bugs       = 0;
            vm.tasks      = 0;
            vm.proposals  = 0;
            vm.enhancements = 0;

            for ( var i = 0; i < vm.issues.length; i++ ) {

              var issue = vm.issues[i];
              if ( issue[ 'kind' ] == 'bug' && issue['priority'] == 'minor' && issue[ 'state'] !== 'resolved' ) { vm.statusMinor = true; }
              if ( issue[ 'kind' ] == 'bug' && issue[ 'priority'] == 'major' && issue[ 'state'] !== 'resolved' ) { vm.statusMajor = true; }

              if ( issue[ 'state' ] !== 'resolved' ) {
                if ( issue[ 'kind' ] == 'task' ) { vm.tasks++; }
                if ( issue[ 'kind' ] == 'bug' ) { vm.bugs++; }
                if ( issue[ 'kind' ] == 'proposal' ) { vm.proposals++; }
                if ( issue[ 'kind' ] == 'enhancement' ) { vm.enhancements++; }
              }
            }

          }
      });
  }

  getBitbucketIssues( repository );

}

SystemsStatusController.$inject = ['$attrs', 'dataService'];
module.exports = SystemsStatusController;
