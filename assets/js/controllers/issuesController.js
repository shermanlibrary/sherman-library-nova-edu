'use strict';

var IssuesController = function( dataService, $routeParams ) {

  var vm = this;

  vm.repositories = [
    'asl-wp-event-plugin',
    'asl-wp-theme',
    'content',
    'electronic-resources',
    'libguides',
    'novacat',
    'sherman-library-nova-edu'
  ];

  vm.issues = '';

  function  getBitbucketIssues( repository, state, kind, assignee ){

      dataService.getBitbucketIssues( repository, state, kind, assignee ).then(function( data ){
          if( data ) {
              vm.issues = data.values;
          }
      });
  }

  if ( $routeParams.repository ) {
    var repository = $routeParams.repository;
    var state = ( $routeParams.state ? $routeParams.state :  'any' );
    var kind = ( $routeParams.kind ? $routeParams.kind : '' );
    var assignee = ( $routeParams.assignee ? $routeParams.assignee : '' );

    vm.repository = repository;

    if ( repository === 'any' ) {
        vm.repositories.forEach( function( repository ) {
            getBitbucketIssues( repository, state, kind, assignee );
        });
    }

    else {
      getBitbucketIssues( repository, state, kind, assignee );
    }



  }

  return false;

} // closing bracket

IssuesController.$inject = [ 'dataService', '$routeParams' ];

module.exports = IssuesController;
