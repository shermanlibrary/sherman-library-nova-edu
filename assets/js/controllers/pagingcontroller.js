'use strict';

var PagingController = function($scope ){
    $scope.pageChangeHandler = function(num) {}
};

PagingController.$inject = [ '$scope' ];

module.exports = PagingController;
