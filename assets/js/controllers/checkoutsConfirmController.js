var dataService = require( '../services/dataService.js' );

dataService.prototype.getCheckouts = function() {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', { job : 'getCheckouts'})
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                //console.log(response.data);
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.addToCheckoutCart  = function( object ) {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', object )
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.removeFromCheckoutCart  = function( object ) {;
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', object )
        .then(function(response){
            if (typeof response.data === 'object') {
                console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.checkoutCartSort  = function( object ) {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', object )
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.getCheckoutsCart  = function() {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', { job : 'getCheckoutsCart'} )
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.fillCheckoutCart  = function( object ) {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', object )
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.emptyCheckoutCart  = function() {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', { job : 'emptyCheckoutCart'} )
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

var CheckoutsConfirmController = function(  $route, $location, dataService ){
    var vm = this;
    vm.checkouts        = null;
    vm.cart             = null;
    vm.renewerror       = null;
    vm.currentPage      = 1;
    vm.pageSize         = 5;
    vm.sortType			= 'duedate_stamp';

    var getCheckoutsCart = function (){
        dataService.getCheckoutsCart().then(function(data){
            //console.log(data);
            if(!data.checkoutscart) {
                $location.path('checkouts');
            } else {
                vm.cart       = data.checkoutscart;

                if( vm.cart.length >= 0) {
                    //vm.pageSize = 5;
                    vm.pageSize = vm.cart.length;
                } else {
                    vm.pageSize = vm.cart.length;
                }
            }
        });
    };

    getCheckoutsCart();

    var showDetails = function(){
        if( vm.detailState  == 'show') {
            vm.detailState = '';
        } else {
            vm.detailState = 'show';
        }
    };

    var addToCart = function(checkout){
        vm.renewerror       = null;
        var object = {
            job             : 'addToCheckoutCart',
            checkout        : checkout

        };
        dataService.addToCheckoutCart(object).then(function(data){
            vm.cart      = data.cart;
            vm.selected  = data.checkouts;
        })
    };

    var removeFromCart = function(item){

        //console.log(item);
        vm.renewerror       = null;
        var object = {
            job    : 'removeFromCheckoutCart',
            item   :  item
        };
        dataService.removeFromCheckoutCart(object).then(function(data){
            //console.log(data);
            vm.cart  = data.checkoutscart;
        });
    };

    var fillCart = function(){
        vm.renewerror       = null;
        var object = {
            job : 'fillCheckoutCart',
            checkouts: vm.checkouts
        };
        dataService.fillCheckoutCart(object).then(function(data){
            vm.cart      = data.cart;
            vm.selected  = data.checkouts;
        })
    };

    var emptyCart = function(){
        vm.renewerror       = null;
        dataService.emptyCheckoutCart().then(function(data){
            vm.cart  = data.checkoutscart;
        });
    };

    var cartSort = function(){
        vm.renewerror       = null;
        vm.duedate = vm.duedate === true  ? null : true;

        if(vm.duedate){
            var object = {
                job : 'checkoutCartSort',
                sortByDueDate: 1
            };
        } else {
            var object = {
                job : 'checkoutCartSort',
                sortByCheckoutDate: 1
            };
        }

        dataService.checkoutCartSort(object).then(function(data){
            //console.log(data);
            if(data.checkouts){
                vm.checkouts = data.checkouts;
                vm.selected  = data.selected;
            }
        });

    };

    var cancelCheckouts = function(){

        dataService.cancelCheckouts().then(function(data){
            if(data.renewerror) {
                vm.renewerror = data.renewerror;
                if(data.errorlist){
                    vm.errorlist = data.errorlist;
                }
                vm.cart = null;
                vm.checkouts = data.checkouts;
                vm.selected  = data.selected;
            } else{
                $location.path('checkouts');
            }
            //getCheckouts();
        });
    } ;


    var checkoutsRenew = function(){
        //$location.path('checkoutsconfirm');

        $location.path('checkoutsrenew');
    };

    var formSubmit = function(){
        $route.reload();
        //$location.path('checkoutsrenew');
        $location.path('checkoutsconfirm');
    };


    vm.getCheckoutsCart = getCheckoutsCart;
    vm.showDetails      = showDetails;
    vm.addToCart        = addToCart;
    vm.removeFromCart   = removeFromCart;
    vm.fillCart         = fillCart;
    vm.emptyCart        = emptyCart;
    vm.cartSort         = cartSort;
    vm.cancelCheckouts  = cancelCheckouts;
    vm.checkoutsRenew	= checkoutsRenew;
    vm.formSubmit       = formSubmit;


};

CheckoutsConfirmController .$inject = [ '$route', '$location', 'dataService' ];

module.exports = CheckoutsConfirmController;