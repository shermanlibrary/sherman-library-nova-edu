/**
 * HoldsConfirmController handles all of the holds services.
 * This isn't a very good comment :/.
 */

var dataService = require( '../services/dataService.js' );

dataService.prototype.getHolds = function() {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', { job : 'getHolds'})
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                //console.log(response.data);
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.addToHoldCart  = function( object ) {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', object )
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.removeFromHoldCart  = function( object ) {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', object )
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.fillHoldCart  = function( object ) {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', object )
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.emptyHoldCart  = function() {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', { job : 'emptyHoldCart'} )
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.holdCartSort  = function( object ) {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', object )
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.getHoldsCart  = function() {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', { job : 'getHoldsCart'} )
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.cancelHolds  = function() {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', { job : 'cancelHolds'} )
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

var HoldsConfirmController = function( $route, $location, dataService ){
    var vm = this;
    vm.requestdate      = true;
    vm.holds            = null;
    vm.cart             = null;
    vm.cancelerror      = null;
    vm.currentPage      = 1;
    vm.pageSize         = 5;


    var getHoldsCart = function getHoldsCart() {
        dataService.getHoldsCart().then(function(data){
            //console.log(data);
            vm.cart       = data.holdscart;
        });
    };

    getHoldsCart();

    var showDetails = function(){
        if( vm.detailState  == 'show') {
            vm.detailState = '';
        } else {
            vm.detailState = 'show';
        }
    };

    var addToCart = function(hold){
        var object = {
            job      : 'addToHoldCart',
            hold     : hold.hold,
            title    : hold.title,
            status   : hold.status,
            pickup   : hold.pickup
        };
        dataService.addToHoldCart(object).then(function(data){
            if(data){
                vm.cart      = data.holdscart;
                vm.selected  = data.holds;
                //console.log(data);
            }
        });
    };

    var removeFromCart = function(hold){
        var object = {
            job    : 'removeFromHoldCart',
            hold   :  hold
        };
        dataService.removeFromHoldCart(object).then(function(data){
            if(data){
                vm.cart      = data.holdscart;
            }
        });
    };

    var fillCart = function(){
        var object = {
            job : 'fillHoldCart',
            holds: vm.holds
        };
        dataService.fillHoldCart(object).then(function(data){
            if(data){
                vm.cart      = data.holdscart;
                vm.selected  = data.holds;
            }

        });
    };

    var emptyCart = function(){
        dataService.emptyHoldCart().then(function(data){
            if(data){
                vm.cart      = data.holdscart;
            }
        });
    };

    var cartSort = function(){
        vm.requestdate = vm.requestdate === true  ? null : true;

        if(vm.requestdate){
            var object = {
                job : 'holdCartSort',
                sortByDueDate: 1
            };
        } else {
            var object = {
                job : 'holdCartSort',
                sortByCheckoutDate: 1
            };
        }

        dataService.holdCartSort(object).then(function(data){
            //console.log(data);
            if(data.checkouts){
                vm.holds        = data.holds;
                vm.selected     = data.selected;
            }
        });
    };

    var formSubmit = function(){
        //console.log('hello');
        $route.reload();
    } ;

    var cancelHolds = function(){
        dataService.cancelHolds().then(function(data){
            if(data.cancelerror) {
                vm.cancelerror = data.cancelerror;
                if(data.errorlist){
                    hc.errorlist = data.errorlist;
                }
                vm.cart = null;
                vm.holds = data.holds;
                vm.selected  = data.selected;
            } else{
                $location.path('holds');
            }

            //getCheckouts();
        });
    } ;

    vm.getHoldsCart     = getHoldsCart;
    vm.showDetails      = showDetails;
    vm.addToCart        = addToCart;
    vm.removeFromCart   = removeFromCart;
    vm.fillCart         = fillCart;
    vm.emptyCart        = emptyCart;
    vm.cartSort         = cartSort;
    vm.formSubmit       = formSubmit;
    vm.cancelHolds      = cancelHolds;


}
HoldsConfirmController .$inject = [ '$route', '$location', 'dataService' ];
module.exports = HoldsConfirmController;