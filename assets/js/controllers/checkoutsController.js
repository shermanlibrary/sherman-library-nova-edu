/**
 * CheckoutsController handles all of the checkouts services.
 * This isn't a very good comment :/.
 */

var dataService = require( '../services/dataService.js' );

dataService.prototype.getCheckouts = function() {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', { job : 'getCheckouts'})
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                //console.log(response.data);
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.addToCheckoutCart  = function( object ) {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', object )
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.removeFromCheckoutCart  = function( object ) {;
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', object )
        .then(function(response){
            if (typeof response.data === 'object') {
                console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.checkoutCartSort  = function( object ) {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', object )
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.getCheckoutsCart  = function() {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', { job : 'getCheckoutsCart'} )
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.fillCheckoutCart  = function( object ) {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', object )
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.emptyCheckoutCart  = function() {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', { job : 'emptyCheckoutCart'} )
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};


var CheckoutsController = function( $location, dataService ){

        var vm = this;
        vm.checkouts        = null;
        vm.cart             = null;
        vm.currentPage      = 1;
        vm.pageSize         = 5;
        vm.sortType			= 'duedate_stamp';
        vm.showbutton		= false;
        vm.hidestatus		= false;
        vm.available		= 0;

        var getCheckouts = function (){

            dataService.getCheckouts().then(function(data){
                console.log(data);
                if( data.error){
                    vm.error = data.error;
                    return;
                }
                if( !data.checkouts){
                    //'No items available';
                }
                if( data.show_button){
                    vm.showbutton = data.show_button;

                }
                vm.checkouts = data.checkouts;
                vm.selected  = data.selected;

                if( vm.checkouts.length >= 0) {
                    vm.available = parseInt(vm.showbutton);
                    if( vm.available === vm.checkouts.length ){
                        vm.hidestatus = true;
                    }
                    vm.pageSize = vm.checkouts.length;
                } else {
                    vm.pageSize = vm.checkouts.length;
                }
            })
        }
        getCheckouts();

        var addToCart = function(checkout){
            var object = {
                job             : 'addToCheckoutCart',
                checkout        : checkout

            };
            //console.log(object);
            dataService.addToCheckoutCart(object).then(function(data){
                if(data){
                    vm.cart      = data.cart;
                    vm.selected  = data.checkouts;
                    //console.log(data);
                }
            })
        };

        var removeFromCart = function(item){
            //console.log(item);
            var object = {
                job    : 'removeFromCheckoutCart',
                item   :  item
            };
            dataService.removeFromCheckoutCart(object).then(function(data){
                if(data){
                    vm.cart      = data.cart;
                    vm.selected  = data.checkouts;
                }
            })
        };


        var fillCart = function(){
            var object = {
                job : 'fillCheckoutCart',
                checkouts: vm.checkouts
            };
            dataService.fillCheckoutCart(object).then(function(data){
                if(data){
                    vm.cart      = data.cart;
                    vm.selected  = data.checkouts;
                }
            })
        };

        var emptyCart = function(){
            dataService.emptyCheckoutCart().then(function(data){
                if(data){
                    vm.cart      = data.cart;
                    vm.selected  = data.checkouts;
                }
            });
        };

        var cartSort = function(){
            vm.duedate = vm.duedate === true  ? null : true;

            if(vm.duedate){
                var object = {
                    job : 'checkoutCartSort',
                    sortByDueDate: 1
                };
            } else {
                var object = {
                    job : 'checkoutCartSort',
                    sortByCheckoutDate: 1
                };
            }

            dataService.checkoutCartSort(object).then(function(data){
                //console.log(data);
                if(data.checkouts){
                    vm.checkouts = data.checkouts;
                    vm.selected  = data.selected;
                }
            });

        };

        var formSubmit = function(){
            //console.log('hello');
            $location.path('checkoutsconfirm');
        } ;


        vm.addToCart        = addToCart;
        vm.removeFromCart   = removeFromCart;
        vm.fillCart         = fillCart;
        vm.emptyCart        = emptyCart;
        vm.cartSort         = cartSort;
        vm.formSubmit       = formSubmit;

    };

CheckoutsController.$inject = [ '$location','dataService' ];
module.exports = CheckoutsController;

