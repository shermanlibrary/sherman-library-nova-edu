/**
 * EventController pulls in events.
 */

var dataService = require( '../services/dataService.js' );

dataService.prototype.getEventsAPI  = function( audience, series ) {
    var _this = this;
    var audience = typeof audience !== 'undefined' ? audience : 'all';
    var series = typeof series !== 'undefined' ? series : '';

    return _this.$http.get('//sherman.library.nova.edu/sites/spotlight/wp-json/events/v2/upcoming/?audience=' + audience + '&series=' + series )
        .then(function(response){
            if (typeof response.data === 'object') {
                return response.data;
            } else {
                return response.data;
            }

        }, function(response) {
            return _this.$q.reject(response.data);
        })
};

var EventController = function( $attrs, dataService ) {

  var vm    = this;
  vm.audience = $attrs.audience;
  vm.series = $attrs.series;
  /**
   * Uses dataService.prototype.getEvents - be sure to remove if this controller is removed too
   */
  function  getEventsAPI( audience, series ){

      dataService.getEventsAPI( audience, series ).then(function(data){
          if( data ) {
            vm.events = data;
          }
      });
  }

  getEventsAPI( vm.audience, vm.series );

}

EventController.$inject = ['$attrs', 'dataService'];
module.exports = EventController;
