/**
 * AdController allows the marketing department to
 * create ads for events or new services and distribute them
 * willy nilly.
 */

var dataService = require( '../services/dataService.js' );

dataService.prototype.getFeatures  = function( audience ) {
    var _this = this;
    var audience = typeof audience !== 'undefined' ? audience : 'all';

    return _this.$http.get('https://sherman.library.nova.edu/sites/wp-json/features/v2/ads/?audience=' + audience )
        .then(function(response){
            if (typeof response.data === 'object') {
                return response.data;
            } else {
                return response.data;
            }

        }, function(response) {
            return _this.$q.reject(response.data);
        })
};

var AdController = function( $attrs, dataService ) {

  var vm    = this;
  vm.audience = $attrs.audience;

  /**
   * Uses dataService.prototype.getFeatures - be sure to remove if this controller is removed too
   */
  function  getFeatures( audience ){

      dataService.getFeatures( audience ).then(function(data){
          if( data ) {
            vm.ads = data;
          }
      });
  }

  getFeatures( vm.audience );

}

AdController.$inject = [ '$attrs', 'dataService'];

module.exports = AdController;