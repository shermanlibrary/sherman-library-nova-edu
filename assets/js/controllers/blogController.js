/**
 * BlogController fetches posts from sherman.library.nova.edu/sites/blog
 */

var dataService = require( '../services/dataService.js' );

dataService.prototype.getBlogAPI = function( audience ) {
  var _this = this;
  var audience = typeof audience !== 'undefined' ? audience : false;

  return _this.$http
    .get('https://sherman.library.nova.edu/sites/wp-json/wp/v2/posts' + ( audience ? '?library-audience=' + audience : null )  )
    .then(function( response ) {
      if ( typeof response.data === 'object' ) { return response.data; }
      else { return response.data; }
    }, function( response ) {
      return _this.$q.reject( response.data );
    })
};

var BlogController = function( $attrs, dataService ) {

  var vm = this;
  vm.audience = $attrs.audience;

  /**
   * Uses dataService.prototype.getBlogAPI - be sure to remove if this controller is removed too
   */

  function getBlogAPI( audience ) {
    dataService
      .getBlogAPI( audience )
      .then( function( data ) {
        if ( data ) { vm.posts = data; }
      });
  }

  getBlogAPI( vm.audience );
};

BlogController.$inject = [ '$attrs', 'dataService' ];
module.exports = BlogController;