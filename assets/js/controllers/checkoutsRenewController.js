var dataService = require( '../services/dataService.js' );

dataService.prototype.submitRenewal = function() {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', { job : 'submitRenewal'} )
        .then(function(response){
            if (typeof response.data === 'object') {
                //console.log(response.data);
                return response.data;
            } else {
                // invalid response
                return _this.$q.reject(response.data);
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};


var CheckoutsRenewController = function(  $route, $location, dataService ){
    var vm    = this;
    vm.currentPage      = 1;
    vm.pageSize         = 5;
    vm.test   = 'Controller is controlling';

    var submitRenewal = function(){

        dataService.submitRenewal().then(function(data){
            console.log(data);
            if(data.renewals) {
                vm.renewals = data.renewals;
                if( vm.renewals.length >= 0) {
                    //vm.pageSize = 5;
                    vm.pageSize = vm.renewals.length;
                } else {
                    vm.pageSize = vm.renewals.length;
                }

            } else{
                $location.path('checkouts');
            }
            //getCheckouts();
        });
    } ;
    submitRenewal();
};

CheckoutsRenewController.$inject = [ '$route', '$location', 'dataService' ];
module.exports = CheckoutsRenewController;