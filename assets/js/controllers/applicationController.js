/**
 * ApplicationController is presently the workhorse we use
 * to fetch user information, set sessions, etc.
 */

var dataService = require( '../services/dataService.js' );

dataService.prototype.getApplication  = function() {
    var _this = this;
    return _this.$http.get('../auth/api/endpoint.php?job=getApplication')
        .then(function(response){
            if (typeof response.data === 'object') {
                return response.data;
            } else {
                return response.data;
            }

        }, function(response) {
            // something went wrong
            return _this.$q.reject(response.data);
        })
};

dataService.prototype.checkSession = function() {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', { job : 'getUserInfo'})
        .then(function(response){
            if (typeof response.data === 'object') {
                return response.data;
            } else {
                return _this.$q.reject(response);
            }

        }, function(response) {
            return 'public';
        })
};

var ApplicationController = function( $rootScope, $window, $interval, dataService, routeService ){

    var vm = this;
    vm.col = 'n';
    vm.libraries = [
        { code: 'main', label: 'Alvin Sherman Library'},
        { code: 'ocean', label: 'Oceanographic Center Library'}
    ];

    vm.changers = ['main', 'ocean'];

    routeService.requireConditions();

    function getApplication(){
        dataService.getApplication().then(function(data){
            if(data) {
                vm.hours  = data.response.hours;
                vm.status = data.response.AppStatus;

                if(vm.status !== 'public'){
                    vm.info             = data.response.info;
                    vm.email_update     = data.response.info.email;
                    vm.activity         = data.response.activity;
                    vm.col              = vm.info.col;
                    vm.selectedLibrary =  vm.info.home_lib;
                }
            }
        });
    }

    function checkSession(){
        dataService.checkSession().then(function(data) {
            if(data) {
                if(data.status === 'public') {
                    $window.location.href = "./logout.php";
                }else{
                }
            }
        });
    }

    //25 minutes to logout
    //$interval(checkSession, 1500000);
    //20 minutes to logout
    $interval(checkSession, 1200000);

    getApplication();

    var userMenu = (function() {

        $rootScope.userMenuToggle = false;

        function toggle() {

            $rootScope.userMenuToggle = !$rootScope.userMenuToggle;
        }

        return {
            toggle: toggle
        }

    })();

    var librariesMenu = (function() {

        $rootScope.librariesMenuToggle = false;

        function toggle() {
            $rootScope.librariesMenuToggle = !$rootScope.librariesMenuToggle;
        }

        return {
            toggle: toggle
        };

    })();

    var showModalWindow = function( object ){
        object.show = true;
        vm.modal = object;
    };

    var closeModalWindow = function(){
        vm.modal = {};
    };

    var updateEmail = function(){
        var object = {
            job: 'updateEmail',
            email: vm.email_update
        };
        console.log(object);
        dataService.updateEmail(object).then(function(data){

            if(data.update_error){
                vm.email_update =  vm.info.email;
                vm.update_message = '<div class="alert alert--warning">' + data.update_error + '</div>';
            }

            if(data.response){
                vm.email_update = data.response;
                vm.info.email = data.response;
                vm.update_message = '<div class="alert alert--success">Email updated!</div>';
            }

        });
    };

    var updateHomeLibrary = function(){

        vm.update_message = null;

        var object = {
            job: 'updateHomeLibrary',
            field: 'fixedFields',
            locx00 : vm.selectedLibrary,
            original : vm.info.home_lib

        };

        dataService.updateHomeLibrary(object).then(function(data){

            if(data.update_error){
                vm.update_message = '<div class="alert alert--warning">' + data.update_error + '</div>';
            }

            if(data.response){

                vm.info.home_lib = data.response;
                vm.update_message = '<div class="alert alert--success">Home Library updated!</div>';
            }

        });
    };

    var inArray =   function(needle, haystack) {
        for(var i in haystack) {
            if(haystack[i] == needle) return true;
        }
        return false;
    };

    $rootScope.librariesMenu    = librariesMenu;
    $rootScope.userMenu         = userMenu;
    vm.showModalWindow          = showModalWindow;
    vm.closeModalWindow         = closeModalWindow;
    vm.updateEmail              = updateEmail;
    vm.updateHomeLibrary        = updateHomeLibrary;
    vm.inArray                  = inArray;
};

ApplicationController .$inject = [ '$rootScope', '$window', '$interval', 'dataService', 'routeService' ];
module.exports = ApplicationController;

