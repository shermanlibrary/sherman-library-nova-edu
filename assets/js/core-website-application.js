'use strict';
( function() {

  /*------------------------------------*\
   $SHERMAN-APP
   \*------------------------------------*/
  /**
   * CONTENTS
   * DEPENDENCIES.........Everything we need to pull-in through webpack.
   * BOOTSTRAP............Where we bootstrap our angular application
   * ROUTES ..............Configure the routes
   * CONTROLLERS..........Controllers we need to include
   */

  /*------------------------------------*\
   $DEPENDENCIES
   \*------------------------------------*/
  /**
   * This script by itself won't work in browsers. It requires that
   * an npm module -- in this case, webpack -- fetches all of the
   * dependencies we need for this thing to work. It will then
   * get squished together in a in a .js file in the /assets/dist
   * directory.
   *
   * This is all so we can reuse our code, and it makes it
   * easier to maintain in the long run.
   */

  /**
   * Angular, v. 1.5.8
   */
  require( 'angular' ); // angular.js
  require( 'angular-route' ); // angular router
  require( 'angular-sanitize' ); // angular sanitize

  /**
   * Services
   */
  var routeService  = require( './services/routeservice.js' );
  var dataService   = require( './services/dataService.js' );

  /*------------------------------------*\
   $BOOTSTRAP
   \*------------------------------------*/
  var app = angular.module('shermanApp', ['ngRoute', 'ngSanitize', 'angularUtils.directives.dirPagination', 'wc.Directives']);

  app.constant('API_URL', '//sherman.library.nova.edu');

  app.run(['$rootScope', '$window', '$location', 'routeService', function($rootScope, $window, $location, routeService ) {

    // Track the route as a pageview with Google Analytics
    $rootScope.$on( '$routeChangeSuccess', function() {
      $window.ga( 'send', 'pageview', { page: $location.url() });
      routeService.requireConditions();
    });

    $rootScope.$on( "$routeChangeError", function(event, next, current, error) {
      if(error === 'AUTH_REQUIRED'){
        var path = $location.path();
        $window.location = '/auth/index.php?action=login&route='+path;
      }
    });


  }]);

  /*------------------------------------*\
   $ROUTES
   \*------------------------------------*/
  app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
    .when('/account', {
      templateUrl: 'views/account.php',
      title : 'My Library Account',
      resolve: {
        auth: [ 'dataService', function( dataService) {

          return dataService.getUserInfo();
        }],

        routeRules: ['routeService', function(routeService){
          routeService.requireConditions();
        }]
      }
    })

    .when('/checkouts', {
      templateUrl: 'views/checkouts.php',
      title: 'Checkouts',
      resolve: {
        auth: [ 'dataService', function(dataService) {
          return dataService.getUserInfo();
        }],

        routeRules: ['routeService', function(routeService){
        }]
      }
    })

    .when('/checkoutsconfirm', {
      templateUrl: 'views/checkoutsconfirm.php',
      resolve: {
        auth: [ 'dataService', function(dataService) {
          return dataService.getUserInfo();
        }],

        routeRules: ['routeService', function(routeService){
        }]
      }
    })

    .when('/friends', {
      templateUrl : 'views/friends.php',
      resolve : {
        routeRules : [ 'routeService', function( routeService ) {
          return routeService.loadCSS( '/cdn/styles/css/public-global/s--friends.css');
        }]
      },
      title : 'Circle of Friends'

    })

    .when('/checkoutsrenew', {
      templateUrl: 'views/checkoutsrenew.php',
      resolve: {
        auth: [ 'dataService', function(dataService) {
          return dataService.getUserInfo();
        }],

        routeRules: ['routeService', function(routeService){
        }]
      }
    })

    .when('/holds', {
      templateUrl: 'views/holds.php',
      resolve: {
        auth: [ 'dataService', function(dataService) {
          return dataService.getUserInfo();
        }],

        routeRules: ['routeService', function(routeService){
        }]
      }
    })

    .when('/holdsconfirm', {
      templateUrl: 'views/holdsconfirm.php',
      resolve: {
        auth: [ 'dataService', function(dataService) {
          return dataService.getUserInfo();
        }],

        routeRules: ['routeService', function(routeService){
        }]
      }
    })

    .when('/holocaust', {
      templateUrl : 'views/holocaust-memorial.php',
      resolve : {
        routeRules : [ 'routeService', function( routeService ) {
          return routeService.loadCSS( '/cdn/styles/css/public-global/s--holocaust.css');
        }]
      },
      title : 'Holocaust Reflection and Resource Room'

    })

    .when('/holds', {
      templateUrl: 'views/holds.php',
      resolve: {
        auth: [ 'dataService', function(dataService) {
          return dataService.getUserInfo();
        }],

        routeRules: ['routeService', function(routeService){
        }]
      }
    })

    .when( '/testimonials', {
      templateUrl: 'views/testimonials.php',
      title: 'Testimonials',
      resolve: {
        routeRules: ['routeService', function(routeService){}]
      }
    })

    .when( '/settings', {
      templateUrl: 'views/settings.php',
      title: 'My Library Account – Settings',
      resolve: {
        auth: [ 'dataService', function( dataService) {
          return dataService.getUserInfo();
        }],

        routeRules: ['routeService', function(routeService){
        }]
      }
    })

    .when( '/', {
      templateUrl: 'views/home.php',
      resolve: {
        routeRules: ['routeService', function( routeService ){
          routeService.loadCSS( '/cdn/styles/css/public-global/s--home.css');
          routeService.requireConditions();
        }]
      }
    })

    .otherwise({
      redirectTo: '/'
    });

    $locationProvider.html5Mode(true);
  }]);


  /*------------------------------------*\
   $CONTROLLERS
   \*------------------------------------*/
  var AccountController = require( './controllers/accountController.js' );
  var AdController = require( './controllers/adController.js' );
  var ApplicationController = require( './controllers/applicationController.js' );
  var BlogController = require( './controllers/blogController.js' );
  var CheckoutsController = require( './controllers/checkoutsController.js' );
  var CheckoutsConfirmController = require( './controllers/checkoutsConfirmController.js' );
  var CheckoutsRenewController = require( './controllers/checkoutsRenewController.js' );
  var HoldsController = require( './controllers/holdsController.js' );
  var HoldsConfirmController = require( './controllers/holdsConfirmController.js' );
  var IssuesController = require( './controllers/issuesController.js' );
  var EventController = require( './controllers/eventsController.js' );
  var PagingController = require( './controllers/pagingController.js' );
  var SystemsStatusController = require( './controllers/systemsStatusController.js' );


  /*------------------------------------*\
   $FIREWORKS
   \*------------------------------------*/
  /**
   * Let's put everything together now.
   */
  app.service('dataService', dataService);
  app.factory('routeService', routeService);
  app.controller('AccountController', AccountController);
  app.controller('AdController', AdController);
  app.controller('ApplicationController', ApplicationController);
  app.controller('BlogController', BlogController);
  app.controller('CheckoutsController', CheckoutsController);
  app.controller('CheckoutsConfirmController', CheckoutsConfirmController);
  app.controller('CheckoutsRenewController', CheckoutsRenewController);
  app.controller('EventController', EventController);
  app.controller('HoldsController', HoldsController);
  app.controller('HoldsConfirmController', HoldsConfirmController);
  app.controller('IssuesController', IssuesController);
  app.controller('PagingController', PagingController);
  app.controller('SystemsStatusController', SystemsStatusController);

  dataService.prototype.getUserInfo = function() {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', { job : 'getUserInfo'})
    .then(function(response){
      if (response.data.status === 'public') {
        // console.log(response.data);
        return _this.$q.reject('AUTH_REQUIRED');
      } else {
        // console.log(response.data);
        return response.data;
      }
    }, function(response) {
      //console.log(response);
      return 'public';
    })
  };

  dataService.prototype.getHome  = function() {
    var _this = this;
    return _this.$http.get('../auth/api/endpoint.php?job=getHome')
    .then(function(response){
      if (typeof response.data === 'object') {
        //console.log(response.data);
        return response.data;
      } else {
        //console.log(response.data);
        return response.data;
        // invalid response
        //return _this.$q.reject(response.data);
      }

    }, function(response) {
      // something went wrong
      //console.log(response);

      return _this.$q.reject(response.data);
    })
  };

  dataService.prototype.getTitles  = function( query ) {
    var _this = this;
    return _this.$http.get('../ftf/endpoint.php?query='+query )
    .then(function(response){
      if (typeof response.data === 'object') {
        //console.log(response.data);
        return response.data;
      } else {
        // invalid response
        return _this.$q.reject(response.data);
      }

    }, function(response) {
      // something went wrong
      return _this.$q.reject(response.data);
    })
  };


  dataService.prototype.getASLBuildingHours  = function() {
    var _this = this;
    return _this.$http.get('../auth/api/endpoint.php?job=getASLBuildingHours')
    .then(function(response){
      if (typeof response.data === 'object') {
        //console.log(response.data);
        return response.data;
      } else {
        //console.log(response.data);
        return response.data;
        // invalid response
        //return _this.$q.reject(response.data);
      }

    }, function(response) {
      // something went wrong
      //console.log(response);
      return _this.$q.reject(response.data);
    })
  };

  dataService.prototype.getAllDbsMenuUnlog  = function() {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', { job : 'getAllDbsMenuUnlog'})
    .then(function(response){
      if (typeof response.data === 'object') {
        //console.log(response.data);
        return response.data;
      } else {
        //console.log(response.data);
        return response.data;
        // invalid response
        //return _this.$q.reject(response.data);
      }

    }, function(response) {
      // something went wrong
      //console.log(response);

      return _this.$q.reject(response.data);
    })
  };

  dataService.prototype.getAllSubjectsMenuUnlog  = function() {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', { job : 'getAllSubjectsMenuUnlog'})
    .then(function(response){
      if (typeof response.data === 'object') {
        //console.log(response.data);
        return response.data;
      } else {
        //console.log(response.data);
        return response.data;
        // invalid response
        //return _this.$q.reject(response.data);
      }

    }, function(response) {
      // something went wrong
      //console.log(response);

      return _this.$q.reject(response.data);
    })
  };


  dataService.prototype.updateEmail  = function( object ) {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', object )
    .then(function(response){
      if (typeof response.data === 'object') {
        //console.log(response.data);
        return response.data;
      } else {
        // invalid response
        return _this.$q.reject(response.data);
      }

    }, function(response) {
      // something went wrong
      return _this.$q.reject(response.data);
    })
  };

  dataService.prototype.updateHomeLibrary  = function( object ) {
    var _this = this;
    return _this.$http.post('../auth/api/endpoint.php', object )
    .then(function(response){
      if (typeof response.data === 'object') {
        //console.log(response.data);
        return response.data;
      } else {
        // invalid response
        return _this.$q.reject(response.data);
      }

    }, function(response) {
      // something went wrong
      return _this.$q.reject(response.data);
    })
  };


  dataService.prototype.getGuides  = function() {
    var _this = this;
    return _this.$http.get( '//lgapi.libapps.com/1.1/subjects?site_id=142&key=ea1d92be94311f8f083ebed28a0b5f10&guide_published=2' )
    .then(function(response){
      if (typeof response.data === 'object') {
        //console.log(response.data);
        return response.data;
      } else {
        // invalid response
        return _this.$q.reject(response.data);
      }

    }, function(response) {
      // something went wrong
      return _this.$q.reject(response.data);
    })
  };

  dataService.prototype.getBitbucketIssues = function( repository, state, kind, assignee ) {

    var _this = this,
      query,
      kind,
      assignee;

    switch ( state ) {

      case 'open' :
        query = '?q=state="open"';
        break;

      case 'new' :
        query = '?q=state="new"';
        break;

      case 'resolved' :
        query = '?q=state="resolved"';
        break;

      default :
        query = '?q=state!="resolved"';
        break;

    }

    return _this.$http.get('https://api.bitbucket.org/2.0/repositories/shermanlibrary/' + repository + '/issues' + query + ( kind ? ' AND kind="' + kind + '"': '') + ( assignee ? ' AND assignee.username="' + assignee + '"': '') )
    .then(function(response){
      if (typeof response.data === 'object') {
        return response.data;
      } else {
        return response.data;
      }

    }, function(response) {
      return _this.$q.reject(response.data);
    })


  };



  require( './directives/dirpagination.js' );
  require( './directives/wcAngularOverlay.js' );

//Home Controller

// CONTROLLERS
  (function(){

    'use strict';

    var HomeController = function( dataService, routeService ){
      var vm          = this;
      vm.showModal    = false;
      vm.databases    = [];

      function getGuides(){
        dataService.getGuides().then(function(data){
          if(data){
            //console.log(data);
            vm.guides = data;
          }
        });
      }

      function  getHome(){
        dataService. getHome().then(function(data){

          if(data){

            vm.databases  = data.response.databases;
            vm.subjects   = data.response.subjects;
            vm.workshops  = data.response.workshops;
            vm.programs   = data.response.programs;
          }
        })


      }

      function init(){
        getGuides();
        getHome();
      }

      init();

      var startsWith = function (actual, expected) {
        var lowerStr = (actual + "").toLowerCase();
        return lowerStr.indexOf(expected.toLowerCase()) === 0;
      };

      vm.startsWith   = startsWith;
    };

    HomeController.$inject = ['dataService', 'routeService'];

    angular.module('shermanApp')
    .controller('HomeController', HomeController);

  })();

//FTF Controller
  (function(){
    'use strict';
    var ftfController = function(  $timeout, dataService ){
      var vm = this;
      vm.selected = false;
      vm.filterTitle = 'startsWith';
      //vm.metadata = null;


      var getTitles = function (){
        vm.selected = false;
        if(vm.model !== undefined) {
          vm.model = vm.model.replace(/\s\s+/g, ' ');
          vm.model = vm.model.replace('.', ' ');
        }

        //console.log(vm.model);
        //vm.hits = null;
        //console.log(query);

        dataService.getTitles(vm.model).then(function(data){
          vm.items = data;
        });
      };

      var startsWith = function (actual, expected) {
        var lowerStr = (actual + "").toLowerCase();
        return lowerStr.indexOf(expected.toLowerCase()) === 0;
      };

      var setCurrent = function(index){
        vm.model = index.title;
        vm.selected = true;
        vm.metadata = index;
      };

      vm.getTitles    = getTitles;
      vm.startsWith   = startsWith;
      vm.setCurrent   = setCurrent;

    };

    ftfController .$inject = ['$timeout', 'dataService' ];
    angular.module('shermanApp')
    .controller('ftfController', ftfController);
  })();

//Checkout Results Directive
  (function(){

    'use strict';

    var checkoutResult = function() {
      var controller = [function(){
        var vm = this;
        vm.link	= 'https://novacat.nova.edu/record=b';
        //console.log(vm.checkout);
        //console.log(vm.selected);


        var checkCart = function(item){

          var check = vm.selected.indexOf(item);
          if( check < 0) {

            return false;
          } else{

            return true;
          }
        };

        var addToCart = function( checkout ){
          vm.add()(checkout);
        };

        var removeFromCart = function( item ){
          vm.remove()(item );
        };

        vm.checkCart      = checkCart;
        vm.addToCart      = addToCart;
        vm.removeFromCart = removeFromCart;

      }];
      return {
        scope: {
          checkout     : '=',
          selected     : '=',
          showbutton	 : '=',
          hidestatus	 : '=',
          add          : '&',
          remove       : '&'
        },
        controller: controller,
        controllerAs: 'vm',
        bindToController: true,
        templateUrl: 'assets/js/templates/checkoutResult.html',
        replace: true
      }
    };
    angular.module('shermanApp')
    .directive('checkoutResult', checkoutResult );
  })();

//Checkout Cart Directive
  (function(){

    'use strict';

    var checkoutCart = function() {
      var controller = [function(){
        var vm = this;
        vm.link	= 'https://novacat.nova.edu/record=b';

        var removeFromCart = function( item ){
          vm.remove()(item );
        };

        vm.removeFromCart = removeFromCart;

      }];
      return {
        scope: {
          checkout     : '=',
          cart		 : '=',
          remove       : '&'
        },
        controller: controller,
        controllerAs: 'vm',
        bindToController: true,
        templateUrl: 'assets/js/templates/checkoutCart.html',
        replace: true
      }
    };
    angular.module('shermanApp')
    .directive('checkoutCart', checkoutCart );
  })(); // comment

//Checkouts Renew Directive
  (function(){

    'use strict';

    var checkoutRenew = function() {
      var controller = [function(){
        var vm = this;
        vm.link	= 'https://novacat.nova.edu/record=b';

      }];
      return {
        scope: {
          renewal     : '='
        },
        controller: controller,
        controllerAs: 'vm',
        bindToController: true,
        templateUrl: 'assets/js/templates/checkoutRenew.html',
        replace: true
      }
    };
    angular.module('shermanApp')
    .directive('checkoutRenew', checkoutRenew );
  })();

  (function() {

    angular.module( 'shermanApp' ).filter( 'trusted', ['$sce', function( $sce ) {
      return function( url ) {
        return $sce.trustAsResourceUrl( url );
      }
    }]);

  })();
})();
