(function(){

    'use strict';

    var checkoutResult = function() {
        var controller = [function(){
            var vm = this;
            //console.log(vm.checkout);
            //console.log(vm.selected);

            var checkCart = function(item){
                var check = vm.selected.indexOf(item);
                if( check < 0) {
                    return false;
                } else{
                    return true;
                }
            };

            var addToCart = function( checkout ){
                vm.add()(checkout);
            };

            var removeFromCart = function( item ){
                vm.remove()(item );
            };

            vm.checkCart      = checkCart;
            vm.addToCart      = addToCart;
            vm.removeFromCart = removeFromCart;

        }];
        return {
            scope: {
                checkout     : '=',
                selected     : '=',
                add          : '&',
                remove       : '&'
            },
            controller: controller,
            controllerAs: 'vm',
            bindToController: true,
            templateUrl: 'assets/js/templates/checkoutResult.html',
            replace: true
        }
    };
    angular.module('shermanApp')
    .directive('checkoutResult', checkoutResult );
})();
