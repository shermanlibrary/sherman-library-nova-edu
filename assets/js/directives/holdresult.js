(function(){

    'use strict';

    var holdResult = function() {
        var controller = [function(){
            var vm = this;

            var checkCart = function(hold){
                var check = vm.selected.indexOf(hold);
                if( check < 0) {
                    return false;
                } else{
                   return true;
                }
            };

            var addToCart = function( hold ){
                vm.add()(hold);
            };

            var removeFromCart = function( hold ){
                vm.remove()(hold);
            };

            vm.checkCart      = checkCart;
            vm.addToCart      = addToCart;
            vm.removeFromCart = removeFromCart;

        }]
        return {
            scope: {
                hold         : '=',
                selected     : '=',
                add          : '&',
                remove       : '&'
            },
            controller: controller,
            controllerAs: 'vm',
            bindToController: true,
            templateUrl: 'assets/js/templates/holdResult.html',
            replace: true
        }
    };
    angular.module('shermanApp')
    .directive('holdResult', holdResult );
})();
