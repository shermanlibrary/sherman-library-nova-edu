var LoginModule =    (function(){
    var
        lm                      = this,
        modal                   = document.getElementById('modal');

    var clearError = function(){
        document.getElementById('sharkErrorResponse').style.display    = 'none';
        document.getElementById('browardErrorResponse').style.display  = 'none';
        document.getElementById('hpdErrorResponse').style.display      = 'none';
        document.getElementById('unsErrorResponse').style.display      = 'none';
    }

    var setError = function( element, target){

        var _elem   = document.querySelector(element),
            _target = document.querySelector(target);
        if(!_elem || !_target){
            return false;
        }
        _target.innerHTML       = _elem.innerHTML;
        _target.style.display   = 'block';
        return true;
    }

    var idFocus = function( element ){
        var _elem   = document.querySelector(element);
        _elem.addEventListener('focus', function(e){
            clearError();
        });
    }

    var quadHelp = function(element, target){
        var _elem   = document.querySelector(element),
            _target = document.querySelector(target),
            _id     = _elem.id;
        _elem.onclick = function (e) {

            modal.setAttribute('class', 'modal semantic-content is-active');
            switch( _id ) {
                case 'sharkHelp':
                    _target.setAttribute('class','show');
                    break;
                case 'browardHelp':
                    _target.setAttribute('class','show');
                    break;
                case 'hpdHelp':
                    _target.setAttribute('class','show');
                    break;
                case 'unsHelp':
                    _target.setAttribute('class','show');
                    break;
            }
            e.preventDefault();
        }
    }

    var closeModal = function( element ){
        var _elem   = document.querySelector(element);
        _elem.addEventListener('click', function(e){
            modal.setAttribute('class', 'modal semantic-content');
            e.preventDefault();
        });
    }

    return {
        clearError      : clearError,
        setError        : setError,
        idFocus         : idFocus,
        quadHelp        : quadHelp,
        closeModal      : closeModal
    };
})();

LoginModule.clearError();
LoginModule.setError('#sharkError', '#sharkErrorResponse' );
LoginModule.setError('#browardError', '#browardErrorResponse' );
LoginModule.setError('#hpdError', '#hpdErrorResponse' );
LoginModule.setError('#unsError', '#unsErrorResponse' );
LoginModule.idFocus('#nuid');
LoginModule.idFocus('#bpwd');
LoginModule.idFocus('#hpwd');
LoginModule.idFocus("#uuid");
LoginModule.quadHelp('#sharkHelp','#shark-help');
LoginModule.quadHelp('#browardHelp','#broward-help');
LoginModule.quadHelp('#hpdHelp','#hpd-help');
LoginModule.quadHelp('#unsHelp','#uns-help');
LoginModule.closeModal('.modal-close');