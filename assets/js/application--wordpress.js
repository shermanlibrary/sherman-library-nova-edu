'use strict';
(function() {

  /**
   * Angular, v. 1.5.8
   */
  require( 'angular' ); // angular.js
  require( 'angular-sanitize' ); // angular sanitize

  /**
   * Services
   */
  var dataService   = require( './services/dataService.js' );

  /*------------------------------------*\
   $CONTROLLERS
   \*------------------------------------*/
  var AdController = require( './controllers/adController.js' );
  var ApplicationController = require( './controllers/applicationController.js' );
  var EventController = require( './controllers/eventsController.js' );

  /*------------------------------------*\
   $BOOTSTRAP
   \*------------------------------------*/
  var app = angular.module('wpApp', ['ngSanitize' ]);
  app.constant('API_URL', '//sherman.library.nova.edu');
  app.run();

  /*------------------------------------*\
   $FIREWORKS
   \*------------------------------------*/
  /**
   * Let's put everything together now.
   */
  app.service('dataService', dataService);
  app.controller( 'AdController', AdController );
  app.controller('ApplicationController', ApplicationController);
  app.controller('EventController', EventController);

  angular.module( 'wpApp' ).filter( 'trusted', ['$sce', function( $sce ) {
    return function( url ) {
      return $sce.trustAsResourceUrl( url );
    }
  }]);

})();