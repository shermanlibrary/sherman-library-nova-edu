// MODULE
(function(){
    'use strict';

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-37110734-2']);
    _gaq.push(['_gat._forceSSL']);
    _gaq.push(['_trackPageview']);

    (function () {
        'use strict';

        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

    var app = angular.module('shermanApp', ['ngRoute', 'ngSanitize', 'angularUtils.directives.dirPagination', 'wc.Directives']);


      app.run(['$rootScope', '$window', '$location', function($rootScope, $window, $location ) {

          $rootScope.$on( "$routeChangeStart", function(event, next, current, error) {
              console.log($location.absUrl());
          });

          $rootScope.$on( "$routeChangeError", function(event, next, current, error) {
              if(error === 'AUTH_REQUIRED'){
                var path = $location.path();
                $window.location = '/auth/index.php?action=login&route='+path;
              }
          });

        }]);

    app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        $routeProvider


            .when('/account', {
                templateUrl: 'views/account.php',
                resolve: {
                    auth: [ 'dataService', function( dataService) {
                        return dataService.getUserInfo();
                    }],

                    routeRules: ['routeService', function(routeService){
                        return routeService.setNonHome();
                    }]
                }
            })

            .when('/checkouts', {
                templateUrl: 'views/checkouts.php',
                resolve: {
                    auth: [ 'dataService', function(dataService) {
                        return dataService.getUserInfo();
                    }],

                    routeRules: ['routeService', function(routeService){
                        return routeService.setNonHome();
                    }]
                }
            })

            .when('/checkoutsconfirm', {
                templateUrl: 'views/checkoutsconfirm.php',
                resolve: {
                    auth: [ 'dataService', function(dataService) {
                        return dataService.getUserInfo();
                    }],

                    routeRules: ['routeService', function(routeService){
                        return routeService.setNonHome();
                    }]
                }
            })

            .when('/holds', {
                templateUrl: 'views/holds.php',
                resolve: {
                    auth: [ 'dataService', function(dataService) {
                        return dataService.getUserInfo();
                    }],

                    routeRules: ['routeService', function(routeService){
                        return routeService.setNonHome();
                    }]
                }
            })

            .when('/holdsconfirm', {
                templateUrl: 'views/holdsconfirm.php',
                resolve: {
                    auth: [ 'dataService', function(dataService) {
                        return dataService.getUserInfo();
                    }],

                    routeRules: ['routeService', function(routeService){
                        return routeService.setNonHome();
                    }]
                }
            })


            .when('/holds', {
                templateUrl: 'views/holds.php',
                resolve: {
                    auth: [ 'dataService', function(dataService) {
                        return dataService.getUserInfo();
                    }],

                    routeRules: ['routeService', function(routeService){
                        return routeService.setNonHome();
                    }]
                }
            })

            .when('/', {
                templateUrl: 'views/home.php',
                resolve: {
                   routeRules: ['routeService', function(routeService){
                       return routeService.setHome();
                   }]

                }
            })

            .otherwise({
                redirectTo: '/'
            });

        $locationProvider.html5Mode(true);
    }]);

})();
