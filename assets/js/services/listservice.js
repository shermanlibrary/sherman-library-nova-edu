/**
 * This handles the fetching of data from
 * librarian-created book and item lists to be
 * parsed or passed into controllers/ListController.
 */
dataService.prototype.getList  = function( slug ) {

  var _this = this;

  return _this.$http.get('http://sherman.library.nova.edu/sites/spotlight/list/' + slug + '/?json=true' )
    .then(function(response){

    if (typeof response.data === 'object') {

      return response.data;
    } else {

      return response.data;
    }
  }, function(response) {

    return _this.$q.reject(response.data);
  });

};
