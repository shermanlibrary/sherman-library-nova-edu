'use strict';
var dataService = function($http, $q) {
    this.$http = $http;
    this.$q = $q;
};

dataService.$inject = ['$http', '$q'];
module.exports = dataService;
