'use strict';

var routeService = function ($rootScope) {

    function requireConditions(){
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = "/cdn/scripts/config.js";
        document.querySelector("body").appendChild(script);
    }

    return {
        loadCSS     : loadCSS,
        requireConditions : requireConditions
    };
};

routeService.$inject=['$rootScope'];
module.exports = routeService;