(function() {
    'use strict';

   var modalService = function(){

    this.show = function( object){
       object.show = true;
       return object;
    };

   this.close = function(){
       var object = { element: false};
       return object;
   };

   };

    modalService.$inject=['dataService'];

    angular.module('shermanApp')
     .service('modalService', modalService);

}());