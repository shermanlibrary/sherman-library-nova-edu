'use strict';
( function() {

    /*------------------------------------*\
     $SHARKLINK-APP
     \*------------------------------------*/
    /**
     * CONTENTS
     * DEPENDENCIES.........Everything we need to pull-in through webpack.
     * BOOTSTRAP............Where we bootstrap our angular application
     * ROUTES ..............Configure the routes
     * CONTROLLERS..........Controllers we need to include
     */

    /*------------------------------------*\
     $DEPENDENCIES
     \*------------------------------------*/
    /**
     * This script by itself won't work in browsers. It requires that
     * an npm module -- in this case, webpack -- fetches all of the
     * dependencies we need for this thing to work. It will then
     * get squished together in a in a .js file in the /assets/dist
     * directory.
     *
     * This is all so we can reuse our code, and it makes it
     * easier to maintain in the long run.
     */

    /**
     * Angular, v. 1.5.8
     */
    require( 'angular' ); // angular.js
    require( 'angular-route' ); // angular router
    require( 'angular-sanitize' ); // angular sanitize

    /**
     * Services
     */
    var routeService  = require( './services/routeservice.js' );
    var dataService   = require( './services/dataService.js' );

    /*------------------------------------*\
     $BOOTSTRAP
     \*------------------------------------*/
    var app = angular.module('sharklinkApp', ['ngRoute', 'ngSanitize', 'angularUtils.directives.dirPagination', 'wc.Directives']);

    app.constant('API_URL', '//sherman.library.nova.edu');
    app.run(['$rootScope', '$window', '$location', function($rootScope, $window, $location ) {

        $rootScope.$on( "$routeChangeError", function(event, next, current, error) {
            if(error === 'AUTH_REQUIRED'){
                var path = $location.path();
                $window.location = '/auth/index.php?action=login&route='+path;
            }
        });

    }]);

    /*------------------------------------*\
     $ROUTES
     \*------------------------------------*/
    app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        $routeProvider

            .when( '/', {
                templateUrl: 'index.php',
                resolve: {
                    routeRules: ['routeService', function( routeService ){
                        routeService.setNonHome();
                    }]
                }
            })

            .otherwise({
                redirectTo: '/'
            });

        $locationProvider.html5Mode(true);
    }]);

    /*------------------------------------*\
     $CONTROLLERS
     \*------------------------------------*/
    /**
     * AdController allows the marketing department to
     * create ads for events or new services and distribute them
     * willy nilly.
     */
    var AccountController = require( './controllers/accountController.js' );
    var ApplicationController = require( './controllers/applicationController.js' );
    var CheckoutsController = require( './controllers/checkoutsController.js' );
    var CheckoutsConfirmController = require( './controllers/checkoutsConfirmController.js' );
    var CheckoutsRenewController = require( './controllers/checkoutsRenewController.js' );
    var HoldsController = require( './controllers/holdsController.js' );
    var HoldsConfirmController = require( './controllers/holdsConfirmController.js' );
    var EventController = require( './controllers/eventsController.js' );
    var PagingController = require( './controllers/pagingController.js' );

    /*------------------------------------*\
     $FIREWORKS
     \*------------------------------------*/
    /**
     * Let's put everything together now.
     */
    app.service('dataService', dataService);
    app.factory('routeService', routeService);
    app.controller('AccountController', AccountController);
    app.controller('ApplicationController', ApplicationController);
    app.controller('CheckoutsController', CheckoutsController);
    app.controller('CheckoutsConfirmController', CheckoutsConfirmController);
    app.controller('CheckoutsRenewController', CheckoutsRenewController);
    app.controller('HoldsController', HoldsController);
    app.controller('HoldsConfirmController', HoldsConfirmController);
    app.controller('EventController', EventController);
    app.controller('PagingController', PagingController);


    dataService.prototype.getUserInfo = function() {
        var _this = this;
        return _this.$http.post('../auth/api/endpoint.php', { job : 'getUserInfo'})
            .then(function(response){
                if (response.data.status === 'public') {
                    // console.log(response.data);
                    return _this.$q.reject('AUTH_REQUIRED');
                } else {
                    // console.log(response.data);
                    return response.data;
                }
            }, function(response) {
                //console.log(response);
                return 'public';
            })
    };


    require( './directives/dirpagination.js' );
    require( './directives/wcAngularOverlay.js' );


//Checkout Results Directive
    (function(){

        'use strict';

        var checkoutResult = function() {
            var controller = [function(){
                var vm = this;
                vm.link	= 'https://novacat.nova.edu/record=b';
                //console.log(vm.checkout);
                //console.log(vm.selected);


                var checkCart = function(item){

                    var check = vm.selected.indexOf(item);
                    if( check < 0) {

                        return false;
                    } else{

                        return true;
                    }
                };

                var addToCart = function( checkout ){
                    vm.add()(checkout);
                };

                var removeFromCart = function( item ){
                    vm.remove()(item );
                };

                vm.checkCart      = checkCart;
                vm.addToCart      = addToCart;
                vm.removeFromCart = removeFromCart;

            }];
            return {
                scope: {
                    checkout     : '=',
                    selected     : '=',
                    showbutton	 : '=',
                    hidestatus	 : '=',
                    add          : '&',
                    remove       : '&'
                },
                controller: controller,
                controllerAs: 'vm',
                bindToController: true,
                templateUrl: 'assets/js/templates/checkoutResult.html',
                replace: true
            }
        };
        angular.module('sharklinkApp')
            .directive('checkoutResult', checkoutResult );
    })();

//Checkout Cart Directive
    (function(){

        'use strict';

        var checkoutCart = function() {
            var controller = [function(){
                var vm = this;
                vm.link	= 'https://novacat.nova.edu/record=b';

                var removeFromCart = function( item ){
                    vm.remove()(item );
                };

                vm.removeFromCart = removeFromCart;

            }];
            return {
                scope: {
                    checkout     : '=',
                    cart		 : '=',
                    remove       : '&'
                },
                controller: controller,
                controllerAs: 'vm',
                bindToController: true,
                templateUrl: 'assets/js/templates/checkoutCart.html',
                replace: true
            }
        };
        angular.module('sharklinkApp')
            .directive('checkoutCart', checkoutCart );
    })();

//Checkouts Renew Directive
    (function(){

        'use strict';

        var checkoutRenew = function() {
            var controller = [function(){
                var vm = this;
                vm.link	= 'https://novacat.nova.edu/record=b';

            }];
            return {
                scope: {
                    renewal     : '='
                },
                controller: controller,
                controllerAs: 'vm',
                bindToController: true,
                templateUrl: 'assets/js/templates/checkoutRenew.html',
                replace: true
            }
        };
        angular.module('sharklinkApp')
            .directive('checkoutRenew', checkoutRenew );
    })();


//Holds Results Directive
    (function(){

        'use strict';

        var holdResult = function() {
            var controller = [function(){
                var vm = this;

                var checkCart = function(hold){
                    var check = vm.selected.indexOf(hold);
                    if( check < 0) {
                        return false;
                    } else{
                        return true;
                    }
                };

                var addToCart = function( hold ){
                    vm.add()(hold);
                };

                var removeFromCart = function( hold ){
                    vm.remove()(hold);
                };

                vm.checkCart      = checkCart;
                vm.addToCart      = addToCart;
                vm.removeFromCart = removeFromCart;

            }]
            return {
                scope: {
                    hold         : '=',
                    selected     : '=',
                    add          : '&',
                    remove       : '&'
                },
                controller: controller,
                controllerAs: 'vm',
                bindToController: true,
                templateUrl: 'assets/js/templates/holdResult.html',
                replace: true
            }
        };
        angular.module('sharklinkApp')
            .directive('holdResult', holdResult );
    })();

    (function() {

        angular.module( 'sharklinkApp' ).filter( 'trusted', ['$sce', function( $sce ) {
            return function( url ) {
                return $sce.trustAsResourceUrl( url );
            }
        }]);

    })();
})();
