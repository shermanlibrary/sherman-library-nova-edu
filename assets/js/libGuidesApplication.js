/*
 * shortcode.js 1.1.0
 * by @nicinabox
 * License: MIT
 * Issues: https://github.com/nicinabox/shortcode.js/issues
 */

/* jshint strict: false, unused: false */

var Shortcode = function(el, tags) {
  if (!el) { return; }

  this.el      = el;
  this.tags    = tags;
  this.matches = [];
  this.regex   = '\\[{name}(.*?)?\\](?:([\\s\\S]*?)(\\[\/{name}\\]))?';

  if (this.el.jquery) {
    this.el = this.el[0];
  }

  this.matchTags();
  this.convertMatchesToNodes();
  this.replaceNodes();
};

Shortcode.prototype.matchTags = function() {
  var html = this.el.outerHTML, instances,
      match, re, contents, regex, tag, options;

  for (var key in this.tags) {
    if (!this.tags.hasOwnProperty(key)) { return; }
    re        = this.template(this.regex, { name: key });
    instances = html.match(new RegExp(re, 'g')) || [];

    for (var i = 0, len = instances.length; i < len; i++) {
      match = instances[i].match(new RegExp(re));
      contents = match[3] ? '' : undefined;
      tag      = match[0];
      regex    = this.escapeTagRegExp(tag);
      options  = this.parseOptions(match[1]);

      if (match[2]) {
        contents = match[2].trim();
        tag      = tag.replace(contents, '');
        regex    = regex.replace(contents, '([\\s\\S]*?)');
      }

      this.matches.push({
        name: key,
        tag: tag,
        regex: regex,
        options: options,
        contents: contents
      });
    }
  }
};

Shortcode.prototype.convertMatchesToNodes = function() {
  var html = this.el.innerHTML, excludes, re, replacer;

  replacer = function(match, p1, p2, p3, p4, offset, string) {
    if (p1) {
      return match;
    } else {
      var node = document.createElement('span');
      node.setAttribute('data-sc-tag', this.tag);
      node.className = 'sc-node sc-node-' + this.name;
      return node.outerHTML;
    }
  };

  for (var i = 0, len = this.matches.length; i < len; i++) {
    excludes = '((data-sc-tag=")|(<pre.*)|(<code.*))?';
    re       = new RegExp(excludes + this.matches[i].regex, 'g');
    html     = html.replace(re, replacer.bind(this.matches[i]));
  }

  this.el.innerHTML = html;
};

Shortcode.prototype.replaceNodes = function() {
  var self = this, html, match, result, done, node, fn, replacer,
      nodes = document.querySelectorAll('.sc-node');

  replacer = function(result) {
    if (result.jquery) { result = result[0]; }

    result = self.parseCallbackResult(result);
    node.parentNode.replaceChild(result, node);
  };

  for (var i = 0, len = this.matches.length; i < len; i++) {
    match = this.matches[i];
    node  = document.querySelector('.sc-node-' + match.name);

    if (node && node.dataset.scTag === match.tag) {
      fn     = this.tags[match.name].bind(match);
      done   = replacer.bind(match);
      result = fn(done);

      if (result !== undefined) {
        done(result);
      }
    }
  }
};

Shortcode.prototype.parseCallbackResult = function(result) {
  var container, fragment, children;

  switch(typeof result) {
    case 'function':
      result = document.createTextNode(result());
      break;

    case 'string':
      container = document.createElement('div');
      fragment  = document.createDocumentFragment();
      container.innerHTML = result;
      children = container.children;

      if (children.length) {
        for (var i = 0, len = children.length; i < len; i++) {
          fragment.appendChild(children[i].cloneNode(true));
        }
        result = fragment;
      } else {
        result = document.createTextNode(result);
      }
      break;

    case 'object':
      if (!result.nodeType) {
        result = JSON.stringify(result);
        result = document.createTextNode(result);
      }
      break;

    case 'default':
      break;
  }

  return result;
};

Shortcode.prototype.parseOptions = function(stringOptions) {
  var options = {}, set;
  if (!stringOptions) { return; }

  set = stringOptions
          .replace(/(\w+=)/g, '\n$1')
          .split('\n');
  set.shift();

  for (var i = 0; i < set.length; i++) {
    var kv = set[i].split('=');
    options[kv[0]] = kv[1].replace(/\'|\"/g, '').trim();
  }

  return options;
};

Shortcode.prototype.escapeTagRegExp = function(regex) {
  return regex.replace(/[\[\]\/]/g, '\\$&');
};

Shortcode.prototype.template = function(s, d) {
  for (var p in d) {
    s = s.replace(new RegExp('{' + p + '}','g'), d[p]);
  }
  return s;
};

// Polyfill .trim()
String.prototype.trim = String.prototype.trim || function () {
  return this.replace(/^\s+|\s+$/g, '');
};

// jQuery plugin wrapper
if (window.jQuery) {
  var pluginName = 'shortcode';
  $.fn[pluginName] = function (tags) {
    this.each(function() {
      if (!$.data(this, pluginName)) {
        $.data(this, pluginName, new Shortcode(this, tags));
      }
    });
    return this;
  };
}


// Application 8/15/16
(function(){
    'use strict';

    var app = angular.module('lgApp', ['ngRoute', 'ngSanitize']);

    app.run(['$rootScope', '$window', '$location', function($rootScope, $window, $location ) {}]);

})();

//Data Service
(function() {
    'use strict';

    var dataService = function($http, $q) {
        this.$http = $http;
        this.$q = $q;
    };

    dataService.$inject = ['$http', '$q'];

    dataService.prototype.getEvents  = function( audience ) {
        var _this = this;
        var audience = typeof audience !== 'undefined' ? audience : 'all';

        return _this.$http.get('http://sherman.library.nova.edu/sites/spotlight/wp-json/events/v2/upcoming/?audience=' + audience )
            .then(function(response){
                if (typeof response.data === 'object') {
                    return response.data;
                } else {
                    return response.data;
                }

            }, function(response) {
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getFeatures  = function( audience ) {
        var _this = this;
        var audience = typeof audience !== 'undefined' ? audience : 'all';

        return _this.$http.get('http://sherman.library.nova.edu/sites/wp-json/features/v2/ads/?audience=' + audience )
            .then(function(response){
                if (typeof response.data === 'object') {
                    return response.data;
                } else {
                    return response.data;
                }

            }, function(response) {
                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.checkSession = function() {
        var _this = this;
        return _this.$http.post('https://sherman.library.nova.edu/api/endpoint.php', { job : 'getUserInfo'})
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response);
                }

            }, function(response) {
                // something went wrong
                return 'public';
            })
    };

    dataService.prototype.getUserInfo = function() {
        var _this = this;
        return _this.$http.post('https://sherman.library.nova.edu/api/endpoint.php', { job : 'getUserInfo'})
            .then(function(response){
                if (response.data.status === 'public') {
                    console.log(response.data);
                    return _this.$q.reject('AUTH_REQUIRED');
                } else {
                    console.log(response.data);
                    return response.data;
                }
            }, function(response) {
                console.log(response);
                //return 'public';
            })
    };

    dataService.prototype.getApplication  = function() {
        var _this = this;
        return _this.$http.get('https://sherman.library.nova.edu/api/endpoint.php?job=getApplication')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);

                return _this.$q.reject(response.data);
            })
    };

    dataService.prototype.getASLBuildingHours  = function() {
        var _this = this;
        return _this.$http.get('https://sherman.library.nova.edu/api/endpoint.php?job=getASLBuildingHours')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);
                return _this.$q.reject(response.data);
            })
    };

    angular.module('lgApp')
        .service('dataService', dataService);

}());

//Application Controller
// CONTROLLERS
(function(){

    'use strict';

    var ApplicationController = function( $rootScope, $window, $interval, dataService, $route ){

        var vm = this;
        vm.col = 'n';
        vm.libraries = [
            { code: 'main', label: 'Alvin Sherman Library'},
            { code: 'nmb', label: 'North Miami Beach'},
            { code: 'ocean', label: 'Oceanographic Center Library'}
        ];

        function getApplication(){
            dataService.getApplication().then(function(data){
                //console.log(data);
                if(data) {
                    vm.hours  = data.response.hours;
                    vm.status = data.response.AppStatus;

                    if(vm.status !== 'public'){
                        vm.info             = data.response.info;
                        //console.log(data.response.info);
                        vm.email_update     = data.response.info.email;
                        vm.activity         = data.response.activity;
                        vm.col              = vm.info.col;
                        vm.selectedLibrary =  vm.info.home_lib;
                    }
                }
            });
        }

        function checkSession(){
            dataService.checkSession().then(function(data) {
                //console.log(data);
                if(data) {
                    if(data.status === 'public') {
                        //console.log('go now');
                        $window.location.href = "./logout.php";
                    }else{
                        //console.log('nope');
                    }
                }
            });
        }

        //25 minutes to logout
        //$interval(checkSession, 1500000);
        //20 minutes to logout
        $interval(checkSession, 1200000);
        //$interval(checkSession, 90);

        getApplication();

        var userMenuToggle = function(){
            $rootScope.display =   $rootScope.display == 'none' ? 'block' : 'none';
        };

        var showModalWindow = function(object){
            object.show = true;
            vm.modal = object;
        };

        var closeModalWindow = function(){
            vm.modal = {};
        };

        $rootScope.userMenuToggle       = userMenuToggle;
        vm.showModalWindow              = showModalWindow;
        vm.closeModalWindow  = closeModalWindow;
    };

    ApplicationController .$inject = [ '$rootScope', '$window', '$interval', 'dataService', '$route' ];

    angular.module('lgApp')
        .controller('ApplicationController', ApplicationController);

})();

(function(){

    'use strict';

    var config, eventsOptions;

    new Shortcode( document.querySelector( '[data-ng-controller]' ), {
      events: function( done ) {

        var audience;

        eventsOptions = this.options;
        if ( eventsOptions.audience ) {
          audience = eventsOptions.audience;
        }


        var markup = '<div class="clearfix" data-ng-controller="EventController as ec" data-audience="' + ( audience ? audience : '' ) + '">';
        markup += '<article data-ng-repeat="event in ec.events | limitTo: ' + ( eventsOptions.count ? eventsOptions.count : '5' ) + '" class="card clearfix">'
        markup += '  <span class="card__color-code card__color-code--' + ( audience ? audience : '' ) + '"></span>'
        markup += '  <header class="card__header" style="margin-bottom: .5em;">'
        markup += '    <a ng-href="{{ event.url }}" class="link link--undecorated _link-blue">'
        markup += '      <h3 class="menu__item__title" style="margin-bottom: .25em;">{{ event.title }}</h3>'
        markup += '    </a>'
        markup += '    <p class="no-margin small-text" >'
        markup += '      <time class="time">'
        markup += '        <span itemprop="startDate"><b>{{ event.start }}</b></span>'
        markup += '        <span class="time__hours" style="color: #999;">{{ event.from }} - {{ event.until }}</span>'
        markup += '      </time>'
        markup += '      <span ng-bind-html="event.series"></span>'
        markup += '    </p>'
        markup += '  </header>'
        markup += '  <section class="content">'
        markup += '    <p class="no-margin">{{ event.excerpt }}</p>'
        markup += '  </section>'
        markup += '</article>'
        markup += '</div>';
        return markup;
      }
    });

    new Shortcode( document.querySelector( '[data-ng-controller]' ), {

      config : function( done ) {
        config = this.options;
      }

    });

    var LibGuidesController = function( $attrs, dataService ) {

      var vm    = this;
      vm.config = config;
      vm.description = $attrs.description;

      if ( vm.config ) {
        switch ( vm.config.navbar ) {
          case 'false' :
            vm.navbar = true;
          break;
        }
      }

    }

    LibGuidesController.$inject = [ '$attrs', 'dataService' ];
    angular.module('lgApp').controller('LibGuidesController', LibGuidesController);


    new Shortcode( document.querySelector( '[data-ng-controller]' ), {
      card : function( done ) {
        var contents = this.contents;
        var markup = '<div class="card">';
            markup += contents;
            markup += '</div>';
        return markup;
      }
    });

})();

(function(){

    'use strict';

    var EventController = function( $attrs, dataService ) {

      var vm    = this;
      vm.audience = $attrs.audience;
      /**
       * Uses dataService.prototype.getFeatures - be sure to remove if this controller is removed too
       */
      function  getEvents( audience ){

          dataService.getEvents( audience ).then(function(data){
              if( data ) {
                vm.events = data;
              }
          });
      }

      getEvents( vm.audience );

    }

    EventController.$inject = [ '$attrs', 'dataService' ];
    angular.module('lgApp').controller('EventController', EventController);

})();
